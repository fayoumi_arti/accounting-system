package io.arti.accounting.it.db;

import io.arti.accounting.dao.accountstree.FourthLevelDao;
import io.arti.accounting.dao.accountstree.ThirdLevelDao;
import io.arti.accounting.it.ITCore;
import io.arti.accounting.model.accountstree.FourthLevel;
import io.arti.accounting.model.accountstree.ThirdLevel;
import io.arti.accounting.model.general.enums.CRDR;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class FourthLevelTest extends ITCore {

    @Autowired
    ThirdLevelDao thirdLevelDao;

    @Autowired
    FourthLevelDao fourthLevelDao;

    @Test
    void testInsertFourthLevel() {
        ThirdLevel thirdLevel = thirdLevelDao.insertEntity(new ThirdLevel("الأول", "first", 2, 2, 2, Collections.singletonList(1)));
        FourthLevel fourthLevel = fourthLevelDao.insertEntity(new FourthLevel("الأول", "first", thirdLevel.getId(), 2 , 2,1,CRDR.CREDIT));

        assertNotNull(thirdLevel);
        assertNotNull(fourthLevel);

        assertEquals(fourthLevel.getThirdLevelId(), thirdLevel.getId());
        assertEquals(fourthLevel.getArName(), "الأول");
        assertEquals(fourthLevel.getEnName(), "first");
        assertEquals(fourthLevel.getCreatedBy(), 2);
        assertEquals(fourthLevel.getUpdatedBy(), 2);

        int count = fourthLevelDao.getCount();
        assertTrue(count> 0);

        fourthLevelDao.deleteEntity(fourthLevel.getId());
        thirdLevelDao.deleteEntity(thirdLevel.getId());
    }
    @Test
    void testUpdateFourthLevel() {
        ThirdLevel thirdLevel = thirdLevelDao.insertEntity(new ThirdLevel("الأول", "first", 2, 2, 2,Collections.singletonList(1)));
        FourthLevel fourthLevel = fourthLevelDao.insertEntity(new FourthLevel("الأول", "first", thirdLevel.getId(), 2 , 2,1,CRDR.CREDIT));
        ThirdLevel updatedThirdLevel = thirdLevelDao.insertEntity(new ThirdLevel("الأول 1", "first 1", 3,3,3,Collections.singletonList(1)));

        fourthLevel.setEnName("first 2");
        fourthLevel.setArName("الأول 2");
        fourthLevel.setUpdatedBy(4);
        fourthLevel.setCreatedBy(4);
        fourthLevel.setThirdLevelId(updatedThirdLevel.getId());

        fourthLevelDao.updateEntity(fourthLevel);

        FourthLevel updatedFourthLevel = fourthLevelDao.getById(fourthLevel.getId());
        assertEquals(updatedFourthLevel.getEnName(), "first 2");
        assertEquals(updatedFourthLevel.getArName(), "الأول 2");
        assertEquals(updatedFourthLevel.getUpdatedBy(), 4);
        assertEquals(updatedFourthLevel.getThirdLevelId(), updatedThirdLevel.getId());
        assertNotNull(updatedThirdLevel.getUpdatedAt());

        fourthLevelDao.deleteEntity(fourthLevel.getId());
        thirdLevelDao.deleteEntity(updatedThirdLevel.getId());
    }

    @Test
    void deleteFourthLevel() {
        ThirdLevel thirdLevel = thirdLevelDao.insertEntity(new ThirdLevel("الأول", "first", 2, 2, 2,Collections.singletonList(1)));
        FourthLevel fourthLevel = fourthLevelDao.insertEntity(new FourthLevel("الأول", "first", thirdLevel.getId(), 2 , 2,1, CRDR.CREDIT));

        assertTrue(fourthLevelDao.isPresent(fourthLevel.getId()));
        fourthLevelDao.deleteEntity(fourthLevel.getId());
        assertFalse(fourthLevelDao.isPresent(fourthLevel.getId()));

        thirdLevelDao.deleteEntity(thirdLevel.getId());
    }

    @Test
    void getFourthLevelEntities() {
        ThirdLevel thirdLevel = thirdLevelDao.insertEntity(new ThirdLevel("الأول", "first", 2, 2, 2,Collections.singletonList(1)));

        fourthLevelDao.insertEntity(new FourthLevel("الأول", "first", thirdLevel.getId(), 2 , 2,1,CRDR.CREDIT));
        fourthLevelDao.insertEntity(new FourthLevel("الأول", "first", thirdLevel.getId(), 2 , 2,1,CRDR.CREDIT));
        fourthLevelDao.insertEntity(new FourthLevel("الأول", "first", thirdLevel.getId(), 2 , 2,1,CRDR.CREDIT));
        fourthLevelDao.insertEntity(new FourthLevel("الأول", "first", thirdLevel.getId(), 2 , 2,1,CRDR.CREDIT));
        fourthLevelDao.insertEntity(new FourthLevel("الأول", "first", thirdLevel.getId(), 2 , 2,1,CRDR.CREDIT));

        List<FourthLevel> fourthLevels = fourthLevelDao.getEntities(5, 0);
        assertTrue(fourthLevels.size() == 5);
        assertTrue(fourthLevelDao.getCount() == 5);

    }
}
