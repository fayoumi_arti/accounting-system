package io.arti.accounting.it.db;

import io.arti.accounting.dao.general.CurrenciesDao;
import io.arti.accounting.it.ITCore;

import io.arti.accounting.model.general.Currencies;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class CurrenciesTest extends ITCore {

    @Autowired
    CurrenciesDao currenciesDao;

    @Test
    void testInsertCurrencies() {

        Currencies currencies = currenciesDao.insertEntity(new Currencies(1, "دينار", "Dinar", "JO", 1f, 1, 1));

        assertNotNull(currencies);
        assertEquals(currencies.getArName(), "دينار");
        assertEquals(currencies.getEnName(), "Dinar");
        assertEquals(currencies.getIso(), "JO");
        assertEquals(currencies.getEvaluationPrice(), 1f);
        assertEquals(currencies.getCreatedBy(), 1);

        int count = currenciesDao.getCount();
        assertTrue(count > 0);
        currenciesDao.deleteEntity(currencies.getId());
    }

    @Test
    void testUpdateCurrencies() {
        Currencies currencies = currenciesDao.insertEntity(new Currencies(4, "ريال", "Rial", "SR", 1f, 1, 1));

        assertNotNull(currencies);
        assertEquals(currencies.getArName(), "ريال");
        assertEquals(currencies.getEnName(), "Rial");
        assertEquals(currencies.getIso(), "SR");
        assertEquals(currencies.getEvaluationPrice(), 1f);
        assertEquals(currencies.getCreatedBy(), 1);

        currencies.setEnName("Frank");
        currencies.setArName("فرنك");
        currencies.setIso("FR");
        currencies.setEvaluationPrice(0.7f);
        currencies.setReferenceId(5);
        currencies.setUpdatedBy(3);

        currenciesDao.updateEntity(currencies);

        Currencies currenciesUpdated = currenciesDao.getById(currencies.getId());
        assertEquals(currenciesUpdated.getUpdatedBy(), 3);
        assertEquals(currenciesUpdated.getEvaluationPrice(), 0.7f);
        assertEquals(currenciesUpdated.getReferenceId(), 5);
        assertEquals(currenciesUpdated.getEnName(), "Frank");
        assertEquals(currenciesUpdated.getArName(), "فرنك");
        assertEquals(currenciesUpdated.getIso(), "FR");
        assertNotNull(currenciesUpdated.getUpdatedAt());

        currenciesDao.deleteEntity(currencies.getId());
    }

    @Test
    void deleteCurrencies() {
        Currencies currencies = currenciesDao.insertEntity(new Currencies(6, "دينار", "Dinar", "SF", 1f, 1, 1));

        assertTrue(currenciesDao.isPresent(currencies.getId()));
        currenciesDao.deleteEntity(currencies.getId());
        assertFalse(currenciesDao.isPresent(currencies.getId()));
    }

    @Test
    void getBranchesCurrencies() {

        currenciesDao.insertEntity(new Currencies(11, "جنيه", "Pound", "EGP", 1f, 1, 1));
        currenciesDao.insertEntity(new Currencies(3, "دولار", "Dollar", "USD", 1f, 1, 1));
        currenciesDao.insertEntity(new Currencies(2, "يورو", "Euro", "EUR", 1f, 1, 1));


        List<Currencies> currenciesList = currenciesDao.getEntities(3, 0);
        assertTrue(currenciesList.size() == 3);
        assertTrue(currenciesDao.getCount() == 3);

    }

}
