package io.arti.accounting.it.db;

import io.arti.accounting.dao.general.BranchesDao;
import io.arti.accounting.it.ITCore;
import io.arti.accounting.model.general.Branches;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class BranchesTest extends ITCore {

    @Autowired
    BranchesDao branchesDao;

    @Test
    void testInsertBranches() {

        Branches branches = branchesDao.insertEntity(new Branches("عمان", "Amman", 1, 1));
        assertNotNull(branches);
        assertEquals(branches.getArName(), "عمان");
        assertEquals(branches.getEnName(), "Amman");
        assertEquals(branches.getCreatedBy(), 1);

        int count = branchesDao.getCount();
        assertTrue(count > 0);
        branchesDao.deleteEntity(branches.getId());
    }

    @Test
    void testUpdateBranches() {
        Branches branches = branchesDao.insertEntity(new Branches("عمان", "Amman", 1, 1));

        assertEquals(branches.getArName(), "عمان");
        assertEquals(branches.getEnName(), "Amman");

        branches.setEnName("Zarqa");
        branches.setArName("الزرقاء");
        branches.setUpdatedBy(3);
        branches.setCreatedBy(3);

        branchesDao.updateEntity(branches);

        Branches branchesUpdated = branchesDao.getById(branches.getId());
        assertEquals(branchesUpdated.getCreatedBy(), 1);
        assertEquals(branchesUpdated.getUpdatedBy(), 3);
        assertEquals(branchesUpdated.getEnName(), "Zarqa");
        assertEquals(branchesUpdated.getArName(), "الزرقاء");
        assertNotNull(branchesUpdated.getUpdatedAt());

        branchesDao.deleteEntity(branches.getId());
    }

    @Test
    void deleteBranches() {
        Branches branches = branchesDao.insertEntity(new Branches("عمان", "Amman", 1, 1));

        assertTrue(branchesDao.isPresent(branches.getId()));
        branchesDao.deleteEntity(branches.getId());
        assertFalse(branchesDao.isPresent(branches.getId()));
    }

    @Test
    void getBranchesEntities() {
        branchesDao.insertEntity(new Branches("عمان", "Amman", 1, 1));
        branchesDao.insertEntity(new Branches("عمان", "Amman", 1, 1));
        branchesDao.insertEntity(new Branches("عمان", "Amman", 1, 1));
        branchesDao.insertEntity(new Branches("عمان", "Amman", 1, 1));
        branchesDao.insertEntity(new Branches("عمان", "Amman", 1, 1));

        List<Branches> branchesList = branchesDao.getEntities(5, 0);
        assertTrue(branchesList.size() == 5);

    }
}
