package io.arti.accounting.it.db;

import io.arti.accounting.it.ITCore;

//TODO: re-write unit tests for journal voucher
public class JournalVoucherTest extends ITCore {
//
//    @Autowired
//    JournalVoucherDao journalVoucherDao;
//
//    @Autowired
//    ThirdLevelDao thirdLevelDao;
//
//    @Autowired
//    FourthLevelDao fourthLevelDao;
//
//    @Autowired
//    CurrenciesDao currenciesDao;
//
//    @Test
//    void testInsertJournalVoucher() {
//
//        ThirdLevel thirdLevel = thirdLevelDao.insertEntity(new ThirdLevel("الأول", "first", 2, 2, 2,1));
//        FourthLevel account = fourthLevelDao.insertEntity(new FourthLevel("الأول", "first", thirdLevel.getId(), 2 , 2,1,1));
//        FourthLevel intraAccount = fourthLevelDao.insertEntity(new FourthLevel("الثاني", "second", thirdLevel.getId(), 2 , 2,1,1));
//
//        Currencies currency = currenciesDao.insertEntity(new Currencies(1, "دينار", "Dinar", "JO", 1f, 1, 1));
//
//
//        JournalVoucher journalVoucher = journalVoucherDao.insertEntity(new JournalVoucher(
//                account.getId(), currency.getId(), 12.5f, 15.2f, CRDR.CREDIT, intraAccount.getId(), "note", 1, 1)
//        );
//
//        assertNotNull(journalVoucher);
//        assertEquals(journalVoucher.getAccountId(), account.getId());
//        assertEquals(journalVoucher.getCurrencyId(), currency.getId());
//        assertEquals(journalVoucher.getAmount(), 12.5f);
//        assertEquals(journalVoucher.getEquivalence(), 15.2f);
//        assertEquals(journalVoucher.getCrdr(), CRDR.CREDIT);
//        assertEquals(journalVoucher.getIntraAccountId(), intraAccount.getId());
//        assertEquals(journalVoucher.getNote(), "note");
//        assertEquals(journalVoucher.getCreatedBy(), 1);
//        assertEquals(journalVoucher.getUpdatedBy(), 1);
//
//        int count = journalVoucherDao.getCount();
//        assertTrue(count > 0);
//        journalVoucherDao.deleteEntity(journalVoucher.getId());
//
//        thirdLevelDao.deleteEntity(thirdLevel.getId());
//        currenciesDao.deleteEntity(currency.getId());
//    }
//
//    @Test
//    void testUpdateJournalVoucher() {
//        ThirdLevel thirdLevel = thirdLevelDao.insertEntity(new ThirdLevel("الأول", "first", 2, 2, 2,1));
//        FourthLevel account = fourthLevelDao.insertEntity(new FourthLevel("الأول", "first", thirdLevel.getId(), 2 , 2,1,1));
//        FourthLevel intraAccount = fourthLevelDao.insertEntity(new FourthLevel("الثاني", "second", thirdLevel.getId(), 2 , 2,1,1));
//
//        Currencies currency = currenciesDao.insertEntity(new Currencies(1, "دينار", "Dinar", "JO", 1f, 1, 1));
//
//
//        JournalVoucher journalVoucher = journalVoucherDao.insertEntity(new JournalVoucher(
//                account.getId(), currency.getId(), 12.5f, 15.2f, CRDR.CREDIT, intraAccount.getId(), "note", 1, 1)
//        );
//
//        assertNotNull(journalVoucher);
//        assertEquals(journalVoucher.getAccountId(), account.getId());
//        assertEquals(journalVoucher.getCurrencyId(), currency.getId());
//        assertEquals(journalVoucher.getAmount(), 12.5f);
//        assertEquals(journalVoucher.getEquivalence(), 15.2f);
//        assertEquals(journalVoucher.getCrdr(), CRDR.CREDIT);
//        assertEquals(journalVoucher.getIntraAccountId(), intraAccount.getId());
//        assertEquals(journalVoucher.getNote(), "note");
//        assertEquals(journalVoucher.getCreatedBy(), 1);
//        assertEquals(journalVoucher.getUpdatedBy(), 1);
//
//        journalVoucher.setCrdr(CRDR.DEBIT);
//        journalVoucher.setAccountId(intraAccount.getId());
//        journalVoucher.setIntraAccountId(account.getId());
//        journalVoucher.setCurrencyId(currency.getId());
//        journalVoucher.setAmount(11.4f);
//        journalVoucher.setEquivalence(5.3f);
//        journalVoucher.setNote("NOTE 2");
//        journalVoucher.setUpdatedBy(3);
//        journalVoucher.setCreatedBy(3);
//
//        journalVoucherDao.updateEntity(journalVoucher);
//
//        JournalVoucher journalVoucherUpdated = journalVoucherDao.getById(journalVoucher.getId());
//        assertEquals(journalVoucherUpdated.getCreatedBy(), 3);
//        assertEquals(journalVoucherUpdated.getUpdatedBy(), 3);
//
//        assertEquals(journalVoucherUpdated.getCrdr(), CRDR.DEBIT);
//        assertEquals(journalVoucherUpdated.getNote(), "NOTE 2");
//        assertEquals(journalVoucherUpdated.getAmount(), 11.4f);
//        assertEquals(journalVoucherUpdated.getEquivalence(), 5.3f);
//        assertEquals(journalVoucherUpdated.getCurrencyId(), currency.getId());
//        assertEquals(journalVoucherUpdated.getAccountId(), intraAccount.getId());
//        assertEquals(journalVoucherUpdated.getIntraAccountId(), account.getId());
//
//        assertNotNull(journalVoucherUpdated.getUpdatedAt());
//
//        journalVoucherDao.deleteEntity(journalVoucher.getId());
//
//        thirdLevelDao.deleteEntity(thirdLevel.getId());
//        currenciesDao.deleteEntity(currency.getId());
//    }
//
//    @Test
//    void deleteJournalVoucher() {
//        ThirdLevel thirdLevel = thirdLevelDao.insertEntity(new ThirdLevel("الأول", "first", 2, 2, 2,1));
//        FourthLevel account = fourthLevelDao.insertEntity(new FourthLevel("الأول", "first", thirdLevel.getId(), 2 , 2,1,1));
//        FourthLevel intraAccount = fourthLevelDao.insertEntity(new FourthLevel("الثاني", "second", thirdLevel.getId(), 2 , 2,1,1));
//
//        Currencies currency = currenciesDao.insertEntity(new Currencies(1, "دينار", "Dinar", "JO", 1f, 1, 1));
//
//
//        JournalVoucher journalVoucher = journalVoucherDao.insertEntity(new JournalVoucher(
//                account.getId(), currency.getId(), 12.5f, 15.2f, CRDR.CREDIT, intraAccount.getId(), "note", 1, 1)
//        );
//
//        assertTrue(journalVoucherDao.isPresent(journalVoucher.getId()));
//        journalVoucherDao.deleteEntity(journalVoucher.getId());
//        assertFalse(journalVoucherDao.isPresent(journalVoucher.getId()));
//
//        thirdLevelDao.deleteEntity(thirdLevel.getId());
//        currenciesDao.deleteEntity(currency.getId());
//    }
//
//    @Test
//    void getJournalVoucherEntities() {
//        ThirdLevel thirdLevel = thirdLevelDao.insertEntity(new ThirdLevel("الأول", "first", 2, 2, 2,1));
//        FourthLevel account = fourthLevelDao.insertEntity(new FourthLevel("الأول", "first", thirdLevel.getId(), 2 , 2,1,1));
//        FourthLevel intraAccount = fourthLevelDao.insertEntity(new FourthLevel("الثاني", "second", thirdLevel.getId(), 2 , 2,1,1));
//
//        Currencies currency = currenciesDao.insertEntity(new Currencies(1, "ليرة", "Lira", "LR", 1f, 1, 1));
//
//
//        journalVoucherDao.insertEntity(new JournalVoucher(
//                account.getId(), currency.getId(), 12.5f, 15.2f, CRDR.CREDIT, intraAccount.getId(), "note", 1, 1)
//        );
//        journalVoucherDao.insertEntity(new JournalVoucher(
//                account.getId(), currency.getId(), 12.5f, 15.2f, CRDR.CREDIT, intraAccount.getId(), "note", 1, 1)
//        );
//        journalVoucherDao.insertEntity(new JournalVoucher(
//                account.getId(), currency.getId(), 12.5f, 15.2f, CRDR.CREDIT, intraAccount.getId(), "note", 1, 1)
//        );
//        journalVoucherDao.insertEntity(new JournalVoucher(
//                account.getId(), currency.getId(), 12.5f, 15.2f, CRDR.CREDIT, intraAccount.getId(), "note", 1, 1)
//        );
//        journalVoucherDao.insertEntity(new JournalVoucher(
//                account.getId(), currency.getId(), 12.5f, 15.2f, CRDR.CREDIT, intraAccount.getId(), "note", 1, 1)
//        );
//        journalVoucherDao.insertEntity(new JournalVoucher(
//                account.getId(), currency.getId(), 12.5f, 15.2f, CRDR.CREDIT, intraAccount.getId(), "note", 1, 1)
//        );
//        journalVoucherDao.insertEntity(new JournalVoucher(
//                account.getId(), currency.getId(), 12.5f, 15.2f, CRDR.CREDIT, intraAccount.getId(), "note", 1, 1)
//        );
//        journalVoucherDao.insertEntity(new JournalVoucher(
//                account.getId(), currency.getId(), 12.5f, 15.2f, CRDR.CREDIT, intraAccount.getId(), "note", 1, 1)
//        );
//        journalVoucherDao.insertEntity(new JournalVoucher(
//                account.getId(), currency.getId(), 12.5f, 15.2f, CRDR.CREDIT, intraAccount.getId(), "note", 1, 1)
//        );
//        journalVoucherDao.insertEntity(new JournalVoucher(
//                account.getId(), currency.getId(), 12.5f, 15.2f, CRDR.CREDIT, intraAccount.getId(), "note", 1, 1)
//        );
//
//        List<JournalVoucher> journalVoucherList = journalVoucherDao.getEntities(10, 0);
//        assertTrue(journalVoucherList.size() == 10);
//        assertTrue(journalVoucherDao.getCount() == 10);
//
//    }
}
