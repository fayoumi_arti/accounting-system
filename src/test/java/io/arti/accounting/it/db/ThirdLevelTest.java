package io.arti.accounting.it.db;

import io.arti.accounting.dao.accountstree.ThirdLevelDao;
import io.arti.accounting.it.ITCore;
import io.arti.accounting.model.accountstree.ThirdLevel;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ThirdLevelTest extends ITCore {

    @Autowired
    ThirdLevelDao thirdLevelDao;

    @Test
    void testInsertThirdLevel() {
        ThirdLevel thirdLevel = thirdLevelDao.insertEntity(new ThirdLevel("الأول", "first", 2, 2, 2, Collections.singletonList(1)));

        assertNotNull(thirdLevel);
        assertEquals(thirdLevel.getSecondLevelId(), 2);
        assertEquals(thirdLevel.getCreatedBy(), 2);
        assertEquals(thirdLevel.getArName(), "الأول");
        assertEquals(thirdLevel.getEnName(), "first");

        int count = thirdLevelDao.getCount();
        assertTrue(count> 0);
        thirdLevelDao.deleteEntity(thirdLevel.getId());
    }

    @Test
    void testUpdateThirdLevel() {
        ThirdLevel thirdLevel = thirdLevelDao.insertEntity(new ThirdLevel("الأول", "first", 2, 2, 2,Collections.singletonList(1)));

        assertEquals(thirdLevel.getSecondLevelId(), 2);
        assertEquals(thirdLevel.getCreatedBy(), 2);

        thirdLevel.setEnName("first 1");
        thirdLevel.setArName("الأول 1");
        thirdLevel.setUpdatedBy(3);
        thirdLevel.setSecondLevelId(3);

        thirdLevelDao.updateEntity(thirdLevel);

        ThirdLevel updatedThirdLevel = thirdLevelDao.getById(thirdLevel.getId());
        assertEquals(updatedThirdLevel.getSecondLevelId(), 3);
        assertEquals(updatedThirdLevel.getUpdatedBy(), 3);
        assertEquals(updatedThirdLevel.getEnName(), "first 1");
        assertEquals(updatedThirdLevel.getArName(), "الأول 1");
        assertNotNull(updatedThirdLevel.getUpdatedAt());

        thirdLevelDao.deleteEntity(thirdLevel.getId());
    }

    @Test
    void deleteThirdLevel() {
        ThirdLevel thirdLevel = thirdLevelDao.insertEntity(new ThirdLevel("الأول", "first", 2, 2, 2,Collections.singletonList(1)));

        assertTrue(thirdLevelDao.isPresent(thirdLevel.getId()));
        thirdLevelDao.deleteEntity(thirdLevel.getId());
        assertFalse(thirdLevelDao.isPresent(thirdLevel.getId()));
    }

    @Test
    void getThirdLevelEntities() {
        thirdLevelDao.insertEntity(new ThirdLevel("الأول", "first", 2, 2, 2,Collections.singletonList(1)));
        thirdLevelDao.insertEntity(new ThirdLevel("الأول", "first", 2, 2, 2,Collections.singletonList(1)));
        thirdLevelDao.insertEntity(new ThirdLevel("الأول", "first", 2, 2, 2,Collections.singletonList(1)));
        thirdLevelDao.insertEntity(new ThirdLevel("الأول", "first", 2, 2, 2,Collections.singletonList(1)));
        thirdLevelDao.insertEntity(new ThirdLevel("الأول", "first", 2, 2, 2,Collections.singletonList(1)));

        List<ThirdLevel> thirdLevels = thirdLevelDao.getEntities(5, 0);
        assertTrue(thirdLevels.size() == 5);
    }
}
