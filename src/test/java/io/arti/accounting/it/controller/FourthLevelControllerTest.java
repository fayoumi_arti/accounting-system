package io.arti.accounting.it.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.arti.accounting.dao.accountstree.FourthLevelDao;
import io.arti.accounting.dao.accountstree.ThirdLevelDao;
import io.arti.accounting.it.ITCore;
import io.arti.accounting.model.accountstree.FourthLevel;
import io.arti.accounting.model.accountstree.ThirdLevel;
import io.arti.accounting.model.general.enums.CRDR;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

public class FourthLevelControllerTest extends ITCore {

    @Autowired
    ThirdLevelDao thirdLevelDao;

    @Autowired
    FourthLevelDao fourthLevelDao;

    @Test
    void fourthLevelCrudTest() throws JsonProcessingException {

        Integer thirdLevelId;
        ThirdLevel thirdLevelRequest = thirdLevelDao.insertEntity(new ThirdLevel("الأول", "first", 2, 2, 2, Collections.singletonList(1)));
        ThirdLevel thirdLevelPostResult = postRequest(baseUrl+"/v1/third-level", thirdLevelRequest, ThirdLevel.class);
        thirdLevelId = thirdLevelPostResult.getId();

        Integer fourthLevelId;
        FourthLevel fourthLevelRequest = fourthLevelDao.insertEntity(new FourthLevel("الأول", "first",thirdLevelId, 2 , 2,1, CRDR.CREDIT));
        FourthLevel fourthLevelPostResult = postRequest(baseUrl+"/v1/fourth-level", fourthLevelRequest, FourthLevel.class);
        fourthLevelId = fourthLevelPostResult.getId();

        assertEquals(fourthLevelRequest.getEnName(), fourthLevelPostResult.getEnName());
        assertEquals(fourthLevelRequest.getArName(), fourthLevelPostResult.getArName());
        assertEquals(fourthLevelRequest.getCreatedBy(), fourthLevelPostResult.getCreatedBy());
        assertEquals(fourthLevelRequest.getUpdatedBy(), fourthLevelPostResult.getUpdatedBy());
        assertEquals(fourthLevelRequest.getThirdLevelId(), thirdLevelPostResult.getId());

        FourthLevel expectedGetResult = fourthLevelDao.getById(fourthLevelId);
        FourthLevel apiResult = getRequest(baseUrl+"/v1/fourth-level/"+fourthLevelId, FourthLevel.class);

        assertEquals(expectedGetResult.getEnName(), apiResult.getEnName());
        assertEquals(expectedGetResult.getArName(), apiResult.getArName());
        assertEquals(expectedGetResult.getCreatedBy(), apiResult.getCreatedBy());
        assertEquals(expectedGetResult.getUpdatedBy(), apiResult.getUpdatedBy());
        assertEquals(expectedGetResult.getThirdLevelId(), apiResult.getThirdLevelId());

        assertTrue(fourthLevelDao.isPresent(fourthLevelId));
        deleteRequest(baseUrl+"/v1/fourth-level/"+fourthLevelId);
        assertFalse(fourthLevelDao.isPresent(fourthLevelId));

        assertTrue(thirdLevelDao.isPresent(thirdLevelId));
        deleteRequest(baseUrl+"/v1/third-level/"+thirdLevelId);
        assertFalse(thirdLevelDao.isPresent(thirdLevelId));
    }
}
