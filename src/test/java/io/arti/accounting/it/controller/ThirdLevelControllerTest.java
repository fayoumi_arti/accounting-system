package io.arti.accounting.it.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.arti.accounting.dao.accountstree.ThirdLevelDao;
import io.arti.accounting.it.ITCore;
import io.arti.accounting.model.accountstree.ThirdLevel;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

public class ThirdLevelControllerTest extends ITCore {

    @Autowired
    ThirdLevelDao thirdLevelDao;

    @Test
    void thirdLevelCrudTest() throws JsonProcessingException {

        Integer thirdLevelId;
        ThirdLevel thirdLevelRequest =  thirdLevelDao.insertEntity(new ThirdLevel("الأول", "first", 2, 2, 2, Collections.singletonList(1)));
        ThirdLevel thirdLevelPostResult = postRequest(baseUrl+"/v1/third-level", thirdLevelRequest, ThirdLevel.class);
        thirdLevelId = thirdLevelPostResult.getId();

        assertEquals(thirdLevelRequest.getEnName(), thirdLevelPostResult.getEnName());
        assertEquals(thirdLevelRequest.getArName(), thirdLevelPostResult.getArName());
        assertEquals(thirdLevelRequest.getSecondLevelId(), thirdLevelPostResult.getSecondLevelId());
        assertEquals(thirdLevelRequest.getTypes().get(0), thirdLevelPostResult.getTypes().get(0));
        assertEquals(thirdLevelRequest.getUpdatedBy(), thirdLevelPostResult.getUpdatedBy());
        assertEquals(thirdLevelRequest.getCreatedBy(), thirdLevelPostResult.getCreatedBy());

        ThirdLevel expectedGetResult = thirdLevelDao.getById(thirdLevelId);
        ThirdLevel apiResult = getRequest(baseUrl+"/v1/third-level/"+thirdLevelId, ThirdLevel.class);

        assertEquals(expectedGetResult.getEnName(), apiResult.getEnName());
        assertEquals(expectedGetResult.getArName(), apiResult.getArName());
        assertEquals(expectedGetResult.getSecondLevelId(), apiResult.getSecondLevelId());
        assertEquals(expectedGetResult.getCreatedBy(), apiResult.getCreatedBy());
        assertEquals(expectedGetResult.getUpdatedBy(), apiResult.getUpdatedBy());


        assertTrue(thirdLevelDao.isPresent(thirdLevelId));
        deleteRequest(baseUrl+"/v1/third-level/"+thirdLevelId);
        assertFalse(thirdLevelDao.isPresent(thirdLevelId));

    }
}
