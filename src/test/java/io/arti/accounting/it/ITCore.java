package io.arti.accounting.it;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.json.JsonReadFeature;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.commons.io.FileUtils;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.RestClientException;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicBoolean;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public abstract class ITCore {

    @LocalServerPort
    protected int port;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    protected TestRestTemplate testRestTemplate;

    protected static final ObjectMapper jsonMapper;
    static {
        jsonMapper = JsonMapper.builder()
                .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
                .enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT)
                .enable(JsonReadFeature.ALLOW_SINGLE_QUOTES)
                .enable(JsonReadFeature.ALLOW_LEADING_ZEROS_FOR_NUMBERS)
                .enable(JsonReadFeature.ALLOW_LEADING_DECIMAL_POINT_FOR_NUMBERS)
                .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
                .addModule(new JavaTimeModule())
                .build();
    }

    protected String baseUrl;

    private final static String DB = "artivisions_test";
    private final static String SCHEMA = "accounting";
    private final static String DROP_SCHEMA = "DROP SCHEMA IF EXISTS " + SCHEMA + " CASCADE";

    private final static AtomicBoolean started = new AtomicBoolean(false);

    @Value("${authentication.api}")
    private String authApi;
    @Value("${authentication.username}")
    private String userName;
    @Value("${authentication.password}")
    private String password;
    private String authToken;

    @Value("${spring.datasource.url}")
    private String dataSourceUrl;

    @Value("${spring.datasource.username}")
    private String userNameDB;

    @Value("${spring.datasource.password}")
    private String passwordDB;


    @BeforeAll
    protected void createNewEnv() throws SQLException, IOException {
        baseUrl = "http://localhost:" + port;

        synchronized (this) {
            if (started.get()) {
                return;
            }

            System.out.println("Starting ARTIVISIONS Integration test ...");
            createDB();
            flywayMigrate();
            started.set(true);
        }
        this.authToken = getAuthToken();
    }

    private void flywayMigrate() {
        Flyway.configure().schemas("accounting")
                .dataSource(dataSourceUrl, userNameDB, passwordDB)
                .table("artivisions_history_schema")
                .validateOnMigrate(true)
                .baselineOnMigrate(true)
                .locations("classpath:db/migration")
                .mixed(true)
                .load()
                .migrate();
    }
    private void createDB() throws SQLException, IOException {
        synchronized (this) {
            Assertions.assertEquals(DB, jdbcTemplate.getDataSource().getConnection().getCatalog());
            jdbcTemplate.execute(DROP_SCHEMA);

            String createSchemaSQL = FileUtils.readFileToString(new File("src/main/resources/db/migration/B1_0_0__InitSchema.sql"), StandardCharsets.UTF_8);
            jdbcTemplate.execute(createSchemaSQL);

            String insertionSchemaSQL = FileUtils.readFileToString(new File("src/main/resources/db/migration/B1_0_1__InitInserts.sql"), StandardCharsets.UTF_8);
            jdbcTemplate.execute(insertionSchemaSQL);

            jdbcTemplate.execute("SET search_path TO " + SCHEMA);

            String searchPath = jdbcTemplate.queryForObject("SHOW search_path", String.class);
            Assertions.assertEquals(SCHEMA, searchPath);

            System.out.println("Schema " + SCHEMA + " created successfully");
        }
    }

    protected <T> T getObjectFromData(JsonNode jsonNode, Class<T> valueType) throws JsonProcessingException {
        return jsonMapper.treeToValue(jsonNode.get("data"), valueType);
    }

    protected <T> T postRequest(String url, Object request, Class<T> valueType) throws JsonProcessingException, RestClientException {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + authToken);
        HttpEntity<?> requestEntity = new HttpEntity<>(request, headers);
        ResponseEntity<JsonNode> result = testRestTemplate.exchange(url, HttpMethod.POST, requestEntity, JsonNode.class);

        if (result.getStatusCode().isError()) {
            throw new RestClientException("Invalid request: " + result.getStatusCode() + " => " + result.getBody());
        }
        return getObjectFromData(result.getBody(), valueType);
    }

    protected <T> T putRequest(String url, Object request, Class<T> valueType) throws JsonProcessingException, RestClientException {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + authToken);
        HttpEntity<?> requestUpdate = new HttpEntity<>(request, headers);
        ResponseEntity<JsonNode> result = testRestTemplate.exchange(url, HttpMethod.PUT, requestUpdate, JsonNode.class);
        if (result.getStatusCode().isError()) {
            throw new RestClientException("Invalid request: " + result.getStatusCode() + " => " + result.getBody());
        }
        return getObjectFromData(result.getBody(), valueType);
    }

    protected <T> T getRequest(String url, Class<T> valueType) throws JsonProcessingException, RestClientException {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + authToken);
        ResponseEntity<JsonNode> result = testRestTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), JsonNode.class);
        if (result.getStatusCode().isError()) {
            throw new RestClientException("Invalid request: " + result.getStatusCode());
        }
        return getObjectFromData(result.getBody(), valueType);
    }

    public void deleteRequest(String url) throws RestClientException {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + authToken);
        testRestTemplate.exchange(url, HttpMethod.DELETE, new HttpEntity<>(headers), String.class);
    }

    private String getAuthToken() {
        ObjectNode request = new ObjectNode(JsonNodeFactory.instance);
        request.put("username", userName);
        request.put("password", password);
        ResponseEntity<JsonNode> response = testRestTemplate.postForEntity(authApi, request, JsonNode.class);
        if (!response.getStatusCode().is2xxSuccessful()) {
            return "invalid request";
        }
        return response.getBody().get("data").get("jwtToken").asText();

    }

}
