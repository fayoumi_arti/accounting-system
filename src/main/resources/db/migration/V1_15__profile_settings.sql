create table profile (
    id SERIAL,
    name_en VARCHAR(255),
    name_ar VARCHAR(255),
    country VARCHAR(255),
    governorate VARCHAR(255),
    address_ar VARCHAR(255),
    address_en VARCHAR(255),
    logo TEXT,
    phone VARCHAR(255),
    fax VARCHAR(255),
    postal_code VARCHAR(255),
    postal_box VARCHAR(255),
    email TEXT,
    created_by INT,
    updated_by INT,
    updated_at timestamp without time zone DEFAULT NOW() NOT NULL,
    created_at timestamp without time zone DEFAULT NOW() NOT NULL
);

CREATE TRIGGER profile_updated_at BEFORE UPDATE ON accounting.profile FOR EACH ROW EXECUTE PROCEDURE accounting.current_timestamp_on_update();
