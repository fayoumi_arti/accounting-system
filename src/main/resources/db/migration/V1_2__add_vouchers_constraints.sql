ALTER TABLE ONLY accounting.expenses_voucher
    ADD CONSTRAINT expenses_voucher_account_id_fkey FOREIGN KEY (account_id) REFERENCES accounting.fourth_level (id) ON DELETE CASCADE;

ALTER TABLE ONLY accounting.expenses_voucher
    ADD CONSTRAINT expenses_voucher_currency_id_fkey FOREIGN KEY (currency_reference_id) REFERENCES accounting.currency (reference_id) ON DELETE CASCADE;

ALTER TABLE ONLY accounting.receipt_vouchers
    ADD CONSTRAINT receipt_vouchers_credit_account_id_fkey FOREIGN KEY (credit_account_id) REFERENCES accounting.fourth_level (id) ON DELETE CASCADE;

ALTER TABLE ONLY accounting.receipt_vouchers
    ADD CONSTRAINT receipt_vouchers_debit_account_id_fkey FOREIGN KEY (debit_account_id) REFERENCES accounting.fourth_level (id) ON DELETE CASCADE;

ALTER TABLE ONLY accounting.receipt_vouchers
    ADD CONSTRAINT receipt_vouchers_currency_id_fkey FOREIGN KEY (currency_reference_id) REFERENCES accounting.currency (reference_id) ON DELETE CASCADE;

ALTER TABLE ONLY accounting.payment_vouchers
    ADD CONSTRAINT payment_vouchers_credit_account_id_fkey FOREIGN KEY (credit_account_id) REFERENCES accounting.fourth_level (id) ON DELETE CASCADE;

ALTER TABLE ONLY accounting.payment_vouchers
    ADD CONSTRAINT payment_vouchers_debit_account_id_fkey FOREIGN KEY (debit_account_id) REFERENCES accounting.fourth_level (id) ON DELETE CASCADE;

ALTER TABLE ONLY accounting.payment_vouchers
    ADD CONSTRAINT payment_vouchers_currency_id_fkey FOREIGN KEY (currency_reference_id) REFERENCES accounting.currency (reference_id) ON DELETE CASCADE;

CREATE TRIGGER payment_vouchers_updated_at BEFORE UPDATE ON accounting.payment_vouchers FOR EACH ROW EXECUTE PROCEDURE accounting.current_timestamp_on_update();
CREATE TRIGGER receipt_vouchers_updated_at BEFORE UPDATE ON accounting.receipt_vouchers FOR EACH ROW EXECUTE PROCEDURE accounting.current_timestamp_on_update();
CREATE TRIGGER expenses_voucher_updated_at BEFORE UPDATE ON accounting.expenses_voucher FOR EACH ROW EXECUTE PROCEDURE accounting.current_timestamp_on_update();
