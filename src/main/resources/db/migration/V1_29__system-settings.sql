CREATE table accounting.settings (
    id SERIAL PRIMARY KEY,
    key VARCHAR(255) UNIQUE,
    value jsonb,
    created_by INT,
    updated_by INT,
    updated_at timestamp without time zone DEFAULT NOW() NOT NULL,
    created_at timestamp without time zone DEFAULT NOW() NOT NULL
);
CREATE TRIGGER settings_updated_at BEFORE UPDATE ON accounting.settings FOR EACH ROW EXECUTE PROCEDURE accounting.current_timestamp_on_update();
CREATE INDEX settings_key_idx ON accounting.settings USING hash (key);

