CREATE TABLE country (
    id SERIAL PRIMARY KEY,
    name_en VARCHAR(255),
    name_ar VARCHAR(255),
    iso_3 VARCHAR(3) UNIQUE,
    created_by BIGINT,
    updated_by BIGINT,
    updated_at timestamp without time zone DEFAULT NOW() NOT NULL,
    created_at timestamp without time zone DEFAULT NOW() NOT NULL
);

CREATE TABLE city (
    id SERIAL PRIMARY KEY,
    country_id INTEGER,
    name_en VARCHAR(255),
    name_ar VARCHAR(255),
    created_by BIGINT,
    updated_by BIGINT,
    updated_at timestamp without time zone DEFAULT NOW() NOT NULL,
    created_at timestamp without time zone DEFAULT NOW() NOT NULL
);

ALTER TABLE accounting.city
    ADD CONSTRAINT fk_country_city FOREIGN KEY (country_id) REFERENCES accounting.country (id) ON DELETE CASCADE ;

CREATE TRIGGER country_updated_at BEFORE UPDATE ON accounting.country FOR EACH ROW EXECUTE PROCEDURE accounting.current_timestamp_on_update();

CREATE TRIGGER city_updated_at BEFORE UPDATE ON accounting.city FOR EACH ROW EXECUTE PROCEDURE accounting.current_timestamp_on_update();