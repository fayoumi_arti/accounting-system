alter table journal_voucher
add column date_time timestamp without time zone DEFAULT NOW() NOT NULL,
add column branch_id BIGINT ,
add column voucher_status accounting.voucher_status DEFAULT 'ENTRY' NOT NULL,
add column account_previous_balance FLOAT8 NOT NULL;

alter table payment_vouchers
add column date_time timestamp without time zone DEFAULT NOW() NOT NULL,
add column branch_id BIGINT ,
add column voucher_status accounting.voucher_status DEFAULT 'ENTRY' NOT NULL,
add column debit_account_previous_balance FLOAT8 NOT NULL,
add column credit_account_previous_balance FLOAT8 NOT NULL;

alter table receipt_vouchers
add column date_time timestamp without time zone DEFAULT NOW() NOT NULL,
add column branch_id BIGINT ,
add column voucher_status accounting.voucher_status DEFAULT 'ENTRY' NOT NULL,
add column debit_account_previous_balance FLOAT8 NOT NULL,
add column credit_account_previous_balance FLOAT8 NOT NULL;

alter table expenses_voucher
add column date_time timestamp without time zone DEFAULT NOW() NOT NULL,
add column branch_id BIGINT ,
add column voucher_status accounting.voucher_status DEFAULT 'ENTRY' NOT NULL,
add column account_previous_balance FLOAT8 NOT NULL;