ALTER TABLE accounting.profile ADD COLUMN currency_id INTEGER;

ALTER TABLE accounting.profile
    ADD CONSTRAINT fk_currency_profile FOREIGN KEY (currency_id) REFERENCES accounting.currency (id) ON DELETE CASCADE;