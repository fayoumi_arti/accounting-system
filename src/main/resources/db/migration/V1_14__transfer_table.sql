CREATE TYPE accounting.transfer_status AS ENUM (
    'PAID',
    'UNPAID',
    'PENDING',
    'DELETED'
);

CREATE CAST (CHARACTER VARYING AS accounting.transfer_status) WITH INOUT AS ASSIGNMENT;

ALTER TABLE accounting.transfer_payment DROP COLUMN paid;
ALTER TABLE accounting.transfer_payment ADD COLUMN status accounting.transfer_status NOT NULL DEFAULT 'PENDING';

CREATE INDEX transfer_payment_currency_id_idx ON accounting.transfer_payment USING hash (currency_id);
CREATE INDEX transfer_payment_sender_acc_id_idx ON accounting.transfer_payment USING hash (sender_account_id);
CREATE INDEX transfer_payment_payer_acc_id_idx ON accounting.transfer_payment USING hash (payer_account_id);
CREATE INDEX transfer_payment_branch_id_idx ON accounting.transfer_payment USING hash (branch_id);
CREATE INDEX transfer_payment_status_idx ON accounting.transfer_payment USING hash (status);
CREATE INDEX transfer_payment_destination_idx ON accounting.transfer_payment USING hash (destination);
CREATE INDEX transfer_payment_date_time_idx ON accounting.transfer_payment USING btree (date_time);
CREATE INDEX transfer_payment_amount_idx ON accounting.transfer_payment USING btree (amount);
