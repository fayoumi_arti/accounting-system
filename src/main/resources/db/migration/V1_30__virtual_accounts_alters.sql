ALTER TABLE accounting.virtual_accounts_settings DROP COLUMN type;
ALTER TABLE accounting.virtual_accounts_settings ADD COLUMN types jsonb;

ALTER TABLE accounting.virtual_accounts_settings DROP COLUMN accounts;
ALTER TABLE accounting.virtual_accounts_settings ADD COLUMN account_id INT;