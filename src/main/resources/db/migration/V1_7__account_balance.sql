CREATE table accounting.account_balances (
    id SERIAL PRIMARY KEY,
    voucher_id BIGINT NOT NULL,
    account_id BIGINT NOT NULL,
    currency_id INT NOT NULL,
    amount FLOAT8 NOT NULL,
    equivalence FLOAT8 NOT NULL,
    date_time timestamp without time zone DEFAULT NOW() NOT NULL,
    previous_balance FLOAT8 NOT NULL,
    current_balance FLOAT8 NOT NULL,
    cr_dr accounting.cr_dr NOT NULL,
    created_by BIGINT,
    updated_by BIGINT,
    updated_at timestamp without time zone DEFAULT NOW() NOT NULL,
    created_at timestamp without time zone DEFAULT NOW() NOT NULL
);
