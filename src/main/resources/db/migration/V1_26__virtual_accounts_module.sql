create table accounting.virtual_accounts_settings (
    id SERIAL,
    type accounting.virtual_accounts,
    accounts jsonb,
    created_by BIGINT,
    updated_by BIGINT,
    updated_at timestamp without time zone DEFAULT NOW() NOT NULL,
    created_at timestamp without time zone DEFAULT NOW() NOT NULL
);

CREATE TRIGGER virtual_accounts_updated_at BEFORE UPDATE ON accounting.virtual_accounts_settings FOR EACH ROW EXECUTE PROCEDURE accounting.current_timestamp_on_update();
