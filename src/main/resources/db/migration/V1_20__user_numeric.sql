alter table accounting.account_balances
alter column amount TYPE numeric,
alter column equivalence TYPE numeric,
alter column previous_balance TYPE numeric,
alter column current_balance TYPE numeric;



alter table accounting.journal_voucher
alter column amount TYPE numeric,
alter column equivalence TYPE numeric;


alter table accounting.payment_vouchers
alter column amount TYPE numeric,
alter column equivalence TYPE numeric;


alter table accounting.receipt_vouchers
alter column amount TYPE numeric,
alter column equivalence TYPE numeric;

alter table accounting.expenses_voucher
alter column amount TYPE numeric,
alter column equivalence TYPE numeric;

alter table accounting.internal_vouchers
alter column debit_amount TYPE numeric,
alter column debit_equivalence TYPE numeric,
alter column credit_amount TYPE numeric,
alter column credit_equivalence TYPE numeric;

alter table accounting.currency
alter column evaluation_price TYPE float;