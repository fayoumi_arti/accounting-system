alter table accounting.fourth_level
add column allowed_currencies jsonb;

CREATE INDEX account_balances_account_id_idx ON accounting.account_balances USING hash (account_id);
CREATE INDEX account_date_time_idx ON accounting.account_balances USING btree (date_time);