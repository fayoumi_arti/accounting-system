ALTER TYPE accounting.voucher_type ADD VALUE 'REVERSED_JOURNAL';
ALTER TYPE accounting.voucher_type ADD VALUE 'REVERSED_PAYMENT';
ALTER TYPE accounting.voucher_type ADD VALUE 'REVERSED_RECEIPT';
ALTER TYPE accounting.voucher_type ADD VALUE 'REVERSED_EXPENSE';
ALTER TYPE accounting.voucher_type ADD VALUE 'REVERSED_INTERNAL';

ALTER TABLE accounting.voucher_base ADD COLUMN attachments jsonb;
ALTER TABLE accounting.voucher_base ADD COLUMN reverse_id BIGINT;
ALTER TABLE accounting.voucher_base ADD COLUMN references_no TEXT;
ALTER TABLE accounting.voucher_base ADD COLUMN reverse_reason TEXT;


ALTER TABLE ONLY accounting.voucher_base
    ADD CONSTRAINT reverse_id_fkey FOREIGN KEY (reverse_id) REFERENCES accounting.voucher_base (id) ON DELETE CASCADE;

CREATE INDEX voucher_base_reverse_id_idx ON accounting.voucher_base USING hash (reverse_id);
CREATE INDEX voucher_base_references_no_idx ON accounting.voucher_base USING hash (references_no);
