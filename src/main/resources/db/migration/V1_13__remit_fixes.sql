ALTER TABLE accounting.transfer_payment ADD COLUMN branch_id INT;
ALTER TABLE accounting.transfer_payment ADD COLUMN paid_commission_ratio FLOAT;