ALTER TABLE accounting.account_balances ADD COLUMN first_level_id BIGINT;
ALTER TABLE accounting.account_balances ADD COLUMN second_level_id BIGINT;
ALTER TABLE accounting.account_balances ADD COLUMN third_level_id BIGINT;
--=========================================================================--
CREATE INDEX first_level_balances_idx ON accounting.account_balances USING hash (first_level_id);
CREATE INDEX second_level_balances_idx ON accounting.account_balances USING hash (second_level_id);
CREATE INDEX third_level_balances_idx ON accounting.account_balances USING hash (third_level_id);
