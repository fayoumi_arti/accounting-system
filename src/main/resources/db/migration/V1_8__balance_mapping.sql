ALTER TABLE accounting.journal_voucher ADD COLUMN balance_id INT NOT NULL default 0;
ALTER TABLE accounting.payment_vouchers
 ADD COLUMN debit_balance_id INT NOT NULL default 0,
 ADD COLUMN credit_balance_id INT NOT NULL default 0;
ALTER TABLE accounting.receipt_vouchers
 ADD COLUMN debit_balance_id INT NOT NULL default 0,
 ADD COLUMN credit_balance_id INT NOT NULL default 0;
ALTER TABLE accounting.expenses_voucher ADD COLUMN balance_id INT NOT NULL default 0;