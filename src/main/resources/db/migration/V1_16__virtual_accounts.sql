CREATE TYPE accounting.virtual_accounts AS ENUM (
    'NORMAL',
    'REMIT_UNPAID',
    'COMMISSION_PAID',
    'COMMISSION_RECEIVED'
    );
CREATE CAST (CHARACTER VARYING AS accounting.virtual_accounts) WITH INOUT AS ASSIGNMENT;

ALTER TABLE accounting.fourth_level add COLUMN virtual_type accounting.virtual_accounts not null default 'NORMAL';