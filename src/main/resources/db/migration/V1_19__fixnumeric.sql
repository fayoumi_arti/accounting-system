alter table accounting.account_balances
alter column amount TYPE decimal,
alter column equivalence TYPE decimal,
alter column previous_balance TYPE decimal,
alter column current_balance TYPE decimal;