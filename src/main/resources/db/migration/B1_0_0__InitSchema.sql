CREATE SCHEMA IF NOT EXISTS accounting;
--=========================================================================--
CREATE FUNCTION accounting.current_timestamp_on_update() RETURNS trigger
    LANGUAGE plpgsql
AS $$
BEGIN
    IF row(NEW.*) IS DISTINCT FROM row(OLD.*) THEN
        NEW.updated_at = now();
        RETURN NEW;
    ELSE
        RETURN OLD;
    END IF;
END;
$$;

-----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION third_level_balance() RETURNS TRIGGER AS
$BODY$
BEGIN
    IF TG_OP='DELETE' THEN
        UPDATE accounting.third_level SET balance = third_level.balance - old.balance;
        RETURN old;
    ELSE
        UPDATE accounting.third_level SET balance = (SELECT COALESCE(SUM(balance),0) FROM accounting.fourth_level WHERE third_level_id = accounting.third_level.id);
    END IF;
    RETURN new;
END;
$BODY$
    LANGUAGE plpgsql;
-----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION second_level_balance() RETURNS TRIGGER AS
$BODY$
BEGIN
    IF TG_OP='DELETE' THEN
        UPDATE accounting.second_level SET balance = second_level.balance - old.balance;
        RETURN old;
    ELSE
        UPDATE accounting.second_level SET balance = (SELECT COALESCE(SUM(balance),0) FROM accounting.third_level WHERE second_level_id = accounting.second_level.id);
    END IF;
    RETURN new;
END;
$BODY$
    LANGUAGE plpgsql;
-----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION first_level_balance() RETURNS TRIGGER AS
$BODY$
BEGIN
    UPDATE accounting.first_level SET balance = (SELECT COALESCE(SUM(balance),0) FROM accounting.second_level WHERE first_level_id = accounting.first_level.id);
    RETURN new;
END;
$BODY$
    LANGUAGE plpgsql;
--=========================================================================--
CREATE TYPE accounting.cr_dr AS ENUM (
    'CREDIT',
    'DEBIT',
    'ALL'
    );
CREATE CAST (CHARACTER VARYING AS accounting.cr_dr) WITH INOUT AS ASSIGNMENT;

CREATE TYPE accounting.voucher_type AS ENUM (
    'JOURNAL',
    'PAYMENT',
    'RECEIPT',
    'EXPENSE',
    'INTERNAL'
    );
CREATE CAST (CHARACTER VARYING AS accounting.voucher_type) WITH INOUT AS ASSIGNMENT;

CREATE TYPE accounting.voucher_status AS ENUM (
    'ENTRY',
    'APPROVED',
    'TRANSFERRED',
    'DENIED'
    );
CREATE CAST (CHARACTER VARYING AS accounting.voucher_status) WITH INOUT AS ASSIGNMENT;
--=========================================================================--
CREATE TABLE IF NOT EXISTS accounting.first_level (
    id SERIAL PRIMARY KEY,
    name_en VARCHAR(255),
    name_ar VARCHAR(255) NOT NULL,
    balance FLOAT8 NOT NULL DEFAULT 0
);

CREATE TABLE IF NOT EXISTS accounting.second_level (
    id SERIAL PRIMARY KEY,
    first_level_id BIGINT,
    name_en VARCHAR(255),
    name_ar VARCHAR(255) NOT NULL,
    balance FLOAT8 NOT NULL DEFAULT 0
);

CREATE TABLE IF NOT EXISTS accounting.third_level (
    id SERIAL PRIMARY KEY,
    item_name_en VARCHAR(255),
    item_name_ar VARCHAR(255) NOT NULL,
    second_level_id int NOT NULL,
    types jsonb,
    balance FLOAT8 NOT NULL DEFAULT 0,
    created_by BIGINT,
    updated_by BIGINT,
    updated_at timestamp without time zone DEFAULT NOW() NOT NULL,
    created_at timestamp without time zone DEFAULT NOW() NOT NULL
);

CREATE TABLE IF NOT EXISTS accounting.fourth_level (
    id SERIAL PRIMARY KEY,
    item_name_en VARCHAR(255),
    item_name_ar VARCHAR(255) NOT NULL,
    third_level_id BIGINT NOT NULL,
    default_currency_id int NOT NULL default 1,
    governorate VARCHAR(255),
    city VARCHAR(255),
    communication_info VARCHAR(255),
    address VARCHAR(255),
    first_phone_number VARCHAR(255),
    second_phone_number VARCHAR(255),
    fax VARCHAR(255),
    first_email VARCHAR(255),
    second_email VARCHAR(255),
    person_in_charge VARCHAR(255),
    cr_dr accounting.cr_dr NOT NULL,
    balance FLOAT8 NOT NULL DEFAULT 0,
    created_by BIGINT,
    updated_by BIGINT,
    updated_at timestamp without time zone DEFAULT NOW() NOT NULL,
    created_at timestamp without time zone DEFAULT NOW() NOT NULL
);

CREATE TABLE accounting.branch (
    id SERIAL PRIMARY KEY,
    ar_name VARCHAR(255) NOT NULL,
    en_name VARCHAR(255),
    created_by BIGINT,
    updated_by BIGINT,
    updated_at timestamp without time zone DEFAULT NOW() NOT NULL,
    created_at timestamp without time zone DEFAULT NOW() NOT NULL
);

CREATE TABLE accounting.currency (
     id SERIAL PRIMARY KEY,
     reference_id INT NOT NULL UNIQUE,
     ar_name VARCHAR(255) NOT NULL,
     en_name VARCHAR(255),
     iso VARCHAR(3) UNIQUE,
     evaluation_price FLOAT8,
     created_by BIGINT,
     updated_by BIGINT,
     updated_at timestamp without time zone DEFAULT NOW() NOT NULL,
     created_at timestamp without time zone DEFAULT NOW() NOT NULL
);

CREATE table accounting.journal_voucher (
    id SERIAL PRIMARY KEY,
    voucher_id BIGINT NOT NULL,
    account_id BIGINT NOT NULL,
    currency_reference_id INT NOT NULL,
    amount FLOAT8 NOT NULL,
    equivalence FLOAT8 NOT NULL,
    cr_dr accounting.cr_dr NOT NULL,
    intra_account_id BIGINT,
    note TEXT,
    created_by BIGINT,
    updated_by BIGINT,
    updated_at timestamp without time zone DEFAULT NOW() NOT NULL,
    created_at timestamp without time zone DEFAULT NOW() NOT NULL

);

CREATE table accounting.payment_vouchers (
    id SERIAL PRIMARY KEY,
    voucher_id BIGINT NOT NULL,
    debit_account_id BIGINT NOT NULL,
    credit_account_id BIGINT NOT NULL,
    currency_reference_id INT NOT NULL,
    amount FLOAT8 NOT NULL,
    equivalence FLOAT8 NOT NULL,
    recipient_name TEXT,
    notes TEXT,
    created_by BIGINT,
    updated_by BIGINT,
    updated_at timestamp without time zone DEFAULT NOW() NOT NULL,
    created_at timestamp without time zone DEFAULT NOW() NOT NULL
);

CREATE table accounting.receipt_vouchers (
    id SERIAL PRIMARY KEY,
    voucher_id BIGINT NOT NULL,
    debit_account_id BIGINT NOT NULL,
    credit_account_id BIGINT NOT NULL,
    currency_reference_id INT NOT NULL,
    amount FLOAT8 NOT NULL,
    equivalence FLOAT8 NOT NULL,
    recipient_name TEXT,
    notes TEXT,
    created_by BIGINT,
    updated_by BIGINT,
    updated_at timestamp without time zone DEFAULT NOW() NOT NULL,
    created_at timestamp without time zone DEFAULT NOW() NOT NULL
);

CREATE table accounting.expenses_voucher (
    id SERIAL PRIMARY KEY,
    voucher_id BIGINT NOT NULL,
    account_id BIGINT NOT NULL,
    currency_reference_id INT NOT NULL,
    amount FLOAT8 NOT NULL,
    equivalence FLOAT8 NOT NULL,
    cr_dr accounting.cr_dr NOT NULL,
    notes TEXT,
    created_by BIGINT,
    updated_by BIGINT,
    updated_at timestamp without time zone DEFAULT NOW() NOT NULL,
    created_at timestamp without time zone DEFAULT NOW() NOT NULL
);

CREATE TABLE accounting.voucher_base (
    id SERIAL PRIMARY KEY,
    date_time timestamp without time zone DEFAULT NOW() NOT NULL,
    voucher_id SERIAL NOT NULL,
    voucher_type accounting.voucher_type NOT NULL,
    voucher_status accounting.voucher_status NOT NULL,
    branch_id BIGINT ,
    report TEXT,
    entry_id BIGINT,
    entry_date timestamp without time zone DEFAULT NOW(),
    approver_id BIGINT,
    approve_date timestamp without time zone DEFAULT NOW(),
    created_by BIGINT,
    updated_by BIGINT,
    updated_at timestamp without time zone DEFAULT NOW() NOT NULL,
    created_at timestamp without time zone DEFAULT NOW() NOT NULL
);
--=========================================================================--
ALTER TABLE ONLY accounting.fourth_level
    ADD CONSTRAINT third_level_id_fkey FOREIGN KEY (third_level_id) REFERENCES accounting.third_level (id) ON DELETE CASCADE;

ALTER TABLE ONLY accounting.journal_voucher
    ADD CONSTRAINT account_id_fkey FOREIGN KEY (account_id) REFERENCES accounting.fourth_level (id) ON DELETE CASCADE;

ALTER TABLE ONLY accounting.journal_voucher
    ADD CONSTRAINT intar_account_id_fkey FOREIGN KEY (intra_account_id) REFERENCES accounting.fourth_level (id) ON DELETE CASCADE;

ALTER TABLE ONLY accounting.journal_voucher
    ADD CONSTRAINT currency_id_fkey FOREIGN KEY (currency_reference_id) REFERENCES accounting.currency (reference_id) ON DELETE CASCADE;

ALTER TABLE ONLY accounting.voucher_base
    ADD CONSTRAINT branch_id_fkey FOREIGN KEY (branch_id) REFERENCES accounting.branch (id) ON DELETE CASCADE;

ALTER TABLE ONLY accounting.voucher_base
    ADD CONSTRAINT voucher_id_type_unique UNIQUE (voucher_id, voucher_type);

ALTER TABLE ONLY accounting.second_level
    ADD CONSTRAINT first_level_id_fkey FOREIGN KEY (first_level_id) REFERENCES accounting.first_level (id) ON DELETE CASCADE;

ALTER TABLE ONLY accounting.third_level
    ADD CONSTRAINT second_level_id_fkey FOREIGN KEY (second_level_id) REFERENCES accounting.second_level (id) ON DELETE CASCADE;
--=========================================================================--
CREATE TRIGGER third_level_updated_at BEFORE UPDATE ON accounting.third_level FOR EACH ROW EXECUTE PROCEDURE accounting.current_timestamp_on_update();
CREATE TRIGGER fourth_level_updated_at BEFORE UPDATE ON accounting.fourth_level FOR EACH ROW EXECUTE PROCEDURE accounting.current_timestamp_on_update();
CREATE TRIGGER branch_updated_at BEFORE UPDATE ON accounting.branch FOR EACH ROW EXECUTE PROCEDURE accounting.current_timestamp_on_update();
CREATE TRIGGER currency_updated_at BEFORE UPDATE ON accounting.currency FOR EACH ROW EXECUTE PROCEDURE accounting.current_timestamp_on_update();
CREATE TRIGGER journal_voucher_updated_at BEFORE UPDATE ON accounting.journal_voucher FOR EACH ROW EXECUTE PROCEDURE accounting.current_timestamp_on_update();
CREATE TRIGGER voucher_base_updated_at BEFORE UPDATE ON accounting.voucher_base FOR EACH ROW EXECUTE PROCEDURE accounting.current_timestamp_on_update();
CREATE TRIGGER third_level_balance_update AFTER INSERT OR UPDATE OR DELETE ON accounting.fourth_level FOR EACH ROW EXECUTE PROCEDURE third_level_balance();
CREATE TRIGGER second_level_balance_update AFTER INSERT OR UPDATE OR DELETE ON accounting.third_level FOR EACH ROW EXECUTE PROCEDURE second_level_balance();
CREATE TRIGGER first_level_balance_update AFTER INSERT OR UPDATE ON accounting.second_level FOR EACH ROW EXECUTE PROCEDURE first_level_balance();
--=========================================================================--
CREATE INDEX second_level_name_idx ON accounting.third_level USING hash (second_level_id);
CREATE INDEX third_level_name_idx ON accounting.fourth_level USING hash (third_level_id);
CREATE INDEX currency_reference_idx ON accounting.currency USING hash (reference_id);
CREATE INDEX currency_ar_name_idx ON accounting.currency USING hash (ar_name);
CREATE INDEX currency_en_name_idx ON accounting.currency USING hash (en_name);
CREATE INDEX voucher_base_vid_idx ON accounting.voucher_base USING hash (voucher_id);
CREATE INDEX voucher_base_vtype_idx ON accounting.voucher_base USING hash (voucher_type);
CREATE INDEX voucher_base_vstatus_idx ON accounting.voucher_base USING hash (voucher_status);
CREATE INDEX voucher_base_branch_idx ON accounting.voucher_base USING hash (branch_id);
--=========================================================================--
