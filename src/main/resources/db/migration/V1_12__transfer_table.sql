CREATE TYPE accounting.transfer_destination AS ENUM (
    'BOX',
    'TELLER'
);

CREATE CAST (CHARACTER VARYING AS accounting.transfer_destination) WITH INOUT AS ASSIGNMENT;

CREATE TABLE IF NOT EXISTS accounting.transfer_payment (
    id SERIAL PRIMARY KEY,
    date_time timestamp without time zone DEFAULT NOW() NOT NULL,
    sender_account_id INT NOT NULL,
    payer_account_id INT NULL,
    destination accounting.transfer_destination NOT NULL,
    currency_id INT NOT NULL,
    amount FLOAT NOT NULL,
    commission_ratio FLOAT,
    received_commission FLOAT,
    paid_commission FLOAT,
    beneficiary_name VARCHAR(255) NOT NULL,
    sender_name VARCHAR(255),
    phone VARCHAR(255),
    notes TEXT,
    country TEXT NOT NULL,
    paid SMALLINT DEFAULT 0 NOT NULL,
    payment_date timestamp without time zone NULL,
    recipient_name VARCHAR(255),
    created_by BIGINT,
    updated_by BIGINT,
    updated_at timestamp without time zone DEFAULT NOW() NOT NULL,
    created_at timestamp without time zone DEFAULT NOW() NOT NULL
);

ALTER TABLE ONLY accounting.transfer_payment
    ADD CONSTRAINT sender_account_id_fkey FOREIGN KEY (sender_account_id) REFERENCES accounting.fourth_level (id) ON DELETE CASCADE;

ALTER TABLE ONLY accounting.transfer_payment
    ADD CONSTRAINT transfer_currency_id_fkey FOREIGN KEY (currency_id) REFERENCES accounting.currency (id) ON DELETE CASCADE;
