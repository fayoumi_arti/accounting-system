CREATE table accounting.internal_vouchers (
    id SERIAL PRIMARY KEY,
    voucher_id BIGINT NOT NULL,
    debit_account_id BIGINT NOT NULL,
    credit_account_id BIGINT NOT NULL,
    debit_currency_id INT NOT NULL,
    debit_amount FLOAT8 NOT NULL,
    debit_equivalence FLOAT8 NOT NULL,
    debit_evaluation_price FLOAT8 NOT NULL,
    credit_currency_id INT NOT NULL,
    credit_amount FLOAT8 NOT NULL,
    credit_equivalence FLOAT8 NOT NULL,
    credit_evaluation_price FLOAT8 NOT NULL,
    notes TEXT,
    created_by BIGINT,
    updated_by BIGINT,
    updated_at timestamp without time zone DEFAULT NOW() NOT NULL,
    created_at timestamp without time zone DEFAULT NOW() NOT NULL,
    date_time timestamp without time zone DEFAULT NOW() NOT NULL,
    branch_id BIGINT ,
    voucher_status accounting.voucher_status DEFAULT 'ENTRY' NOT NULL,
    debit_account_previous_balance FLOAT8 NOT NULL,
    credit_account_previous_balance FLOAT8 NOT NULL,
    debit_balance_id INT NOT NULL,
    credit_balance_id INT NOT NULL
);

ALTER TABLE ONLY accounting.internal_vouchers
    ADD CONSTRAINT internal_vouchers_credit_account_id_fkey FOREIGN KEY (credit_account_id) REFERENCES accounting.fourth_level (id) ON DELETE CASCADE;

ALTER TABLE ONLY accounting.internal_vouchers
    ADD CONSTRAINT internal_vouchers_debit_account_id_fkey FOREIGN KEY (debit_account_id) REFERENCES accounting.fourth_level (id) ON DELETE CASCADE;

ALTER TABLE ONLY accounting.internal_vouchers
    ADD CONSTRAINT internal_vouchers_credit_currency_id_fkey FOREIGN KEY (credit_currency_id) REFERENCES accounting.currency (id) ON DELETE CASCADE;

ALTER TABLE ONLY accounting.internal_vouchers
    ADD CONSTRAINT internal_vouchers_debit_currency_id_fkey FOREIGN KEY (debit_currency_id) REFERENCES accounting.currency (id) ON DELETE CASCADE;

CREATE TRIGGER internal_vouchers_updated_at BEFORE UPDATE ON accounting.internal_vouchers FOR EACH ROW EXECUTE PROCEDURE accounting.current_timestamp_on_update();