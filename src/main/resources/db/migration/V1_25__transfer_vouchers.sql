ALTER TYPE accounting.voucher_type ADD VALUE 'TRANSFER';
ALTER TYPE accounting.voucher_type ADD VALUE 'PAID_TRANSFER';
ALTER TYPE accounting.voucher_type ADD VALUE 'REVERSED_TRANSFER';
ALTER TYPE accounting.voucher_type ADD VALUE 'REVERSED_PAID_TRANSFER';

ALTER TABLE accounting.transfer_payment ADD COLUMN voucher_base_id INT;


ALTER TABLE accounting.transfer_payment
    ADD CONSTRAINT fk_voucher_base_transfer_payment FOREIGN KEY (voucher_base_id) REFERENCES accounting.voucher_base (id) ON DELETE CASCADE;
