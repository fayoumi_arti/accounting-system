-- CREATE TYPE accounting.modification_type AS ENUM (
--     'EDIT',
--     'DELETE',
--     'REVERSE'
-- );
-- CREATE CAST (CHARACTER VARYING AS accounting.modification_type) WITH INOUT AS ASSIGNMENT;
--
-- CREATE TABLE accounting.modifications (
--    id SERIAL PRIMARY KEY,
--    base_id BIGINT,
--    modification_sequence BIGINT,
--    voucher_type accounting.voucher_type NOT NULL,
--    voucher_date_time timestamp without time zone DEFAULT NOW() NOT NULL,
--    original_entry_id BIGINT,
--    approve_date timestamp without time zone DEFAULT NOW() NOT NULL,
--    approver_id BIGINT,
--    modifier_id BIGINT,
--    modification_reason TEXT,
--    approved BOOLEAN,
--    before_modify jsonb,
--    after_modify jsonb,
--    modification_type accounting.modification_type NOT NULL,
--    updated_at timestamp without time zone DEFAULT NOW() NOT NULL,
--    created_at timestamp without time zone DEFAULT NOW() NOT NULL
-- );

CREATE TYPE accounting.submission_type AS ENUM (
    'ADD',
    'EDIT',
    'DELETE',
    'REVERSE'
);
CREATE CAST (CHARACTER VARYING AS accounting.submission_type) WITH INOUT AS ASSIGNMENT;

ALTER TABLE accounting.voucher_base ADD COLUMN submission_type accounting.submission_type DEFAULT 'ADD';
ALTER TABLE accounting.voucher_base ADD COLUMN  deny_reason TEXT;
ALTER TABLE accounting.voucher_base ADD COLUMN  modification_reason TEXT;
ALTER TABLE accounting.voucher_base DROP COLUMN IF EXISTS reverse_reason;



