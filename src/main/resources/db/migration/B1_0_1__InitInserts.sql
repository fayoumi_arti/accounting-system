INSERT INTO accounting.first_level (name_ar, name_en)
VALUES
    ('الاصول', 'assets'),
    ('الالتزامات', 'liabilities'),
    ('حقوق الملكية', 'owners equity'),
    ('قائمة الدخل', 'income statement');

INSERT INTO accounting.second_level (name_ar, name_en, first_level_id)
VALUES
    ('اصول متداولة', 'current assets', 1),
    ('اصول ثابتة', 'fixed assets', 1),
    ('التزامات قصيرة الاجل', 'short term liabilities', 2),
    ('التزامات طويلة الاجل', 'long term liabilities',  2),
    ('راس المال', 'capital', 3),
    ('الاحتياطيات', 'reserve', 3),
    ('الارباح والخسائر المدورة', 'retained profits and losses', 3),
    ('المبيعات', 'sales', 4),
    ('مردودات المبيعات', 'sales returns and allowances', 4),
    ('خصم مبيعات', 'sales discount', 4),
    ('مشتريات', 'purchases', 4),
    ('خصم مشتريات', 'purchases discount', 4),
    ('مردودات المشتريات', 'purchases returns and allowances', 4),
    ('مصاريف تشغيلية', 'operation expenses', 4),
    ('مصاريف بيعية', 'selling expenses', 4),
    ('ايرادات', 'revenue', 4),
    ('ايرادات اخرى', 'other revenue', 4),
    ('مصاريف ادارية وعمومية', 'general and administrative', 4),
    ('ارباح خسائر اصول', 'profit loss assets', 4);