ALTER TABLE accounting.journal_voucher DROP CONSTRAINT currency_id_fkey;
ALTER TABLE accounting.payment_vouchers DROP CONSTRAINT payment_vouchers_currency_id_fkey;
ALTER TABLE accounting.receipt_vouchers DROP CONSTRAINT receipt_vouchers_currency_id_fkey;
ALTER TABLE accounting.expenses_voucher DROP CONSTRAINT expenses_voucher_currency_id_fkey;

ALTER TABLE accounting.journal_voucher DROP COLUMN currency_reference_id;
ALTER TABLE accounting.payment_vouchers DROP COLUMN currency_reference_id;
ALTER TABLE accounting.receipt_vouchers DROP COLUMN currency_reference_id;
ALTER TABLE accounting.expenses_voucher DROP COLUMN currency_reference_id;

ALTER TABLE accounting.journal_voucher ADD COLUMN currency_id INT NOT NULL;
ALTER TABLE accounting.payment_vouchers ADD COLUMN currency_id INT NOT NULL;
ALTER TABLE accounting.receipt_vouchers ADD COLUMN currency_id INT NOT NULL;
ALTER TABLE accounting.expenses_voucher ADD COLUMN currency_id INT NOT NULL;

ALTER TABLE ONLY accounting.journal_voucher
    ADD CONSTRAINT journal_vouchers_currency_id_fkey FOREIGN KEY (currency_id) REFERENCES accounting.currency (id) ON DELETE CASCADE;

ALTER TABLE ONLY accounting.payment_vouchers
    ADD CONSTRAINT payment_vouchers_currency_id_fkey FOREIGN KEY (currency_id) REFERENCES accounting.currency (id) ON DELETE CASCADE;

ALTER TABLE ONLY accounting.receipt_vouchers
    ADD CONSTRAINT receipt_vouchers_currency_id_fkey FOREIGN KEY (currency_id) REFERENCES accounting.currency (id) ON DELETE CASCADE;

ALTER TABLE ONLY accounting.expenses_voucher
    ADD CONSTRAINT expenses_voucher_currency_id_fkey FOREIGN KEY (currency_id) REFERENCES accounting.currency (id) ON DELETE CASCADE;

ALTER TABLE accounting.journal_voucher ADD COLUMN evaluation_price FLOAT;
ALTER TABLE accounting.payment_vouchers ADD COLUMN evaluation_price FLOAT;
ALTER TABLE accounting.receipt_vouchers ADD COLUMN evaluation_price FLOAT;
ALTER TABLE accounting.expenses_voucher ADD COLUMN evaluation_price FLOAT;
