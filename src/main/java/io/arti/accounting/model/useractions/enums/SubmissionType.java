package io.arti.accounting.model.useractions.enums;

public enum SubmissionType {
    ADD,
    EDIT,
    DELETE,
    REVERSE
}
