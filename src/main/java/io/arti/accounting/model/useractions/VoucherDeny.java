package io.arti.accounting.model.useractions;

import java.io.Serializable;

public class VoucherDeny implements Serializable {

    private Integer baseId;
    private String denyReason;
    private Integer approverId;

    public VoucherDeny() {
    }

    public VoucherDeny(Integer baseId, String denyReason, Integer approverId) {
        this.baseId = baseId;
        this.denyReason = denyReason;
        this.approverId = approverId;
    }

    public Integer getBaseId() {
        return baseId;
    }

    public void setBaseId(Integer baseId) {
        this.baseId = baseId;
    }

    public String getDenyReason() {
        return denyReason;
    }

    public void setDenyReason(String denyReason) {
        this.denyReason = denyReason;
    }

    public Integer getApproverId() {
        return approverId;
    }

    public void setApproverId(Integer approverId) {
        this.approverId = approverId;
    }

    @Override
    public String toString() {
        return "VoucherDeny{" +
                "baseId=" + baseId +
                ", denyReason='" + denyReason + '\'' +
                ", approverId=" + approverId +
                '}';
    }
}
