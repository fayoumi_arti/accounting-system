package io.arti.accounting.model.useractions.enums;

public enum ApproveStatus {
    ACCEPT,
    REJECT
}
