package io.arti.accounting.model.useractions;

import java.io.Serializable;

public class VoucherApprove implements Serializable {

    private Integer baseId;
    private Integer approverId;

    public VoucherApprove() {
    }

    public VoucherApprove(Integer baseId, Integer approverId) {
        this.baseId = baseId;
        this.approverId = approverId;
    }

    public Integer getBaseId() {
        return baseId;
    }

    public void setBaseId(Integer baseId) {
        this.baseId = baseId;
    }

    public Integer getApproverId() {
        return approverId;
    }

    public void setApproverId(Integer approverId) {
        this.approverId = approverId;
    }

    @Override
    public String toString() {
        return "VoucherApprove{" +
                "baseId=" + baseId +
                ", approverId=" + approverId +
                '}';
    }
}
