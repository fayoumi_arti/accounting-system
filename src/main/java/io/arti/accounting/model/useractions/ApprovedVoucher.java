package io.arti.accounting.model.useractions;

import io.arti.accounting.model.useractions.enums.ApproveStatus;

import java.io.Serializable;
import java.time.OffsetDateTime;

public class ApprovedVoucher implements Serializable {

    private Integer approverId;
    private Integer entryId;
    private OffsetDateTime approvalDate;

    public ApprovedVoucher() {
    }

    public ApprovedVoucher(Integer approverId, Integer entryId, OffsetDateTime approvalDate) {
        this.approverId = approverId;
        this.entryId = entryId;
        this.approvalDate = approvalDate;
    }

    public Integer getApproverId() {
        return approverId;
    }

    public void setApproverId(Integer approverId) {
        this.approverId = approverId;
    }

    public Integer getEntryId() {
        return entryId;
    }

    public void setEntryId(Integer entryId) {
        this.entryId = entryId;
    }

    public OffsetDateTime getApprovalDate() {
        return approvalDate;
    }

    public void setApprovalDate(OffsetDateTime approvalDate) {
        this.approvalDate = approvalDate;
    }


    @Override
    public String toString() {
        return "ApprovedVoucher{" +
                "approverId=" + approverId +
                ", entryId=" + entryId +
                ", approvalDate=" + approvalDate +
                '}';
    }
}
