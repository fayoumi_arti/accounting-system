package io.arti.accounting.model.useractions;

import io.arti.accounting.model.useractions.enums.SubmissionType;
import io.arti.accounting.model.vouchers.enums.VoucherType;
import org.springframework.data.relational.core.sql.In;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class SubmissionRequest implements Serializable {

    @NotNull(message = "entry id can not be null")
    private Integer entryId;
    @NotNull(message = "voucher id can not be null")
    private Integer voucherId;

    @NotNull(message = "base id can not be null")
    private Integer baseId;
    @NotNull(message = "voucher type can not be null")
    private VoucherType voucherType;
    @NotNull(message = "submission type can not be null")
    private SubmissionType submissionType;

    private String modificationReason;

    public SubmissionRequest() {
    }

    public SubmissionRequest(Integer entryId, Integer voucherId, Integer baseId, VoucherType voucherType, SubmissionType submissionType, String modificationReason) {
        this.entryId = entryId;
        this.voucherId = voucherId;
        this.baseId = baseId;
        this.voucherType = voucherType;
        this.submissionType = submissionType;
        this.modificationReason = modificationReason;
    }

    public Integer getEntryId() {
        return entryId;
    }

    public void setEntryId(Integer entryId) {
        this.entryId = entryId;
    }

    public Integer getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(Integer voucherId) {
        this.voucherId = voucherId;
    }

    public Integer getBaseId() {
        return baseId;
    }

    public void setBaseId(Integer baseId) {
        this.baseId = baseId;
    }

    public VoucherType getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(VoucherType voucherType) {
        this.voucherType = voucherType;
    }

    public SubmissionType getSubmissionType() {
        return submissionType;
    }

    public void setSubmissionType(SubmissionType submissionType) {
        this.submissionType = submissionType;
    }

    public String getModificationReason() {
        return modificationReason;
    }

    public void setModificationReason(String modificationReason) {
        this.modificationReason = modificationReason;
    }

    @Override
    public String toString() {
        return "SubmissionRequest{" +
                "entryId=" + entryId +
                ", voucherId=" + voucherId +
                ", baseId=" + baseId +
                ", voucherType=" + voucherType +
                ", submissionType=" + submissionType +
                ", modificationReason='" + modificationReason + '\'' +
                '}';
    }
}
