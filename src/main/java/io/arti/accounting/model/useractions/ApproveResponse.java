package io.arti.accounting.model.useractions;

import io.arti.accounting.model.useractions.enums.ApproveStatus;
import io.arti.accounting.model.useractions.enums.SubmissionType;
import io.arti.accounting.model.vouchers.enums.VoucherType;

import java.io.Serializable;

public class ApproveResponse implements Serializable {

    Integer approverId;
    ApproveStatus approveStatus;
    SubmissionType submissionType;
    Integer voucherId;
    VoucherType voucherType;
    String rejectionReason;

    public ApproveResponse() {
    }

    public ApproveResponse(Integer approverId, ApproveStatus approveStatus, SubmissionType submissionType, Integer voucherId, VoucherType voucherType, String rejectionReason) {
        this.approverId = approverId;
        this.approveStatus = approveStatus;
        this.submissionType = submissionType;
        this.voucherId = voucherId;
        this.voucherType = voucherType;
        this.rejectionReason = rejectionReason;
    }

    public Integer getApproverId() {
        return approverId;
    }

    public void setApproverId(Integer approverId) {
        this.approverId = approverId;
    }

    public ApproveStatus getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(ApproveStatus approveStatus) {
        this.approveStatus = approveStatus;
    }

    public SubmissionType getApproveType() {
        return submissionType;
    }

    public void setApproveType(SubmissionType submissionType) {
        this.submissionType = submissionType;
    }

    public Integer getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(Integer voucherId) {
        this.voucherId = voucherId;
    }

    public VoucherType getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(VoucherType voucherType) {
        this.voucherType = voucherType;
    }

    public String getRejectionReason() {
        return rejectionReason;
    }

    public void setRejectionReason(String rejectionReason) {
        this.rejectionReason = rejectionReason;
    }

    @Override
    public String toString() {
        return "ApproveResponse{" +
                "approverId=" + approverId +
                ", approveStatus=" + approveStatus +
                ", approveType=" + submissionType +
                ", voucherId=" + voucherId +
                ", voucherType=" + voucherType +
                ", rejectionReason='" + rejectionReason + '\'' +
                '}';
    }
}
