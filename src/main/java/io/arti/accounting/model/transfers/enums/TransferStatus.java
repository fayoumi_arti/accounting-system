package io.arti.accounting.model.transfers.enums;

public enum TransferStatus {
    PAID,
    UNPAID,
    PENDING,
    DELETED
}