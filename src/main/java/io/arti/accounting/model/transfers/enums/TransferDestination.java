package io.arti.accounting.model.transfers.enums;

public enum TransferDestination {
    BOX,
    TELLER
}
