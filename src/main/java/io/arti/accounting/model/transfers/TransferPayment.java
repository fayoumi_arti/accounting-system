package io.arti.accounting.model.transfers;

import io.arti.accounting.model.transfers.enums.TransferDestination;
import io.arti.accounting.model.transfers.enums.TransferStatus;
import io.arti.accounting.model.vouchers.Attachments;
import io.arti.accounting.model.vouchers.JournalBaseVoucher;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.List;

public class TransferPayment implements Serializable {

    private int id;
    private OffsetDateTime dateTime;
    @NotNull(message = "Sender account Id can not be null")
    private Integer senderAccountId;
    private Integer payerAccountId;
    private TransferDestination destination;
    @NotNull(message = "Currency Id can not be null")
    private Integer currencyId;
    private float amount;
    private float commissionRatio;
    private float receivedCommission;
    private float paidCommission;
    @NotNull(message = "Beneficiary name can not be null")
    private String beneficiaryName;
    private String senderName;
    private String phone;
    @NotNull(message = "Country can not be null")
    private String country;
    private TransferStatus status;
    private String notes;
    private OffsetDateTime paymentDate;
    private String recipientName;
    private Integer createdBy;
    private Integer updatedBy;
    private Integer branchId;
    private Float paidCommissionRatio;

    private List<Attachments> attachments;
    private List<JournalBaseVoucher> journalBaseVouchers;

    private Integer voucherBaseId;

    private OffsetDateTime updatedAt;
    private OffsetDateTime createdAt;

    public TransferPayment() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public OffsetDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(OffsetDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public Integer getSenderAccountId() {
        return senderAccountId;
    }

    public void setSenderAccountId(Integer senderAccountId) {
        this.senderAccountId = senderAccountId;
    }

    public Integer getPayerAccountId() {
        return payerAccountId;
    }

    public void setPayerAccountId(Integer payerAccountId) {
        this.payerAccountId = payerAccountId;
    }

    public TransferDestination getDestination() {
        return destination;
    }

    public void setDestination(TransferDestination destination) {
        this.destination = destination;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public float getCommissionRatio() {
        return commissionRatio;
    }

    public void setCommissionRatio(float commissionRatio) {
        this.commissionRatio = commissionRatio;
    }

    public float getReceivedCommission() {
        return receivedCommission;
    }

    public void setReceivedCommission(float receivedCommission) {
        this.receivedCommission = receivedCommission;
    }

    public float getPaidCommission() {
        return paidCommission;
    }

    public void setPaidCommission(float paidCommission) {
        this.paidCommission = paidCommission;
    }

    public String getBeneficiaryName() {
        return beneficiaryName;
    }

    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public TransferStatus getStatus() {
        return status;
    }

    public void setStatus(TransferStatus status) {
        this.status = status;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public OffsetDateTime getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(OffsetDateTime paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    public Float getPaidCommissionRatio() {
        return paidCommissionRatio;
    }

    public void setPaidCommissionRatio(Float paidCommissionRatio) {
        this.paidCommissionRatio = paidCommissionRatio;
    }

    public OffsetDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(OffsetDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public OffsetDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(OffsetDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public List<JournalBaseVoucher> getJournalBaseVouchers() {
        return journalBaseVouchers;
    }

    public void setJournalBaseVouchers(List<JournalBaseVoucher> journalBaseVouchers) {
        this.journalBaseVouchers = journalBaseVouchers;
    }

    public Integer getVoucherBaseId() {
        return voucherBaseId;
    }

    public void setVoucherBaseId(Integer voucherBaseId) {
        this.voucherBaseId = voucherBaseId;
    }

    public List<Attachments> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<Attachments> attachments) {
        this.attachments = attachments;
    }

    @Override
    public String toString() {
        return "TransferPayment{" +
                "id=" + id +
                ", dateTime=" + dateTime +
                ", senderAccountId=" + senderAccountId +
                ", payerAccountId=" + payerAccountId +
                ", destination=" + destination +
                ", currencyId=" + currencyId +
                ", amount=" + amount +
                ", commissionRatio=" + commissionRatio +
                ", receivedCommission=" + receivedCommission +
                ", paidCommission=" + paidCommission +
                ", beneficiaryName='" + beneficiaryName + '\'' +
                ", senderName='" + senderName + '\'' +
                ", phone='" + phone + '\'' +
                ", country='" + country + '\'' +
                ", status=" + status +
                ", notes='" + notes + '\'' +
                ", paymentDate=" + paymentDate +
                ", recipientName='" + recipientName + '\'' +
                ", createdBy=" + createdBy +
                ", updatedBy=" + updatedBy +
                ", branchId=" + branchId +
                ", paidCommissionRatio=" + paidCommissionRatio +
                ", attachments=" + attachments +
                ", journalBaseVouchers=" + journalBaseVouchers +
                ", voucherBaseId=" + voucherBaseId +
                ", updatedAt=" + updatedAt +
                ", createdAt=" + createdAt +
                '}';
    }
}
