package io.arti.accounting.model.general;

import java.io.Serializable;
import java.time.OffsetDateTime;

public class Profile implements Serializable {

    private Integer id;
    private Integer currencyId;
    private String nameEn;
    private String nameAr;
    private String country;
    private String governorate;
    private String addressAr;
    private String addressEn;
    private String logo;
    private String phone;
    private String fax;
    private String postalCode;
    private String postalBox;
    private String email;
    private Integer createdBy;
    private Integer updatedBy;
    private OffsetDateTime updatedAt;
    private OffsetDateTime createdAt;

    public Profile() {
    }

    public Profile(Integer currencyId, String nameEn, String nameAr, String country, String governorate, String addressAr, String addressEn, String logo, String phone, String fax, String postalCode, String postalBox, String email, Integer created, Integer createdBy, Integer updatedBy, OffsetDateTime updatedAt, OffsetDateTime createdAt) {
        this.currencyId = currencyId;
        this.nameEn = nameEn;
        this.nameAr = nameAr;
        this.country = country;
        this.governorate = governorate;
        this.addressAr = addressAr;
        this.addressEn = addressEn;
        this.logo = logo;
        this.phone = phone;
        this.fax = fax;
        this.postalCode = postalCode;
        this.postalBox = postalBox;
        this.email = email;
        this.createdBy = createdBy;
        this.updatedBy = updatedBy;
        this.updatedAt = updatedAt;
        this.createdAt = createdAt;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameAr() {
        return nameAr;
    }

    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getGovernorate() {
        return governorate;
    }

    public void setGovernorate(String governorate) {
        this.governorate = governorate;
    }

    public String getAddressAr() {
        return addressAr;
    }

    public void setAddressAr(String addressAr) {
        this.addressAr = addressAr;
    }

    public String getAddressEn() {
        return addressEn;
    }

    public void setAddressEn(String addressEn) {
        this.addressEn = addressEn;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPostalBox() {
        return postalBox;
    }

    public void setPostalBox(String postalBox) {
        this.postalBox = postalBox;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public OffsetDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(OffsetDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public OffsetDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(OffsetDateTime createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "Profile{" +
                "id=" + id +
                ", currencyId=" + currencyId +
                ", nameEn='" + nameEn + '\'' +
                ", nameAr='" + nameAr + '\'' +
                ", country='" + country + '\'' +
                ", governorate='" + governorate + '\'' +
                ", addressAr='" + addressAr + '\'' +
                ", addressEn='" + addressEn + '\'' +
                ", logo='" + logo + '\'' +
                ", phone='" + phone + '\'' +
                ", fax='" + fax + '\'' +
                ", postalCode='" + postalCode + '\'' +
                ", postalBox='" + postalBox + '\'' +
                ", email='" + email + '\'' +
                ", createdBy=" + createdBy +
                ", updatedBy=" + updatedBy +
                ", updatedAt=" + updatedAt +
                ", createdAt=" + createdAt +
                '}';
    }
}
