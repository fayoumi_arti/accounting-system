package io.arti.accounting.model.general;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.OffsetDateTime;

public class Branches implements Serializable {

    private Integer id;

    @NotNull(message = "Arabic name can not be null")
    private String arName;
    private String enName;

    private Integer createdBy;
    private Integer updatedBy;

    private OffsetDateTime updatedAt;
    private OffsetDateTime createdAt;

    public Branches() {
    }

    public Branches(String arName, String enName, Integer createdBy, Integer updatedBy) {
        this.arName = arName;
        this.enName = enName;
        this.createdBy = createdBy;
        this.updatedBy = updatedBy;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getArName() {
        return arName;
    }

    public void setArName(String arName) {
        this.arName = arName;
    }

    public String getEnName() {
        return enName;
    }

    public void setEnName(String enName) {
        this.enName = enName;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public OffsetDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(OffsetDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public OffsetDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(OffsetDateTime createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "Branches{" +
                "id=" + id +
                ", arName='" + arName + '\'' +
                ", enName='" + enName + '\'' +
                ", createdBy=" + createdBy +
                ", updatedBy=" + updatedBy +
                ", updatedAt=" + updatedAt +
                ", createdAt=" + createdAt +
                '}';
    }
}
