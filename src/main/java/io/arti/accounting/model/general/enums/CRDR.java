package io.arti.accounting.model.general.enums;

public enum CRDR {
    DEBIT(1,"Debt","مدين"),
    CREDIT(2,"Credit","دائن"),
    ALL(3,"All","الجميع");

    private int id;
    private final String nameEn;
    private final String nameAr;

    CRDR(int id, String nameEn, String nameAr) {
        this.id = id;
        this.nameEn = nameEn;
        this.nameAr = nameAr;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameEn() {
        return nameEn;
    }

    public String getNameAr() {
        return nameAr;
    }
}
