package io.arti.accounting.model.general;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.OffsetDateTime;

public class Currencies implements Serializable {

    private Integer id;
    @NotNull(message = "Reference Id can not be null")
    private Integer referenceId;

    @NotNull(message = "Arabic name can not be null")
    private String arName;
    private String enName;
    @NotNull(message = "ISO can not be null")
    private String iso;
    private double evaluationPrice;

    private Integer createdBy;
    private Integer updatedBy;

    private OffsetDateTime updatedAt;
    private OffsetDateTime createdAt;

    public Currencies() {
    }

    public Currencies(Integer referenceId, String arName, String enName, String iso, Float evaluationPrice, Integer createdBy, Integer updatedBy) {
        this.referenceId = referenceId;
        this.arName = arName;
        this.enName = enName;
        this.iso = iso;
        this.evaluationPrice = evaluationPrice;
        this.createdBy = createdBy;
        this.updatedBy = updatedBy;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(Integer referenceId) {
        this.referenceId = referenceId;
    }

    public String getArName() {
        return arName;
    }

    public void setArName(String arName) {
        this.arName = arName;
    }

    public String getEnName() {
        return enName;
    }

    public void setEnName(String enName) {
        this.enName = enName;
    }

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    public double getEvaluationPrice() {
        return evaluationPrice;
    }

    public void setEvaluationPrice(double evaluationPrice) {
        this.evaluationPrice = evaluationPrice;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public OffsetDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(OffsetDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public OffsetDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(OffsetDateTime createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "Currencies{" +
                "id=" + id +
                ", referenceId=" + referenceId +
                ", arName='" + arName + '\'' +
                ", enName='" + enName + '\'' +
                ", iso='" + iso + '\'' +
                ", evaluationPrice=" + evaluationPrice +
                ", createdBy=" + createdBy +
                ", updatedBy=" + updatedBy +
                ", updatedAt=" + updatedAt +
                ", createdAt=" + createdAt +
                '}';
    }
}
