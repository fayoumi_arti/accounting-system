package io.arti.accounting.model.vouchers;

import io.arti.accounting.model.vouchers.enums.VoucherStatus;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.OffsetDateTime;

public class InternalVoucher {
    private Integer id;

    private Integer voucherId;
    @NotNull
    @Valid
    private VoucherBase voucherBase;
    @NotNull
    private Integer creditAccountId;

    @NotNull
    private Integer debitAccountId;

    private Integer debitCurrencyId;
    private Integer creditCurrencyId;
    private BigDecimal debitAmount;
    private BigDecimal debitEquivalence;
    private float debitEvaluationPrice;
    private BigDecimal creditAmount;
    private BigDecimal creditEquivalence;
    private float creditEvaluationPrice;
    private String notes;
    private Integer baseId;

    private float creditAccountPreviousBalance;
    private float debitAccountPreviousBalance;
    private VoucherStatus voucherStatus;
    private Integer branchId;
    private Integer debitBalanceId;
    private Integer creditBalanceId;
    private Integer createdBy;
    private Integer updatedBy;
    private OffsetDateTime dateTime;
    private OffsetDateTime updatedAt;
    private OffsetDateTime createdAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(Integer voucherId) {
        this.voucherId = voucherId;
    }

    public VoucherBase getVoucherBase() {
        return voucherBase;
    }

    public void setVoucherBase(VoucherBase voucherBase) {
        this.voucherBase = voucherBase;
    }

    public Integer getCreditAccountId() {
        return creditAccountId;
    }

    public void setCreditAccountId(Integer creditAccountId) {
        this.creditAccountId = creditAccountId;
    }

    public Integer getDebitAccountId() {
        return debitAccountId;
    }

    public void setDebitAccountId(Integer debitAccountId) {
        this.debitAccountId = debitAccountId;
    }

    public Integer getDebitCurrencyId() {
        return debitCurrencyId;
    }

    public void setDebitCurrencyId(Integer debitCurrencyId) {
        this.debitCurrencyId = debitCurrencyId;
    }

    public Integer getCreditCurrencyId() {
        return creditCurrencyId;
    }

    public void setCreditCurrencyId(Integer creditCurrencyId) {
        this.creditCurrencyId = creditCurrencyId;
    }


    public float getDebitEvaluationPrice() {
        return debitEvaluationPrice;
    }

    public void setDebitEvaluationPrice(float debitEvaluationPrice) {
        this.debitEvaluationPrice = debitEvaluationPrice;
    }

    public BigDecimal getDebitAmount() {
        return debitAmount;
    }

    public void setDebitAmount(BigDecimal debitAmount) {
        this.debitAmount = debitAmount;
    }

    public BigDecimal getDebitEquivalence() {
        return debitEquivalence;
    }

    public void setDebitEquivalence(BigDecimal debitEquivalence) {
        this.debitEquivalence = debitEquivalence;
    }

    public BigDecimal getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(BigDecimal creditAmount) {
        this.creditAmount = creditAmount;
    }

    public BigDecimal getCreditEquivalence() {
        return creditEquivalence;
    }

    public void setCreditEquivalence(BigDecimal creditEquivalence) {
        this.creditEquivalence = creditEquivalence;
    }

    public float getCreditEvaluationPrice() {
        return creditEvaluationPrice;
    }

    public void setCreditEvaluationPrice(float creditEvaluationPrice) {
        this.creditEvaluationPrice = creditEvaluationPrice;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Integer getBaseId() {
        return baseId;
    }

    public void setBaseId(Integer baseId) {
        this.baseId = baseId;
    }

    public float getCreditAccountPreviousBalance() {
        return creditAccountPreviousBalance;
    }

    public void setCreditAccountPreviousBalance(float creditAccountPreviousBalance) {
        this.creditAccountPreviousBalance = creditAccountPreviousBalance;
    }

    public float getDebitAccountPreviousBalance() {
        return debitAccountPreviousBalance;
    }

    public void setDebitAccountPreviousBalance(float debitAccountPreviousBalance) {
        this.debitAccountPreviousBalance = debitAccountPreviousBalance;
    }

    public VoucherStatus getVoucherStatus() {
        return voucherStatus;
    }

    public void setVoucherStatus(VoucherStatus voucherStatus) {
        this.voucherStatus = voucherStatus;
    }

    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    public Integer getDebitBalanceId() {
        return debitBalanceId;
    }

    public void setDebitBalanceId(Integer debitBalanceId) {
        this.debitBalanceId = debitBalanceId;
    }

    public Integer getCreditBalanceId() {
        return creditBalanceId;
    }

    public void setCreditBalanceId(Integer creditBalanceId) {
        this.creditBalanceId = creditBalanceId;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public OffsetDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(OffsetDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public OffsetDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(OffsetDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public OffsetDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(OffsetDateTime createdAt) {
        this.createdAt = createdAt;
    }
}
