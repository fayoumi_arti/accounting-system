package io.arti.accounting.model.vouchers.enums;

public enum VoucherStatus {
    ENTRY,
    TRANSFERRED,
    APPROVED,
    DENIED
}
