package io.arti.accounting.model.vouchers;

import io.arti.accounting.model.accountstree.AccountBalance;
import io.arti.accounting.model.accountstree.FourthLevel;

import java.math.BigDecimal;
import java.util.List;

public class AccountStatement {
    private FourthLevel account;
    private BigDecimal totalDebit;
    private BigDecimal totalCredit;
    private BigDecimal totalBalances;
    private BigDecimal startingBalance;
    private BigDecimal currentBalance;
    private int debitTransactions;
    private int creditTransactions;
    private int totalTransactions;
    private List<JournalVoucher> JournalTransactions;
    private List<PaymentReceiptVoucher> paymentTransactions;
    private List<PaymentReceiptVoucher> receiptTransactions;
    private List<ExpensesAccount> expensesTransactions;
    private List<InternalVoucher> internalTransactions;
    private List<AccountBalance> accountBalances;


    public FourthLevel getAccount() {
        return account;
    }

    public void setAccount(FourthLevel account) {
        this.account = account;
    }

    public BigDecimal getTotalDebit() {
        return totalDebit;
    }

    public void setTotalDebit(BigDecimal totalDebit) {
        this.totalDebit = totalDebit;
    }

    public BigDecimal getTotalCredit() {
        return totalCredit;
    }

    public void setTotalCredit(BigDecimal totalCredit) {
        this.totalCredit = totalCredit;
    }

    public BigDecimal getTotalBalances() {
        return totalBalances;
    }

    public void setTotalBalances(BigDecimal totalBalances) {
        this.totalBalances = totalBalances;
    }

    public BigDecimal getStartingBalance() {
        return startingBalance;
    }

    public void setStartingBalance(BigDecimal startingBalance) {
        this.startingBalance = startingBalance;
    }

    public void setCurrentBalance(BigDecimal currentBalance) {
        this.currentBalance = currentBalance;
    }

    public int getDebitTransactions() {
        return debitTransactions;
    }

    public void setDebitTransactions(int debitTransactions) {
        this.debitTransactions = debitTransactions;
    }

    public int getCreditTransactions() {
        return creditTransactions;
    }

    public void setCreditTransactions(int creditTransactions) {
        this.creditTransactions = creditTransactions;
    }

    public int getTotalTransactions() {
        return totalTransactions;
    }

    public void setTotalTransactions(int totalTransactions) {
        this.totalTransactions = totalTransactions;
    }

    public List<JournalVoucher> getJournalTransactions() {
        return JournalTransactions;
    }

    public void setJournalTransactions(List<JournalVoucher> journalTransactions) {
        JournalTransactions = journalTransactions;
    }

    public List<PaymentReceiptVoucher> getPaymentTransactions() {
        return paymentTransactions;
    }

    public void setPaymentTransactions(List<PaymentReceiptVoucher> paymentTransactions) {
        this.paymentTransactions = paymentTransactions;
    }

    public List<PaymentReceiptVoucher> getReceiptTransactions() {
        return receiptTransactions;
    }

    public void setReceiptTransactions(List<PaymentReceiptVoucher> receiptTransactions) {
        this.receiptTransactions = receiptTransactions;
    }

    public List<ExpensesAccount> getExpensesTransactions() {
        return expensesTransactions;
    }

    public void setExpensesTransactions(List<ExpensesAccount> expensesTransactions) {
        this.expensesTransactions = expensesTransactions;
    }

    public List<AccountBalance> getAccountBalances() {
        return accountBalances;
    }

    public void setAccountBalances(List<AccountBalance> accountBalances) {
        this.accountBalances = accountBalances;
    }

    public BigDecimal getCurrentBalance() {
        return currentBalance;
    }

    public List<InternalVoucher> getInternalTransactions() {
        return internalTransactions;
    }

    public void setInternalTransactions(List<InternalVoucher> internalTransactions) {
        this.internalTransactions = internalTransactions;
    }
}
