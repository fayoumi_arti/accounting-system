package io.arti.accounting.model.vouchers;

import java.io.Serializable;

public class Attachments implements Serializable {

    private String url;
    private String description;

    public Attachments() {
    }

    public Attachments(String url, String description) {
        this.url = url;
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Attachments{" +
                "url='" + url + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
