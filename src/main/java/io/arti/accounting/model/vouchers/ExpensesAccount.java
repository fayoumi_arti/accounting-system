package io.arti.accounting.model.vouchers;

import io.arti.accounting.model.general.enums.CRDR;
import io.arti.accounting.model.vouchers.enums.VoucherStatus;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.OffsetDateTime;

public class ExpensesAccount implements Serializable {
    @NotNull
    private Integer accountId;
    private Integer id;
    private Integer baseId;
    private Integer voucherId;
    private BigDecimal amount;
    private BigDecimal equivalence;
    @NotNull
    private Integer currencyId;
    private float evaluationPrice;

    @NotNull
    private CRDR crdr;
    private String notes;
    private OffsetDateTime dateTime;
    private Integer branchId;
    private Integer balanceId;
    private VoucherStatus voucherStatus;
    private float accountPreviousBalance;
    private Integer createdBy;
    private Integer updatedBy;
    private OffsetDateTime updatedAt;
    private OffsetDateTime createdAt;

    public ExpensesAccount() {
    }

    public ExpensesAccount(Integer id, Integer voucherId, BigDecimal amount, BigDecimal equivalence, Integer currencyId, float evaluationPrice, CRDR crdr, String notes, Integer createdBy, Integer updatedBy) {
        this.id = id;
        this.voucherId = voucherId;
        this.amount = amount;
        this.equivalence = equivalence;
        this.currencyId = currencyId;
        this.evaluationPrice = evaluationPrice;
        this.crdr = crdr;
        this.notes = notes;
        this.createdBy = createdBy;
        this.updatedBy = updatedBy;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(Integer voucherId) {
        this.voucherId = voucherId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getEquivalence() {
        return equivalence;
    }

    public void setEquivalence(BigDecimal equivalence) {
        this.equivalence = equivalence;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    public float getEvaluationPrice() {
        return evaluationPrice;
    }

    public void setEvaluationPrice(float evaluationPrice) {
        this.evaluationPrice = evaluationPrice;
    }

    public CRDR getCrdr() {
        return crdr;
    }

    public void setCrdr(CRDR crdr) {
        this.crdr = crdr;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public OffsetDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(OffsetDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public OffsetDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(OffsetDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public OffsetDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(OffsetDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    public VoucherStatus getVoucherStatus() {
        return voucherStatus;
    }

    public void setVoucherStatus(VoucherStatus voucherStatus) {
        this.voucherStatus = voucherStatus;
    }

    public float getAccountPreviousBalance() {
        return accountPreviousBalance;
    }

    public void setAccountPreviousBalance(float accountPreviousBalance) {
        this.accountPreviousBalance = accountPreviousBalance;
    }

    public Integer getBalanceId() {
        return balanceId;
    }

    public void setBalanceId(Integer balanceId) {
        this.balanceId = balanceId;
    }

    public Integer getBaseId() {
        return baseId;
    }

    public void setBaseId(Integer baseId) {
        this.baseId = baseId;
    }

    @Override
    public String toString() {
        return "ExpensesAccount{" +
                "accountId=" + accountId +
                ", id=" + id +
                ", voucherId=" + voucherId +
                ", amount=" + amount +
                ", equivalence=" + equivalence +
                ", currencyId=" + currencyId +
                ", evaluationPrice=" + evaluationPrice +
                ", crdr=" + crdr +
                ", notes='" + notes + '\'' +
                ", createdBy=" + createdBy +
                ", updatedBy=" + updatedBy +
                ", updatedAt=" + updatedAt +
                ", createdAt=" + createdAt +
                '}';
    }
}
