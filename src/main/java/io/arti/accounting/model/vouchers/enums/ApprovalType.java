package io.arti.accounting.model.vouchers.enums;

public enum ApprovalType {
    SUBMIT,
    DELETE,
    UPDATE
}
