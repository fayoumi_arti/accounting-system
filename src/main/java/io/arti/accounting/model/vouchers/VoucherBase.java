package io.arti.accounting.model.vouchers;

import io.arti.accounting.model.useractions.enums.SubmissionType;
import io.arti.accounting.model.vouchers.enums.VoucherStatus;
import io.arti.accounting.model.vouchers.enums.VoucherType;

import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.List;

public class VoucherBase implements Serializable {
    private Integer id;
    private OffsetDateTime dateTime;

    private Integer voucherId;

    private Integer reverseId;

    private String modificationReason;
    private String referenceNo;
    private VoucherType voucherType;
    private VoucherStatus voucherStatus;

    private Integer branchId;
    private String report;

    private SubmissionType submissionType;
    private Integer entryId;
    private OffsetDateTime entryDate;
    private Integer approverId;
    private OffsetDateTime approvalDate;


    private Integer createdBy;
    private Integer updatedBy;

    private OffsetDateTime updatedAt;
    private OffsetDateTime createdAt;
    private List<Attachments> attachments;

    private String denyReason;

    public VoucherBase() {
    }


    public VoucherBase(OffsetDateTime dateTime, String denyReason, SubmissionType submissionType, Integer voucherId, Integer reverseId, String modificationReason, String referenceNo, VoucherType voucherType, VoucherStatus voucherStatus, Integer branchId, String report, Integer entryId, OffsetDateTime entryDate, Integer approverId, OffsetDateTime approvalDate, Integer createdBy, Integer updatedBy, OffsetDateTime updatedAt, OffsetDateTime createdAt, List<Attachments> attachments) {
        this.dateTime = dateTime;
        this.voucherId = voucherId;
        this.reverseId = reverseId;
        this.modificationReason = modificationReason;
        this.referenceNo = referenceNo;
        this.voucherType = voucherType;
        this.voucherStatus = voucherStatus;
        this.branchId = branchId;
        this.report = report;
        this.entryId = entryId;
        this.entryDate = entryDate;
        this.approverId = approverId;
        this.approvalDate = approvalDate;
        this.createdBy = createdBy;
        this.updatedBy = updatedBy;
        this.updatedAt = updatedAt;
        this.createdAt = createdAt;
        this.attachments = attachments;
        this.submissionType = submissionType;
        this.denyReason = denyReason;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public OffsetDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(OffsetDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public Integer getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(Integer voucherId) {
        this.voucherId = voucherId;
    }

    public Integer getReverseId() {
        return reverseId;
    }

    public void setReverseId(Integer reverseId) {
        this.reverseId = reverseId;
    }

    public String getModificationReason() {
        return modificationReason;
    }

    public void setModificationReason(String modificationReason) {
        this.modificationReason = modificationReason;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public VoucherType getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(VoucherType voucherType) {
        this.voucherType = voucherType;
    }

    public VoucherStatus getVoucherStatus() {
        return voucherStatus;
    }

    public void setVoucherStatus(VoucherStatus voucherStatus) {
        this.voucherStatus = voucherStatus;
    }

    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    public String getReport() {
        return report;
    }

    public void setReport(String report) {
        this.report = report;
    }

    public Integer getEntryId() {
        return entryId;
    }

    public void setEntryId(Integer entryId) {
        this.entryId = entryId;
    }

    public OffsetDateTime getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(OffsetDateTime entryDate) {
        this.entryDate = entryDate;
    }

    public Integer getApproverId() {
        return approverId;
    }

    public void setApproverId(Integer approverId) {
        this.approverId = approverId;
    }

    public OffsetDateTime getApprovalDate() {
        return approvalDate;
    }

    public void setApprovalDate(OffsetDateTime approvalDate) {
        this.approvalDate = approvalDate;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public OffsetDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(OffsetDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public OffsetDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(OffsetDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public List<Attachments> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<Attachments> attachments) {
        this.attachments = attachments;
    }

    public SubmissionType getSubmissionType() {
        return submissionType;
    }

    public void setSubmissionType(SubmissionType submissionType) {
        this.submissionType = submissionType;
    }

    public String getDenyReason() {
        return denyReason;
    }

    public void setDenyReason(String denyReason) {
        this.denyReason = denyReason;
    }

    @Override
    public String toString() {
        return "VoucherBase{" +
                "id=" + id +
                ", dateTime=" + dateTime +
                ", voucherId=" + voucherId +
                ", reverseId=" + reverseId +
                ", modificationReason='" + modificationReason + '\'' +
                ", referenceNo='" + referenceNo + '\'' +
                ", voucherType=" + voucherType +
                ", voucherStatus=" + voucherStatus +
                ", branchId=" + branchId +
                ", report='" + report + '\'' +
                ", submissionType=" + submissionType +
                ", entryId=" + entryId +
                ", entryDate=" + entryDate +
                ", approverId=" + approverId +
                ", approvalDate=" + approvalDate +
                ", createdBy=" + createdBy +
                ", updatedBy=" + updatedBy +
                ", updatedAt=" + updatedAt +
                ", createdAt=" + createdAt +
                ", attachments=" + attachments +
                ", denyReason='" + denyReason + '\'' +
                '}';
    }
}