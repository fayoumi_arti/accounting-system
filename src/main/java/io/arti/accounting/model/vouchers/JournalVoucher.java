package io.arti.accounting.model.vouchers;

import io.arti.accounting.model.general.enums.CRDR;
import io.arti.accounting.model.vouchers.enums.VoucherStatus;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.OffsetDateTime;

public class JournalVoucher implements Serializable {


    private Integer id;

    private Integer voucherId;
    private Integer balanceId;
    private Integer accountId;
    private Integer currencyId;

    private BigDecimal amount;
    private BigDecimal equivalence;
    private float evaluationPrice;

    private CRDR crdr;
    private String note;
    private OffsetDateTime dateTime;
    private Integer branchId;
    private VoucherStatus voucherStatus;
    private float accountPreviousBalance;
    private Integer createdBy;
    private Integer updatedBy;

    private OffsetDateTime updatedAt;
    private OffsetDateTime createdAt;

    private Integer baseId;

    public JournalVoucher() {
    }

    public JournalVoucher(Integer voucherId, Integer accountId, Integer currencyId, BigDecimal amount, BigDecimal equivalence, float evaluationPrice, CRDR crdr, String note, Integer createdBy, Integer updatedBy) {
        this.voucherId = voucherId;
        this.accountId = accountId;
        this.currencyId = currencyId;
        this.amount = amount;
        this.equivalence = equivalence;
        this.evaluationPrice = evaluationPrice;
        this.crdr = crdr;
        this.note = note;
        this.createdBy = createdBy;
        this.updatedBy = updatedBy;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(Integer voucherId) {
        this.voucherId = voucherId;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getEquivalence() {
        return equivalence;
    }

    public void setEquivalence(BigDecimal equivalence) {
        this.equivalence = equivalence;
    }

    public float getEvaluationPrice() {
        return evaluationPrice;
    }

    public void setEvaluationPrice(float evaluationPrice) {
        this.evaluationPrice = evaluationPrice;
    }

    public CRDR getCrdr() {
        return crdr;
    }

    public void setCrdr(CRDR crdr) {
        this.crdr = crdr;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public OffsetDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(OffsetDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public OffsetDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(OffsetDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public OffsetDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(OffsetDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    public VoucherStatus getVoucherStatus() {
        return voucherStatus;
    }

    public void setVoucherStatus(VoucherStatus voucherStatus) {
        this.voucherStatus = voucherStatus;
    }

    public float getAccountPreviousBalance() {
        return accountPreviousBalance;
    }

    public void setAccountPreviousBalance(float accountPreviousBalance) {
        this.accountPreviousBalance = accountPreviousBalance;
    }

    public Integer getBalanceId() {
        return balanceId;
    }

    public void setBalanceId(Integer balanceId) {
        this.balanceId = balanceId;
    }

    public Integer getBaseId() {
        return baseId;
    }

    public void setBaseId(Integer baseId) {
        this.baseId = baseId;
    }

    @Override
    public String toString() {
        return "JournalVoucher{" +
                "id=" + id +
                ", voucherId=" + voucherId +
                ", accountId=" + accountId +
                ", currencyId=" + currencyId +
                ", amount=" + amount +
                ", equivalence=" + equivalence +
                ", evaluationPrice=" + evaluationPrice +
                ", crdr=" + crdr +
                ", note='" + note + '\'' +
                ", createdBy=" + createdBy +
                ", updatedBy=" + updatedBy +
                ", updatedAt=" + updatedAt +
                ", createdAt=" + createdAt +
                '}';
    }
}
