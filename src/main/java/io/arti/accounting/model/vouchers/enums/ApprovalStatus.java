package io.arti.accounting.model.vouchers.enums;

public enum ApprovalStatus {
    APPROVED,
    REJECTED
}
