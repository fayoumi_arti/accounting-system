package io.arti.accounting.model.vouchers;

import java.math.BigDecimal;

public class DrCrBalance {
    private BigDecimal drBalance;
    private BigDecimal crBalance;

    public DrCrBalance() {
        this.drBalance = BigDecimal.ZERO;
        this.crBalance = BigDecimal.ZERO;
    }

    public BigDecimal getDrBalance() {
        return drBalance;
    }

    public void setDrBalance(BigDecimal drBalance) {
        this.drBalance = drBalance;
    }

    public BigDecimal getCrBalance() {
        return crBalance;
    }

    public void setCrBalance(BigDecimal crBalance) {
        this.crBalance = crBalance;
    }

    public void increaseDrBalance(BigDecimal addedBalance) {
        this.drBalance = this.drBalance.add(addedBalance);
    }
    public void increaseCrBalance(BigDecimal addedBalance) {
        this.crBalance = this.crBalance.add(addedBalance);
    }

    public boolean isValid(){
        return this.crBalance.equals(this.drBalance);
    }

    @Override
    public String toString() {
        return "DrCrBalance{" +
                "drBalance=" + drBalance +
                ", crBalance=" + crBalance +
                '}';
    }
}
