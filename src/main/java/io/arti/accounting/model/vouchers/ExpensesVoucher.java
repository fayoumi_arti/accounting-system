package io.arti.accounting.model.vouchers;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

public class ExpensesVoucher {

    @NotNull
    @Valid
    private VoucherBase voucherBase;

    @NotNull
    private List<ExpensesAccount> accounts;

    public ExpensesVoucher(List<ExpensesAccount> accounts, VoucherBase voucherBase) {
        this.voucherBase = voucherBase;
        this.accounts = accounts;
    }

    public VoucherBase getVoucherBase() {
        return voucherBase;
    }

    public void setVoucherBase(VoucherBase voucherBase) {
        this.voucherBase = voucherBase;
    }

    public List<ExpensesAccount> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<ExpensesAccount> accounts) {
        this.accounts = accounts;
    }
}
