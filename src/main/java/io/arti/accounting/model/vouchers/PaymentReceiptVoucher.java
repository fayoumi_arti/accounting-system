package io.arti.accounting.model.vouchers;

import io.arti.accounting.model.vouchers.enums.VoucherStatus;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.OffsetDateTime;

public class PaymentReceiptVoucher {
    private Integer id;

    private Integer voucherId;
    @NotNull
    @Valid
    private VoucherBase voucherBase;
    @NotNull
    private Integer creditAccountId;

    @NotNull
    private Integer debitAccountId;

    private Integer currencyId;
    private BigDecimal amount;
    private BigDecimal equivalence;
    private float evaluationPrice;

    @NotEmpty
    private String recipientName;
    private String notes;
    private Integer baseId;

    private float creditAccountPreviousBalance;
    private float debitAccountPreviousBalance;
    private VoucherStatus voucherStatus;
    private Integer branchId;
    private Integer debitBalanceId;
    private Integer creditBalanceId;
    private Integer createdBy;
    private Integer updatedBy;
    private OffsetDateTime dateTime;
    private OffsetDateTime updatedAt;
    private OffsetDateTime createdAt;
    public PaymentReceiptVoucher() {
    }

    public PaymentReceiptVoucher(Integer voucherId, VoucherBase voucherBase, Integer creditAccountId, Integer debitAccountId, Integer currencyId, BigDecimal amount, BigDecimal equivalence, float evaluationPrice, String recipientName, String notes, Integer createdBy, Integer updatedBy) {
        this.voucherId = voucherId;
        this.voucherBase = voucherBase;
        this.creditAccountId = creditAccountId;
        this.debitAccountId = debitAccountId;
        this.currencyId = currencyId;
        this.amount = amount;
        this.equivalence = equivalence;
        this.evaluationPrice = evaluationPrice;
        this.recipientName = recipientName;
        this.notes = notes;
        this.createdBy = createdBy;
        this.updatedBy = updatedBy;
    }

    public Integer getBaseId() {
        return baseId;
    }

    public void setBaseId(Integer baseId) {
        this.baseId = baseId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(Integer voucherId) {
        this.voucherId = voucherId;
    }

    public VoucherBase getVoucherBase() {
        return voucherBase;
    }

    public void setVoucherBase(VoucherBase voucherBase) {
        this.voucherBase = voucherBase;
    }

    public Integer getCreditAccountId() {
        return creditAccountId;
    }

    public void setCreditAccountId(Integer creditAccountId) {
        this.creditAccountId = creditAccountId;
    }

    public Integer getDebitAccountId() {
        return debitAccountId;
    }

    public void setDebitAccountId(Integer debitAccountId) {
        this.debitAccountId = debitAccountId;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getEquivalence() {
        return equivalence;
    }

    public void setEquivalence(BigDecimal equivalence) {
        this.equivalence = equivalence;
    }

    public float getEvaluationPrice() {
        return evaluationPrice;
    }

    public void setEvaluationPrice(float evaluationPrice) {
        this.evaluationPrice = evaluationPrice;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public OffsetDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(OffsetDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public OffsetDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(OffsetDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public float getCreditAccountPreviousBalance() {
        return creditAccountPreviousBalance;
    }

    public void setCreditAccountPreviousBalance(float creditAccountPreviousBalance) {
        this.creditAccountPreviousBalance = creditAccountPreviousBalance;
    }

    public float getDebitAccountPreviousBalance() {
        return debitAccountPreviousBalance;
    }

    public void setDebitAccountPreviousBalance(float debitAccountPreviousBalance) {
        this.debitAccountPreviousBalance = debitAccountPreviousBalance;
    }

    public VoucherStatus getVoucherStatus() {
        return voucherStatus;
    }

    public void setVoucherStatus(VoucherStatus voucherStatus) {
        this.voucherStatus = voucherStatus;
    }

    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    public OffsetDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(OffsetDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public Integer getDebitBalanceId() {
        return debitBalanceId;
    }

    public void setDebitBalanceId(Integer debitBalanceId) {
        this.debitBalanceId = debitBalanceId;
    }

    public Integer getCreditBalanceId() {
        return creditBalanceId;
    }

    public void setCreditBalanceId(Integer creditBalanceId) {
        this.creditBalanceId = creditBalanceId;
    }

    @Override
    public String toString() {
        return "PaymentReceiptVoucher{" +
                "id=" + id +
                ", voucherId=" + voucherId +
                ", voucherBase=" + voucherBase +
                ", creditAccountId=" + creditAccountId +
                ", debitAccountId=" + debitAccountId +
                ", currencyId=" + currencyId +
                ", amount=" + amount +
                ", equivalence=" + equivalence +
                ", evaluationPrice=" + evaluationPrice +
                ", recipientName='" + recipientName + '\'' +
                ", notes='" + notes + '\'' +
                ", creditAccountPreviousBalance=" + creditAccountPreviousBalance +
                ", debitAccountPreviousBalance=" + debitAccountPreviousBalance +
                ", voucherStatus=" + voucherStatus +
                ", branchId=" + branchId +
                ", createdBy=" + createdBy +
                ", updatedBy=" + updatedBy +
                ", dateTime=" + dateTime +
                ", updatedAt=" + updatedAt +
                ", createdAt=" + createdAt +
                '}';
    }
}
