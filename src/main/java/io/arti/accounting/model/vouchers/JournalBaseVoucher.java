package io.arti.accounting.model.vouchers;

import io.arti.accounting.model.vouchers.enums.VoucherType;

import java.io.Serializable;
import java.util.List;

public class JournalBaseVoucher implements Serializable {

    private List<JournalVoucher> journalVoucherList;
    private VoucherBase voucherBase;

    private VoucherType voucherType;

    public JournalBaseVoucher() {
    }

    public JournalBaseVoucher(List<JournalVoucher> journalVoucherList, VoucherBase voucherBase, VoucherType voucherType) {
        this.journalVoucherList = journalVoucherList;
        this.voucherBase = voucherBase;
        this.voucherType = voucherType;
    }

    public List<JournalVoucher> getJournalVoucherList() {
        return journalVoucherList;
    }

    public void setJournalVoucherList(List<JournalVoucher> journalVoucherList) {
        this.journalVoucherList = journalVoucherList;
    }

    public VoucherBase getVoucherBase() {
        return voucherBase;
    }

    public void setVoucherBase(VoucherBase voucherBase) {
        this.voucherBase = voucherBase;
    }

    public VoucherType getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(VoucherType voucherType) {
        this.voucherType = voucherType;
    }

    @Override
    public String toString() {
        return "JournalBaseVoucher{" +
                "journalVoucherList=" + journalVoucherList +
                ", voucherBase=" + voucherBase +
                ", voucherType=" + voucherType +
                '}';
    }
}
