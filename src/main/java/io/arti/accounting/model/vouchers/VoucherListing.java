package io.arti.accounting.model.vouchers;

import java.io.Serializable;
import java.util.List;

public class VoucherListing implements Serializable {

    private VoucherBase voucherBase;
    private List<Object> voucher;

    public VoucherListing() {
    }

    public VoucherListing(VoucherBase voucherBase, List<Object> voucher) {
        this.voucherBase = voucherBase;
        this.voucher = voucher;
    }

    public VoucherBase getVoucherBase() {
        return voucherBase;
    }

    public void setVoucherBase(VoucherBase voucherBase) {
        this.voucherBase = voucherBase;
    }

    public List<Object> getVoucher() {
        return voucher;
    }

    public void setVoucher(List<Object> voucher) {
        this.voucher = voucher;
    }

    @Override
    public String toString() {
        return "VoucherListing{" +
                "voucherBase=" + voucherBase +
                ", voucher=" + voucher +
                '}';
    }
}
