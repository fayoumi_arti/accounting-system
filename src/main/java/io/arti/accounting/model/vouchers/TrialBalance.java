package io.arti.accounting.model.vouchers;

import io.arti.accounting.model.general.enums.CRDR;

import java.math.BigDecimal;
import java.util.Objects;

public class TrialBalance {
    private Integer accountId;
    private Integer currencyId;
    private String nameAr;
    private String nameEn;
    private BigDecimal balance;
    private BigDecimal localBalance;
    private BigDecimal openingBalance;
    private BigDecimal debitBalance;
    private BigDecimal creditBalance;
    private double evaluationPrice;
    private CRDR crdr;
    private Integer thirdLevelId;
    private Integer secondLevelId;
    private Integer firstLevelId;

    public TrialBalance() {
    }

    public TrialBalance(Integer currencyId) {
        this.currencyId = currencyId;
    }

    public TrialBalance(TrialBalance tb,String nameEn,String nameAr) {

        this.currencyId = tb.getCurrencyId();
        this.balance = tb.getBalance();
        this.localBalance = tb.getLocalBalance();
        this.openingBalance = tb.getOpeningBalance();
        this.debitBalance = tb.getDebitBalance();
        this.creditBalance = tb.getCreditBalance();
        this.evaluationPrice = tb.getEvaluationPrice();
        this.crdr = tb.getCrdr();
        this.thirdLevelId = tb.getThirdLevelId();
        this.secondLevelId = tb.getSecondLevelId();
        this.firstLevelId = tb.getFirstLevelId();
        this.nameEn = nameEn;
        this.nameAr = nameAr;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    public String getNameAr() {
        return nameAr;
    }

    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getLocalBalance() {
        return localBalance;
    }

    public void setLocalBalance(BigDecimal localBalance) {
        this.localBalance = localBalance;
    }

    public BigDecimal getOpeningBalance() {
        return openingBalance;
    }

    public void setOpeningBalance(BigDecimal openingBalance) {
        this.openingBalance = openingBalance;
    }

    public BigDecimal getDebitBalance() {
        return debitBalance;
    }

    public void setDebitBalance(BigDecimal debitBalance) {
        this.debitBalance = debitBalance;
    }

    public BigDecimal getCreditBalance() {
        return creditBalance;
    }

    public void setCreditBalance(BigDecimal creditBalance) {
        this.creditBalance = creditBalance;
    }

    public double getEvaluationPrice() {
        return evaluationPrice;
    }

    public void setEvaluationPrice(double evaluationPrice) {
        this.evaluationPrice = evaluationPrice;
    }

    public CRDR getCrdr() {
        return crdr;
    }

    public void setCrdr(CRDR crdr) {
        this.crdr = crdr;
    }

    public Integer getThirdLevelId() {
        return thirdLevelId;
    }

    public void setThirdLevelId(Integer thirdLevelId) {
        this.thirdLevelId = thirdLevelId;
    }

    public Integer getSecondLevelId() {
        return secondLevelId;
    }

    public void setSecondLevelId(Integer secondLevelId) {
        this.secondLevelId = secondLevelId;
    }

    public Integer getFirstLevelId() {
        return firstLevelId;
    }

    public void setFirstLevelId(Integer firstLevelId) {
        this.firstLevelId = firstLevelId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TrialBalance that = (TrialBalance) o;
        return currencyId.equals(that.currencyId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(currencyId);
    }
}
