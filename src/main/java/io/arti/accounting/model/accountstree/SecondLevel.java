package io.arti.accounting.model.accountstree;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SecondLevel implements Serializable {

    private Integer id;
    private String arName;
    private String enName;
    private Integer firstLevelId;
    private Float balance;

    private List<ThirdLevel> thirdLevelList = new ArrayList<>();

    public SecondLevel() {
    }

    public SecondLevel(Integer id, String arName, String enName, Integer firstLevelId) {
        this.id = id;
        this.arName = arName;
        this.firstLevelId = firstLevelId;
        this.enName = enName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getArName() {
        return arName;
    }

    public void setArName(String arName) {
        this.arName = arName;
    }

    public String getEnName() {
        return enName;
    }

    public void setEnName(String enName) {
        this.enName = enName;
    }

    public Integer getFirstLevelId() {
        return firstLevelId;
    }

    public void setFirstLevelId(Integer firstLevelId) {
        this.firstLevelId = firstLevelId;
    }

    public Float getBalance() {
        return balance;
    }

    public void setBalance(Float balance) {
        this.balance = balance;
    }

    public List<ThirdLevel> getThirdLevelList() {
        return thirdLevelList;
    }

    public void setThirdLevelList(List<ThirdLevel> thirdLevelList) {
        this.thirdLevelList = thirdLevelList;
    }

    @Override
    public String toString() {
        return "SecondLevel{" +
                "id=" + id +
                ", arName='" + arName + '\'' +
                ", enName='" + enName + '\'' +
                ", firstLevelId=" + firstLevelId +
                ", balance=" + balance +
                ", thirdLevelList=" + thirdLevelList +
                '}';
    }
}
