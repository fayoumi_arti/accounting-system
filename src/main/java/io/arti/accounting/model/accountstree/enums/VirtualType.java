package io.arti.accounting.model.accountstree.enums;

public enum VirtualType {
    NORMAL,
    REMIT_UNPAID,
    COMMISSION_PAID,
    COMMISSION_RECEIVED
}
