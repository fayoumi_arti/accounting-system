package io.arti.accounting.model.accountstree;

import io.arti.accounting.model.accountstree.enums.VirtualType;
import io.swagger.v3.oas.models.security.SecurityScheme;

import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.List;

public class VirtualAccounts implements Serializable {

    private Integer id;
    private Integer accountId;
    private List<String> types;

    private Integer createdBy;
    private Integer updatedBy;
    private OffsetDateTime updatedAt;
    private OffsetDateTime createdAt;

    public VirtualAccounts() {}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public OffsetDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(OffsetDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public OffsetDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(OffsetDateTime createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "VirtualAccounts{" +
                "id=" + id +
                ", accountId=" + accountId +
                ", types=" + types +
                ", createdBy=" + createdBy +
                ", updatedBy=" + updatedBy +
                ", updatedAt=" + updatedAt +
                ", createdAt=" + createdAt +
                '}';
    }
}
