package io.arti.accounting.model.accountstree.enums;


import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum MainAccountType {
    CASH(1,"cash","صناديق"),
    BANKS(2,"Banks","بنوك"),
    SENDERS_BALANCES(3,"Senders Balances","ارصدة مرسلين"),
    TRANSFERS(4,"Transfers","حوالات"),
    OUTCOME(5,"outcome","مصاريف"),
    INCOME(6,"Income","ايرادات");

    private int id;
    private final String nameEn;
    private final String nameAr;

    MainAccountType(int id, String nameEn, String nameAr) {
        this.id = id;
        this.nameEn = nameEn;
        this.nameAr = nameAr;
    }

    public int getId() {
        return id;
    }

    public String getNameEn() {
        return nameEn;
    }

    public String getNameAr() {
        return nameAr;
    }

}
