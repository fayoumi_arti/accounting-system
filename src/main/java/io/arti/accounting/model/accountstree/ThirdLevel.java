package io.arti.accounting.model.accountstree;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

public class ThirdLevel implements Serializable {
    private int id;

    @NotNull(message = "Arabic name can not be null")
    private String arName;
    @NotNull(message = "English name can not be null")
    private String enName;
    @NotNull(message = "Second level value can not be null")
    private Integer secondLevelId;
    @NotNull(message = "Types can not be null")
    private List<Integer> types;

    Float balance;
    private Integer createdBy;
    private Integer updatedBy;
    private OffsetDateTime updatedAt;
    private OffsetDateTime createdAt;

    public ThirdLevel() {
    }
    public ThirdLevel(String arName, String enName, Integer secondLevelId, Integer createdBy, Integer updatedBy,List<Integer> types) {
        this.arName = arName;
        this.enName = enName;
        this.secondLevelId = secondLevelId;
        this.createdBy = createdBy;
        this.updatedBy = updatedBy;
        this.types = types;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getArName() {
        return arName;
    }

    public void setArName(String arName) {
        this.arName = arName;
    }

    public String getEnName() {
        return enName;
    }

    public void setEnName(String enName) {
        this.enName = enName;
    }

    public Integer getSecondLevelId() {
        return secondLevelId;
    }

    public void setSecondLevelId(Integer secondLevelId) {
        this.secondLevelId = secondLevelId;
    }

    public List<Integer> getTypes() {
        return types;
    }

    public void setTypes(List<Integer> types) {
        this.types = types;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public OffsetDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(OffsetDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public OffsetDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(OffsetDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public Float getBalance() {
        return balance;
    }

    public void setBalance(Float balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "ThirdLevel{" +
                "id=" + id +
                ", arName='" + arName + '\'' +
                ", enName='" + enName + '\'' +
                ", secondLevelId=" + secondLevelId +
                ", types=" + types +
                ", balance=" + balance +
                ", createdBy=" + createdBy +
                ", updatedBy=" + updatedBy +
                ", updatedAt=" + updatedAt +
                ", createdAt=" + createdAt +
                '}';
    }
}
