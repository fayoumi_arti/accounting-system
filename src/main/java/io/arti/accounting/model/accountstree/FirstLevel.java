package io.arti.accounting.model.accountstree;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FirstLevel implements Serializable {

    private Integer id;
    private String arName;
    private String enName;

    private Float balance;

    private List<SecondLevel> secondLevelList = new ArrayList<>();

    public FirstLevel() {
    }

    public FirstLevel(Integer id, String arName, String enName) {
        this.id = id;
        this.arName = arName;
        this.enName = enName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getArName() {
        return arName;
    }

    public void setArName(String arName) {
        this.arName = arName;
    }

    public String getEnName() {
        return enName;
    }

    public void setEnName(String enName) {
        this.enName = enName;
    }

    public Float getBalance() {
        return balance;
    }

    public void setBalance(Float balance) {
        this.balance = balance;
    }

    public List<SecondLevel> getSecondLevelList() {
        return secondLevelList;
    }

    public void setSecondLevelList(List<SecondLevel> secondLevelList) {
        this.secondLevelList = secondLevelList;
    }

    @Override
    public String toString() {
        return "FirstLevel{" +
                "id=" + id +
                ", arName='" + arName + '\'' +
                ", enName='" + enName + '\'' +
                ", balance=" + balance +
                ", secondLevelList=" + secondLevelList +
                '}';
    }
}
