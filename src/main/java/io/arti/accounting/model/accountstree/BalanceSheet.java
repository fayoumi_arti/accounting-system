package io.arti.accounting.model.accountstree;

import java.time.OffsetDateTime;
import java.util.List;

public class BalanceSheet {
    private int branchId;
    private String from;
    private String to;
    private List<FirstLevel> firstLevelList;

    public BalanceSheet() {
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public List<FirstLevel> getFirstLevelList() {
        return firstLevelList;
    }

    public void setFirstLevelList(List<FirstLevel> firstLevelList) {
        this.firstLevelList = firstLevelList;
    }

    @Override
    public String toString() {
        return "BalanceSheet{" +
                "branchId=" + branchId +
                ", from=" + from +
                ", to=" + to +
                ", firstLevelList=" + firstLevelList +
                '}';
    }
}
