package io.arti.accounting.model.accountstree;


import io.arti.accounting.model.accountstree.enums.VirtualType;
import io.arti.accounting.model.general.enums.CRDR;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.List;

public class FourthLevel implements Serializable {

    private int id;

    @NotNull(message = "Arabic name can not be null")
    private String arName;
    @NotNull(message = "English name can not be null")
    private String enName;
    @NotNull(message = "Third level ID can not be null")
    private Integer thirdLevelId;
    private Integer defaultCurrencyId;
    private String governorate;
    private String city;
    private String communicationInfo;
    private String address;
    private String firstPhoneNumber;
    private String secondPhoneNumber;
    private String fax;
    private String firstEmail;
    private String secondEmail;
    private String personInCharge;
    private CRDR crdr;
    private Float balance;
    private List<Integer> allowedCurrencies;

    private VirtualType virtualType;
    private Integer createdBy;
    private Integer updatedBy;
    private OffsetDateTime updatedAt;
    private OffsetDateTime createdAt;

    public FourthLevel() {
    }

    public FourthLevel(String arName, String enName, Integer thirdLevelId, Integer createdBy, Integer updatedBy,Integer defaultCurrencyId, CRDR crdr) {
        this.arName = arName;
        this.enName = enName;
        this.thirdLevelId = thirdLevelId;
        this.createdBy = createdBy;
        this.updatedBy = updatedBy;
        this.defaultCurrencyId = defaultCurrencyId;
        this.crdr = crdr;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getArName() {
        return arName;
    }

    public void setArName(String arName) {
        this.arName = arName;
    }

    public String getEnName() {
        return enName;
    }

    public void setEnName(String enName) {
        this.enName = enName;
    }

    public Integer getThirdLevelId() {
        return thirdLevelId;
    }

    public void setThirdLevelId(Integer thirdLevelId) {
        this.thirdLevelId = thirdLevelId;
    }

    public Integer getDefaultCurrencyId() {
        return defaultCurrencyId;
    }

    public void setDefaultCurrencyId(Integer defaultCurrencyId) {
        this.defaultCurrencyId = defaultCurrencyId;
    }

    public String getGovernorate() {
        return governorate;
    }

    public void setGovernorate(String governorate) {
        this.governorate = governorate;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCommunicationInfo() {
        return communicationInfo;
    }

    public void setCommunicationInfo(String communicationInfo) {
        this.communicationInfo = communicationInfo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFirstPhoneNumber() {
        return firstPhoneNumber;
    }

    public void setFirstPhoneNumber(String firstPhoneNumber) {
        this.firstPhoneNumber = firstPhoneNumber;
    }

    public String getSecondPhoneNumber() {
        return secondPhoneNumber;
    }

    public void setSecondPhoneNumber(String secondPhoneNumber) {
        this.secondPhoneNumber = secondPhoneNumber;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getFirstEmail() {
        return firstEmail;
    }

    public void setFirstEmail(String firstEmail) {
        this.firstEmail = firstEmail;
    }

    public String getSecondEmail() {
        return secondEmail;
    }

    public void setSecondEmail(String secondEmail) {
        this.secondEmail = secondEmail;
    }

    public String getPersonInCharge() {
        return personInCharge;
    }

    public void setPersonInCharge(String personInCharge) {
        this.personInCharge = personInCharge;
    }

    public CRDR getCrdr() {
        return crdr;
    }

    public void setCrdr(CRDR crdr) {
        this.crdr = crdr;
    }

    public Float getBalance() {
        return balance;
    }

    public void setBalance(Float balance) {
        this.balance = balance;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public OffsetDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(OffsetDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public OffsetDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(OffsetDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public List<Integer> getAllowedCurrencies() {
        return allowedCurrencies;
    }

    public void setAllowedCurrencies(List<Integer> allowedCurrencies) {
        this.allowedCurrencies = allowedCurrencies;
    }

    public VirtualType getVirtualType() {
        return virtualType;
    }

    public void setVirtualType(VirtualType virtualType) {
        this.virtualType = virtualType;
    }

    @Override
    public String toString() {
        return "FourthLevel{" +
                "id=" + id +
                ", arName='" + arName + '\'' +
                ", enName='" + enName + '\'' +
                ", thirdLevelId=" + thirdLevelId +
                ", defaultCurrencyId=" + defaultCurrencyId +
                ", governorate='" + governorate + '\'' +
                ", city='" + city + '\'' +
                ", communicationInfo='" + communicationInfo + '\'' +
                ", address='" + address + '\'' +
                ", firstPhoneNumber='" + firstPhoneNumber + '\'' +
                ", secondPhoneNumber='" + secondPhoneNumber + '\'' +
                ", fax='" + fax + '\'' +
                ", firstEmail='" + firstEmail + '\'' +
                ", secondEmail='" + secondEmail + '\'' +
                ", personInCharge='" + personInCharge + '\'' +
                ", crdr=" + crdr +
                ", balance=" + balance +
                ", allowedCurrencies=" + allowedCurrencies +
                ", virtualType=" + virtualType +
                ", createdBy=" + createdBy +
                ", updatedBy=" + updatedBy +
                ", updatedAt=" + updatedAt +
                ", createdAt=" + createdAt +
                '}';
    }
}
