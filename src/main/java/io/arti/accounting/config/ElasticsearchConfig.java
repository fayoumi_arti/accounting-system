package io.arti.accounting.config;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Configuration
@ConfigurationProperties("elastic")
@Service
public class ElasticsearchConfig {

    private static class Connection {

        String host;
        Integer port;

        public String getHost() {
            return host;
        }

        public void setHost(String host) {
            this.host = host;
        }

        public Integer getPort() {
            return port;
        }

        public void setPort(Integer port) {
            this.port = port;
        }
    }

    public Connection[] nodes;

    public boolean xpackEnabled;

    public String userName;
    public String password;
    private int timeout;

    public Connection[] getNodes() {
        return nodes;
    }

    public void setNodes(Connection[] nodes) {
        this.nodes = nodes;
    }

    public boolean isXpackEnabled() {
        return xpackEnabled;
    }

    public void setXpackEnabled(boolean xpackEnabled) {
        this.xpackEnabled = xpackEnabled;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    @Bean
    public RestHighLevelClient client() {
        final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();

        if (xpackEnabled) {
            credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(userName, password));
        }

        RestClientBuilder builder = RestClient
                .builder(Arrays.stream(nodes).map(httpHost -> new HttpHost(httpHost.getHost(), httpHost.getPort())).toArray(HttpHost[]::new))
                .setHttpClientConfigCallback(httpClientBuilder -> httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider));
        builder.setRequestConfigCallback(requestConfigureBuilder -> requestConfigureBuilder
                .setConnectTimeout(timeout * 1000)
                .setSocketTimeout(timeout * 1000)
                .setConnectionRequestTimeout(0)
        );

        return new RestHighLevelClient(builder);
    }
}
