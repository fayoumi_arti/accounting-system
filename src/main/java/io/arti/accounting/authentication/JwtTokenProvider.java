package io.arti.accounting.authentication;


import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.function.Function;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

@Component
public class JwtTokenProvider {

    @Value("${jwt.secret}")
    private String secret;
    @Value("${jwt.jwtExpiration}")
    private long jwtTokenValidity;
    private static final String AUTHORITIES_KEY = "scopes";

    public String getUsernameFromToken(String token) {
        return getClaimFromToken(token, Claims::getSubject);
    }
    public List<GrantedAuthority> getAuthoritiesFromToken(String token) {
        final Claims claims = getAllClaimsFromToken(token);

        return AuthorityUtils.commaSeparatedStringToAuthorityList(claims.get("scopes", String.class));
    }

    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }
    public Integer getBranchIdFromToken(String token) {
        final Claims claims = getAllClaimsFromToken(token);
        return claims.get("branchId", Integer.class);
    }

    public Boolean validateToken(String token) {
        final String username = getUsernameFromToken(token);
        return isNotBlank(username) && !isTokenExpired(token);
    }

    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
    }

    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

}
