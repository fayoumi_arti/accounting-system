package io.arti.accounting.dao.transfers;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.arti.accounting.dao.DaoOperations;
import io.arti.accounting.dao.accountstree.FourthLevelDao;
import io.arti.accounting.dao.mapper.transfers.TransferPaymentMapper;
import io.arti.accounting.model.transfers.TransferPayment;
import io.arti.accounting.model.vouchers.JournalBaseVoucher;
import io.arti.accounting.service.response.error.exception.client.BadRequestException;
import io.arti.accounting.service.response.error.exception.server.ServerErrorException;
import io.arti.accounting.service.transfers.TransferPaymentService;
import io.arti.accounting.service.vouchers.JournalVoucherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.*;

@Service
public class TransferPaymentDao implements DaoOperations<TransferPayment> {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    FourthLevelDao fourthLevelDao;

    @Autowired
    JournalVoucherService journalVoucherService;

    @Autowired
    TransferPaymentService transferPaymentService;

    private static final String CORE_COUNT = "select count(*) from transfer_payment";
    private static final String CORE_SELECT = "select id, date_time, sender_account_id, payer_account_id, destination, currency_id, amount, commission_ratio, received_commission, paid_commission, beneficiary_name, sender_name, phone, notes, country, created_by, status, payment_date, recipient_name, branch_id, paid_commission_ratio, voucher_base_id, attachments, updated_by, updated_at, created_at from transfer_payment";
    private static final String INSERT_ENTITY = "insert into transfer_payment (date_time, sender_account_id, payer_account_id, destination, currency_id, amount, commission_ratio, received_commission, paid_commission, beneficiary_name, sender_name, phone, notes, country, status, payment_date, recipient_name,  branch_id, paid_commission_ratio,  attachments, created_by) values (?, ?, ?,  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?::json, ?)";
    private static final String INSERT_BOX_ENTITY = "insert into transfer_payment (date_time, sender_account_id, destination, currency_id, amount, commission_ratio, received_commission, beneficiary_name, sender_name, phone, notes, country, status, branch_id,  attachments, created_by) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?::json, ?)";
    private static final String UPDATE_ENTITY = "update transfer_payment set date_time=?, sender_account_id=?, payer_account_id=?, destination=?, currency_id=?, amount=?, " +
            "commission_ratio=?, received_commission=?, paid_commission=?, beneficiary_name=?, sender_name=?, phone=?, notes=?, country=?, status=?, payment_date=?, recipient_name=?,  branch_id=?, paid_commission_ratio=?, voucher_base_id=?, attachments=to_json(?::json), updated_by=?  where id=?";
    private static final String DELETE_ENTITY = "update transfer_payment set status=? where id=?";

    public List<TransferPayment> getEntitiesByDate(int size, int offset, String dateTime, Integer accountId, String status) {
        String statusIn = transferPaymentService.processSqlInClause(status);

        String sql = CORE_SELECT + " where date_time::date = ?::timestamp and sender_account_id=? and status IN (" + statusIn + ") order by date_time desc limit ? offset ?";
        return jdbcTemplate.query(sql, new TransferPaymentMapper(), dateTime, accountId, size, offset);
    }

    public List<TransferPayment> getUnpaidEntitiesByAccountAndCurrency(Integer senderAccountId, Float amountMax, Float amountMin, Integer currencyId, String beneficiaryName, String senderName, String status, String from, String to, int size, int offset) {

        Map<String, Object> transferFields = new HashMap<>();
        List<String> query = new ArrayList<>();

        transferFields.put("sender_account_id", senderAccountId);
        transferFields.put("currency_id", currencyId);
        transferFields.put("beneficiary_name", beneficiaryName);
        transferFields.put("sender_name", senderName);

        for (Map.Entry<String, Object> set : transferFields.entrySet()) {
            if (set.getValue() != null) {
                if (set.getValue() instanceof String) {
                    query.add(set.getKey() + " ILIKE '%" + set.getValue() + "%'");
                } else {
                    query.add(String.format("%s=%s", set.getKey(), set.getValue()));
                }
            }
        }

        if (status != null) {
            query.add(String.format("status IN (%s)", transferPaymentService.processSqlInClause(status)));
        }

        if (from != null && to != null) {
            query.add(String.format("date_time between '%s' and '%s'", from, to));
        }

        if (amountMax > 0) {
            query.add(String.format("%s<=%s", "amount", amountMax));
        }

        if (amountMin > 0) {
            query.add(String.format("%s>=%s", "amount", amountMin));
        }

        String sqlQuery = String.join(" and ", query);
        sqlQuery = " where " + sqlQuery;

        String sql = CORE_SELECT + sqlQuery + " order by date_time desc limit ? offset ?";

        return jdbcTemplate.query(sql, new TransferPaymentMapper(), size, offset);
    }

    @Override
    public List<TransferPayment> getEntities(int size, int offset) {
        String sql = CORE_SELECT + " order by id desc limit ? offset ?";
        return jdbcTemplate.query(sql, new TransferPaymentMapper(), size, offset);
    }

    @Override
    public List<TransferPayment> getEntities(int size, int offset, String name) {
        return null;
    }

    @Override
    public boolean isPresent(Integer id) {
        String sql = CORE_COUNT + " where id = ?";
        Integer result = jdbcTemplate.queryForObject(sql, Integer.class, id);

        return result != null && result > 0;
    }

    @Override
    public TransferPayment getById(Integer id) {
        String sql = CORE_SELECT + " where id=?";
        return jdbcTemplate.queryForObject(sql, new TransferPaymentMapper(), new Object[]{id});
    }

    @Override
    public TransferPayment insertEntity(TransferPayment transferPayment) {
        return null;
    }

    @Transactional
    public TransferPayment insertEntity(TransferPayment transferPayment, String token) throws BadRequestException {

        List<Integer> baseIds = new ArrayList<>();

        for (JournalBaseVoucher journalBaseVoucher: transferPayment.getJournalBaseVouchers()) {
            JournalBaseVoucher insertedJournalBaseVoucher = journalVoucherService.insertEntity(journalBaseVoucher, token);
            baseIds.add(insertedJournalBaseVoucher.getVoucherBase().getId());
        }

        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(INSERT_ENTITY, Statement.RETURN_GENERATED_KEYS);

            ps.setObject(1, transferPayment.getDateTime());
            ps.setInt(2, transferPayment.getSenderAccountId());
            ps.setInt(3, transferPayment.getPayerAccountId());
            ps.setObject(4, transferPayment.getDestination().toString());
            ps.setInt(5, transferPayment.getCurrencyId());
            ps.setFloat(6, transferPayment.getAmount());
            ps.setFloat(7, transferPayment.getCommissionRatio());
            ps.setFloat(8, transferPayment.getReceivedCommission());
            ps.setFloat(9, transferPayment.getPaidCommission());
            ps.setString(10, transferPayment.getBeneficiaryName());
            ps.setString(11, transferPayment.getSenderName());
            ps.setString(12, transferPayment.getPhone());
            ps.setString(13, transferPayment.getNotes());
            ps.setString(14, transferPayment.getCountry());
            ps.setObject(15, transferPayment.getStatus().toString());
            ps.setObject(16, transferPayment.getPaymentDate());
            ps.setString(17, transferPayment.getRecipientName());
            ps.setInt(18, transferPayment.getBranchId());
            ps.setFloat(19, transferPayment.getPaidCommissionRatio());
            try {
                ps.setString(20, defaultMapper.writeValueAsString(transferPayment.getAttachments()));
            } catch (JsonProcessingException e) {
                throw new ServerErrorException("Failed to create transfer payment due to error while parsing Attachments array");
            }
            ps.setInt(21, transferPayment.getCreatedBy());

            return ps;
        }, keyHolder);

        return getById(Integer.parseInt(keyHolder.getKeys().get("id").toString()));
    }

    @Transactional
    public TransferPayment insertBoxEntity(TransferPayment transferPayment, String token) throws BadRequestException {
        List<Integer> baseIds = new ArrayList<>();

        for (JournalBaseVoucher journalBaseVoucher: transferPayment.getJournalBaseVouchers()) {
            JournalBaseVoucher insertedJournalBaseVoucher = journalVoucherService.insertEntity(journalBaseVoucher, token);
            baseIds.add(insertedJournalBaseVoucher.getVoucherBase().getId());

        }

        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(INSERT_BOX_ENTITY, Statement.RETURN_GENERATED_KEYS);

            ps.setObject(1, transferPayment.getDateTime());
            ps.setInt(2, transferPayment.getSenderAccountId());
            ps.setObject(3, transferPayment.getDestination().toString());
            ps.setInt(4, transferPayment.getCurrencyId());
            ps.setFloat(5, transferPayment.getAmount());
            ps.setFloat(6, transferPayment.getCommissionRatio());
            ps.setFloat(7, transferPayment.getReceivedCommission());
            ps.setString(8, transferPayment.getBeneficiaryName());
            ps.setString(9, transferPayment.getSenderName());
            ps.setString(10, transferPayment.getPhone());
            ps.setString(11, transferPayment.getNotes());
            ps.setString(12, transferPayment.getCountry());
            ps.setString(13, transferPayment.getStatus().toString());
            ps.setInt(14, transferPayment.getBranchId());
            try {
                ps.setString(15, defaultMapper.writeValueAsString(transferPayment.getAttachments()));
            } catch (JsonProcessingException e) {
                throw new ServerErrorException("Failed to create transfer payment due to error while parsing Attachments array");
            }
            ps.setInt(16, transferPayment.getCreatedBy());

            return ps;
        }, keyHolder);

        return getById(Integer.parseInt(keyHolder.getKeys().get("id").toString()));
    }

    @Transactional
    @Override
    public void updateEntity(TransferPayment transferPayment) {

//        List<Integer> baseIds = new ArrayList<>();
//
//        for (JournalBaseVoucher journalBaseVoucher: transferPayment.getJournalBaseVouchers()) {
//            JournalBaseVoucher insertedJournalBaseVoucher = journalVoucherService.insertEntity(journalBaseVoucher);
//            baseIds.add(insertedJournalBaseVoucher.getVoucherBase().getId());
//        }

        try {
            jdbcTemplate.update(UPDATE_ENTITY,
                    transferPayment.getDateTime(),
                    transferPayment.getSenderAccountId(),
                    transferPayment.getPayerAccountId(),
                    transferPayment.getDestination().toString(),
                    transferPayment.getCurrencyId(),
                    transferPayment.getAmount(),
                    transferPayment.getCommissionRatio(),
                    transferPayment.getReceivedCommission(),
                    transferPayment.getPaidCommission(),
                    transferPayment.getBeneficiaryName(),
                    transferPayment.getSenderName(),
                    transferPayment.getPhone(),
                    transferPayment.getNotes(),
                    transferPayment.getCountry(),
                    transferPayment.getStatus().toString(),
                    transferPayment.getPaymentDate(),
                    transferPayment.getRecipientName(),
                    transferPayment.getBranchId(),
                    transferPayment.getPaidCommissionRatio(),
                    //TODO: Change
                    1,
                    defaultMapper.writeValueAsString(transferPayment.getAttachments()),
                    transferPayment.getUpdatedBy(),
                    transferPayment.getId()
            );
        } catch (JsonProcessingException e) {
            throw new ServerErrorException(e.getMessage(), e);
        }
    }

    @Override
    public void deleteEntity(Integer id) {
        jdbcTemplate.update(DELETE_ENTITY, "DELETED", id);
    }
}
