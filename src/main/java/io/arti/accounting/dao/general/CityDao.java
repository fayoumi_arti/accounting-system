package io.arti.accounting.dao.general;

import io.arti.accounting.dao.DaoOperations;
import io.arti.accounting.dao.mapper.general.CityMapper;
import io.arti.accounting.model.general.City;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;
import java.util.Objects;

@Repository
public class CityDao implements DaoOperations<City> {

    private static final String CORE_SELECT = "select id, country_id, name_en, name_ar, created_by, updated_by, created_at, updated_at from city";
    private static final String CORE_COUNT = "select count(*) from city";
    private static final String INSERT_ENTITY = "insert into city (country_id ,name_en, name_ar, created_by) values (?, ?, ?, ?)";
    private static final String UPDATE_ENTITY = "update city set country_id=?, name_en=?, name_ar=?, updated_by=? where id=?";
    private static final String DELETE_ENTITY = "delete from city where id=?";

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public List<City> getEntities(int size, int offset) {
        String sql = CORE_SELECT + " order by id desc limit ? offset ?";
        return jdbcTemplate.query(sql, new CityMapper(), size, offset);
    }

    @Override
    public List<City> getEntities(int size, int offset, String name) {
        return null;
    }

    @Override
    public boolean isPresent(Integer id) {
        String sql = CORE_COUNT + " where id = ?";
        Integer result = jdbcTemplate.queryForObject(sql, Integer.class, id);
        return result != null && result > 0;
    }

    @Override
    public City getById(Integer id) {
        String sql = CORE_SELECT + " where id=?";
        return jdbcTemplate.queryForObject(sql, new CityMapper(), id);
    }

    @Override
    public City insertEntity(City city) {
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(INSERT_ENTITY, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, city.getCountryId());
            ps.setString(2, city.getNameEn());
            ps.setString(3, city.getNameAr());
            ps.setInt(4, city.getCreatedBy());
            return ps;
        }, keyHolder);

        return getById(Integer.parseInt(Objects.requireNonNull(keyHolder.getKeys()).get("id").toString()));
    }

    @Override
    public void updateEntity(City city) {
        jdbcTemplate.update(UPDATE_ENTITY,
                city.getCountryId(),
                city.getNameEn(),
                city.getNameAr(),
                city.getUpdatedBy(),
                city.getId()
        );
    }

    @Override
    public void deleteEntity(Integer id) {
        jdbcTemplate.update(DELETE_ENTITY,id);
    }
}
