package io.arti.accounting.dao.general;

import io.arti.accounting.dao.DaoOperations;
import io.arti.accounting.dao.mapper.general.ProfileMapper;
import io.arti.accounting.model.general.Profile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Service;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

@Service
public class ProfileDao implements DaoOperations<Profile> {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private static final String CORE_SELECT = "select id, currency_id, name_ar, name_en, country, governorate, address_ar, address_en," +
            " logo, phone, fax, postal_code, postal_box, email, created_by, updated_by, created_at, updated_at from profile";
    private static final String CORE_COUNT = "select count(*) from profile";
    private static final String INSERT_ENTITY = "insert into profile (currency_id, name_ar, name_en, country, governorate, address_ar, address_en, " +
            "logo, phone, fax, postal_code, postal_box, email, created_by) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String UPDATE_ENTITY = "update profile set currency_id=?, name_ar=?, name_en=?, country=?, governorate=?, address_ar=?, address_en=?, " +
            "logo=?, phone=?, fax=?, postal_code=?, postal_box=?, email=?, updated_by=? where id=?";
    private static final String DELETE_ENTITY = "delete from profile where id=?";

    @Override
    public List<Profile> getEntities(int size, int offset) {
        String sql = CORE_SELECT + " order by id desc limit ? offset ?";
        return jdbcTemplate.query(sql, new ProfileMapper(), size, offset);
    }

    @Override
    public List<Profile> getEntities(int size, int offset, String name) {
        return null;
    }

    @Override
    public boolean isPresent(Integer id) {
        String sql = CORE_COUNT + " where id = ?";
        Integer result = jdbcTemplate.queryForObject(sql, Integer.class, id);

        return result != null && result > 0;
    }

    public boolean isEmptyProfile() {
        Integer result = jdbcTemplate.queryForObject(CORE_COUNT, Integer.class);
        return !(result != null && result > 0);
    }

    @Override
    public Profile getById(Integer id) {
        String sql = CORE_SELECT + " where id=?";
        return jdbcTemplate.queryForObject(sql, new ProfileMapper(), new Object[]{id});
    }

    @Override
    public Profile insertEntity(Profile profile) {
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(INSERT_ENTITY, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, profile.getCurrencyId());
            ps.setString(2, profile.getNameAr());
            ps.setString(3, profile.getNameEn());
            ps.setString(4, profile.getCountry());
            ps.setString(5, profile.getGovernorate());
            ps.setString(6, profile.getAddressAr());
            ps.setString(7, profile.getAddressEn());
            ps.setString(8, profile.getLogo());
            ps.setString(9, profile.getPhone());
            ps.setString(10, profile.getFax());
            ps.setString(11, profile.getPostalCode());
            ps.setString(12, profile.getPostalBox());
            ps.setString(13, profile.getEmail());
            ps.setInt(14, profile.getCreatedBy());
            return ps;
        }, keyHolder);

        return getById(Integer.parseInt(keyHolder.getKeys().get("id").toString()));
    }

    @Override
    public void updateEntity(Profile profile) {

        jdbcTemplate.update(UPDATE_ENTITY,
                profile.getCurrencyId(),
                profile.getNameAr(),
                profile.getNameEn(),
                profile.getCountry(),
                profile.getGovernorate(),
                profile.getAddressAr(),
                profile.getAddressEn(),
                profile.getLogo(),
                profile.getPhone(),
                profile.getFax(),
                profile.getPostalCode(),
                profile.getPostalBox(),
                profile.getEmail(),
                profile.getUpdatedBy(),
                profile.getId()
        );
    }

    @Override
    public void deleteEntity(Integer id) {
        jdbcTemplate.update(DELETE_ENTITY, id);
    }
}
