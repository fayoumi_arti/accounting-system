package io.arti.accounting.dao.general;

import io.arti.accounting.dao.DaoOperations;
import io.arti.accounting.dao.mapper.general.BranchesMapper;
import io.arti.accounting.dao.mapper.general.CurrenciesMapper;
import io.arti.accounting.model.general.Currencies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Service;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

@Service
public class CurrenciesDao implements DaoOperations<Currencies> {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private static final String CORE_SELECT = "select id, reference_id, ar_name, en_name, evaluation_price, iso, created_by, updated_by, created_at, updated_at from currency";
    private static final String CORE_COUNT = "select count(*) from currency";
    private static final String INSERT_ENTITY = "insert into currency (reference_id, ar_name, en_name, evaluation_price, iso, created_by) values (?, ?, ?, ?, ?, ?)";
    private static final String UPDATE_ENTITY = "update currency set reference_id=?, ar_name=?,  en_name=?, evaluation_price=?, iso=?, updated_by=? where id=?";
    private static final String DELETE_ENTITY = "delete from currency where id=?";

    @Override
    public List<Currencies> getEntities(int size, int offset) {
        String sql = CORE_SELECT + " order by id desc limit ? offset ?";
        return jdbcTemplate.query(sql, new CurrenciesMapper(), size, offset);
    }

    @Override
    public List<Currencies> getEntities(int size, int offset, String name) {
        return null;
    }

    @Override
    public boolean isPresent(Integer id) {
        String sql = CORE_COUNT + " where id = ?";
        Integer result = jdbcTemplate.queryForObject(sql, Integer.class, id);

        return result != null && result > 0;
    }

    @Override
    public Currencies getById(Integer id) {
        String sql = CORE_SELECT + " where id=?";
        return jdbcTemplate.queryForObject(sql, new CurrenciesMapper(), new Object[]{id});
    }

    @Override
    public Currencies insertEntity(Currencies currencies) {
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(INSERT_ENTITY, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, currencies.getReferenceId());
            ps.setString(2, currencies.getArName());
            ps.setString(3, currencies.getEnName());
            ps.setDouble(4, currencies.getEvaluationPrice());
            ps.setString(5, currencies.getIso());
            ps.setInt(6, currencies.getCreatedBy());
            return ps;
        }, keyHolder);

        return getById(Integer.parseInt(keyHolder.getKeys().get("id").toString()));
    }

    public Integer getCount() {
        String sql = CORE_COUNT;
        return jdbcTemplate.queryForObject(sql, Integer.class);
    }

    @Override
    public void updateEntity(Currencies currencies) {
        jdbcTemplate.update(UPDATE_ENTITY,
                currencies.getReferenceId(),
                currencies.getArName(),
                currencies.getEnName(),
                currencies.getEvaluationPrice(),
                currencies.getIso(),
                currencies.getUpdatedBy(),
                currencies.getId()
        );
    }

    @Override
    public void deleteEntity(Integer id) {
        jdbcTemplate.update(DELETE_ENTITY, id);
    }
}
