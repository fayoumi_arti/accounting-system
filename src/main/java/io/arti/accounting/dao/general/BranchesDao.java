package io.arti.accounting.dao.general;

import io.arti.accounting.dao.DaoOperations;
import io.arti.accounting.dao.mapper.general.BranchesMapper;
import io.arti.accounting.model.general.Branches;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Service;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

@Service
public class BranchesDao implements DaoOperations<Branches> {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private static final String CORE_SELECT = "select id, ar_name, en_name,  created_by, updated_by, created_at, updated_at from branch";
    private static final String CORE_COUNT = "select count(*) from branch";
    private static final String INSERT_ENTITY = "insert into branch (ar_name, en_name, created_by) values (?, ?, ?)";
    private static final String UPDATE_ENTITY = "update branch set ar_name=?,  en_name=?, updated_by=? where id=?";
    private static final String DELETE_ENTITY = "delete from branch where id=?";

    @Override
    public List<Branches> getEntities(int size, int offset) {
        String sql = CORE_SELECT + " order by id desc limit ? offset ?";
        return jdbcTemplate.query(sql, new BranchesMapper(), size, offset);
    }

    @Override
    public List<Branches> getEntities(int size, int offset, String name) {
        return null;
    }

    @Override
    public boolean isPresent(Integer id) {
        String sql = CORE_COUNT + " where id = ?";
        Integer result = jdbcTemplate.queryForObject(sql, Integer.class, id);

        return result != null && result > 0;
    }

    @Override
    public Branches getById(Integer id) {
        String sql = CORE_SELECT + " where id=?";
        return jdbcTemplate.queryForObject(sql, new BranchesMapper(), new Object[]{id});
    }

    @Override
    public Branches insertEntity(Branches branches) {
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(INSERT_ENTITY, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, branches.getArName());
            ps.setString(2, branches.getEnName());
            ps.setInt(3, branches.getCreatedBy());
            return ps;
        }, keyHolder);

        return getById(Integer.parseInt(keyHolder.getKeys().get("id").toString()));
    }

    public Integer getCount() {
        String sql = CORE_COUNT;
        return jdbcTemplate.queryForObject(sql, Integer.class);
    }
    @Override
    public void updateEntity(Branches branches) {
        jdbcTemplate.update(UPDATE_ENTITY,
                branches.getArName(),
                branches.getEnName(),
                branches.getUpdatedBy(),
                branches.getId()
        );
    }

    @Override
    public void deleteEntity(Integer id) {
        jdbcTemplate.update(DELETE_ENTITY, id);
    }
}
