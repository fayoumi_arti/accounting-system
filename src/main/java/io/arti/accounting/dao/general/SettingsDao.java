package io.arti.accounting.dao.general;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.arti.accounting.dao.DaoOperations;
import io.arti.accounting.dao.mapper.general.SettingsMapper;
import io.arti.accounting.model.general.Settings;
import io.arti.accounting.service.response.error.exception.server.ServerErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Service;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

@Service
public class SettingsDao implements DaoOperations<Settings> {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private static final String CORE_SELECT = "select id, key, value, created_by, updated_by, created_at, updated_at from settings";
    private static final String CORE_COUNT = "select count(*) from settings";
    private static final String INSERT_ENTITY = "insert into settings (key, value, created_by) values (?, ?::json, ?)";
    private static final String UPDATE_ENTITY = "update settings set key=?, value=?::json, updated_by=? where id=?";
    private static final String DELETE_ENTITY = "delete from settings where id=?";


    @Override
    public List<Settings> getEntities(int size, int offset) {
        String sql = CORE_SELECT + " order by id desc limit ? offset ?";
        return jdbcTemplate.query(sql, new SettingsMapper(), size, offset);
    }

    @Override
    public List<Settings> getEntities(int size, int offset, String name) {
        return null;
    }

    @Override
    public boolean isPresent(Integer id) {
        String sql = CORE_COUNT + " where id = ?";
        Integer result = jdbcTemplate.queryForObject(sql, Integer.class, id);

        return result != null && result > 0;
    }

    public boolean isPresentKey(String key) {
        String sql = CORE_COUNT + " where key = ?";
        Integer result = jdbcTemplate.queryForObject(sql, Integer.class, key);

        return result != null && result > 0;
    }

    public Settings getByKey(String key) {
        String sql = CORE_SELECT + " where key = ?";
        return  jdbcTemplate.queryForObject(sql, new SettingsMapper(), new Object[]{key});
    }

    @Override
    public Settings getById(Integer id) {
        String sql = CORE_SELECT + " where id=?";
        return jdbcTemplate.queryForObject(sql, new SettingsMapper(), new Object[]{id});
    }

    @Override
    public Settings insertEntity(Settings settings) {
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(INSERT_ENTITY, Statement.RETURN_GENERATED_KEYS);

            ps.setObject(1, settings.getKey());
            try {
                ps.setString(2, defaultMapper.writeValueAsString(settings.getValue()));
            } catch (JsonProcessingException e) {
                throw new ServerErrorException("Failed to create settings entity due to error while parsing settings array");
            }
            ps.setInt(3, settings.getCreatedBy());

            return ps;
        }, keyHolder);

        return getById(Integer.parseInt(keyHolder.getKeys().get("id").toString()));
    }

    @Override
    public void updateEntity(Settings settings) {


        try {
            jdbcTemplate.update(UPDATE_ENTITY,
                    settings.getKey(),
                    defaultMapper.writeValueAsString(settings.getValue()),
                    settings.getUpdatedBy(),
                    settings.getId()
            );
        } catch (JsonProcessingException e) {
            throw new ServerErrorException(e.getMessage(), e);
        }
    }

    @Override
    public void deleteEntity(Integer id) {
        jdbcTemplate.update(DELETE_ENTITY, id);
    }
}
