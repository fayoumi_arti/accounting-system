package io.arti.accounting.dao.general;

import io.arti.accounting.dao.DaoOperations;
import io.arti.accounting.dao.mapper.general.CountryMapper;
import io.arti.accounting.model.general.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;
import java.util.Objects;

@Repository
public class CountryDao implements DaoOperations<Country> {

    private static final String CORE_SELECT = "select id, name_en, name_ar, iso_3, created_by, updated_by, created_at, updated_at  from country";
    private static final String CORE_COUNT = "select count(*) from country";
    private static final String INSERT_ENTITY = "insert into country (name_en, name_ar, iso_3, created_by) values (?, ?, ?, ?)";
    private static final String UPDATE_ENTITY = "update country set name_en=?, name_ar=?, iso_3=?, updated_by=? where id=?";
    private static final String DELETE_ENTITY = "delete from country where id=?";

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public List<Country> getEntities(int size, int offset) {
        String sql = CORE_SELECT + " order by id desc limit ? offset ?";
        return jdbcTemplate.query(sql, new CountryMapper(), size, offset);
    }

    @Override
    public List<Country> getEntities(int size, int offset, String name) {
        return null;
    }

    @Override
    public boolean isPresent(Integer id) {
        String sql = CORE_COUNT + " where id = ?";
        Integer result = jdbcTemplate.queryForObject(sql, Integer.class, id);
        return result != null && result > 0;
    }

    @Override
    public Country getById(Integer id) {
        String sql = CORE_SELECT + " where id=?";
        return jdbcTemplate.queryForObject(sql, new CountryMapper(), id);
    }

    @Override
    public Country insertEntity(Country country) {
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(INSERT_ENTITY, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, country.getNameEn());
            ps.setString(2, country.getNameAr());
            ps.setString(3, country.getIso3());
            ps.setInt(4, country.getCreatedBy());
            return ps;
        }, keyHolder);

        return getById(Integer.parseInt(Objects.requireNonNull(keyHolder.getKeys()).get("id").toString()));
    }

    @Override
    public void updateEntity(Country country) {
        jdbcTemplate.update(UPDATE_ENTITY,
                country.getNameEn(),
                country.getNameAr(),
                country.getIso3(),
                country.getUpdatedBy(),
                country.getId()
                );
    }

    @Override
    public void deleteEntity(Integer id) {
        jdbcTemplate.update(DELETE_ENTITY, id);
    }

    public boolean isIsoPresent(String iso3) {
        String sql = CORE_COUNT + " where iso_3 = ?";
        Integer result = jdbcTemplate.queryForObject(sql, Integer.class, iso3);
        return result != null && result > 0;
    }
}
