package io.arti.accounting.dao;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

public interface DaoOperations<T> {

    ObjectMapper defaultMapper = new ObjectMapper();
    List<T> getEntities(int size, int offset);

    List<T> getEntities(int size, int offset, String name);

    boolean isPresent(Integer id);

    T getById(Integer id);

    T insertEntity(T t);

    void updateEntity(T t);

    void deleteEntity(Integer id);
}

