package io.arti.accounting.dao.vouchers;

import io.arti.accounting.dao.DaoOperations;
import io.arti.accounting.dao.accountstree.AccountBalanceDao;
import io.arti.accounting.dao.accountstree.FourthLevelDao;
import io.arti.accounting.dao.mapper.vouchers.ExpensesAccountMapper;
import io.arti.accounting.model.accountstree.AccountBalance;
import io.arti.accounting.model.accountstree.FourthLevel;
import io.arti.accounting.model.general.enums.CRDR;
import io.arti.accounting.model.vouchers.*;
import io.arti.accounting.model.vouchers.enums.VoucherStatus;
import io.arti.accounting.model.vouchers.enums.VoucherType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class ExpensesVoucherDao implements DaoOperations<ExpensesAccount> {
    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    VoucherBaseDao voucherBaseDao;

    @Autowired
    FourthLevelDao fourthLevelDao;
    @Autowired
    AccountBalanceDao accountBalanceDao;
    private static final String CORE_SELECT = "select id, voucher_id, account_id, currency_id, amount, evaluation_price, equivalence, cr_dr, notes, created_by," +
            " updated_by, created_at, updated_at,account_previous_balance,voucher_status,branch_id,date_time,balance_id from expenses_voucher";
    private static final String CORE_COUNT = "select count(*) from expenses_voucher";
    private static final String INSERT_ENTITY = "insert into expenses_voucher (account_id, currency_id, amount, equivalence, cr_dr," +
            " notes, created_by, voucher_id, evaluation_price,account_previous_balance,voucher_status,branch_id,date_time,balance_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)";
    private static final String UPDATE_ENTITY = "update expenses_voucher set account_id=?, currency_id=?, amount=?," +
            " equivalence=?, cr_dr=?, note=?, updated_by=?, evaluation_price=?, evaluation_price,voucher_status=?,branch_id=?,date_time =?, balance_id=? where id=?";
    private static final String DELETE_ENTITY = "delete from expenses_voucher where id=?";
    private static final String DELETE_BY_VOUCHER_ID = "delete from expenses_voucher where voucher_id=?";

    private static final String DELETE_IDS = "delete from expenses_voucher where id in (%s) ";

    private static final String MAX_VOUCHER_ID = "select coalesce(max(voucher_id) + 1, 1) from expenses_voucher";

    @Override
    public List<ExpensesAccount> getEntities(int size, int offset) {
        String sql = CORE_SELECT + " order by id desc limit ? offset ?";
        return jdbcTemplate.query(sql, new ExpensesAccountMapper(), size, offset);
    }

    public List<ExpensesAccount> getEntities(Integer voucherId) {
        String sql = CORE_SELECT + " where voucher_id = ?";
        return jdbcTemplate.query(sql, new ExpensesAccountMapper(), voucherId);
    }

    @Override
    public List<ExpensesAccount> getEntities(int size, int offset, String name) {
        return null;
    }

    @Override
    public boolean isPresent(Integer id) {
        String sql = CORE_COUNT + " where id = ?";
        Integer result = jdbcTemplate.queryForObject(sql, Integer.class, id);

        return result != null && result > 0;
    }

    public boolean isPresentByVoucherId(Integer id) {
        String sql = CORE_COUNT + " where voucher_id = ?";
        Integer result = jdbcTemplate.queryForObject(sql, Integer.class, id);

        return result != null && result > 0;
    }
    public Integer getVoucherID() {
        return jdbcTemplate.queryForObject(MAX_VOUCHER_ID, Integer.class);
    }

    @Override
    public ExpensesAccount getById(Integer id) {
        String sql = CORE_SELECT + " where id=?";
        return jdbcTemplate.queryForObject(sql, new ExpensesAccountMapper(), new Object[]{id});
    }
    public VoucherListing getByVoucherId(Integer id) {
        String sql = CORE_SELECT + " where voucher_id = ?";

        VoucherListing voucherListing = new VoucherListing();
        VoucherBase voucherBase = voucherBaseDao.getByVoucherIdAndType(id, VoucherType.EXPENSE, VoucherType.REVERSED_EXPENSE);
        if (voucherBase == null) {
            return null;
        }
        voucherListing.setVoucherBase(voucherBase);
        List<ExpensesAccount> expensesAccounts = jdbcTemplate.query(sql, new ExpensesAccountMapper(), id);
        voucherListing.setVoucher(Collections.singletonList(expensesAccounts));

        return voucherListing;
    }

    @Override
    public ExpensesAccount insertEntity(ExpensesAccount expensesAccount) {
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(INSERT_ENTITY, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, expensesAccount.getAccountId());
            ps.setInt(2, expensesAccount.getCurrencyId());
            ps.setBigDecimal(3, expensesAccount.getAmount());
            ps.setBigDecimal(4, expensesAccount.getEquivalence());
            ps.setString(5, expensesAccount.getCrdr().toString());
            ps.setString(6, expensesAccount.getNotes());
            ps.setInt(7, expensesAccount.getCreatedBy());
            ps.setInt(8, expensesAccount.getVoucherId());
            ps.setFloat(9, expensesAccount.getEvaluationPrice());
            ps.setFloat(9, expensesAccount.getEvaluationPrice());
            ps.setFloat(10,expensesAccount.getAccountPreviousBalance());
            ps.setString(11,expensesAccount.getVoucherStatus().toString());
            ps.setInt(12,expensesAccount.getBranchId());
            ps.setObject(13,expensesAccount.getDateTime());
            ps.setInt(14,expensesAccount.getBalanceId());

            return ps;
        }, keyHolder);

        return getById(Integer.parseInt(keyHolder.getKeys().get("id").toString()));
    }

    public List<ExpensesAccount> getListById(Integer id) {
        String sql = CORE_SELECT + " where voucher_id = ?";

        return jdbcTemplate.query(sql, new ExpensesAccountMapper(), id);
    }

    public List<ExpensesAccount> getListByAccountId(Integer id, Integer currencyId, String from, String to, Integer branchId) {

        String sql = null;
        List<ExpensesAccount> expenses = null;

        if (branchId == null) {
            sql = CORE_SELECT + " where account_id=? and date_time between ?::timestamp and ?::timestamp and currency_id=? order by date_time";
            expenses = jdbcTemplate.query(sql, new ExpensesAccountMapper(), id, from, to, currencyId);
        } else {
            sql = CORE_SELECT + " where account_id=? and date_time between ?::timestamp and ?::timestamp and currency_id=? and branch_id=? order by date_time";
            expenses = jdbcTemplate.query(sql, new ExpensesAccountMapper(), id, from, to, currencyId, branchId);

        }

        for (ExpensesAccount e: expenses) {
            e.setBaseId(voucherBaseDao.getBaseIdByVoucherId(e.getVoucherId(),VoucherType.EXPENSE));
        }
        return expenses;
    }
    @Transactional
    public ExpensesVoucher insertBulk(ExpensesVoucher expensesVoucher) {
        Integer voucherId = getVoucherID();

        List<ExpensesAccount> accounts = expensesVoucher.getAccounts();
        VoucherBase voucherBase = expensesVoucher.getVoucherBase();
        for (ExpensesAccount account: accounts) {
            FourthLevel fourthLevel = fourthLevelDao.getById(account.getAccountId());
            account.setAccountPreviousBalance(fourthLevel.getBalance());
            account.setDateTime(voucherBase.getDateTime());
            account.setBranchId(voucherBase.getBranchId());
            account.setVoucherStatus(voucherBase.getVoucherStatus());
            if (account.getCrdr().equals(CRDR.DEBIT)) {
                fourthLevelDao.balanceIncrease(account.getEquivalence().floatValue(), account.getAccountId());
            } else if (account.getCrdr().equals(CRDR.CREDIT)) {
                fourthLevelDao.balanceDecrease(account.getEquivalence().floatValue(), account.getAccountId());
            }
            account.setVoucherId(voucherId);
            int balanceId = insertAccountBalance(account);
            account.setBalanceId(balanceId);
            insertEntity(account);


        }

        voucherBase.setVoucherId(voucherId);
         voucherBase = voucherBaseDao.insertEntity(voucherBase);
        expensesVoucher.setVoucherBase(voucherBase);

        return expensesVoucher;
    }
    public void removeIdsList(List<Integer> ids) {
        String idsList = String.join(",", ids.stream().map(id -> id+"").collect(Collectors.toList()));
        jdbcTemplate.update(String.format(DELETE_IDS, idsList));

    }

    public Integer getCount() {
        String sql = CORE_COUNT;
        return jdbcTemplate.queryForObject(sql, Integer.class);
    }


    @Override
    public void updateEntity(ExpensesAccount expensesAccount) {
        jdbcTemplate.update(UPDATE_ENTITY,
                expensesAccount.getAccountId(),
                expensesAccount.getCurrencyId(),
                expensesAccount.getAmount(),
                expensesAccount.getEquivalence(),
                expensesAccount.getCrdr().toString(),
                expensesAccount.getNotes(),
                expensesAccount.getUpdatedBy(),
                expensesAccount.getEvaluationPrice(),
                expensesAccount.getVoucherStatus(),
                expensesAccount.getBranchId(),
                expensesAccount.getDateTime(),
                expensesAccount.getBalanceId(),
                expensesAccount.getId()
        );
    }

    @Override
    public void deleteEntity(Integer id) {
        jdbcTemplate.update(DELETE_ENTITY, id);
    }

    public void deleteByVoucherId(Integer voucherId) {
        jdbcTemplate.update(DELETE_BY_VOUCHER_ID, voucherId);
    }

    public List<VoucherListing> getWithBase(int size, int offset, VoucherType type, VoucherStatus voucherStatus) {

        List<VoucherListing> resultList = new ArrayList<>();
        List<VoucherBase> voucherBaseList = voucherBaseDao.getEntities(size, offset, type, voucherStatus);

        for (VoucherBase item: voucherBaseList) {
            List<ExpensesAccount> expensesAccount = getEntities(item.getVoucherId());
            VoucherListing voucherListing = new VoucherListing(item, Collections.singletonList(expensesAccount));
            resultList.add(voucherListing);
        }
        return resultList;
    }
    private int insertAccountBalance(ExpensesAccount account) {
        AccountBalance accountBalance = new AccountBalance();
        accountBalance.setAccountId(account.getAccountId());
        accountBalance.setAmount(account.getAmount());
        accountBalance.setEquivalence(account.getEquivalence());
        accountBalance.setCreatedBy(account.getCreatedBy());
        accountBalance.setVoucherId(account.getVoucherId());
        accountBalance.setCrdr(account.getCrdr());
        accountBalance.setCurrencyId(account.getCurrencyId());
        accountBalance.setDateTime(account.getDateTime());
        accountBalance.setBranchId(account.getBranchId());
        AccountBalance balance = accountBalanceDao.insertEntity(accountBalance);
        return balance.getId();
    }

    private void updateAccountBalance(ExpensesAccount account) {
        AccountBalance accountBalance = new AccountBalance();
        accountBalance.setId(account.getBalanceId());
        accountBalance.setAccountId(account.getAccountId());
        accountBalance.setAmount(account.getAmount());
        accountBalance.setEquivalence(account.getEquivalence());
        accountBalance.setCreatedBy(account.getCreatedBy());
        accountBalance.setVoucherId(account.getVoucherId());
        accountBalance.setCrdr(account.getCrdr());
        accountBalance.setCurrencyId(account.getCurrencyId());
        accountBalance.setDateTime(account.getDateTime());
        accountBalance.setBranchId(account.getBranchId());
        accountBalanceDao.updateEntity(accountBalance);
    }
}
