package io.arti.accounting.dao.vouchers;

import io.arti.accounting.dao.DaoOperations;
import io.arti.accounting.dao.accountstree.AccountBalanceDao;
import io.arti.accounting.dao.accountstree.FourthLevelDao;
import io.arti.accounting.dao.mapper.vouchers.PaymentVoucherMapper;
import io.arti.accounting.model.accountstree.AccountBalance;
import io.arti.accounting.model.general.enums.CRDR;
import io.arti.accounting.model.vouchers.*;
import io.arti.accounting.model.vouchers.enums.VoucherStatus;
import io.arti.accounting.model.vouchers.enums.VoucherType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
@Repository
public class PaymentVoucherDao implements DaoOperations<PaymentReceiptVoucher> {

    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    VoucherBaseDao voucherBaseDao;
    @Autowired
    FourthLevelDao fourthLevelDao;
    @Autowired
    AccountBalanceDao accountBalanceDao;

    private static final String CORE_COUNT = "select count(*) from payment_vouchers";
    private static final String CORE_SELECT = "select id, voucher_id, debit_account_id,credit_account_id, currency_id, amount, equivalence, evaluation_price,recipient_name, notes, created_by," +
            " updated_by, created_at, updated_at,credit_account_previous_balance,debit_account_previous_balance,voucher_status,branch_id,date_time,credit_balance_id,debit_balance_id from payment_vouchers";
    private static final String INSERT_ENTITY = "insert into payment_vouchers (debit_account_id, credit_account_id, currency_id, amount, equivalence, " +
            " notes, created_by, voucher_id,recipient_name,evaluation_price,credit_account_previous_balance,debit_account_previous_balance,voucher_status,branch_id,date_time,credit_balance_id,debit_balance_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?)";
    private static final String UPDATE_ENTITY = "update payment_vouchers set debit_account_id=?, credit_account_id=?, currency_id=?, amount=?," +
            " equivalence=?, notes=?, updated_by=?,recipient_name=?,evaluation_price=?,voucher_status=?,branch_id=?, date_time=?,credit_balance_id=?,debit_balance_id=? where id=?";
    private static final String DELETE_ENTITY = "delete from payment_vouchers where id=?";
    private static final String DELETE_BY_VOUCHER_ID = "delete from payment_vouchers where voucher_id=?";
    private static final String DELETE_IDS = "delete from payment_vouchers where id in (%s) ";

    private static final String MAX_VOUCHER_ID = "select coalesce(max(voucher_id) + 1, 1) from payment_vouchers";

    @Override
    public List<PaymentReceiptVoucher> getEntities(int size, int offset) {
        String sql = CORE_SELECT + " order by id desc limit ? offset ?";
        return jdbcTemplate.query(sql, new PaymentVoucherMapper(), size, offset);
    }

    public List<PaymentReceiptVoucher> getEntities(Integer voucherId) {
        String sql = CORE_SELECT + " where voucher_id = ?";
        return jdbcTemplate.query(sql, new PaymentVoucherMapper(), voucherId);
    }
    @Override
    public List<PaymentReceiptVoucher> getEntities(int size, int offset, String name) {
        return null;
    }

    @Override
    public boolean isPresent(Integer id) {
        String sql = CORE_COUNT + " where id = ?";
        Integer result = jdbcTemplate.queryForObject(sql, Integer.class, id);

        return result != null && result > 0;
    }
    public boolean isPresentByVoucherId(Integer id) {
        String sql = CORE_COUNT + " where voucher_id = ?";
        Integer result = jdbcTemplate.queryForObject(sql, Integer.class, id);

        return result != null && result > 0;
    }
    public Integer getVoucherID() {
        return jdbcTemplate.queryForObject(MAX_VOUCHER_ID, Integer.class);
    }

    @Override
    public PaymentReceiptVoucher getById(Integer id) {
        String sql = CORE_SELECT + " where id=?";
        return jdbcTemplate.queryForObject(sql, new PaymentVoucherMapper(), new Object[]{id});
    }

    public PaymentReceiptVoucher getVoucherId(Integer id) {
        String sql = CORE_SELECT + " where voucher_id=?";
        return jdbcTemplate.queryForObject(sql, new PaymentVoucherMapper(), new Object[]{id});
    }

    public VoucherListing getByVoucherId(Integer id) {
        String sql = CORE_SELECT + " where voucher_id = ?";
        VoucherListing voucherListing = new VoucherListing();
        VoucherBase voucherBase = voucherBaseDao.getByVoucherIdAndType(id, VoucherType.PAYMENT, VoucherType.REVERSED_PAYMENT);
        if (voucherBase == null) {
            return null;
        }
        voucherListing.setVoucherBase(voucherBase);
        List<PaymentReceiptVoucher> paymentReceiptVouchers = jdbcTemplate.query(sql, new PaymentVoucherMapper(), id);
        voucherListing.setVoucher(Collections.singletonList(paymentReceiptVouchers));

        return voucherListing;
    }


    @Override
    @Transactional
    public PaymentReceiptVoucher insertEntity(PaymentReceiptVoucher paymentVoucher) {

        Integer voucherId = getVoucherID();
        paymentVoucher.setCreditAccountPreviousBalance(fourthLevelDao.getById(paymentVoucher.getCreditAccountId()).getBalance());
        paymentVoucher.setDebitAccountPreviousBalance(fourthLevelDao.getById(paymentVoucher.getDebitAccountId()).getBalance());
        paymentVoucher.setBranchId(paymentVoucher.getVoucherBase().getBranchId());
        paymentVoucher.setVoucherStatus(paymentVoucher.getVoucherBase().getVoucherStatus());
        paymentVoucher.setVoucherId(voucherId);
        fourthLevelDao.balanceIncrease(paymentVoucher.getEquivalence().floatValue(), paymentVoucher.getDebitAccountId());
        fourthLevelDao.balanceDecrease(paymentVoucher.getEquivalence().floatValue(), paymentVoucher.getCreditAccountId());
        int debitBalanceId = insertAccountBalance(paymentVoucher, CRDR.DEBIT, paymentVoucher.getDebitAccountId());
        int creditBalanceId = insertAccountBalance(paymentVoucher, CRDR.CREDIT, paymentVoucher.getCreditAccountId());
        paymentVoucher.setDebitBalanceId(debitBalanceId);
        paymentVoucher.setCreditBalanceId(creditBalanceId);
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(INSERT_ENTITY, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, paymentVoucher.getDebitAccountId());
            ps.setInt(2, paymentVoucher.getCreditAccountId());
            ps.setInt(3, paymentVoucher.getCurrencyId());
            ps.setBigDecimal(4, paymentVoucher.getAmount());
            ps.setBigDecimal(5, paymentVoucher.getEquivalence());
            ps.setString(6, paymentVoucher.getNotes());
            ps.setInt(7, paymentVoucher.getCreatedBy());
            ps.setInt(8, voucherId);
            ps.setString(9, paymentVoucher.getRecipientName());
            ps.setFloat(10, paymentVoucher.getEvaluationPrice());
            ps.setFloat(11, paymentVoucher.getCreditAccountPreviousBalance());
            ps.setFloat(12, paymentVoucher.getDebitAccountPreviousBalance());
            ps.setString(13,paymentVoucher.getVoucherStatus().toString());
            ps.setInt(14,paymentVoucher.getBranchId());
            ps.setObject(15,paymentVoucher.getVoucherBase().getDateTime());
            ps.setInt(16,paymentVoucher.getCreditBalanceId());
            ps.setInt(17,paymentVoucher.getDebitBalanceId());

            return ps;
        }, keyHolder);

        paymentVoucher.getVoucherBase().setVoucherId(voucherId);
        paymentVoucher.setVoucherBase(voucherBaseDao.insertEntity(paymentVoucher.getVoucherBase()));


        return paymentVoucher;
    }

    @Override
    @Transactional
    public void updateEntity(PaymentReceiptVoucher paymentVoucher) {
        jdbcTemplate.update(UPDATE_ENTITY,
                paymentVoucher.getDebitAccountId(),
                paymentVoucher.getCreditAccountId(),
                paymentVoucher.getCurrencyId(),
                paymentVoucher.getAmount(),
                paymentVoucher.getEquivalence(),
                paymentVoucher.getNotes(),
                paymentVoucher.getUpdatedBy(),
                paymentVoucher.getRecipientName(),
                paymentVoucher.getEvaluationPrice(),
                paymentVoucher.getVoucherBase().getVoucherStatus().toString(),
                paymentVoucher.getVoucherBase().getBranchId(),
                paymentVoucher.getVoucherBase().getDateTime(),
                paymentVoucher.getCreditBalanceId(),
                paymentVoucher.getDebitBalanceId(),
                paymentVoucher.getId()
        );
        updateAccountBalance(paymentVoucher, CRDR.DEBIT, paymentVoucher.getDebitAccountId());
        updateAccountBalance(paymentVoucher, CRDR.CREDIT, paymentVoucher.getCreditAccountId());
    }

    @Override
    public void deleteEntity(Integer id) {
        jdbcTemplate.update(DELETE_ENTITY, id);
    }

    public void deleteByVoucherId(Integer voucherId) {
        System.out.println(voucherId + " ::::::::::::::::");
        jdbcTemplate.update(DELETE_BY_VOUCHER_ID, voucherId);
    }

    public Integer getCount() {
        String sql = CORE_COUNT;
        return jdbcTemplate.queryForObject(sql, Integer.class);
    }

    public List<VoucherListing> getWithBase(int size, int offset, VoucherType type, VoucherStatus voucherStatus) {

        List<io.arti.accounting.model.vouchers.VoucherListing> resultList = new ArrayList<>();
        List<VoucherBase> voucherBaseList = voucherBaseDao.getEntities(size, offset, type, voucherStatus);

        for (VoucherBase item: voucherBaseList) {
            List<PaymentReceiptVoucher> paymentReceiptVoucher = getEntities(item.getVoucherId());
           VoucherListing voucherListing = new VoucherListing(item, Collections.singletonList(paymentReceiptVoucher));
            resultList.add(voucherListing);
        }
        return resultList;
    }

    public List<PaymentReceiptVoucher> getListByAccountId(Integer id, Integer currencyId, String from, String to, Integer branchId) {
        String sql = null;
        List<PaymentReceiptVoucher> payments = null;

        if (branchId == null) {
            sql = CORE_SELECT + " where debit_account_id=? OR credit_account_id = ? and date_time between ?::timestamp and ?::timestamp and currency_id=? order by date_time";
            payments = jdbcTemplate.query(sql, new PaymentVoucherMapper(), id, id, from, to, currencyId);
        } else {
            sql = CORE_SELECT + " where debit_account_id=? OR credit_account_id = ? and date_time between ?::timestamp and ?::timestamp and currency_id=? and branch_id=? order by date_time";
            payments = jdbcTemplate.query(sql, new PaymentVoucherMapper(), id, id, from, to, currencyId, branchId);
        }

        for (PaymentReceiptVoucher p : payments) {
            p.setBaseId(voucherBaseDao.getBaseIdByVoucherId(p.getVoucherId(), VoucherType.PAYMENT));
        }
        return payments;
    }
    private int insertAccountBalance(PaymentReceiptVoucher voucher, CRDR crdr, Integer accountId) {
        AccountBalance accountBalance = new AccountBalance();
        accountBalance.setAccountId(accountId);
        accountBalance.setAmount(voucher.getAmount());
        accountBalance.setEquivalence(voucher.getEquivalence());
        accountBalance.setCreatedBy(voucher.getCreatedBy());
        accountBalance.setVoucherId(voucher.getVoucherId());
        accountBalance.setCrdr(crdr);
        accountBalance.setCurrencyId(voucher.getCurrencyId());
        accountBalance.setDateTime(voucher.getDateTime());
        accountBalance.setBranchId(voucher.getBranchId());
        AccountBalance balance = accountBalanceDao.insertEntity(accountBalance);
        return balance.getId();
    }

    private void updateAccountBalance(PaymentReceiptVoucher voucher, CRDR crdr, Integer accountId) {
        AccountBalance accountBalance = new AccountBalance();
        accountBalance.setId(crdr == CRDR.DEBIT ? voucher.getDebitBalanceId() : voucher.getCreditBalanceId());
        accountBalance.setAccountId(accountId);
        accountBalance.setAmount(voucher.getAmount());
        accountBalance.setEquivalence(voucher.getEquivalence());
        accountBalance.setCreatedBy(voucher.getCreatedBy());
        accountBalance.setVoucherId(voucher.getVoucherId());
        accountBalance.setCrdr(crdr);
        accountBalance.setCurrencyId(voucher.getCurrencyId());
        accountBalance.setDateTime(voucher.getDateTime());
        accountBalance.setBranchId(voucher.getBranchId());
        accountBalanceDao.updateEntity(accountBalance);
    }

}
