package io.arti.accounting.dao.vouchers;

import io.arti.accounting.dao.DaoOperations;
import io.arti.accounting.dao.accountstree.AccountBalanceDao;
import io.arti.accounting.dao.accountstree.FourthLevelDao;
import io.arti.accounting.dao.mapper.vouchers.JournalVoucherMapper;
import io.arti.accounting.model.accountstree.AccountBalance;
import io.arti.accounting.model.accountstree.FourthLevel;
import io.arti.accounting.model.general.enums.CRDR;
import io.arti.accounting.model.vouchers.JournalBaseVoucher;
import io.arti.accounting.model.vouchers.JournalVoucher;
import io.arti.accounting.model.vouchers.VoucherBase;
import io.arti.accounting.model.vouchers.VoucherListing;
import io.arti.accounting.model.vouchers.enums.VoucherStatus;
import io.arti.accounting.model.vouchers.enums.VoucherType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;
import java.util.*;


@Service
public class JournalVoucherDao implements DaoOperations<JournalVoucher> {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    VoucherBaseDao voucherBaseDao;

    @Autowired
    FourthLevelDao fourthLevelDao;
    @Autowired
    AccountBalanceDao accountBalanceDao;

    private static final String CORE_SELECT = "select id, voucher_id, account_id, currency_id, amount, equivalence, cr_dr, note, evaluation_price, created_by," +
            " updated_by, created_at, updated_at,date_time, branch_id, voucher_status, account_previous_balance,balance_id from journal_voucher";
    private static final String CORE_COUNT = "select count(*) from journal_voucher";
    private static final String INSERT_ENTITY = "insert into journal_voucher (account_id, currency_id, amount, equivalence, cr_dr," +
            "  note, created_by, voucher_id, evaluation_price, date_time, branch_id, voucher_status, account_previous_balance,balance_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String UPDATE_ENTITY = "update journal_voucher set account_id=?, currency_id=?, amount=?," +
            " equivalence=?, cr_dr=?,  note=?, updated_by=?, evaluation_price=?, date_time=?, branch_id=?, voucher_status=?,balance_id=? where id=?";
    private static final String DELETE_ENTITY = "delete from journal_voucher where id=?";
    private static final String DELETE_BY_VOUCHER_ID = "delete from journal_voucher where voucher_id=?";

    private static final String DELETE_IDS = "delete from journal_voucher where id in (%s) ";

    private static final String MAX_VOUCHER_ID = "select coalesce(max(voucher_id) + 1, 1) from journal_voucher";


    @Override
    public List<JournalVoucher> getEntities(int size, int offset) {
        String sql = CORE_SELECT + " order by id desc limit ? offset ?";
        return jdbcTemplate.query(sql, new JournalVoucherMapper(), size, offset);
    }

    public List<JournalVoucher> getEntities(Integer voucherId) {
        String sql = CORE_SELECT + " where voucher_id = ?";
        return jdbcTemplate.query(sql, new JournalVoucherMapper(), voucherId);
    }

    @Override
    public List<JournalVoucher> getEntities(int size, int offset, String name) {
        return null;
    }

    @Override
    public boolean isPresent(Integer id) {
        String sql = CORE_COUNT + " where id = ?";
        Integer result = jdbcTemplate.queryForObject(sql, Integer.class, id);

        return result != null && result > 0;
    }

    public boolean isVoucherPresent(Integer id) {
        String sql = CORE_COUNT + " where voucher_id = ?";
        Integer result = jdbcTemplate.queryForObject(sql, Integer.class, id);

        return result != null && result > 0;
    }

    public Integer getVoucherID() {
        return jdbcTemplate.queryForObject(MAX_VOUCHER_ID, Integer.class);
    }
    @Override
    public JournalVoucher getById(Integer id) {
        String sql = CORE_SELECT + " where id=?";
        return jdbcTemplate.queryForObject(sql, new JournalVoucherMapper(), new Object[]{id});
    }

    public VoucherListing getByVoucherId(Integer id) {
        String sql = CORE_SELECT + " where voucher_id = ?";

        VoucherListing voucherListing = new VoucherListing();
        VoucherBase voucherBase = voucherBaseDao.getByVoucherIdAndType(id, VoucherType.JOURNAL, VoucherType.REVERSED_JOURNAL);
        if (voucherBase == null) {
            return null;
        }
        voucherListing.setVoucherBase(voucherBase);
        List<JournalVoucher> journalVoucherList = jdbcTemplate.query(sql, new JournalVoucherMapper(), id);
        voucherListing.setVoucher(Collections.singletonList(journalVoucherList));

        return voucherListing;
    }

    public List<JournalVoucher> getListById(Integer id) {
        String sql = CORE_SELECT + " where voucher_id = ?";

        return jdbcTemplate.query(sql, new JournalVoucherMapper(), id);
    }

    public List<JournalVoucher> getListByAccountId(Integer id, Integer currencyId,String from, String to, Integer branchId) {
        String sql = null;
        List<JournalVoucher> journalVouchers = null;

        if (branchId == null) {
            sql = CORE_SELECT + " where account_id = ? and date_time between ?::timestamp and ?::timestamp and currency_id=? order by date_time";
            journalVouchers = jdbcTemplate.query(sql, new JournalVoucherMapper(), id, from, to, currencyId);

        } else {
            sql = CORE_SELECT + " where account_id = ? and date_time between ?::timestamp and ?::timestamp and currency_id=? and branch_id=? order by date_time";
            journalVouchers = jdbcTemplate.query(sql, new JournalVoucherMapper(), id, from, to, currencyId, branchId);

        }

        for (JournalVoucher j : journalVouchers) {
            j.setBaseId(voucherBaseDao.getBaseIdByVoucherId(j.getVoucherId(), VoucherType.JOURNAL));
        }

        return journalVouchers;
    }


    @Override
    public JournalVoucher insertEntity(JournalVoucher journalVoucher) {
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(INSERT_ENTITY, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, journalVoucher.getAccountId());
            ps.setInt(2, journalVoucher.getCurrencyId());
            ps.setBigDecimal(3, journalVoucher.getAmount());
            ps.setBigDecimal(4, journalVoucher.getEquivalence());
            ps.setString(5, journalVoucher.getCrdr().toString());
            ps.setString(6, journalVoucher.getNote());
            ps.setInt(7, journalVoucher.getCreatedBy());
            ps.setInt(8, journalVoucher.getVoucherId());
            ps.setFloat(9, journalVoucher.getEvaluationPrice());
            ps.setObject(10,journalVoucher.getDateTime());
            ps.setInt(11,journalVoucher.getBranchId());
            ps.setString(12,journalVoucher.getVoucherStatus().toString());
            ps.setFloat(13,journalVoucher.getAccountPreviousBalance());
            ps.setInt(14,journalVoucher.getBalanceId());
            return ps;
        }, keyHolder);

        return getById(Integer.parseInt(keyHolder.getKeys().get("id").toString()));
    }
    @Transactional
    public JournalBaseVoucher insertBulk(JournalBaseVoucher journalBaseVouchers) {
        Integer voucherId = getVoucherID();

        List<JournalVoucher> journalVoucherList = journalBaseVouchers.getJournalVoucherList();
        VoucherBase oldVoucherBase = journalBaseVouchers.getVoucherBase();
        for (JournalVoucher item: journalVoucherList) {
            FourthLevel fourthLevel = fourthLevelDao.getById(item.getAccountId());
            item.setAccountPreviousBalance(fourthLevel.getBalance());
            if (item.getCrdr().equals(CRDR.DEBIT)) {
                fourthLevelDao.balanceIncrease(item.getEquivalence().floatValue(), item.getAccountId());
            } else if (item.getCrdr().equals(CRDR.CREDIT)) {
                fourthLevelDao.balanceDecrease(item.getEquivalence().floatValue(), item.getAccountId());
            }
            item.setVoucherId(voucherId);
            item.setDateTime(oldVoucherBase.getDateTime());
            item.setBranchId(oldVoucherBase.getBranchId());
            item.setVoucherStatus(oldVoucherBase.getVoucherStatus());
            int balanceId = insertAccountBalance(item);
            item.setBalanceId(balanceId);
            insertEntity(item);

        }

        journalBaseVouchers.getVoucherBase().setVoucherId(voucherId);
        VoucherBase voucherBase = voucherBaseDao.insertEntity(journalBaseVouchers.getVoucherBase());
        journalBaseVouchers.setVoucherBase(voucherBase);
        return journalBaseVouchers;
    }


    public Integer getCount() {
        String sql = CORE_COUNT;
        return jdbcTemplate.queryForObject(sql, Integer.class);
    }

    @Override
    @Transactional
    public void updateEntity(JournalVoucher journalVoucher) {

        jdbcTemplate.update(UPDATE_ENTITY,
                journalVoucher.getAccountId(),
                journalVoucher.getCurrencyId(),
                journalVoucher.getAmount(),
                journalVoucher.getEquivalence(),
                journalVoucher.getCrdr().toString(),
                journalVoucher.getNote(),
                journalVoucher.getUpdatedBy(),
                journalVoucher.getEvaluationPrice(),
                journalVoucher.getDateTime(),
                journalVoucher.getBranchId(),
                journalVoucher.getVoucherStatus(),
                journalVoucher.getBalanceId(),
                journalVoucher.getId()
        );
        updateAccountBalance(journalVoucher);

    }

    @Override
    public void deleteEntity(Integer id) {
        jdbcTemplate.update(DELETE_ENTITY, id);
    }

    public void deleteByVoucherId(Integer voucherId) {
        jdbcTemplate.update(DELETE_BY_VOUCHER_ID, voucherId);
    }

    public List<VoucherListing> getWithBase(int size, int offset, VoucherType type, VoucherStatus voucherStatus) {

        List<VoucherListing> resultList = new ArrayList<>();
        List<VoucherBase> voucherBaseList = voucherBaseDao.getEntities(size, offset, type, voucherStatus);

        for (VoucherBase item: voucherBaseList) {
            List<JournalVoucher> journalVoucher = getEntities(item.getVoucherId());
            VoucherListing voucherListing = new VoucherListing(item, Collections.singletonList(journalVoucher));
            resultList.add(voucherListing);
        }
        return resultList;
    }

    private int insertAccountBalance(JournalVoucher item) {
        AccountBalance accountBalance = new AccountBalance();
        accountBalance.setAccountId(item.getAccountId());
        accountBalance.setAmount(item.getAmount());
        accountBalance.setEquivalence(item.getEquivalence());
        accountBalance.setCreatedBy(item.getCreatedBy());
        accountBalance.setVoucherId(item.getVoucherId());
        accountBalance.setCrdr(item.getCrdr());
        accountBalance.setCurrencyId(item.getCurrencyId());
        accountBalance.setDateTime(item.getDateTime());
        accountBalance.setBranchId(item.getBranchId());
        AccountBalance balance = accountBalanceDao.insertEntity(accountBalance);
        return balance.getId();
    }
    private void updateAccountBalance(JournalVoucher item) {
        AccountBalance accountBalance = new AccountBalance();
        accountBalance.setId(item.getBalanceId());
        accountBalance.setAccountId(item.getAccountId());
        accountBalance.setAmount(item.getAmount());
        accountBalance.setEquivalence(item.getEquivalence());
        accountBalance.setCreatedBy(item.getCreatedBy());
        accountBalance.setVoucherId(item.getVoucherId());
        accountBalance.setCrdr(item.getCrdr());
        accountBalance.setCurrencyId(item.getCurrencyId());
        accountBalance.setDateTime(item.getDateTime());
        accountBalance.setBranchId(item.getBranchId());
        accountBalanceDao.updateEntity(accountBalance);
    }


}
