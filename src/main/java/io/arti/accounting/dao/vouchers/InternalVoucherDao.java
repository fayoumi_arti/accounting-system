package io.arti.accounting.dao.vouchers;

import io.arti.accounting.dao.DaoOperations;
import io.arti.accounting.dao.accountstree.AccountBalanceDao;
import io.arti.accounting.dao.accountstree.FourthLevelDao;
import io.arti.accounting.dao.mapper.vouchers.InternalVoucherMapper;
import io.arti.accounting.model.accountstree.AccountBalance;
import io.arti.accounting.model.general.enums.CRDR;
import io.arti.accounting.model.vouchers.InternalVoucher;
import io.arti.accounting.model.vouchers.VoucherBase;
import io.arti.accounting.model.vouchers.VoucherListing;
import io.arti.accounting.model.vouchers.enums.VoucherStatus;
import io.arti.accounting.model.vouchers.enums.VoucherType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
@Repository
public class InternalVoucherDao implements DaoOperations<InternalVoucher> {

    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    VoucherBaseDao voucherBaseDao;
    @Autowired
    FourthLevelDao fourthLevelDao;
    @Autowired
    AccountBalanceDao accountBalanceDao;

    private static final String CORE_COUNT = "select count(*) from internal_vouchers";
    private static final String CORE_SELECT = "select id, voucher_id, debit_account_id,credit_account_id, debit_currency_id, debit_amount, debit_equivalence, debit_evaluation_price, credit_currency_id, credit_amount, credit_equivalence, credit_evaluation_price,notes, created_by," +
            " updated_by, created_at, updated_at,credit_account_previous_balance,debit_account_previous_balance,voucher_status,branch_id,date_time,credit_balance_id,debit_balance_id from internal_vouchers";
    private static final String INSERT_ENTITY = "insert into internal_vouchers (debit_account_id, credit_account_id, debit_currency_id, debit_amount, debit_equivalence, debit_evaluation_price, credit_currency_id, credit_amount, credit_equivalence, credit_evaluation_price, " +
            " notes, created_by, voucher_id,credit_account_previous_balance,debit_account_previous_balance,voucher_status,branch_id,date_time,credit_balance_id,debit_balance_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?)";
    private static final String UPDATE_ENTITY = "update internal_vouchers set debit_account_id=?, credit_account_id=?, debit_currency_id=?, debit_amount=?, debit_equivalence=?, debit_evaluation_price=?, credit_currency_id=?, credit_amount=?, credit_equivalence=?, credit_evaluation_price=?," +
            " notes=?, updated_by=?,voucher_status=?,branch_id=?, date_time=?,credit_balance_id=?,debit_balance_id=? where id=?";
    private static final String DELETE_ENTITY = "delete from internal_vouchers where id=?";
    private static final String DELETE_BY_VOUCHER_ID = "delete from internal_vouchers where voucher_id=?";
    private static final String DELETE_IDS = "delete from internal_vouchers where id in (%s) ";

    private static final String MAX_VOUCHER_ID = "select coalesce(max(voucher_id) + 1, 1) from internal_vouchers";

    @Override
    public List<InternalVoucher> getEntities(int size, int offset) {
        String sql = CORE_SELECT + " order by id desc limit ? offset ?";
        return jdbcTemplate.query(sql, new InternalVoucherMapper(), size, offset);
    }

    public List<InternalVoucher> getEntities(Integer voucherId) {
        String sql = CORE_SELECT + " where voucher_id = ?";
        return jdbcTemplate.query(sql, new InternalVoucherMapper(), voucherId);
    }
    @Override
    public List<InternalVoucher> getEntities(int size, int offset, String name) {
        return null;
    }

    @Override
    public boolean isPresent(Integer id) {
        String sql = CORE_COUNT + " where id = ?";
        Integer result = jdbcTemplate.queryForObject(sql, Integer.class, id);

        return result != null && result > 0;
    }
    public boolean isPresentByVoucherId(Integer id) {
        String sql = CORE_COUNT + " where voucher_id = ?";
        Integer result = jdbcTemplate.queryForObject(sql, Integer.class, id);

        return result != null && result > 0;
    }
    public Integer getVoucherID() {
        return jdbcTemplate.queryForObject(MAX_VOUCHER_ID, Integer.class);
    }

    @Override
    public InternalVoucher getById(Integer id) {
        String sql = CORE_SELECT + " where id=?";
        return jdbcTemplate.queryForObject(sql, new InternalVoucherMapper(), new Object[]{id});
    }

    public InternalVoucher getVoucherId(Integer id) {
        String sql = CORE_SELECT + " where voucher_id=?";
        return jdbcTemplate.queryForObject(sql, new InternalVoucherMapper(), new Object[]{id});
    }

    public VoucherListing getByVoucherId(Integer id) {
        String sql = CORE_SELECT + " where voucher_id = ?";
        VoucherListing voucherListing = new VoucherListing();
        VoucherBase voucherBase = voucherBaseDao.getByVoucherIdAndType(id, VoucherType.PAYMENT, VoucherType.REVERSED_PAYMENT);
        if (voucherBase == null) {
            return null;
        }
        voucherListing.setVoucherBase(voucherBase);
        List<InternalVoucher> internalVouchers = jdbcTemplate.query(sql, new InternalVoucherMapper(), id);
        voucherListing.setVoucher(Collections.singletonList(internalVouchers));

        return voucherListing;
    }


    @Override
    @Transactional
    public InternalVoucher insertEntity(InternalVoucher internalVoucher) {

        Integer voucherId = getVoucherID();
        internalVoucher.setCreditAccountPreviousBalance(fourthLevelDao.getById(internalVoucher.getCreditAccountId()).getBalance());
        internalVoucher.setDebitAccountPreviousBalance(fourthLevelDao.getById(internalVoucher.getDebitAccountId()).getBalance());
        internalVoucher.setBranchId(internalVoucher.getVoucherBase().getBranchId());
        internalVoucher.setVoucherStatus(internalVoucher.getVoucherBase().getVoucherStatus());
        internalVoucher.setVoucherId(voucherId);
        fourthLevelDao.balanceIncrease(internalVoucher.getDebitEquivalence().floatValue(), internalVoucher.getDebitAccountId());
        fourthLevelDao.balanceDecrease(internalVoucher.getCreditEquivalence().floatValue(), internalVoucher.getCreditAccountId());
        int debitBalanceId = insertAccountBalance(internalVoucher, CRDR.DEBIT, internalVoucher.getDebitAccountId());
        int creditBalanceId = insertAccountBalance(internalVoucher, CRDR.CREDIT, internalVoucher.getCreditAccountId());
        internalVoucher.setDebitBalanceId(debitBalanceId);
        internalVoucher.setCreditBalanceId(creditBalanceId);
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(INSERT_ENTITY, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, internalVoucher.getDebitAccountId());
            ps.setInt(2, internalVoucher.getCreditAccountId());
            ps.setInt(3, internalVoucher.getDebitCurrencyId());
            ps.setBigDecimal(4, internalVoucher.getDebitAmount());
            ps.setBigDecimal(5, internalVoucher.getDebitEquivalence());
            ps.setFloat(6, internalVoucher.getDebitEvaluationPrice());
            ps.setInt(7, internalVoucher.getCreditCurrencyId());
            ps.setBigDecimal(8, internalVoucher.getCreditAmount());
            ps.setBigDecimal(9, internalVoucher.getCreditEquivalence());
            ps.setFloat(10, internalVoucher.getCreditEvaluationPrice());
            ps.setString(11, internalVoucher.getNotes());
            ps.setInt(12, internalVoucher.getCreatedBy());
            ps.setInt(13, voucherId);
            ps.setFloat(14, internalVoucher.getCreditAccountPreviousBalance());
            ps.setFloat(15, internalVoucher.getDebitAccountPreviousBalance());
            ps.setString(16,internalVoucher.getVoucherStatus().toString());
            ps.setInt(17,internalVoucher.getBranchId());
            ps.setObject(18,internalVoucher.getVoucherBase().getDateTime());
            ps.setInt(19,internalVoucher.getCreditBalanceId());
            ps.setInt(20,internalVoucher.getDebitBalanceId());

            return ps;
        }, keyHolder);

        internalVoucher.getVoucherBase().setVoucherId(voucherId);
        internalVoucher.setVoucherBase(voucherBaseDao.insertEntity(internalVoucher.getVoucherBase()));


        return internalVoucher;
    }

    @Override
    @Transactional
    public void updateEntity(InternalVoucher internalVoucher) {
        jdbcTemplate.update(UPDATE_ENTITY,
                internalVoucher.getDebitAccountId(),
                internalVoucher.getCreditAccountId(),
                internalVoucher.getDebitCurrencyId(),
                internalVoucher.getDebitAmount(),
                internalVoucher.getDebitEquivalence(),
                internalVoucher.getDebitEvaluationPrice(),
                internalVoucher.getCreditCurrencyId(),
                internalVoucher.getCreditAmount(),
                internalVoucher.getCreditEquivalence(),
                internalVoucher.getCreditEvaluationPrice(),
                internalVoucher.getNotes(),
                internalVoucher.getUpdatedBy(),
                internalVoucher.getVoucherBase().getVoucherStatus().toString(),
                internalVoucher.getVoucherBase().getBranchId(),
                internalVoucher.getVoucherBase().getDateTime(),
                internalVoucher.getCreditBalanceId(),
                internalVoucher.getDebitBalanceId(),
                internalVoucher.getId()
        );
        updateAccountBalance(internalVoucher, CRDR.DEBIT, internalVoucher.getDebitAccountId());
        updateAccountBalance(internalVoucher, CRDR.CREDIT, internalVoucher.getCreditAccountId());
    }

    @Override
    public void deleteEntity(Integer id) {
        jdbcTemplate.update(DELETE_ENTITY, id);
    }

    public void deleteByVoucherId(Integer voucherId) {
        System.out.println(voucherId + " ::::::::::::::::");
        jdbcTemplate.update(DELETE_BY_VOUCHER_ID, voucherId);
    }

    public Integer getCount() {
        String sql = CORE_COUNT;
        return jdbcTemplate.queryForObject(sql, Integer.class);
    }

    public List<VoucherListing> getWithBase(int size, int offset, VoucherType type, VoucherStatus voucherStatus) {

        List<io.arti.accounting.model.vouchers.VoucherListing> resultList = new ArrayList<>();
        List<VoucherBase> voucherBaseList = voucherBaseDao.getEntities(size, offset, type, voucherStatus);

        for (VoucherBase item: voucherBaseList) {
            List<InternalVoucher> internalVouchers = getEntities(item.getVoucherId());
            VoucherListing voucherListing = new VoucherListing(item, Collections.singletonList(internalVouchers));
            resultList.add(voucherListing);
        }
        return resultList;
    }

    public List<InternalVoucher> getListByAccountId(Integer id, Integer currencyId, String from, String to, Integer branchId) {
        String sql = null;
        List<InternalVoucher> internalVouchers = null;

        if (branchId == null) {
            sql = CORE_SELECT + " where debit_account_id=? OR credit_account_id = ? and date_time between ?::timestamp and ?::timestamp and (debit_currency_id=? or credit_currency_id=?) order by date_time";
            internalVouchers = jdbcTemplate.query(sql, new InternalVoucherMapper(), id, id, from, to, currencyId, currencyId);
        } else {
            sql = CORE_SELECT + " where debit_account_id=? OR credit_account_id = ? and date_time between ?::timestamp and ?::timestamp and branch_id=? and (debit_currency_id=? or credit_currency_id=?) order by date_time";
            internalVouchers = jdbcTemplate.query(sql, new InternalVoucherMapper(), id, id, from, to, branchId, currencyId, currencyId);
        }

        for (InternalVoucher v : internalVouchers) {
            v.setBaseId(voucherBaseDao.getBaseIdByVoucherId(v.getVoucherId(), VoucherType.INTERNAL));
        }
        return internalVouchers;
    }
    private int insertAccountBalance(InternalVoucher voucher, CRDR crdr, Integer accountId) {
        AccountBalance accountBalance = new AccountBalance();
        accountBalance.setAccountId(accountId);
        if (crdr == CRDR.CREDIT) {
            accountBalance.setAmount(voucher.getCreditAmount());
            accountBalance.setEquivalence(voucher.getCreditEquivalence());
            accountBalance.setCurrencyId(voucher.getCreditCurrencyId());
        }else{
            accountBalance.setAmount(voucher.getDebitAmount());
            accountBalance.setEquivalence(voucher.getDebitEquivalence());
            accountBalance.setCurrencyId(voucher.getDebitCurrencyId());
        }

        accountBalance.setCreatedBy(voucher.getCreatedBy());
        accountBalance.setVoucherId(voucher.getVoucherId());
        accountBalance.setCrdr(crdr);
        accountBalance.setDateTime(voucher.getDateTime());
        accountBalance.setBranchId(voucher.getBranchId());
        AccountBalance balance = accountBalanceDao.insertEntity(accountBalance);
        return balance.getId();
    }

    private void updateAccountBalance(InternalVoucher voucher, CRDR crdr, Integer accountId) {
        AccountBalance accountBalance = new AccountBalance();
        accountBalance.setId(crdr == CRDR.DEBIT ? voucher.getDebitBalanceId() : voucher.getCreditBalanceId());
        accountBalance.setAccountId(accountId);
        if (crdr == CRDR.CREDIT) {
            accountBalance.setAmount(voucher.getCreditAmount());
            accountBalance.setEquivalence(voucher.getCreditEquivalence());
            accountBalance.setCurrencyId(voucher.getCreditCurrencyId());
        }else{
            accountBalance.setAmount(voucher.getDebitAmount());
            accountBalance.setEquivalence(voucher.getDebitEquivalence());
            accountBalance.setCurrencyId(voucher.getDebitCurrencyId());
        }
        accountBalance.setCreatedBy(voucher.getCreatedBy());
        accountBalance.setVoucherId(voucher.getVoucherId());
        accountBalance.setCrdr(crdr);
        accountBalance.setDateTime(voucher.getDateTime());
        accountBalance.setBranchId(voucher.getBranchId());
        accountBalanceDao.updateEntity(accountBalance);
    }

}
