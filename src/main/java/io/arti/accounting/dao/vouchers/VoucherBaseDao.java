package io.arti.accounting.dao.vouchers;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.arti.accounting.dao.DaoOperations;
import io.arti.accounting.dao.mapper.vouchers.VoucherBaseMapper;
import io.arti.accounting.model.useractions.SubmissionRequest;
import io.arti.accounting.model.useractions.enums.SubmissionType;
import io.arti.accounting.model.vouchers.VoucherBase;
import io.arti.accounting.model.vouchers.enums.VoucherStatus;
import io.arti.accounting.model.vouchers.enums.VoucherType;
import io.arti.accounting.service.response.error.exception.server.ServerErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Service;

import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class VoucherBaseDao implements DaoOperations<VoucherBase> {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    JournalVoucherDao journalVoucherDao;

    private static final String CORE_SELECT = "select id, date_time, voucher_id, voucher_type, voucher_status, branch_id, report, " +
            " entry_id, entry_date, approver_id, approve_date, submission_type, deny_reason, created_by, updated_by, created_at, updated_at, attachments, reverse_id, modification_reason, references_no from voucher_base";
    private static final String CORE_COUNT = "select count(*) from voucher_base";
    private static final String INSERT_ENTITY = "insert into voucher_base (date_time, voucher_id, voucher_type, voucher_status, branch_id, report, " +
            " entry_id, entry_date, created_by, attachments, references_no, modification_reason) values (?, ?, ?, ?, ?, ?, ?, ?, ?,?::json,?,?)";
    private static final String UPDATE_ENTITY = "update voucher_base set date_time=?, voucher_id=?, voucher_type=?, voucher_status=?, " +
            "branch_id=?, report=?,  entry_id=?, entry_date=?, updated_by=?,attachments=to_json(?::json), references_no=?, modification_reason=? where id=?";
    private static final String DELETE_ENTITY = "delete from voucher_base where id=?";

    @Override
    public List<VoucherBase> getEntities(int size, int offset) {
        String sql = CORE_SELECT + " order by id desc limit ? offset ?";
        return jdbcTemplate.query(sql, new VoucherBaseMapper(), size, offset);
    }

    @Override
    public List<VoucherBase> getEntities(int size, int offset, String name) {
        return null;
    }

    public List<VoucherBase> getEntities(int size, int offset, VoucherType type, VoucherStatus voucherStatus) {

        String reverse = "REVERSED_" + type.toString();

        if (voucherStatus == null) {
            String sql = CORE_SELECT + " where voucher_type in (?::voucher_type, ?::voucher_type) order by id desc limit ? offset ?";
            return jdbcTemplate.query(sql, new VoucherBaseMapper(), type.toString(), reverse, size, offset);
        }
        String sql = CORE_SELECT + " where voucher_type in (?::voucher_type, ?::voucher_type) and voucher_status = ?::voucher_status order by id desc limit ? offset ?";
        return jdbcTemplate.query(sql, new VoucherBaseMapper(), type.toString(), reverse, voucherStatus.toString(), size, offset);

    }

    @Override
    public boolean isPresent(Integer id) {
        String sql = CORE_COUNT + " where id = ?";
        Integer result = jdbcTemplate.queryForObject(sql, Integer.class, id);

        return result != null && result > 0;
    }

    public boolean isPresent(Integer voucherId, VoucherType voucherType) {
        String sql = CORE_COUNT + " where voucher_id =? and voucher_type = ?::voucher_type";
        Integer result = jdbcTemplate.queryForObject(sql, Integer.class, voucherId, voucherType.toString());

        return result != null && result > 0;
    }

    public List<Integer> getIdsList() {
        String sql = "select id from voucher_base";
        List<Integer> ids = jdbcTemplate.queryForList(sql, Integer.class);
        return ids;
    }

    @Override
    public VoucherBase getById(Integer id) {
        String sql = CORE_SELECT + " where id=?";
        return jdbcTemplate.queryForObject(sql, new VoucherBaseMapper(), new Object[]{id});
    }


    public VoucherBase getByVoucherIdAndType(Integer id, VoucherType voucherType, VoucherType reversedType) {
        String sql = CORE_SELECT + " where voucher_id =? and (voucher_type = ?::voucher_type or voucher_type = ?::voucher_type)";
        List<VoucherBase> result = jdbcTemplate.query(sql, new VoucherBaseMapper(), id, voucherType.toString(), reversedType.toString());
        if (result.size() > 0) {
            return result.get(0);
        }
        return null;
    }

    public Integer getBaseIdByVoucherId(Integer id, VoucherType voucherType) {
        String sql;
        if (voucherType == VoucherType.JOURNAL) {
            sql = "select id from voucher_base where voucher_id =? and voucher_type in ('JOURNAL'::voucher_type, 'REVERSED_JOURNAL'::voucher_type,'TRANSFER'::voucher_type, 'REVERSED_TRANSFER'::voucher_type,'PAID_TRANSFER'::voucher_type, 'REVERSED_PAID_TRANSFER'::voucher_type) limit 1";
            return jdbcTemplate.queryForObject(sql, Integer.class, id);
        } else {
            String reversed = "REVERSED_" + voucherType.toString();
            sql = "select id from voucher_base where voucher_id =? and voucher_type in (?::voucher_type, ?::voucher_type) limit 1";
            return jdbcTemplate.queryForObject(sql, Integer.class, id, voucherType.toString(), reversed);
        }

    }

    public void voucherSubmission(SubmissionRequest submissionRequest) {
        String sql = "update voucher_base set voucher_status = 'TRANSFERRED', submission_type=?::submission_type where id = ?";
        jdbcTemplate.update(sql,
                submissionRequest.getSubmissionType().toString(),
                submissionRequest.getBaseId()
        );
    }

    public void voucherApprove(Integer id, Integer approverId) {
        String sql = "update voucher_base set voucher_status = 'APPROVED', approver_id=?, approve_date=NOW() where id = ?";
        jdbcTemplate.update(sql, approverId, id);
    }

    public void voucherDeny(Integer id, Integer approverId, String denyReason) {
        String sql = "update voucher_base set voucher_status = 'DENIED', approver_id=?, deny_reason=?, approve_date=NOW() where id = ?";
        jdbcTemplate.update(sql, approverId, denyReason, id);
    }

    public SubmissionType submissionType(Integer id) {
        String sql = "select submission_type from voucher_base where id = ?";
        SubmissionType submissionType = jdbcTemplate.queryForObject(sql, SubmissionType.class, id);
        return submissionType;
    }

    public VoucherStatus getStatus(Integer baseId) {
        String sql = "select voucher_status from voucher_base where id = ?";
        VoucherStatus voucherStatus = jdbcTemplate.queryForObject(sql, VoucherStatus.class, baseId);
        return voucherStatus;
    }

    @Override
    public VoucherBase insertEntity(VoucherBase voucherBase) {
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(INSERT_ENTITY, Statement.RETURN_GENERATED_KEYS);
            ps.setObject(1, voucherBase.getDateTime());
            ps.setInt(2, voucherBase.getVoucherId());
            ps.setString(3, voucherBase.getVoucherType().toString());
            ps.setString(4, voucherBase.getVoucherStatus().toString());
            ps.setInt(5, voucherBase.getBranchId());
            ps.setString(6, voucherBase.getReport());
            ps.setInt(7, voucherBase.getEntryId());
            ps.setObject(8, voucherBase.getEntryDate());
            ps.setInt(9, voucherBase.getCreatedBy());
            try {
                ps.setString(10, defaultMapper.writeValueAsString(voucherBase.getAttachments()));
            } catch (JsonProcessingException e) {
                throw new ServerErrorException("Failed to create Voucher base due to error while parsing Attachments array");
            }
            ps.setString(11, voucherBase.getReferenceNo());
            ps.setString(12, voucherBase.getModificationReason());
            return ps;
        }, keyHolder);

        return getById(Integer.parseInt(keyHolder.getKeys().get("id").toString()));

    }

    public Integer getCount() {
        String sql = CORE_COUNT;
        return jdbcTemplate.queryForObject(sql, Integer.class);
    }

    @Override
    public void updateEntity(VoucherBase voucherBase) {

        try {
            jdbcTemplate.update(UPDATE_ENTITY,
                    voucherBase.getDateTime(),
                    voucherBase.getVoucherId(),
                    voucherBase.getVoucherType().toString(),
                    voucherBase.getVoucherStatus().toString(),
                    voucherBase.getBranchId(),
                    voucherBase.getReport(),
                    voucherBase.getEntryId(),
                    voucherBase.getEntryDate(),
                    voucherBase.getUpdatedBy(),
                    defaultMapper.writeValueAsString(voucherBase.getAttachments()),
                    voucherBase.getReferenceNo(),
                    voucherBase.getModificationReason(),
                    voucherBase.getId()
            );
        } catch (JsonProcessingException e) {
            throw new ServerErrorException(e.getMessage(), e);
        }
    }

    @Override
    public void deleteEntity(Integer id) {
        jdbcTemplate.update(DELETE_ENTITY, id);
    }

    public void setReverseId(Integer originalId, Integer newId) {
        jdbcTemplate.update("update voucher_base set reverse_id = ? where id = ?", originalId, newId);
        jdbcTemplate.update("update voucher_base set reverse_id = ? where id = ?", newId, originalId);
    }
}
