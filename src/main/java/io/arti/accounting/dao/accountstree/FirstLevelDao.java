package io.arti.accounting.dao.accountstree;

import io.arti.accounting.dao.DaoOperations;
import io.arti.accounting.dao.mapper.accountstree.FirstLevelMapper;
import io.arti.accounting.model.accountstree.FirstLevel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FirstLevelDao implements DaoOperations<FirstLevel> {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private static final String CORE_SELECT = "select id, name_en, name_ar, balance from first_level";
    private static final String CORE_COUNT = "select count(*) from first_level";


    @Override
    public List<FirstLevel> getEntities(int size, int offset) {
        return null;
    }

    public List<FirstLevel> getEntities() {
        String sql = CORE_SELECT + " order by id";
        return jdbcTemplate.query(sql, new FirstLevelMapper());
    }

    @Override
    public List<FirstLevel> getEntities(int size, int offset, String name) {
        return null;
    }

    @Override
    public boolean isPresent(Integer id) {
        String sql = CORE_COUNT + " where id = ?";
        Integer result = jdbcTemplate.queryForObject(sql, Integer.class, id);

        return result != null && result > 0;
    }

    @Override
    public FirstLevel getById(Integer id) {
        String sql = CORE_SELECT + " where id=?";
        return jdbcTemplate.queryForObject(sql, new FirstLevelMapper(), new Object[]{id});
    }

    public List<FirstLevel> getByName(String name) {
        String sql = CORE_SELECT + " where name_en ILIKE '%" + name + "%' or name_ar ILIKE '%" + name + "%'";
        return jdbcTemplate.query(sql, new FirstLevelMapper());
    }

    @Override
    public FirstLevel insertEntity(FirstLevel firstLevel) {
        return null;
    }

    @Override
    public void updateEntity(FirstLevel firstLevel) {

    }

    @Override
    public void deleteEntity(Integer id) {

    }
}
