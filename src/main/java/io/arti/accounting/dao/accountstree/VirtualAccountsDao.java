package io.arti.accounting.dao.accountstree;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.arti.accounting.dao.DaoOperations;
import io.arti.accounting.dao.mapper.accountstree.VirtualAccountsMapper;
import io.arti.accounting.model.accountstree.VirtualAccounts;
import io.arti.accounting.service.response.error.exception.server.ServerErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

@Service
public class VirtualAccountsDao implements DaoOperations<VirtualAccounts> {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private static final String CORE_SELECT = "select id, types, account_id, created_by, updated_by, created_at, updated_at from virtual_accounts_settings";
    private static final String CORE_COUNT = "select count(*) from virtual_accounts_settings";
    private static final String INSERT_ENTITY = "insert into virtual_accounts_settings (types, account_id, created_by) values (?::json, ?, ?)";
    private static final String UPDATE_ENTITY = "update virtual_accounts_settings set types=?::json, account_id=?, updated_by=? where id=?";
    private static final String DELETE_ENTITY = "delete from virtual_accounts_settings where id=?";
    private static final String TRUNCATE_TABLE = "TRUNCATE virtual_accounts_settings RESTART IDENTITY CASCADE;";

    @Override
    public List<VirtualAccounts> getEntities(int size, int offset) {
        String sql = CORE_SELECT + " order by id desc limit ? offset ?";
        return jdbcTemplate.query(sql, new VirtualAccountsMapper(), size, offset);
    }

    @Override
    public List<VirtualAccounts> getEntities(int size, int offset, String name) {
        return null;
    }

    public List<VirtualAccounts> getEntities() {
        String sql = CORE_SELECT + " order by id desc";
        return jdbcTemplate.query(sql, new VirtualAccountsMapper());
    }

    @Override
    public boolean isPresent(Integer id) {
        String sql = CORE_COUNT + " where id = ?";
        Integer result = jdbcTemplate.queryForObject(sql, Integer.class, id);

        return result != null && result > 0;
    }

    @Override
    public VirtualAccounts getById(Integer id) {
        String sql = CORE_SELECT + " where id=?";
        return jdbcTemplate.queryForObject(sql, new VirtualAccountsMapper(), new Object[]{id});
    }

    @Transactional
    public void insertBulk(List<VirtualAccounts> virtualAccounts) {
        jdbcTemplate.update(TRUNCATE_TABLE);
        for (VirtualAccounts virtualAccount: virtualAccounts) {
            insertEntity(virtualAccount);
        }
    }

    @Override
    public VirtualAccounts insertEntity(VirtualAccounts virtualAccounts) {
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(INSERT_ENTITY, Statement.RETURN_GENERATED_KEYS);
            try {
                ps.setString(1, defaultMapper.writeValueAsString(virtualAccounts.getTypes()));
            } catch (JsonProcessingException e) {
                throw new ServerErrorException("Failed to create new Virtual Account due to error while parsing accounts array");
            }
            ps.setInt(2, virtualAccounts.getAccountId());

            ps.setInt(3, virtualAccounts.getCreatedBy());

            return ps;
        }, keyHolder);

        return getById(Integer.parseInt(keyHolder.getKeys().get("id").toString()));
    }

    @Transactional
    public void updateBulk(List<VirtualAccounts> virtualAccounts) {
        for (VirtualAccounts virtualAccount: virtualAccounts) {
            updateEntity(virtualAccount);
        }
    }
    @Override
    public void updateEntity(VirtualAccounts virtualAccounts) {
        try {
            jdbcTemplate.update(UPDATE_ENTITY,
                    defaultMapper.writeValueAsString(virtualAccounts.getTypes()),
                    virtualAccounts.getAccountId(),
                    virtualAccounts.getUpdatedBy(),
                    virtualAccounts.getId()
            );
        } catch (JsonProcessingException e) {
            throw new ServerErrorException(e.getMessage(), e);
        }
    }

    @Override
    public void deleteEntity(Integer id) {
        jdbcTemplate.update(DELETE_ENTITY, id);
    }
}
