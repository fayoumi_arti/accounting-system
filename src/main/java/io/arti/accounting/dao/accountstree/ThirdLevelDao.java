package io.arti.accounting.dao.accountstree;


import com.fasterxml.jackson.core.JsonProcessingException;
import io.arti.accounting.dao.DaoOperations;
import io.arti.accounting.dao.mapper.accountstree.FirstLevelMapper;
import io.arti.accounting.dao.mapper.accountstree.ThirdLevelMapper;
import io.arti.accounting.model.accountstree.FirstLevel;
import io.arti.accounting.model.accountstree.ThirdLevel;
import io.arti.accounting.service.response.error.exception.server.ServerErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Service;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

@Service
public class ThirdLevelDao  implements DaoOperations<ThirdLevel> {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private static final String CORE_SELECT = "select id, item_name_en, item_name_ar, second_level_id, types, balance, created_by, updated_by, created_at, updated_at from third_level";
    private static final String CORE_COUNT = "select count(*) from third_level";
    private static final String INSERT_ENTITY = "insert into third_level (item_name_en, item_name_ar, second_level_id, types, created_by) values (?, ?, ?, ?::json, ?)";
    private static final String UPDATE_ENTITY = "update third_level set item_name_en=?,  item_name_ar=?, second_level_id=?,types=to_json(?::json),  updated_by=? where id=?";
    private static final String DELETE_ENTITY = "delete from third_level where id=?";


    @Override
    public List<ThirdLevel> getEntities(int size, int offset) {
        String sql = CORE_SELECT + " order by id desc limit ? offset ?";
        return jdbcTemplate.query(sql, new ThirdLevelMapper(), size, offset);
    }

    @Override
    public List<ThirdLevel> getEntities(int size, int offset, String name) {
        return null;
    }

    @Override
    public boolean isPresent(Integer id) {
        String sql = CORE_COUNT + " where id = ?";
        Integer result = jdbcTemplate.queryForObject(sql, Integer.class, id);

        return result != null && result > 0;
    }

    public Integer getCount() {
        String sql = CORE_COUNT;
        return jdbcTemplate.queryForObject(sql, Integer.class);
    }

    public Integer getParentId(Integer id) {
        String sql = "select second_level_id from third_level where id = ?";
        return jdbcTemplate.queryForObject(sql, Integer.class, id);
    }

    @Override
    public ThirdLevel getById(Integer id) {
        String sql = CORE_SELECT + " where id=?";
        return jdbcTemplate.queryForObject(sql, new ThirdLevelMapper(), new Object[]{id});
    }

    public List<ThirdLevel> getByName(String name) {
        String sql = CORE_SELECT + " where item_name_en ILIKE '%" + name + "%' or item_name_ar ILIKE '%" + name + "%'";
        return jdbcTemplate.query(sql, new ThirdLevelMapper());
    }

    public List<ThirdLevel> getEntitiesByParent(Integer parentId) {
        String sql = CORE_SELECT + " where second_level_id = ?";
        return jdbcTemplate.query(sql, new ThirdLevelMapper(), parentId);
    }

    @Override
    public ThirdLevel insertEntity(ThirdLevel thirdLevel) {
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
           PreparedStatement ps = connection.prepareStatement(INSERT_ENTITY, Statement.RETURN_GENERATED_KEYS);
           ps.setString(1, thirdLevel.getEnName());
           ps.setString(2, thirdLevel.getArName());
           ps.setInt(3, thirdLevel.getSecondLevelId());
            try {
                ps.setString(4, defaultMapper.writeValueAsString(thirdLevel.getTypes()));
            } catch (JsonProcessingException e) {
                throw new ServerErrorException("Failed to create new Main Account due to error while parsing types array");
            }
            ps.setInt(5, thirdLevel.getCreatedBy());
           return ps;
        }, keyHolder);

        return getById(Integer.parseInt(keyHolder.getKeys().get("id").toString()));
    }

    @Override
    public void updateEntity(ThirdLevel thirdLevel) {
        try {
            jdbcTemplate.update(UPDATE_ENTITY,
                    thirdLevel.getEnName(),
                    thirdLevel.getArName(),
                    thirdLevel.getSecondLevelId(),
                    defaultMapper.writeValueAsString(thirdLevel.getTypes()),
                    thirdLevel.getUpdatedBy(),
                    thirdLevel.getId()
            );
        } catch (JsonProcessingException e) {
            throw new ServerErrorException(e.getMessage(), e);
        }
    }

    @Override
    public void deleteEntity(Integer id) { jdbcTemplate.update(DELETE_ENTITY, id); }

}
