package io.arti.accounting.dao.accountstree;

import io.arti.accounting.dao.DaoOperations;
import io.arti.accounting.dao.mapper.accountstree.AccountBalanceMapper;
import io.arti.accounting.model.accountstree.AccountBalance;
import io.arti.accounting.model.general.enums.CRDR;
import io.arti.accounting.model.vouchers.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;
import java.util.Map;

@Repository
public class AccountBalanceDao implements DaoOperations<AccountBalance> {
    private static final String CORE_SELECT = "select id, voucher_id, branch_id, account_id, third_level_id, second_level_id, first_level_id, currency_id, amount, equivalence, cr_dr, created_by," +
            " updated_by, created_at, updated_at,date_time, current_balance, previous_balance from account_balances";
    private static final String CORE_COUNT = "select count(*) from account_balances";
    private static final String INSERT_ENTITY = "insert into account_balances (account_id, third_level_id, second_level_id, first_level_id, currency_id, amount, equivalence, cr_dr," +
            "   created_by, voucher_id, date_time, current_balance, previous_balance, branch_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?::timestamp, ?, ?,?)";
    private static final String UPDATE_ENTITY = "update account_balances set account_id=?, currency_id=?, amount=?," +
            " equivalence=?, cr_dr=?,  updated_by=?, date_time=?, previous_balance=?, current_balance=?, branch_id=? where id=?";
    private static final String UPDATE_GREATER_ENTITIES_DEBIT = "update account_balances set previous_balance = (previous_balance + ?::numeric) , current_balance = (current_balance + ?::numeric) where date_time > ?::timestamp and currency_id=? and account_id =?;";
    private static final String UPDATE_GREATER_ENTITIES_CREDIT = "update account_balances set previous_balance = (previous_balance - ?::numeric) , current_balance = (current_balance - ?::numeric) where date_time > ?::timestamp and currency_id=? and account_id =?;";
    private static final String DELETE_ENTITY = "delete from account_balances where id=?";
    private static final String DELETE_IDS = "delete from account_balances where id in (%s) ";
    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    SecondLevelDao secondLevelDao;

    @Autowired
    ThirdLevelDao thirdLevelDao;

    @Autowired
    FourthLevelDao fourthLevelDao;

    @Override
    public List<AccountBalance> getEntities(int size, int offset) {
        return null;
    }

    @Override
    public List<AccountBalance> getEntities(int size, int offset, String name) {
        return null;
    }

    @Override
    public boolean isPresent(Integer id) {
        return false;
    }

    @Override
    public AccountBalance getById(Integer id) {
        String sql = CORE_SELECT + " where id=?";

        return jdbcTemplate.queryForObject(sql, new AccountBalanceMapper(), id);
    }


    public AccountBalance getById(Integer accountId, Integer voucherId) {
        String sql = CORE_SELECT + " where account_id = ? and voucher_id=? limit 1 ";

        return jdbcTemplate.queryForObject(sql, new AccountBalanceMapper(), accountId, voucherId);
    }

    public List<AccountBalance> getEntitiesByAccountAndCurrency(int accountId, int currencyId, String from, String to, Integer branchId) {

        String sql = null;

        if (branchId == null) {
            sql = CORE_SELECT + " where account_id = ? and date_time between ?::timestamp and ?::timestamp and currency_id=? order by date_time";
            return jdbcTemplate.query(sql, new AccountBalanceMapper(), accountId, from, to, currencyId);
        } else {
            sql = CORE_SELECT + " where account_id = ? and date_time between ?::timestamp and ?::timestamp and currency_id=? and branch_id=? order by date_time";
            return jdbcTemplate.query(sql, new AccountBalanceMapper(), accountId, from, to, currencyId, branchId);
        }

    }

    public List<AccountBalance> getEntitiesByAccountAndCurrency(int accountId, int currencyId, String to) {

        String sql = CORE_SELECT + " where account_id = ? and date_time <= ?::timestamp and currency_id=? order by date_time";

        return jdbcTemplate.query(sql, new AccountBalanceMapper(), accountId, to, currencyId);
    }

    public List<AccountBalance> getEntitiesByAccountAndCurrencyAndBranch(int accountId, int currencyId, String to, Integer branchId) {
        if (branchId == null || branchId == 0) {
            return getEntitiesByAccountAndCurrency(accountId, currencyId, to);
        }
        String sql = CORE_SELECT + " where account_id = ? and date_time <= ?::timestamp and currency_id=? and branch_id=? order by date_time";

        return jdbcTemplate.query(sql, new AccountBalanceMapper(), accountId, to, currencyId, branchId);
    }

    public boolean isPresentByAccountAndCurrency(int accountId, int currencyId) {

        String sql = CORE_COUNT + " where account_id = ?  and currency_id=?";
        Integer result = jdbcTemplate.queryForObject(sql, Integer.class, accountId, currencyId);

        return result != null && result > 0;
    }

    public boolean isPresentByAccountAndCurrencyAndDate(int accountId, int currencyId, String to) {

        String sql = CORE_COUNT + " where account_id = ?  and date_time <= ?::timestamp and currency_id=?";
        Integer result = jdbcTemplate.queryForObject(sql, Integer.class, accountId, to, currencyId);

        return result != null && result > 0;
    }

    public boolean isPresentByAccountAndCurrencyAndDateAndBranch(int accountId, int currencyId, String to, Integer branchId) {

        String sql = CORE_COUNT + " where account_id = ? and branch_id=?  and date_time <= ?::timestamp and currency_id=?";
        Integer result = jdbcTemplate.queryForObject(sql, Integer.class, accountId, branchId, to, currencyId);

        return result != null && result > 0;
    }

    public AccountBalance getLatestEntityByAccountAndCurrency(int accountId, int currencyId) {
        if (isPresentByAccountAndCurrency(accountId, currencyId)) {

            String sql = CORE_SELECT + " where account_id = ?  and currency_id=? order by date_time desc limit 1";

            return jdbcTemplate.queryForObject(sql, new AccountBalanceMapper(), accountId, currencyId);
        }
        return null;
    }

    public AccountBalance getLatestEntityByAccountAndCurrency(int accountId, int currencyId, String date) {
        if (isPresentByAccountAndCurrencyAndDate(accountId, currencyId, date)) {

            String sql = CORE_SELECT + " where account_id = ? and date_time < ?::timestamp and currency_id=? order by date_time desc limit 1";

            return jdbcTemplate.queryForObject(sql, new AccountBalanceMapper(), accountId, date, currencyId);
        }
        return null;
    }

    public boolean isAccountUsed(int accountId) {

        String sql = CORE_COUNT + " where account_id = ? limit 1";
        Integer result = jdbcTemplate.queryForObject(sql, Integer.class, accountId);
        return result != null && result > 0;

    }

    public AccountBalance getLatestEntityByAccountAndCurrencyAndBranch(int accountId, int currencyId, String date, Integer branchId) {
        if (branchId == null || branchId == 0) {
            return getLatestEntityByAccountAndCurrency(accountId, currencyId, date);
        }
        if (isPresentByAccountAndCurrencyAndDateAndBranch(accountId, currencyId, date, branchId)) {
            String sql = CORE_SELECT + " where account_id = ? and branch_id = ? and date_time <= ?::timestamp and currency_id=? order by date_time desc limit 1";
            return jdbcTemplate.queryForObject(sql, new AccountBalanceMapper(), accountId, branchId, date, currencyId);
        }
        return null;
    }

    public List<Map<String, Object>> getEntitiesByThirdLevelAccountBalanceByBranchAndDate(Integer branch, String from, String to, Boolean isIncomeStatement) {
        String fromWhere = ((from != null) ? String.format(" and date_time::date >= '%s'::timestamp ", from) : "");
        String incomeStatementOperation = (isIncomeStatement ? "=" : "!=");

        String sql = "select third_level_id::integer as account_id, sum(case when cr_dr = 'DEBIT' then equivalence else -equivalence end)::float as balance from accounting.account_balances\n" +
                "where first_level_id " + incomeStatementOperation + " 4 and branch_id = " + branch + fromWhere + " and date_time::date <= '" + to + "'::timestamp GROUP BY third_level_id";
        return jdbcTemplate.queryForList(sql);
    }

    public List<Map<String, Object>> getEntitiesBySecondLevelAccountBalanceByBranchAndDate(Integer branch, String from, String to, Boolean isIncomeStatement) {
        String fromWhere = ((from != null) ? String.format(" and date_time::date >= '%s'::timestamp ", from) : "");
        String incomeStatementOperation = (isIncomeStatement ? "=" : "!=");

        String sql = "select second_level_id::integer as account_id, sum(case when cr_dr = 'DEBIT' then equivalence else -equivalence end)::float as balance from accounting.account_balances\n" +
                "where first_level_id " + incomeStatementOperation + " 4 and branch_id = " + branch + fromWhere + " and date_time::date <= '" + to + "'::timestamp GROUP BY second_level_id";
        return jdbcTemplate.queryForList(sql);
    }

    public List<Map<String, Object>> getEntitiesByFirstLevelAccountBalanceByBranchAndDate(Integer branch, String from, String to, Boolean isIncomeStatement) {
        String fromWhere = ((from != null) ? String.format(" and date_time::date >= '%s'::timestamp ", from) : "");
        String incomeStatementOperation = (isIncomeStatement ? "=" : "!=");

        String sql = "select first_level_id::integer as account_id, sum(case when cr_dr = 'DEBIT' then equivalence else -equivalence end)::float as balance from accounting.account_balances\n" +
                "where first_level_id " + incomeStatementOperation + " 4 and branch_id = " + branch + fromWhere + " and date_time::date <= '" + to + "'::timestamp GROUP BY first_level_id";
        return jdbcTemplate.queryForList(sql);
    }

    @Override
    @Transactional
    public AccountBalance insertEntity(AccountBalance accountBalance) {

        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
        AccountBalance latestAccountBalance = getLatestEntityByAccountAndCurrencyAndBranch(accountBalance.getAccountId(), accountBalance.getCurrencyId(), accountBalance.getDateTime().toString(), accountBalance.getBranchId());
        BigDecimal netBalance = accountBalance.getAmount();
        if (latestAccountBalance == null) {
            accountBalance.setPreviousBalance(BigDecimal.ZERO);
            accountBalance.setCurrentBalance(accountBalance.getCrdr() == CRDR.CREDIT ? accountBalance.getAmount().multiply(BigDecimal.valueOf(-1)) : accountBalance.getAmount());
        } else {
            accountBalance.setPreviousBalance(latestAccountBalance.getCurrentBalance());
            if (accountBalance.getCrdr() == CRDR.DEBIT) {
                accountBalance.setCurrentBalance(netBalance.add(accountBalance.getPreviousBalance()));
            } else {
                netBalance = netBalance.multiply(BigDecimal.valueOf(-1));
                accountBalance.setCurrentBalance(accountBalance.getPreviousBalance().subtract(accountBalance.getAmount()));
            }
        }

        Integer thirdLevelId = fourthLevelDao.getParentId(accountBalance.getAccountId());
        Integer secondLevelId = thirdLevelDao.getParentId(thirdLevelId);
        Integer firstLevelId = secondLevelDao.getParentId(secondLevelId);

        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(INSERT_ENTITY, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, accountBalance.getAccountId());
            ps.setInt(2, thirdLevelId);
            ps.setInt(3, secondLevelId);
            ps.setInt(4, firstLevelId);
            ps.setInt(5, accountBalance.getCurrencyId());
            ps.setBigDecimal(6, accountBalance.getAmount());
            ps.setBigDecimal(7, accountBalance.getEquivalence());
            ps.setString(8, accountBalance.getCrdr().toString());
            ps.setInt(9, accountBalance.getCreatedBy());
            ps.setInt(10, accountBalance.getVoucherId());
            ps.setObject(11, accountBalance.getDateTime().toString());
            ps.setBigDecimal(12, accountBalance.getCurrentBalance());
            ps.setBigDecimal(13, accountBalance.getPreviousBalance());
            ps.setInt(14, accountBalance.getBranchId());
            return ps;
        }, keyHolder);
        accountBalance.setId(Integer.valueOf(keyHolder.getKeys().get("id").toString()));
        updateGreaterEntities(netBalance, accountBalance,false);

        return accountBalance;
    }

    @Override
    public void updateEntity(AccountBalance accountBalance) {

        AccountBalance latestAccountBalance = getLatestEntityByAccountAndCurrencyAndBranch(accountBalance.getAccountId(), accountBalance.getCurrencyId(), accountBalance.getDateTime().toString(), accountBalance.getBranchId());
        AccountBalance oldAccountBalance = getById(accountBalance.getId());
        BigDecimal oldBalance = oldAccountBalance.getCrdr() == CRDR.CREDIT ? oldAccountBalance.getAmount() : oldAccountBalance.getAmount().multiply(BigDecimal.valueOf(-1));
        BigDecimal updatedBalance = accountBalance.getCrdr() == CRDR.DEBIT ? accountBalance.getAmount() : accountBalance.getAmount().multiply(BigDecimal.valueOf(-1));
        BigDecimal netAmount = oldBalance.add(updatedBalance);

        if (latestAccountBalance == null) {
            accountBalance.setPreviousBalance(BigDecimal.ZERO);
            accountBalance.setCurrentBalance(accountBalance.getAmount());
        } else {
            accountBalance.setPreviousBalance(latestAccountBalance.getCurrentBalance());
            if (accountBalance.getCrdr() == CRDR.DEBIT) {
                accountBalance.setCurrentBalance(accountBalance.getAmount().add(accountBalance.getPreviousBalance()));
            } else {
                accountBalance.setCurrentBalance(accountBalance.getPreviousBalance().subtract(accountBalance.getAmount()));
            }
        }
        jdbcTemplate.update(UPDATE_ENTITY,
                accountBalance.getAccountId(),
                accountBalance.getCurrencyId(),
                accountBalance.getAmount(),
                accountBalance.getEquivalence(),
                accountBalance.getCrdr().toString(),
                accountBalance.getUpdatedBy(),
                accountBalance.getDateTime(),
                accountBalance.getPreviousBalance(),
                accountBalance.getCurrentBalance(),
                accountBalance.getBranchId(),
                accountBalance.getId()
        );
        updateGreaterEntities(netAmount, accountBalance,false);
    }

    @Override
    public void deleteEntity(Integer id) {
        jdbcTemplate.update(DELETE_ENTITY, id);
    }

    private void updateGreaterEntities(BigDecimal netAmount, AccountBalance accountBalance, boolean isDelete) {
        String sql;
        if (isDelete) {
            sql = accountBalance.getCrdr() == CRDR.CREDIT ? UPDATE_GREATER_ENTITIES_DEBIT : UPDATE_GREATER_ENTITIES_CREDIT;
        }else{
            sql = accountBalance.getCrdr() == CRDR.CREDIT ? UPDATE_GREATER_ENTITIES_CREDIT : UPDATE_GREATER_ENTITIES_DEBIT;

        }

        jdbcTemplate.update(sql,
                netAmount,
                netAmount,
                accountBalance.getDateTime().toString(),
                accountBalance.getCurrencyId(),
                accountBalance.getAccountId()
        );
    }

    @Transactional
    public void deleteVoucherListing(VoucherListing voucherListing) {
        if (voucherListing == null || voucherListing.getVoucher() == null || voucherListing.getVoucher().isEmpty() || voucherListing.getVoucherBase() == null) {
            return;
        }
        switch (voucherListing.getVoucherBase().getVoucherType()) {
            case JOURNAL:
                List<JournalVoucher> vouchers = (List<JournalVoucher>) voucherListing.getVoucher().get(0);
                for (JournalVoucher journal : vouchers) {
                    AccountBalance accountBalance = getById(journal.getAccountId(), journal.getVoucherId());
                    deleteEntity(accountBalance.getId());
                    BigDecimal netAmount = accountBalance.getAmount();
                    updateGreaterEntities(netAmount, accountBalance, true);
                }
                break;
            case EXPENSE:
                List<ExpensesAccount> expenses = (List<ExpensesAccount>) voucherListing.getVoucher().get(0);
                for (ExpensesAccount expensesAccount : expenses) {
                    AccountBalance accountBalance = getById(expensesAccount.getAccountId(), expensesAccount.getVoucherId());
                    deleteEntity(accountBalance.getId());
                    BigDecimal netAmount = accountBalance.getAmount();
                    updateGreaterEntities(netAmount, accountBalance, true);
                }
                break;
            case PAYMENT:
            case RECEIPT:
                for (Object obj : voucherListing.getVoucher()) {
                    PaymentReceiptVoucher payment = ((List<PaymentReceiptVoucher>) obj).get(0);
                    AccountBalance debitAccountBalance = getById(payment.getDebitAccountId(), payment.getVoucherId());
                    AccountBalance creditAccountBalance = getById(payment.getCreditAccountId(), payment.getVoucherId());
                    deleteEntity(debitAccountBalance.getId());
                    deleteEntity(creditAccountBalance.getId());
                    updateGreaterEntities(debitAccountBalance.getAmount(), debitAccountBalance,true);
                    updateGreaterEntities(creditAccountBalance.getAmount(), creditAccountBalance, true);
                }
                break;
            case INTERNAL:
                for (Object obj : voucherListing.getVoucher()) {
                    InternalVoucher internalVoucher = ((List<InternalVoucher>) obj).get(0);
                    AccountBalance debitAccountBalance = getById(internalVoucher.getDebitAccountId(), internalVoucher.getVoucherId());
                    AccountBalance creditAccountBalance = getById(internalVoucher.getCreditAccountId(), internalVoucher.getVoucherId());
                    deleteEntity(debitAccountBalance.getId());
                    deleteEntity(creditAccountBalance.getId());
                    updateGreaterEntities(debitAccountBalance.getAmount(), debitAccountBalance, true);
                    updateGreaterEntities(creditAccountBalance.getAmount(), creditAccountBalance, true);
                }
                break;
        }

    }
}
