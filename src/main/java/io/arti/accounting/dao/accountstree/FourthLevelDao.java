package io.arti.accounting.dao.accountstree;


import com.fasterxml.jackson.core.JsonProcessingException;
import io.arti.accounting.dao.DaoOperations;
import io.arti.accounting.dao.mapper.accountstree.FourthLevelMapper;
import io.arti.accounting.model.accountstree.FourthLevel;
import io.arti.accounting.service.response.error.exception.client.BadRequestException;
import io.arti.accounting.service.response.error.exception.server.ServerErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Service;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Service
public class FourthLevelDao implements DaoOperations<FourthLevel> {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private static final String CORE_SELECT = "select id, item_name_en, item_name_ar, third_level_id, default_currency_id ,governorate, " +
            "city, communication_info, address, first_phone_number, second_phone_number, fax, first_email," +
            "second_email, person_in_charge, cr_dr, balance, created_by, updated_by, created_at, updated_at,allowed_currencies from fourth_level";

    private static final String SELECT_EXPENSES = "select id, item_name_en, item_name_ar, third_level_id, default_currency_id ,governorate, " +
            "city, communication_info, address, first_phone_number, second_phone_number, fax, first_email," +
            "second_email, person_in_charge, cr_dr, balance, created_by, updated_by, created_at, updated_at,allowed_currencies from fourth_level where third_level_id in " +
            "(select id from third_level where second_level_id in (select id from second_level where name_en in ('operation expenses','selling expenses','general and administrative'))) ";
    private static final String CORE_COUNT = "select count(*) from fourth_level";
    private static final String INSERT_ENTITY = "insert into fourth_level (item_name_en, item_name_ar, third_level_id,default_currency_id,governorate,city,communication_info,address,first_phone_number,second_phone_number,fax,first_email,second_email,person_in_charge,cr_dr, balance, created_by,allowed_currencies) values (?, ?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?,?,?::json)";
    private static final String UPDATE_ENTITY = "update fourth_level set item_name_en=?,  item_name_ar=?, third_level_id=?,default_currency_id=?,governorate=?,city=?,communication_info=?,address=?,first_phone_number=?,second_phone_number=?,fax=?,first_email=?,second_email=?,person_in_charge=?,cr_dr=?, updated_by=?, allowed_currencies=?::json where id=?";
    private static final String DELETE_ENTITY = "delete from fourth_level where id=?";

    private static final String UPDATE_BALANCE_INCREASE = "update fourth_level set balance = balance + ? where id=?";
    private static final String UPDATE_BALANCE_DECREASE = "update fourth_level set balance = balance - ? where id=?";

    @Override
    public List<FourthLevel> getEntities(int size, int offset) {
        String sql = CORE_SELECT + " order by id desc limit ? offset ?";
        return jdbcTemplate.query(sql, new FourthLevelMapper(), size, offset);
    }
    public List<FourthLevel> getExpensesEntities(int size, int offset) {
        String sql = SELECT_EXPENSES + " order by id desc limit ? offset ?";
        return jdbcTemplate.query(sql, new FourthLevelMapper(), size, offset);
    }

    @Override
    public List<FourthLevel> getEntities(int size, int offset, String name) {
        return null;
    }
    public List<FourthLevel> getEntities() {
        String sql = CORE_SELECT + " order by id desc";
        return jdbcTemplate.query(sql, new FourthLevelMapper());
    }
    @Override
    public boolean isPresent(Integer id) {
        String sql = CORE_COUNT + " where id = ?";
        Integer result = jdbcTemplate.queryForObject(sql, Integer.class, id);

        return result != null && result > 0;
    }

    public boolean isPresentByCurrency(Integer currencyId) {
        String sql = CORE_COUNT + " where allowed_currencies @> '%s' limit 1;";
        Integer result = jdbcTemplate.queryForObject(String.format(sql,currencyId), Integer.class);

        return result != null && result > 0;
    }

    @Override
    public FourthLevel getById(Integer id) {
        String sql = CORE_SELECT + " where id=?";
        return jdbcTemplate.queryForObject(sql, new FourthLevelMapper(), new Object[]{id});
    }

    public List<FourthLevel> getByName(String name) {
        String sql = CORE_SELECT + " where item_name_en ILIKE '%" + name + "%' or item_name_ar ILIKE '%" + name + "%'";
        return jdbcTemplate.query(sql, new FourthLevelMapper());
    }

    public Boolean nameEnExists(String nameEn, Integer id) {
        String sql;
        Integer result;
        if (id == null) {
            sql = CORE_COUNT + " where lower(item_name_en) = ? ";
            result = jdbcTemplate.queryForObject(sql, Integer.class, nameEn);
        } else {
            sql = CORE_COUNT + " where lower(item_name_en) = ? and id != ?";
            result = jdbcTemplate.queryForObject(sql, Integer.class, nameEn, id);
        }

        return result != null && result > 0;
    }

    public Boolean nameArExists(String nameAr, Integer id) {
        String sql;
        Integer result;
        if (id == null) {
            sql = CORE_COUNT + " where  item_name_ar = ? ";
            result = jdbcTemplate.queryForObject(sql, Integer.class, nameAr);
        } else {
            sql = CORE_COUNT + " where item_name_ar = ? and id != ?";
            result = jdbcTemplate.queryForObject(sql, Integer.class, nameAr, id);
        }
        return result != null && result > 0;
    }



    public void validateName(FourthLevel fourthLevel, Integer id) {
        if (nameArExists(fourthLevel.getArName(), id)) {
            throw new BadRequestException("Arabic account name " + fourthLevel.getArName() + " already exists");
        }

        if (nameEnExists(fourthLevel.getEnName(), id)) {
            throw new BadRequestException("English account name " + fourthLevel.getEnName() + " already exists");
        }
    }

    @Override
    public FourthLevel insertEntity(FourthLevel fourthLevel) {
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();

        validateName(fourthLevel, null);

        if (fourthLevel.getBalance() == null) {
            fourthLevel.setBalance(0f);
        }
        if (fourthLevel.getAllowedCurrencies() == null || fourthLevel.getAllowedCurrencies().isEmpty()) {
            List<Integer> allowedCurrencies = new ArrayList<>();
            allowedCurrencies.add(fourthLevel.getDefaultCurrencyId());
            fourthLevel.setAllowedCurrencies(allowedCurrencies);
        }


        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(INSERT_ENTITY, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, fourthLevel.getEnName());
            ps.setString(2, fourthLevel.getArName());
            ps.setInt(3, fourthLevel.getThirdLevelId());
            ps.setInt(4, fourthLevel.getDefaultCurrencyId());
            ps.setString(5, fourthLevel.getGovernorate());
            ps.setString(6, fourthLevel.getCity());
            ps.setString(7, fourthLevel.getCommunicationInfo());
            ps.setString(8, fourthLevel.getAddress());
            ps.setString(9, fourthLevel.getFirstPhoneNumber());
            ps.setString(10, fourthLevel.getSecondPhoneNumber());
            ps.setString(11, fourthLevel.getFax());
            ps.setString(12, fourthLevel.getFirstEmail());
            ps.setString(13, fourthLevel.getSecondEmail());
            ps.setString(14, fourthLevel.getPersonInCharge());
            ps.setString(15, fourthLevel.getCrdr().toString());
            ps.setFloat(16, fourthLevel.getBalance());
            ps.setInt(17, fourthLevel.getCreatedBy());
            try {
                ps.setString(18, defaultMapper.writeValueAsString(fourthLevel.getAllowedCurrencies()));
            } catch (JsonProcessingException e) {
                throw new ServerErrorException("Failed to create new Fourth level Account due to error while parsing allowedCurrencies array");
            }
            return ps;
        }, keyHolder);

        return getById(Integer.parseInt(keyHolder.getKeys().get("id").toString()));
    }

    public Integer getCount() {
        String sql = CORE_COUNT;
        return jdbcTemplate.queryForObject(sql, Integer.class);
    }

    public Integer getParentId(Integer id) {
        String sql = "select third_level_id from fourth_level where id = ?";
        return jdbcTemplate.queryForObject(sql, Integer.class, id);
    }

    @Override
    public void updateEntity(FourthLevel fourthLevel) {

        validateName(fourthLevel, fourthLevel.getId());

        try {
            jdbcTemplate.update(UPDATE_ENTITY,
                    fourthLevel.getEnName(),
                    fourthLevel.getArName(),
                    fourthLevel.getThirdLevelId(),
                    fourthLevel.getDefaultCurrencyId(),
                    fourthLevel.getGovernorate(),
                    fourthLevel.getCity(),
                    fourthLevel.getCommunicationInfo(),
                    fourthLevel.getAddress(),
                    fourthLevel.getFirstPhoneNumber(),
                    fourthLevel.getSecondPhoneNumber(),
                    fourthLevel.getFax(),
                    fourthLevel.getFirstEmail(),
                    fourthLevel.getSecondEmail(),
                    fourthLevel.getPersonInCharge(),
                    fourthLevel.getCrdr().toString(),
                    fourthLevel.getUpdatedBy(),
                    defaultMapper.writeValueAsString(fourthLevel.getAllowedCurrencies()),
                    fourthLevel.getId()
            );
        } catch (JsonProcessingException e) {
            throw new ServerErrorException(e.getMessage(), e);
        }
    }

    @Override
    public void deleteEntity(Integer id) { jdbcTemplate.update(DELETE_ENTITY, id); }

    public List<FourthLevel> getEntitiesByParent(Integer parentId) {
        String sql = CORE_SELECT + " where third_level_id = ?";
        return jdbcTemplate.query(sql, new FourthLevelMapper(), parentId);
    }

    public void balanceIncrease(Float balance, Integer id) {
        jdbcTemplate.update(UPDATE_BALANCE_INCREASE, balance, id);
    }

    public void balanceDecrease(Float balance, Integer id) {
        jdbcTemplate.update(UPDATE_BALANCE_DECREASE, balance, id);
    }
}
