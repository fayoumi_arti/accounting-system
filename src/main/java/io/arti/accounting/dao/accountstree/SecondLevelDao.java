package io.arti.accounting.dao.accountstree;

import io.arti.accounting.dao.DaoOperations;
import io.arti.accounting.dao.mapper.accountstree.FirstLevelMapper;
import io.arti.accounting.dao.mapper.accountstree.SecondLevelMapper;
import io.arti.accounting.model.accountstree.FirstLevel;
import io.arti.accounting.model.accountstree.SecondLevel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SecondLevelDao implements DaoOperations<SecondLevel> {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private static final String CORE_SELECT = "select id, first_level_id, name_en, name_ar, balance from second_level";
    private static final String CORE_COUNT = "select count(*) from second_level";

    @Override
    public List<SecondLevel> getEntities(int size, int offset) {
        return null;
    }

    public List<SecondLevel> getEntities() {
        String sql = CORE_SELECT + " order by id";
        return jdbcTemplate.query(sql, new SecondLevelMapper());
    }

    @Override
    public List<SecondLevel> getEntities(int size, int offset, String name) {
        return null;
    }

    @Override
    public boolean isPresent(Integer id) {
        String sql = CORE_COUNT + " where id = ?";
        Integer result = jdbcTemplate.queryForObject(sql, Integer.class, id);

        return result != null && result > 0;
    }

    public Integer getParentId(Integer id) {
        String sql = "select first_level_id from second_level where id = ?";
        return jdbcTemplate.queryForObject(sql, Integer.class, id);
    }

    @Override
    public SecondLevel getById(Integer id) {
        String sql = CORE_SELECT + " where id=?";
        return jdbcTemplate.queryForObject(sql, new SecondLevelMapper(), new Object[]{id});
    }

    public List<SecondLevel> getByName(String name) {
        String sql = CORE_SELECT + " where name_en ILIKE '%" + name + "%' or name_ar ILIKE '%" + name + "%'";
        return jdbcTemplate.query(sql, new SecondLevelMapper());
    }

    public List<SecondLevel> getByParent(Integer parentId) {
        String sql = CORE_SELECT + " where first_level_id=?";
        return jdbcTemplate.query(sql, new SecondLevelMapper(), parentId);
    }

    @Override
    public SecondLevel insertEntity(SecondLevel secondLevel) {
        return null;
    }

    @Override
    public void updateEntity(SecondLevel secondLevel) {

    }

    @Override
    public void deleteEntity(Integer id) {

    }
}
