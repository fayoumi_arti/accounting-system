package io.arti.accounting.dao.mapper.accountstree;

import io.arti.accounting.model.accountstree.AccountBalance;
import io.arti.accounting.model.general.enums.CRDR;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.OffsetDateTime;

public class AccountBalanceMapper implements RowMapper<AccountBalance> {
    @Override
    public AccountBalance mapRow(ResultSet rs, int rowNum) throws SQLException {
        AccountBalance accountBalance = new AccountBalance();
        accountBalance.setId(rs.getInt("id"));
        accountBalance.setVoucherId(rs.getInt("voucher_id"));
        accountBalance.setBranchId(rs.getInt("branch_id"));
        accountBalance.setAccountId(rs.getInt("account_id"));
        accountBalance.setThirdLevelId(rs.getInt("third_level_id"));
        accountBalance.setSecondLevelId(rs.getInt("second_level_id"));
        accountBalance.setFirstLevelId(rs.getInt("first_level_id"));
        accountBalance.setCurrencyId(rs.getInt("currency_id"));
        accountBalance.setAmount(rs.getBigDecimal("amount"));
        accountBalance.setEquivalence(rs.getBigDecimal("equivalence"));
        accountBalance.setCrdr(CRDR.valueOf(rs.getString("cr_dr")));
        accountBalance.setDateTime(rs.getObject("date_time", OffsetDateTime.class));
        accountBalance.setPreviousBalance(rs.getBigDecimal("previous_balance"));
        accountBalance.setCurrentBalance(rs.getBigDecimal("current_balance"));
        accountBalance.setCreatedBy(rs.getInt("created_by"));
        accountBalance.setUpdatedBy(rs.getInt("updated_by"));
        accountBalance.setCreatedAt(rs.getObject("created_at", OffsetDateTime.class));
        accountBalance.setUpdatedAt(rs.getObject("updated_at", OffsetDateTime.class));
        return accountBalance;
    }
}
