package io.arti.accounting.dao.mapper.accountstree;

import io.arti.accounting.model.accountstree.SecondLevel;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SecondLevelMapper implements RowMapper<SecondLevel> {
    @Override
    public SecondLevel mapRow(ResultSet resultSet, int i) throws SQLException {

        SecondLevel secondLevel = new SecondLevel();

        secondLevel.setId(resultSet.getInt("id"));
        secondLevel.setFirstLevelId(resultSet.getInt("first_level_id"));
        secondLevel.setEnName(resultSet.getString("name_en"));
        secondLevel.setArName(resultSet.getString("name_ar"));
        secondLevel.setBalance(resultSet.getFloat("balance"));

        return secondLevel;
    }
}
