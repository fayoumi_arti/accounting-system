package io.arti.accounting.dao.mapper.general;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import io.arti.accounting.model.general.Settings;
import io.arti.accounting.model.vouchers.Attachments;
import io.arti.accounting.service.response.error.exception.server.ServerErrorException;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Map;

public class SettingsMapper implements RowMapper<Settings> {

    ObjectMapper mapper = new ObjectMapper();

    @Override
    public Settings mapRow(ResultSet resultSet, int rowNum) throws SQLException {

        Settings settings = new Settings();

        settings.setId(resultSet.getInt("id"));
        settings.setKey(resultSet.getString("key"));

        try {
            settings.setValue(mapper.readValue(resultSet.getString("value"), new TypeReference<List<Map<String, String>>>() {
            }));

        } catch (JsonProcessingException e) {
            throw new ServerErrorException(String.format("Failed to parse attachments array  due to: %s", e.getMessage()), e);
        }

        settings.setCreatedBy(resultSet.getInt("created_by"));
        settings.setUpdatedBy(resultSet.getInt("updated_by"));
        settings.setCreatedAt(resultSet.getObject("created_at", OffsetDateTime.class));
        settings.setUpdatedAt(resultSet.getObject("updated_at", OffsetDateTime.class));

        return settings;
    }
}
