package io.arti.accounting.dao.mapper.accountstree;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.arti.accounting.model.accountstree.FourthLevel;
import io.arti.accounting.model.accountstree.enums.VirtualType;
import io.arti.accounting.model.general.enums.CRDR;
import io.arti.accounting.service.response.error.exception.server.ServerErrorException;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.OffsetDateTime;
import java.util.List;

public class FourthLevelMapper implements RowMapper<FourthLevel> {
    ObjectMapper mapper = new ObjectMapper();
    @Override
    public FourthLevel mapRow(ResultSet resultSet, int i) throws SQLException {
        FourthLevel fourthLevel = new FourthLevel();

        fourthLevel.setId(resultSet.getInt("id"));
        fourthLevel.setEnName(resultSet.getString("item_name_en"));
        fourthLevel.setArName(resultSet.getString("item_name_ar"));
        fourthLevel.setThirdLevelId((resultSet.getInt("third_level_id")));
        fourthLevel.setDefaultCurrencyId((resultSet.getInt("default_currency_id")));
        fourthLevel.setGovernorate((resultSet.getString("governorate")));
        fourthLevel.setCity((resultSet.getString("city")));
        fourthLevel.setCommunicationInfo((resultSet.getString("communication_info")));
        fourthLevel.setAddress((resultSet.getString("address")));
        fourthLevel.setFirstPhoneNumber((resultSet.getString("first_phone_number")));
        fourthLevel.setSecondPhoneNumber((resultSet.getString("second_phone_number")));
        fourthLevel.setFax((resultSet.getString("fax")));
        fourthLevel.setFirstEmail((resultSet.getString("first_email")));
        fourthLevel.setSecondEmail((resultSet.getString("second_email")));
        fourthLevel.setPersonInCharge((resultSet.getString("person_in_charge")));
        fourthLevel.setCrdr(CRDR.valueOf(resultSet.getString("cr_dr")));
        fourthLevel.setBalance(resultSet.getFloat("balance"));
        fourthLevel.setCreatedBy(resultSet.getInt("created_by"));
        fourthLevel.setUpdatedBy(resultSet.getInt("updated_by"));
        fourthLevel.setCreatedAt(resultSet.getObject("created_at", OffsetDateTime.class));
        fourthLevel.setUpdatedAt(resultSet.getObject("updated_at", OffsetDateTime.class));

        try {
            fourthLevel.setAllowedCurrencies(mapper.readValue(resultSet.getString("allowed_currencies"), new TypeReference<List<Integer>>() {
            }));
        } catch (JsonProcessingException e) {
            throw new ServerErrorException(String.format("Failed to parse allowed_currencies array  due to: %s", e.getMessage()), e);
        }

        return fourthLevel;
    }
}
