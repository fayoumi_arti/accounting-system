package io.arti.accounting.dao.mapper.vouchers;

import io.arti.accounting.model.vouchers.PaymentReceiptVoucher;
import io.arti.accounting.model.vouchers.enums.VoucherStatus;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.OffsetDateTime;

public class PaymentVoucherMapper implements RowMapper<PaymentReceiptVoucher> {
    @Override
    public PaymentReceiptVoucher mapRow(ResultSet rs, int rowNum) throws SQLException {
        PaymentReceiptVoucher voucher = new PaymentReceiptVoucher();

        voucher.setId(rs.getInt("id"));
        voucher.setVoucherId(rs.getInt("voucher_id"));
        voucher.setDebitAccountId(rs.getInt("debit_account_id"));
        voucher.setCreditAccountId(rs.getInt("credit_account_id"));
        voucher.setDebitBalanceId(rs.getInt("debit_balance_id"));
        voucher.setCreditBalanceId(rs.getInt("credit_balance_id"));
        voucher.setCurrencyId(rs.getInt("currency_id"));
        voucher.setEvaluationPrice(rs.getFloat("evaluation_price"));
        voucher.setAmount(rs.getBigDecimal("amount"));
        voucher.setEquivalence(rs.getBigDecimal("equivalence"));
        voucher.setNotes(rs.getString("notes"));
        voucher.setCreditAccountPreviousBalance(rs.getFloat("credit_account_previous_balance"));
        voucher.setDebitAccountPreviousBalance(rs.getFloat("debit_account_previous_balance"));
        voucher.setRecipientName(rs.getString("recipient_name"));
        voucher.setVoucherStatus(VoucherStatus.valueOf(rs.getString("voucher_status")));
        voucher.setBranchId(rs.getInt("branch_id"));
        voucher.setCreatedBy(rs.getInt("created_by"));
        voucher.setUpdatedBy(rs.getInt("updated_by"));
        voucher.setCreatedAt(rs.getObject("created_at", OffsetDateTime.class));
        voucher.setUpdatedAt(rs.getObject("updated_at", OffsetDateTime.class));
        voucher.setDateTime(rs.getObject("date_time", OffsetDateTime.class));

        return voucher;
    }
}
