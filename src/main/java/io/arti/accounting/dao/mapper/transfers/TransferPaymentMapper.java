package io.arti.accounting.dao.mapper.transfers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.arti.accounting.model.transfers.TransferPayment;
import io.arti.accounting.model.transfers.enums.TransferDestination;
import io.arti.accounting.model.transfers.enums.TransferStatus;
import io.arti.accounting.model.vouchers.Attachments;
import io.arti.accounting.service.response.error.exception.server.ServerErrorException;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.OffsetDateTime;
import java.util.List;

public class TransferPaymentMapper implements RowMapper<TransferPayment> {
    ObjectMapper mapper = new ObjectMapper();



    @Override
    public TransferPayment mapRow(ResultSet rs, int rowNum) throws SQLException {
        TransferPayment transferPayment = new TransferPayment();
        transferPayment.setId(rs.getInt("id"));
        transferPayment.setVoucherBaseId(rs.getInt("voucher_base_id"));
        transferPayment.setDateTime(rs.getObject("date_time", OffsetDateTime.class));
        transferPayment.setSenderAccountId(rs.getInt("sender_account_id"));
        transferPayment.setPayerAccountId(rs.getInt("payer_account_id"));
        transferPayment.setBranchId(rs.getInt("branch_id"));
        transferPayment.setDestination(TransferDestination.valueOf(rs.getString("destination")));
        transferPayment.setCurrencyId(rs.getInt("currency_id"));
        transferPayment.setAmount(rs.getFloat("amount"));
        transferPayment.setPaidCommissionRatio(rs.getFloat("paid_commission_ratio"));
        transferPayment.setCommissionRatio(rs.getFloat("commission_ratio"));
        transferPayment.setReceivedCommission(rs.getFloat("received_commission"));
        transferPayment.setPaidCommission(rs.getFloat("paid_commission"));
        transferPayment.setBeneficiaryName(rs.getString("beneficiary_name"));
        transferPayment.setSenderName(rs.getString("sender_name"));
        transferPayment.setPhone(rs.getString("phone"));
        transferPayment.setNotes(rs.getString("notes"));
        transferPayment.setCountry(rs.getString("country"));
        transferPayment.setStatus(TransferStatus.valueOf(rs.getString("status")));
        transferPayment.setRecipientName(rs.getString("recipient_name"));
        transferPayment.setPaymentDate(rs.getObject("payment_date", OffsetDateTime.class));
        transferPayment.setCreatedBy(rs.getInt("created_by"));
        transferPayment.setUpdatedBy(rs.getInt("updated_by"));
        transferPayment.setCreatedAt(rs.getObject("created_at", OffsetDateTime.class));
        transferPayment.setUpdatedAt(rs.getObject("updated_at", OffsetDateTime.class));

        try {
                transferPayment.setAttachments(mapper.readValue(rs.getString("attachments"), new TypeReference<List<Attachments>>() {

                }));
        } catch (JsonProcessingException e) {
            throw new ServerErrorException(String.format("Failed to parse attachments array  due to: %s", e.getMessage()), e);
        }

        return transferPayment;
    }
}
