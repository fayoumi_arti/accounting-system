package io.arti.accounting.dao.mapper.accountstree;

import io.arti.accounting.model.accountstree.FirstLevel;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class FirstLevelMapper implements RowMapper<FirstLevel> {
    @Override
    public FirstLevel mapRow(ResultSet resultSet, int i) throws SQLException {

        FirstLevel firstLevel = new FirstLevel();

        firstLevel.setId(resultSet.getInt("id"));
        firstLevel.setEnName(resultSet.getString("name_en"));
        firstLevel.setArName(resultSet.getString("name_ar"));
        firstLevel.setBalance(resultSet.getFloat("balance"));

        return firstLevel;
    }
}
