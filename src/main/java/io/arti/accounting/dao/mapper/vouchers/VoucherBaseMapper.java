package io.arti.accounting.dao.mapper.vouchers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.arti.accounting.model.useractions.enums.SubmissionType;
import io.arti.accounting.model.vouchers.Attachments;
import io.arti.accounting.model.vouchers.VoucherBase;
import io.arti.accounting.model.vouchers.enums.VoucherStatus;
import io.arti.accounting.model.vouchers.enums.VoucherType;
import io.arti.accounting.service.response.error.exception.server.ServerErrorException;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.OffsetDateTime;
import java.util.List;

public class VoucherBaseMapper implements RowMapper<VoucherBase> {
    ObjectMapper mapper = new ObjectMapper();
    @Override
    public VoucherBase mapRow(ResultSet resultSet, int i) throws SQLException {

        VoucherBase voucherBase = new VoucherBase();

        voucherBase.setId(resultSet.getInt("id"));
        voucherBase.setDateTime(resultSet.getObject("date_time", OffsetDateTime.class));
        voucherBase.setVoucherId(resultSet.getInt("voucher_id"));
        voucherBase.setReverseId(resultSet.getInt("reverse_id"));
        voucherBase.setReferenceNo(resultSet.getString("references_no"));
        voucherBase.setModificationReason(resultSet.getString("modification_reason"));
        voucherBase.setDenyReason(resultSet.getString("deny_reason"));
        voucherBase.setVoucherType(VoucherType.valueOf(resultSet.getString("voucher_type")));
        voucherBase.setSubmissionType(SubmissionType.valueOf(resultSet.getString("submission_type")));
        voucherBase.setVoucherStatus(VoucherStatus.valueOf(resultSet.getString("voucher_status")));
        voucherBase.setBranchId(resultSet.getInt("branch_id"));
        voucherBase.setReport(resultSet.getString("report"));
        voucherBase.setEntryId(resultSet.getInt("entry_id"));
        voucherBase.setEntryDate(resultSet.getObject("entry_date", OffsetDateTime.class));
        voucherBase.setApproverId(resultSet.getInt("approver_id"));
        voucherBase.setApprovalDate(resultSet.getObject("approve_date", OffsetDateTime.class));
        voucherBase.setCreatedBy(resultSet.getInt("created_by"));
        voucherBase.setUpdatedBy(resultSet.getInt("updated_by"));
        voucherBase.setCreatedAt(resultSet.getObject("created_at", OffsetDateTime.class));
        voucherBase.setUpdatedAt(resultSet.getObject("updated_at", OffsetDateTime.class));
        try {
            voucherBase.setAttachments(mapper.readValue(resultSet.getString("attachments"), new TypeReference<List<Attachments>>() {
            }));
        } catch (JsonProcessingException e) {
            throw new ServerErrorException(String.format("Failed to parse attachments array  due to: %s", e.getMessage()), e);
        }
        return voucherBase;
    }
}
