package io.arti.accounting.dao.mapper.general;

import io.arti.accounting.model.general.Country;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.OffsetDateTime;

public class CountryMapper implements RowMapper<Country> {

    @Override
    public Country mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        Country country = new Country();

        country.setId(resultSet.getInt("id"));
        country.setNameEn(resultSet.getString("name_en"));
        country.setNameAr(resultSet.getString("name_ar"));
        country.setIso3(resultSet.getString("iso_3"));
        country.setCreatedBy(resultSet.getInt("created_by"));
        country.setUpdatedBy(resultSet.getInt("updated_by"));
        country.setCreatedAt(resultSet.getObject("created_at", OffsetDateTime.class));
        country.setUpdatedAt(resultSet.getObject("updated_at", OffsetDateTime.class));

        return country;
    }
}
