package io.arti.accounting.dao.mapper.accountstree;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.arti.accounting.model.accountstree.ThirdLevel;
import io.arti.accounting.service.response.error.exception.server.ServerErrorException;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.OffsetDateTime;
import java.util.List;

public class ThirdLevelMapper implements RowMapper<ThirdLevel> {
    ObjectMapper mapper = new ObjectMapper();
    @Override
    public ThirdLevel mapRow(ResultSet resultSet, int i) throws SQLException {

        ThirdLevel thirdLevel = new ThirdLevel();

        thirdLevel.setId(resultSet.getInt("id"));
        thirdLevel.setEnName(resultSet.getString("item_name_en"));
        thirdLevel.setArName(resultSet.getString("item_name_ar"));
        thirdLevel.setSecondLevelId(resultSet.getInt("second_level_id"));
        thirdLevel.setBalance(resultSet.getFloat("balance"));
        thirdLevel.setCreatedBy(resultSet.getInt("created_by"));
        thirdLevel.setUpdatedBy(resultSet.getInt("updated_by"));
        thirdLevel.setCreatedAt(resultSet.getObject("created_at", OffsetDateTime.class));
        thirdLevel.setUpdatedAt(resultSet.getObject("updated_at", OffsetDateTime.class));
        try {
            thirdLevel.setTypes(mapper.readValue(resultSet.getString("types"), new TypeReference<List<Integer>>() {
            }));
        } catch (JsonProcessingException e) {
            throw new ServerErrorException(String.format("Failed to parse types array  due to: %s", e.getMessage()), e);
        }
        return thirdLevel;
    }
}
