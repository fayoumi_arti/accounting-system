package io.arti.accounting.dao.mapper.general;

import io.arti.accounting.model.general.Currencies;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.OffsetDateTime;


public class CurrenciesMapper implements RowMapper<Currencies> {

    @Override
    public Currencies mapRow(ResultSet resultSet, int i) throws SQLException {

        Currencies currencies = new Currencies();

        currencies.setId(resultSet.getInt("id"));
        currencies.setReferenceId(resultSet.getInt("reference_id"));
        currencies.setEnName(resultSet.getString("en_name"));
        currencies.setArName(resultSet.getString("ar_name"));
        currencies.setIso(resultSet.getString("iso"));
        currencies.setEvaluationPrice(resultSet.getDouble("evaluation_price"));
        currencies.setCreatedBy(resultSet.getInt("created_by"));
        currencies.setUpdatedBy(resultSet.getInt("updated_by"));
        currencies.setCreatedAt(resultSet.getObject("created_at", OffsetDateTime.class));
        currencies.setUpdatedAt(resultSet.getObject("updated_at", OffsetDateTime.class));

        return currencies;
    }
}
