package io.arti.accounting.dao.mapper.accountstree;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.arti.accounting.model.accountstree.VirtualAccounts;
import io.arti.accounting.model.accountstree.enums.VirtualType;
import io.arti.accounting.service.response.error.exception.server.ServerErrorException;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.OffsetDateTime;
import java.util.List;

public class VirtualAccountsMapper implements RowMapper<VirtualAccounts> {

    ObjectMapper mapper = new ObjectMapper();

    @Override
    public VirtualAccounts mapRow(ResultSet resultSet, int rowNum) throws SQLException {


        VirtualAccounts virtualAccounts = new VirtualAccounts();

        virtualAccounts.setId(resultSet.getInt("id"));
        virtualAccounts.setAccountId(resultSet.getInt("account_id"));
        virtualAccounts.setCreatedBy(resultSet.getInt("created_by"));
        virtualAccounts.setUpdatedBy(resultSet.getInt("updated_by"));
        virtualAccounts.setCreatedAt(resultSet.getObject("created_at", OffsetDateTime.class));
        virtualAccounts.setUpdatedAt(resultSet.getObject("updated_at", OffsetDateTime.class));
        try {
            virtualAccounts.setTypes(mapper.readValue(resultSet.getString("types"), new TypeReference<List<String>>() {
            }));
        } catch (JsonProcessingException e) {
            throw new ServerErrorException(String.format("Failed to parse types array  due to: %s", e.getMessage()), e);
        }

        return virtualAccounts;
    }
}
