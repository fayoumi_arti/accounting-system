package io.arti.accounting.dao.mapper.general;

import io.arti.accounting.model.general.Profile;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.OffsetDateTime;

public class ProfileMapper implements RowMapper<Profile> {
    @Override
    public Profile mapRow(ResultSet resultSet, int rowNum) throws SQLException {

        Profile profile = new Profile();

        profile.setId(resultSet.getInt("id"));
        profile.setCurrencyId(resultSet.getInt("currency_id"));
        profile.setNameAr(resultSet.getString("name_ar"));
        profile.setNameEn(resultSet.getString("name_en"));
        profile.setCountry(resultSet.getString("country"));
        profile.setGovernorate(resultSet.getString("governorate"));
        profile.setAddressAr(resultSet.getString("address_ar"));
        profile.setAddressEn(resultSet.getString("address_en"));
        profile.setLogo(resultSet.getString("logo"));
        profile.setPhone(resultSet.getString("phone"));
        profile.setFax(resultSet.getString("fax"));
        profile.setPostalCode(resultSet.getString("postal_code"));
        profile.setPostalBox(resultSet.getString("postal_box"));
        profile.setEmail(resultSet.getString("email"));
        profile.setCreatedBy(resultSet.getInt("created_by"));
        profile.setUpdatedBy(resultSet.getInt("updated_by"));
        profile.setCreatedAt(resultSet.getObject("created_at", OffsetDateTime.class));
        profile.setUpdatedAt(resultSet.getObject("updated_at", OffsetDateTime.class));

        return profile;

    }
}
