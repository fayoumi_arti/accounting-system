package io.arti.accounting.dao.mapper.vouchers;

import io.arti.accounting.model.general.enums.CRDR;
import io.arti.accounting.model.vouchers.JournalVoucher;
import io.arti.accounting.model.vouchers.enums.VoucherStatus;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.OffsetDateTime;

public class JournalVoucherMapper implements RowMapper<JournalVoucher> {
    @Override
    public JournalVoucher mapRow(ResultSet resultSet, int rowNum) throws SQLException {

        JournalVoucher journalVoucher = new JournalVoucher();

        journalVoucher.setId(resultSet.getInt("id"));
        journalVoucher.setVoucherId(resultSet.getInt("voucher_id"));
        journalVoucher.setAccountId(resultSet.getInt("account_id"));
        journalVoucher.setBalanceId(resultSet.getInt("balance_id"));
        journalVoucher.setCurrencyId(resultSet.getInt("currency_id"));
        journalVoucher.setEvaluationPrice(resultSet.getFloat("evaluation_price"));
        journalVoucher.setAmount(resultSet.getBigDecimal("amount"));
        journalVoucher.setEquivalence(resultSet.getBigDecimal("equivalence"));
        journalVoucher.setCrdr(CRDR.valueOf(resultSet.getString("cr_dr")));
        journalVoucher.setNote(resultSet.getString("note"));
        journalVoucher.setDateTime(resultSet.getObject("date_time", OffsetDateTime.class));
        journalVoucher.setVoucherStatus(VoucherStatus.valueOf(resultSet.getString("voucher_status")));
        journalVoucher.setBranchId(resultSet.getInt("branch_id"));
        journalVoucher.setAccountPreviousBalance(resultSet.getFloat("account_previous_balance"));
        journalVoucher.setCreatedBy(resultSet.getInt("created_by"));
        journalVoucher.setUpdatedBy(resultSet.getInt("updated_by"));
        journalVoucher.setCreatedAt(resultSet.getObject("created_at", OffsetDateTime.class));
        journalVoucher.setUpdatedAt(resultSet.getObject("updated_at", OffsetDateTime.class));

        return journalVoucher;
    }
}
