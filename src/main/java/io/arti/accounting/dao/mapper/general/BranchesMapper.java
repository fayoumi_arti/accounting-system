package io.arti.accounting.dao.mapper.general;

import io.arti.accounting.model.general.Branches;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.OffsetDateTime;

public class BranchesMapper implements RowMapper<Branches> {
    @Override
    public Branches mapRow(ResultSet resultSet, int i) throws SQLException {

        Branches branches = new Branches();

        branches.setId(resultSet.getInt("id"));
        branches.setEnName(resultSet.getString("en_name"));
        branches.setArName(resultSet.getString("ar_name"));
        branches.setCreatedBy(resultSet.getInt("created_by"));
        branches.setUpdatedBy(resultSet.getInt("updated_by"));
        branches.setCreatedAt(resultSet.getObject("created_at", OffsetDateTime.class));
        branches.setUpdatedAt(resultSet.getObject("updated_at", OffsetDateTime.class));

        return branches;
    }
}
