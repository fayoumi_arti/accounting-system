package io.arti.accounting.dao.mapper.vouchers;

import io.arti.accounting.model.general.enums.CRDR;
import io.arti.accounting.model.vouchers.ExpensesAccount;
import io.arti.accounting.model.vouchers.enums.VoucherStatus;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.OffsetDateTime;

public class ExpensesAccountMapper implements RowMapper<ExpensesAccount> {
    @Override
    public ExpensesAccount mapRow(ResultSet rs, int rowNum) throws SQLException {
        ExpensesAccount account = new ExpensesAccount();
        account.setId(rs.getInt("id"));
        account.setVoucherId(rs.getInt("voucher_id"));
        account.setAccountId(rs.getInt("account_id"));
        account.setBalanceId(rs.getInt("balance_id"));
        account.setCurrencyId(rs.getInt("currency_id"));
        account.setEvaluationPrice(rs.getFloat("evaluation_price"));
        account.setAmount(rs.getBigDecimal("amount"));
        account.setEquivalence(rs.getBigDecimal("equivalence"));
        account.setCrdr(CRDR.valueOf(rs.getString("cr_dr")));
        account.setNotes(rs.getString("notes"));
        account.setDateTime(rs.getObject("date_time", OffsetDateTime.class));
        account.setVoucherStatus(VoucherStatus.valueOf(rs.getString("voucher_status")));
        account.setBranchId(rs.getInt("branch_id"));
        account.setAccountPreviousBalance(rs.getFloat("account_previous_balance"));
        account.setCreatedBy(rs.getInt("created_by"));
        account.setUpdatedBy(rs.getInt("updated_by"));
        account.setCreatedAt(rs.getObject("created_at", OffsetDateTime.class));
        account.setUpdatedAt(rs.getObject("updated_at", OffsetDateTime.class));

        return account;
    }
}
