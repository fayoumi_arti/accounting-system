package io.arti.accounting.controller.transfer;

import io.arti.accounting.dao.transfers.TransferPaymentDao;
import io.arti.accounting.model.transfers.TransferPayment;
import io.arti.accounting.model.transfers.enums.TransferStatus;
import io.arti.accounting.service.response.Response;
import io.arti.accounting.service.response.error.exception.client.BadRequestException;
import io.arti.accounting.service.response.success.CreatedResponse;
import io.arti.accounting.service.response.success.OkResponse;
import io.arti.accounting.service.transfers.TransferPaymentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping(value = "/v1/transfers")
public class TransferPaymentController {

    @Autowired
    private TransferPaymentDao transferPaymentDao;

    @Autowired
    private TransferPaymentService transferPaymentService;

    @RequestMapping(method = RequestMethod.GET)
    @Operation(summary = "Get Transfer Payments")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful return list of Transfer Payments")})
    public Response getTransferPayments(
            @RequestParam(value = "size", required = false, defaultValue = "5") @Min(value=-1) @Max(value = 25) int size,
            @RequestParam(value = "offset", required = false, defaultValue = "0") @Min(value=0) int offset) {

        if (size < 0) {
            size = Integer.MAX_VALUE;
            offset = 0;
        }
        List<TransferPayment> transferPaymentList = transferPaymentDao.getEntities(size, offset);
        return new OkResponse("Successful", transferPaymentList, transferPaymentList.size(), size, offset);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @Operation(summary = "Get Transfer Payments by Account, Name, Currency, and Status")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful return unpaid Transfer Payment Entities"),
            @ApiResponse(responseCode = "400", description = "Invalid Transfer Payment ID")
    })
    public Response getUnpaidTransfersByAccountAndCurrency(@RequestParam(value = "size", required = false, defaultValue = "5") @Min(value=-1) @Max(value = 25) int size,
                                                           @RequestParam(value = "offset", required = false, defaultValue = "0") @Min(value=0) int offset,
                                                           @RequestParam(value = "senderAccountId", required = false) Integer senderAccountId,
                                                           @RequestParam(value = "from", required = false) String from,
                                                           @RequestParam(value = "to", required = false) String to,
                                                           @RequestParam(value = "amountMax", required = false, defaultValue = "0") Float amountMax,
                                                           @RequestParam(value = "amountMin", required = false, defaultValue = "0") Float amountMin,
                                                           @RequestParam(value = "currencyId", required = false) Integer currencyId,
                                                           @RequestParam(value = "beneficiaryName", required = false) String beneficiaryName,
                                                           @RequestParam(value = "senderName", required = false) String senderName,
                                                           @RequestParam(value = "status", required = false, defaultValue = "UNPAID") String status) throws BadRequestException {

        if (size < 0) {
            size = Integer.MAX_VALUE;
            offset = 0;
        }

        List<TransferPayment> transferPaymentList = transferPaymentService.getUnpaidTransfersByAccountAndCurrency(senderAccountId, amountMax, amountMin, currencyId, beneficiaryName, senderName, status, from, to, size, offset);
        return new OkResponse("Success response", transferPaymentList, transferPaymentList.size(), size, offset);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @Operation(summary = "Get Transfer Payment by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful return Transfer Payment Entity by ID"),
            @ApiResponse(responseCode = "400", description = "Invalid Transfer Payment ID")
    })
    public Response getTransferPaymentById(@PathVariable("id") Integer id) throws BadRequestException {
        if (!transferPaymentDao.isPresent(id)) {
            throw new BadRequestException("Invalid Id");
        }
        return new OkResponse("Success response", transferPaymentDao.getById(id));
    }

    @RequestMapping(method = RequestMethod.POST)
    @Operation(summary = "Add Transfer Payment Entity")
    @ApiResponses(value = {@ApiResponse(responseCode = "201", description = "Successful adding Transfer Payment entity")})
    public Response addTransfer(@NotNull(message = "Request body cannot be null") @Valid @RequestBody TransferPayment transferPayment,
                                HttpServletRequest request) throws BadRequestException {

        return new CreatedResponse("Transfer Payment created successfully", transferPaymentService.insert(transferPayment, request.getHeader(HttpHeaders.AUTHORIZATION)));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @Operation(summary = "Update Transfer Payment Entity")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful updating Transfer Payment entity")})
    public Response updateTransferPayment(@PathVariable("id") Integer id, @NotNull(message = "Request body cannot be null") @Valid @RequestBody TransferPayment transferPayment,
                                   HttpServletRequest request) throws BadRequestException {

        if (!transferPaymentDao.isPresent(id)) {
            throw new BadRequestException("Invalid Id");
        }

        transferPayment.setId(id);
        transferPaymentService.update(transferPayment, false);
        return new OkResponse("Transfer Payment entity updated successfully", transferPaymentDao.getById(id));
    }

    @RequestMapping(value = "/payment/{id}", method = RequestMethod.PUT)
    @Operation(summary = "Pay Transfer Payment Entity")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful updating Transfer Payment entity")})
    public Response payTransferPayment(@PathVariable("id") Integer id, @NotNull(message = "Request body cannot be null") @Valid @RequestBody TransferPayment transferPayment,
                                          HttpServletRequest request) throws BadRequestException {

        if (!transferPaymentDao.isPresent(id)) {
            throw new BadRequestException("Invalid Id");
        }

        transferPayment.setId(id);
        transferPayment.setStatus(TransferStatus.PAID);
        transferPaymentService.update(transferPayment, true);
        return new OkResponse("Transfer Payment entity updated successfully", transferPaymentDao.getById(id));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @Operation(summary = "Delete Transfer Payment by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful deleted Transfer Payment Entity by ID"),
            @ApiResponse(responseCode = "400", description = "Invalid Transfer Payment ID")
    })
    public Response deleteTransferPaymentById(@PathVariable("id") Integer id) throws BadRequestException {
        if (!transferPaymentDao.isPresent(id)) {
            throw new BadRequestException("Invalid Id");
        }
        transferPaymentDao.deleteEntity(id);
        return new OkResponse("Transfer Payment deleted successfully");
    }
}
