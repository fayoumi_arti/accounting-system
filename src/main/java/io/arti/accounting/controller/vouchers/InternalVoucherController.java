package io.arti.accounting.controller.vouchers;

import io.arti.accounting.dao.vouchers.VoucherBaseDao;
import io.arti.accounting.model.vouchers.InternalVoucher;
import io.arti.accounting.service.response.Response;
import io.arti.accounting.service.response.error.exception.client.BadRequestException;
import io.arti.accounting.service.response.success.CreatedResponse;
import io.arti.accounting.service.response.success.OkResponse;
import io.arti.accounting.service.useractions.UserActionsService;
import io.arti.accounting.service.vouchers.InternalVoucherService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
@RestController
@RequestMapping("/v1/internal-voucher")
public class InternalVoucherController {
    @Autowired
    InternalVoucherService internalVoucherService;

    @Autowired
    VoucherBaseDao voucherBaseDao;

    @Autowired
    UserActionsService userActionsService;

    @GetMapping("/{id}")
    @Operation(summary = "Get Internal Voucher by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful return Payment Voucher Entity by ID"),
            @ApiResponse(responseCode = "400", description = "Invalid Currency ID")
    })
    public Response getInternalVoucherByVoucherId(@PathVariable("id") Integer id) throws BadRequestException {

        return new OkResponse("Success response", internalVoucherService.getByVoucherId(id));
    }


    @RequestMapping(method = RequestMethod.POST)
    @Operation(summary = "Add Internal Voucher Entity")
    @ApiResponses(value = {@ApiResponse(responseCode = "201", description = "Successful adding Internal Voucher entity")})
    public Response addInternalVoucher(@NotNull(message = "Request body cannot be null") @Valid @RequestBody InternalVoucher internalVoucher,
                                      HttpServletRequest request) throws BadRequestException {

        return new CreatedResponse("Internal Voucher created successfully", internalVoucherService.insertEntity(internalVoucher));
    }
    @RequestMapping(value = "/base/{id}", method = RequestMethod.DELETE)
    @Operation(summary = "Delete Voucher by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful deleted Voucher Entity by ID"),
            @ApiResponse(responseCode = "400", description = "Invalid Voucher ID")
    })
    public Response deleteVoucherById(@PathVariable("id") Integer id) throws BadRequestException {
        if (!voucherBaseDao.isPresent(id)) {
            throw new BadRequestException("Invalid Id");
        }

        userActionsService.entryActionValidate(id);
        userActionsService.entryVoucherDelete(id);
        return new OkResponse("Voucher deleted successfully");
    }

    @RequestMapping(value = "/base/{id}", method = RequestMethod.PUT)
    @Operation(summary = "Update Internal Voucher Entity")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful updating Internal Voucher entity")})
    public Response updateVoucher(@PathVariable("id") Integer id, @NotNull(message = "Request body cannot be null") @Valid @RequestBody InternalVoucher internalVoucher,
                                   HttpServletRequest request) throws BadRequestException {
        internalVoucher.getVoucherBase().setId(id);
        userActionsService.entryActionValidate(id);
        return new OkResponse("Internal Voucher entity updated successfully", internalVoucherService.updateEntry(internalVoucher, id));
    }
}
