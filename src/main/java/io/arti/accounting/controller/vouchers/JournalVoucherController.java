package io.arti.accounting.controller.vouchers;

import io.arti.accounting.dao.vouchers.VoucherBaseDao;
import io.arti.accounting.model.vouchers.JournalBaseVoucher;
import io.arti.accounting.service.response.Response;
import io.arti.accounting.service.response.error.exception.client.BadRequestException;
import io.arti.accounting.service.response.success.CreatedResponse;
import io.arti.accounting.service.response.success.OkResponse;
import io.arti.accounting.service.useractions.UserActionsService;
import io.arti.accounting.service.vouchers.JournalVoucherService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping(value = "/v1/journal-voucher")
public class JournalVoucherController {

    @Autowired
    JournalVoucherService journalVoucherService;
    @Autowired
    VoucherBaseDao voucherBaseDao;

    @Autowired
    UserActionsService userActionsService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @Operation(summary = "Get Journal Voucher by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful return Journal Voucher Entity by ID"),
            @ApiResponse(responseCode = "400", description = "Invalid Currency ID")
    })
    public Response getJournalVoucherById(@PathVariable("id") Integer id) throws BadRequestException {

        return new OkResponse("Success response", journalVoucherService.getByVoucherId(id));
    }


    @RequestMapping(method = RequestMethod.POST)
    @Operation(summary = "Add Journal Voucher Entity")
    @ApiResponses(value = {@ApiResponse(responseCode = "201", description = "Successful adding Journal Voucher entity")})
    public Response addJournalVoucher(@NotNull(message = "Request body cannot be null") @Valid @RequestBody JournalBaseVoucher journalBaseVoucher,
                              HttpServletRequest request) throws BadRequestException {

        return new CreatedResponse("Journal Voucher created successfully", journalVoucherService.insertEntity(journalBaseVoucher, request.getHeader(HttpHeaders.AUTHORIZATION)));
    }

    @RequestMapping(value = "/base/{id}", method = RequestMethod.DELETE)
    @Operation(summary = "Delete Voucher by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful deleted Voucher Entity by ID"),
            @ApiResponse(responseCode = "400", description = "Invalid Voucher ID")
    })
    public Response deleteCurrencyById(@PathVariable("id") Integer id) throws BadRequestException {
        if (!voucherBaseDao.isPresent(id)) {
            throw new BadRequestException("Invalid Id");
        }

        userActionsService.entryActionValidate(id);
        userActionsService.entryVoucherDelete(id);
        return new OkResponse("Voucher deleted successfully");
    }

    @RequestMapping(value = "/base/{id}", method = RequestMethod.PUT)
    @Operation(summary = "Update Journal Voucher Entity")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful updating Journal Voucher entity")})
    public Response updateCurrency(@PathVariable("id") Integer id, @NotNull(message = "Request body cannot be null") @Valid @RequestBody JournalBaseVoucher journalBaseVoucher,
                                   HttpServletRequest request) throws BadRequestException {
        journalBaseVoucher.getVoucherBase().setId(id);
        userActionsService.entryActionValidate(id);
        return new OkResponse("Journal Voucher entity updated successfully", journalVoucherService.updateEntry(journalBaseVoucher, id));
    }
}
