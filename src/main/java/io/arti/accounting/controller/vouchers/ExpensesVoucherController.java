package io.arti.accounting.controller.vouchers;

import io.arti.accounting.dao.vouchers.VoucherBaseDao;
import io.arti.accounting.model.general.Currencies;
import io.arti.accounting.model.vouchers.ExpensesVoucher;
import io.arti.accounting.model.vouchers.enums.VoucherStatus;
import io.arti.accounting.service.response.Response;
import io.arti.accounting.service.response.error.exception.client.BadRequestException;
import io.arti.accounting.service.response.success.CreatedResponse;
import io.arti.accounting.service.response.success.OkResponse;
import io.arti.accounting.service.useractions.UserActionsService;
import io.arti.accounting.service.vouchers.ExpensesVoucherService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping(value = "/v1/expenses-voucher")
public class ExpensesVoucherController {
    @Autowired
    ExpensesVoucherService expensesVoucherService;

    @Autowired
    VoucherBaseDao voucherBaseDao;

    @Autowired
    UserActionsService userActionsService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @Operation(summary = "Get Expenses Voucher by Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful return Journal Voucher Entity by ID"),
            @ApiResponse(responseCode = "400", description = "Invalid Currency ID")
    })
    public Response getExpensesVoucherById(@PathVariable("id") Integer id) throws BadRequestException {

        return new OkResponse("Success response", expensesVoucherService.getByVoucherId(id));
    }


    @RequestMapping(method = RequestMethod.POST)
    @Operation(summary = "Add Expenses Voucher Entity")
    @ApiResponses(value = {@ApiResponse(responseCode = "201", description = "Successful adding Expenses Voucher entity")})
    public Response addExpensesVoucher(@NotNull(message = "Request body cannot be null") @Valid @RequestBody ExpensesVoucher expensesVoucher,
                                      HttpServletRequest request) throws BadRequestException {
        return new CreatedResponse("Expenses Voucher created successfully", expensesVoucherService.insertEntity(expensesVoucher, request.getHeader(HttpHeaders.AUTHORIZATION)));
    }

    @RequestMapping(value = "/base/{id}", method = RequestMethod.DELETE)
    @Operation(summary = "Delete Voucher by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful deleted Voucher Entity by ID"),
            @ApiResponse(responseCode = "400", description = "Invalid Voucher ID")
    })
    public Response deleteCurrencyById(@PathVariable("id") Integer id) throws BadRequestException {
        if (!voucherBaseDao.isPresent(id)) {
            throw new BadRequestException("Invalid Id");
        }
        userActionsService.entryActionValidate(id);
        userActionsService.entryVoucherDelete(id);
        return new OkResponse("Voucher deleted successfully");
    }

    @RequestMapping(value = "/base/{id}", method = RequestMethod.PUT)
    @Operation(summary = "Update Expenses Voucher Entity")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful updating Expenses Voucher entity")})
    public Response updateExpensesVoucher(@PathVariable("id") Integer id, @NotNull(message = "Request body cannot be null") @Valid @RequestBody ExpensesVoucher expensesVoucher,
                                   HttpServletRequest request) throws BadRequestException {
        expensesVoucher.getVoucherBase().setId(id);
        userActionsService.entryActionValidate(id);
        return new OkResponse("Expenses Voucher entity updated successfully", expensesVoucherService.updateEntry(expensesVoucher, id));
    }
}
