package io.arti.accounting.controller.vouchers;

import io.arti.accounting.dao.vouchers.VoucherBaseDao;
import io.arti.accounting.model.vouchers.JournalBaseVoucher;
import io.arti.accounting.model.vouchers.PaymentReceiptVoucher;
import io.arti.accounting.model.vouchers.enums.VoucherStatus;
import io.arti.accounting.service.response.Response;
import io.arti.accounting.service.response.error.exception.client.BadRequestException;
import io.arti.accounting.service.response.success.CreatedResponse;
import io.arti.accounting.service.response.success.OkResponse;
import io.arti.accounting.service.useractions.UserActionsService;
import io.arti.accounting.service.vouchers.PaymentVoucherService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/v1/payment-voucher")
public class PaymentVoucherController {
    @Autowired
    PaymentVoucherService paymentVoucherService;

    @Autowired
    VoucherBaseDao voucherBaseDao;

    @Autowired
    UserActionsService userActionsService;

    @GetMapping("/{id}")
    @Operation(summary = "Get Payment Voucher by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful return Payment Voucher Entity by ID"),
            @ApiResponse(responseCode = "400", description = "Invalid Currency ID")
    })
    public Response getPaymentVoucherByVoucherId(@PathVariable("id") Integer id) throws BadRequestException {

        return new OkResponse("Success response", paymentVoucherService.getByVoucherId(id));
    }


    @RequestMapping(method = RequestMethod.POST)
    @Operation(summary = "Add Payment Voucher Entity")
    @ApiResponses(value = {@ApiResponse(responseCode = "201", description = "Successful adding Payment Voucher entity")})
    public Response addPaymentVoucher(@NotNull(message = "Request body cannot be null") @Valid @RequestBody PaymentReceiptVoucher paymentVoucher,
                                      HttpServletRequest request) throws BadRequestException {

        return new CreatedResponse("Payment Voucher created successfully", paymentVoucherService.insertEntity(paymentVoucher));
    }
    @RequestMapping(value = "/base/{id}", method = RequestMethod.DELETE)
    @Operation(summary = "Delete Voucher by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful deleted Voucher Entity by ID"),
            @ApiResponse(responseCode = "400", description = "Invalid Voucher ID")
    })
    public Response deleteCurrencyById(@PathVariable("id") Integer id) throws BadRequestException {
        if (!voucherBaseDao.isPresent(id)) {
            throw new BadRequestException("Invalid Id");
        }

        userActionsService.entryActionValidate(id);
        userActionsService.entryVoucherDelete(id);
        return new OkResponse("Voucher deleted successfully");
    }

    @RequestMapping(value = "/base/{id}", method = RequestMethod.PUT)
    @Operation(summary = "Update Payment Voucher Entity")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful updating Payment Voucher entity")})
    public Response updateCurrency(@PathVariable("id") Integer id, @NotNull(message = "Request body cannot be null") @Valid @RequestBody PaymentReceiptVoucher paymentReceiptVoucher,
                                   HttpServletRequest request) throws BadRequestException {
        paymentReceiptVoucher.getVoucherBase().setId(id);
        userActionsService.entryActionValidate(id);
        return new OkResponse("Payment Voucher entity updated successfully", paymentVoucherService.updateEntry(paymentReceiptVoucher, id));
    }
}
