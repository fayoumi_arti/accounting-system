package io.arti.accounting.controller.vouchers;

import io.arti.accounting.dao.vouchers.VoucherBaseDao;
import io.arti.accounting.model.vouchers.PaymentReceiptVoucher;
import io.arti.accounting.model.vouchers.enums.VoucherStatus;
import io.arti.accounting.service.response.Response;
import io.arti.accounting.service.response.error.exception.client.BadRequestException;
import io.arti.accounting.service.response.success.CreatedResponse;
import io.arti.accounting.service.response.success.OkResponse;
import io.arti.accounting.service.useractions.UserActionsService;
import io.arti.accounting.service.vouchers.ReceiptVoucherService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/v1/receipt-voucher")
public class ReceiptVoucherController {
    @Autowired
    ReceiptVoucherService receiptVoucherService;

    @Autowired
    VoucherBaseDao voucherBaseDao;

    @Autowired
    UserActionsService userActionsService;

    @GetMapping("/{id}")
    @Operation(summary = "Get Receipt Voucher by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful return Receipt Voucher Entity by ID"),
            @ApiResponse(responseCode = "400", description = "Invalid Currency ID")
    })
    public Response getReceiptVoucherByVoucherId(@PathVariable("id") Integer id) throws BadRequestException {

        return new OkResponse("Success response", receiptVoucherService.getByVoucherId(id));
    }


    @RequestMapping(method = RequestMethod.POST)
    @Operation(summary = "Add Receipt Voucher Entity")
    @ApiResponses(value = {@ApiResponse(responseCode = "201", description = "Successful adding Receipt Voucher entity")})
    public Response addReceiptVoucher(@NotNull(message = "Request body cannot be null") @Valid @RequestBody PaymentReceiptVoucher receiptVoucher,
                                      HttpServletRequest request) throws BadRequestException {

        return new CreatedResponse("Receipt Voucher created successfully", receiptVoucherService.insertEntity(receiptVoucher));
    }

    @RequestMapping(value = "/base/{id}", method = RequestMethod.DELETE)
    @Operation(summary = "Delete Voucher by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful deleted Voucher Entity by ID"),
            @ApiResponse(responseCode = "400", description = "Invalid Voucher ID")
    })
    public Response deleteCurrencyById(@PathVariable("id") Integer id) throws BadRequestException {
        if (!voucherBaseDao.isPresent(id)) {
            throw new BadRequestException("Invalid Id");
        }
        userActionsService.entryActionValidate(id);
        userActionsService.entryVoucherDelete(id);
        return new OkResponse("Voucher deleted successfully");
    }

    @RequestMapping(value = "/base/{id}", method = RequestMethod.PUT)
    @Operation(summary = "Update Receipt Voucher Entity")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful updating Receipt Voucher entity")})
    public Response updateCurrency(@PathVariable("id") Integer id, @NotNull(message = "Request body cannot be null") @Valid @RequestBody PaymentReceiptVoucher receiptVoucher,
                                   HttpServletRequest request) throws BadRequestException {
        receiptVoucher.getVoucherBase().setId(id);
        userActionsService.entryActionValidate(id);
        return new OkResponse("Receipt Voucher entity updated successfully", receiptVoucherService.updateEntry(receiptVoucher, id));
    }

}
