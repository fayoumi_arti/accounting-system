package io.arti.accounting.controller.vouchers;

import io.arti.accounting.dao.vouchers.VoucherBaseDao;
import io.arti.accounting.model.vouchers.VoucherListing;
import io.arti.accounting.model.vouchers.enums.VoucherStatus;
import io.arti.accounting.model.vouchers.enums.VoucherType;
import io.arti.accounting.service.response.Response;
import io.arti.accounting.service.response.success.OkResponse;
import io.arti.accounting.service.useractions.UserActionsService;
import io.arti.accounting.service.vouchers.VoucherListingService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.List;



@RestController
@RequestMapping(value = "/v1/voucher-base")
public class VoucherBaseController {

    @Autowired
    VoucherListingService voucherListingService;

    @Autowired
    UserActionsService userActionsService;

    @Autowired
    VoucherBaseDao voucherBaseDao;

    @RequestMapping(method = RequestMethod.GET)
    @Operation(summary = "Get Vouchers")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful return list of Vouchers")})
    public Response getJournalVouchers(
            @RequestParam(value = "size", required = false, defaultValue = "5") @Min(value=-1) @Max(value = 25) int size,
            @RequestParam(value = "offset", required = false, defaultValue = "0") @Min(value=0) int offset,
            @RequestParam(value="type", required = true) VoucherType type,
            @RequestParam(value="status", required = false) VoucherStatus voucherStatus) {

        if (size < 0) {
            size = Integer.MAX_VALUE;
            offset = 0;
        }
        List<VoucherListing> voucherListing = voucherListingService.getVouchers(size, offset, type, voucherStatus);

        return new OkResponse("Successful", voucherListing, voucherListing.size(), size, offset);
    }

}
