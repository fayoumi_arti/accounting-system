package io.arti.accounting.controller.reports;

import io.arti.accounting.service.response.Response;
import io.arti.accounting.service.response.success.OkResponse;
import io.arti.accounting.service.vouchers.AccountStatementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/account-statement")
public class AccountStatementController {
    @Autowired
    AccountStatementService accountStatementService;
    @GetMapping("/{accountId}")
    public Response getAccountStatement(@PathVariable Integer accountId,
                                        @RequestParam(value = "from") String from, @RequestParam(value = "to") String to,
                                        @RequestParam(value = "currency") Integer currencyId,
                                        @RequestParam(value = "branch", required = false) Integer branchId) {

        return new OkResponse("success", accountStatementService.getAccountStatement(accountId, currencyId, from, to, branchId));
    }
}
