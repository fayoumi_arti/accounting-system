package io.arti.accounting.controller.reports;

import io.arti.accounting.service.response.Response;
import io.arti.accounting.service.response.success.OkResponse;
import io.arti.accounting.service.vouchers.GeneralLedgerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/general-ledger")
public class GeneralLedgerController {
    @Autowired
    GeneralLedgerService generalLedgerService;
    @GetMapping()
    public Response getGeneralLedger( @RequestParam(value = "from") String from,
                                      @RequestParam(value = "to") String to,
                                      @RequestParam(value = "branch") Integer branchId) {

        return new OkResponse("success", generalLedgerService.getGeneralLedger(from, to, branchId));
    }
}
