package io.arti.accounting.controller.reports;

import io.arti.accounting.service.account.AccountBalanceSheetService;
import io.arti.accounting.service.response.Response;
import io.arti.accounting.service.response.success.OkResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/v1/balance-sheet")
public class BalanceSheetController {

    @Autowired
    private AccountBalanceSheetService accountBalanceSheetService;

    @RequestMapping(method = RequestMethod.GET)
    @Operation(summary = "Get Account Balance Sheet")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful return list of accounts balance.")})
    public Response getBalanceSheet(@RequestParam(value = "from", required = false) String from,
                                    @RequestParam(value = "to") String to,
                                    @RequestParam(value = "branch") Integer branchId) {

        return new OkResponse("success", accountBalanceSheetService.getAccountsLevelBalancesSheet(branchId, from, to, false));
    }

    @RequestMapping(value = "/income-statement", method = RequestMethod.GET)
    @Operation(summary = "Get Income Statement")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful return list of income statements.")})
    public Response getIncomeStatement(@RequestParam(value = "from", required = false) String from,
                                       @RequestParam(value = "to") String to,
                                       @RequestParam(value = "branch") Integer branchId) {

        return new OkResponse("success", accountBalanceSheetService.getAccountsLevelBalancesSheet(branchId, from, to, true));
    }

}
