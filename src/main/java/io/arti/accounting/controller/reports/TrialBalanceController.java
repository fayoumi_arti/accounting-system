package io.arti.accounting.controller.reports;

import io.arti.accounting.service.response.Response;
import io.arti.accounting.service.response.success.OkResponse;
import io.arti.accounting.service.vouchers.TrialBalanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotEmpty;

@RestController
@RequestMapping("/v1/trial-balance")
public class TrialBalanceController {
    @Autowired
    TrialBalanceService trialBalanceService;
    @GetMapping("/date-range")
    public Response getTrialBalanceFromToDate(@RequestParam(value = "from") @NotEmpty String from,@RequestParam(value = "to") @NotEmpty String to,
                                              @RequestParam(value = "branch", required = false) Integer branchId) {

        return new OkResponse("success", trialBalanceService.getTrialBalanceFromToDate(from, to, branchId));
    }

    @GetMapping("/to-date")
    public Response getTrialBalanceToDate(@RequestParam(value = "to") @NotEmpty String to,
                                          @RequestParam(value = "branch", required = false) Integer branchId) {

        return new OkResponse("success", trialBalanceService.getTrialBalanceToDate(to,branchId));
    }
}
