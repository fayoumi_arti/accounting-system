package io.arti.accounting.controller.Inqueries;

import io.arti.accounting.dao.vouchers.VoucherBaseDao;
import io.arti.accounting.model.useractions.SubmissionRequest;
import io.arti.accounting.model.vouchers.VoucherBase;
import io.arti.accounting.service.Inquiries.IndexerService;
import io.arti.accounting.service.Inquiries.*;
import io.arti.accounting.service.response.Response;
import io.arti.accounting.service.response.error.exception.client.BadRequestException;
import io.arti.accounting.service.response.success.CreatedResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.IOException;

@RestController
@RequestMapping(value = "/v1/voucher-inquery")
public class VoucherInqueries {

    @Autowired
    IndexerService indexerService;

    @Autowired
    VoucherBaseDao voucherBaseDao;

    @RequestMapping(value = "/index-all", method = RequestMethod.GET)
    @Operation(summary = "voucher indexer")
    @ApiResponses(value = {@ApiResponse(responseCode = "201", description = "Successful indexed vouchers")})
    public Response indexAll(HttpServletRequest request) throws BadRequestException, IOException {
        indexerService.indexAllVouchers();
        return new CreatedResponse("Vouchers indexed successfully");
    }
}
