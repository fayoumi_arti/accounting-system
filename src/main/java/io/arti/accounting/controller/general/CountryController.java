package io.arti.accounting.controller.general;

import io.arti.accounting.dao.general.CountryDao;
import io.arti.accounting.model.general.Country;
import io.arti.accounting.service.response.Response;
import io.arti.accounting.service.response.error.exception.client.BadRequestException;
import io.arti.accounting.service.response.success.CreatedResponse;
import io.arti.accounting.service.response.success.OkResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping(value = "/v1/countries")
public class CountryController {

    @Autowired
    CountryDao countryDao;

    @RequestMapping(method = RequestMethod.GET)
    @Operation(summary = "Get Countries")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful return list of Countries")})
    public Response getCountries(
            @RequestParam(value = "size", required = false, defaultValue = "5") @Min(value = -1) @Max(value = 25) int size,
            @RequestParam(value = "offset", required = false, defaultValue = "0") @Min(value = 0) int offset) {

        if (size < 0) {
            size = Integer.MAX_VALUE;
            offset = 0;
        }

        List<Country> countriesList = countryDao.getEntities(size, offset);
        return new OkResponse("Successful", countriesList, countriesList.size(), size, offset);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @Operation(summary = "Get Country by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful return of Country Entity by ID"),
            @ApiResponse(responseCode = "400", description = "Invalid Country ID")
    })
    public Response getCountryById(@PathVariable("id") Integer id) throws BadRequestException {
        if (!countryDao.isPresent(id)) {
            throw new BadRequestException("Invalid Id");
        }

        return new OkResponse("Success response", countryDao.getById(id));
    }

    @RequestMapping(method = RequestMethod.POST)
    @Operation(summary = "Add Country Entity")
    @ApiResponses(value = {@ApiResponse(responseCode = "201", description = "Successful adding of country entity")})
    public Response addCountry(@NotNull(message = "Request body cannot be null") @Valid @RequestBody Country country)
            throws BadRequestException {

        if(countryDao.isIsoPresent(country.getIso3())) {
            throw new BadRequestException("Ios3 code cannot be duplicated");
        }

        return new CreatedResponse("Country added successfully", countryDao.insertEntity(country));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @Operation(summary = "Update Country Entity")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful updating of country entity")})
    public Response updateCountry(
            @PathVariable("id") Integer id,
            @NotNull(message = "Request body cannot be null") @Valid @RequestBody Country country)
            throws BadRequestException {

        if (!countryDao.isPresent(id)) {
            throw new BadRequestException("Invalid Id");
        }

        country.setId(id);
        countryDao.updateEntity(country);
        return new OkResponse("Country entity updated successfully", countryDao.getById(id));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @Operation(summary = "Delete Country by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful deleting of country entity"),
            @ApiResponse(responseCode = "400", description = "Invalid Country ID")
    })
    public Response deleteCountryById(@PathVariable("id") Integer id) throws BadRequestException {
        if (!countryDao.isPresent(id)) {
            throw new BadRequestException("Invalid Id");
        }

        countryDao.deleteEntity(id);
        return new OkResponse("Country deleted successfully");
    }

}
