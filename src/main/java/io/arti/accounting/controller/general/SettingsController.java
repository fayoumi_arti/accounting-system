package io.arti.accounting.controller.general;

import io.arti.accounting.dao.general.SettingsDao;
import io.arti.accounting.model.general.Settings;
import io.arti.accounting.service.response.Response;
import io.arti.accounting.service.response.error.exception.client.BadRequestException;
import io.arti.accounting.service.response.success.CreatedResponse;
import io.arti.accounting.service.response.success.OkResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping(value = "/v1/settings")
public class SettingsController {

    @Autowired
    SettingsDao settingsDao;

    @RequestMapping(method = RequestMethod.GET)
    @Operation(summary = "Get Settings")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful return list of Settings")})
    public Response getSettings(
            @RequestParam(value = "size", required = false, defaultValue = "5") @Min(value=-1) @Max(value = 25) int size,
            @RequestParam(value = "offset", required = false, defaultValue = "0") @Min(value=0) int offset) {

        if (size < 0) {
            size = Integer.MAX_VALUE;
            offset = 0;
        }
        List<Settings> SettingsList = settingsDao.getEntities(size, offset);
        return new OkResponse("Successful", SettingsList, SettingsList.size(), size, offset);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @Operation(summary = "Get Setting by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful return Setting Entity by ID"),
            @ApiResponse(responseCode = "400", description = "Invalid Setting ID")
    })
    public Response getSettingById(@PathVariable("id") Integer id) throws BadRequestException {
        if (!settingsDao.isPresent(id)) {
            throw new BadRequestException("Invalid Id");
        }
        return new OkResponse("Success response", settingsDao.getById(id));
    }

    @RequestMapping(value = "/key/{key}", method = RequestMethod.GET)
    @Operation(summary = "Get Setting by Name")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful return Setting Entity by Name"),
            @ApiResponse(responseCode = "400", description = "Invalid Setting Name")
    })
    public Response getSettingByKey(@PathVariable("key") String key) throws BadRequestException {

        if (!settingsDao.isPresentKey(key)) {
            throw new BadRequestException("Invalid Key");
        }

        return new OkResponse("Success response", settingsDao.getByKey(key));
    }

    @RequestMapping(method = RequestMethod.POST)
    @Operation(summary = "Add Settings Entity")
    @ApiResponses(value = {@ApiResponse(responseCode = "201", description = "Successful adding Settings entity")})
    public Response addSetting(@NotNull(message = "Request body cannot be null") @Valid @RequestBody Settings settings,
                              HttpServletRequest request) throws BadRequestException {

        return new CreatedResponse("Settings created successfully", settingsDao.insertEntity(settings));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @Operation(summary = "Update Settings Entity")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful updating Settings entity")})
    public Response updateSettings(@PathVariable("id") Integer id, @NotNull(message = "Request body cannot be null") @Valid @RequestBody Settings settings,
                                 HttpServletRequest request) throws BadRequestException {

        if (!settingsDao.isPresent(id)) {
            throw new BadRequestException("Invalid Id");
        }

        settings.setId(id);
        settingsDao.updateEntity(settings);
        return new OkResponse("Settings entity updated successfully", settingsDao.getById(id));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @Operation(summary = "Delete Setting by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful deleted Setting Entity by ID"),
            @ApiResponse(responseCode = "400", description = "Invalid Setting ID")
    })
    public Response deleteSettingById(@PathVariable("id") Integer id) throws BadRequestException {
        if (!settingsDao.isPresent(id)) {
            throw new BadRequestException("Invalid Id");
        }
        settingsDao.deleteEntity(id);
        return new OkResponse("Settings deleted successfully");
    }
}
