package io.arti.accounting.controller.general;

import io.arti.accounting.dao.general.CityDao;
import io.arti.accounting.model.general.City;
import io.arti.accounting.service.response.Response;
import io.arti.accounting.service.response.error.exception.client.BadRequestException;
import io.arti.accounting.service.response.success.CreatedResponse;
import io.arti.accounting.service.response.success.OkResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping(value = "/v1/cities")
public class CityController {

    @Autowired
    CityDao cityDao;

    @RequestMapping(method = RequestMethod.GET)
    @Operation(summary = "Get Cities")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful return list of Cities")})
    public Response getCities(
            @RequestParam(value = "size", required = false, defaultValue = "5") @Min(value = -1) @Max(value = 25) int size,
            @RequestParam(value = "offset", required = false, defaultValue = "0") @Min(value = 0) int offset) {

        if (size < 0) {
            size = Integer.MAX_VALUE;
            offset = 0;
        }

        List<City> citiesList = cityDao.getEntities(size, offset);
        return new OkResponse("Successful", citiesList, citiesList.size(), size, offset);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @Operation(summary = "Get City by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful Return of City Entity by ID"),
            @ApiResponse(responseCode = "400", description = "Invalid City ID")
    })
    public Response getCityById(@PathVariable("id") Integer id) throws BadRequestException {
        if (!cityDao.isPresent(id)) {
            throw new BadRequestException("Invalid Id");
        }

        return new OkResponse("Success response", cityDao.getById(id));
    }

    @RequestMapping(method = RequestMethod.POST)
    @Operation(summary = "Add City Entity")
    @ApiResponses(value = {@ApiResponse(responseCode = "201", description = "Successful adding of city entity")})
    public Response addCity(@NotNull(message = "Request body cannot be null") @Valid @RequestBody City city)
            throws BadRequestException {

        return new CreatedResponse("City added successfully", cityDao.insertEntity(city));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @Operation(summary = "Update City Entity")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful updating of city entity")})
    public Response updateCity(
            @PathVariable("id") Integer id,
            @NotNull(message = "Request body cannot be null") @Valid @RequestBody City city)
            throws BadRequestException {

        if (!cityDao.isPresent(id)) {
            throw new BadRequestException("Invalid Id");
        }

        city.setId(id);
        cityDao.updateEntity(city);
        return new OkResponse("City entity updated successfully", cityDao.getById(id));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @Operation(summary = "Delete City by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful deleting of city entity"),
            @ApiResponse(responseCode = "400", description = "Invalid city ID")
    })
    public Response deleteCityById(@PathVariable("id") Integer id) throws BadRequestException {
        if (!cityDao.isPresent(id)) {
            throw new BadRequestException("Invalid Id");
        }

        cityDao.deleteEntity(id);
        return new OkResponse("City deleted successfully");
    }

}
