package io.arti.accounting.controller.general;

import io.arti.accounting.dao.general.ProfileDao;
import io.arti.accounting.model.general.Profile;
import io.arti.accounting.service.response.Response;
import io.arti.accounting.service.response.error.exception.client.BadInsertException;
import io.arti.accounting.service.response.error.exception.client.BadRequestException;
import io.arti.accounting.service.response.success.CreatedResponse;
import io.arti.accounting.service.response.success.OkResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping(value = "/v1/profile")
public class ProfileController {

    @Autowired
    ProfileDao profileDao;

    @RequestMapping(method = RequestMethod.GET)
    @Operation(summary = "Get Profiles")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful return list of Profiles")})
    public Response getProfiles(
            @RequestParam(value = "size", required = false, defaultValue = "5") @Min(value=-1) @Max(value = 25) int size,
            @RequestParam(value = "offset", required = false, defaultValue = "0") @Min(value=0) int offset) {

        if (size < 0) {
            size = Integer.MAX_VALUE;
            offset = 0;
        }
        List<Profile> ProfilesList = profileDao.getEntities(size, offset);
        return new OkResponse("Successful", ProfilesList, ProfilesList.size(), size, offset);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @Operation(summary = "Get Profile by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful return Profile Entity by ID"),
            @ApiResponse(responseCode = "400", description = "Invalid Profile ID")
    })
    public Response getProfileById(@PathVariable("id") Integer id) throws BadRequestException {
        if (!profileDao.isPresent(id)) {
            throw new BadRequestException("Invalid Id");
        }
        return new OkResponse("Success response", profileDao.getById(id));
    }

    @RequestMapping(method = RequestMethod.POST)
    @Operation(summary = "Add Profile Entity")
    @ApiResponses(value = {@ApiResponse(responseCode = "201", description = "Successful adding Profile entity")})
    public Response addProfile(@NotNull(message = "Request body cannot be null") @Valid @RequestBody Profile profiles,
                              HttpServletRequest request) throws BadRequestException {

        if(!profileDao.isEmptyProfile()) {
            throw new BadInsertException("Only one record allowed");
        }

        return new CreatedResponse("Profile created successfully", profileDao.insertEntity(profiles));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @Operation(summary = "Update Profile Entity")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful updating Profile entity")})
    public Response updateProfile(@PathVariable("id") Integer id, @NotNull(message = "Request body cannot be null") @Valid @RequestBody Profile profiles,
                                 HttpServletRequest request) throws BadRequestException {

        if (!profileDao.isPresent(id)) {
            throw new BadRequestException("Invalid Id");
        }

        profiles.setId(id);
        profileDao.updateEntity(profiles);
        return new OkResponse("Profile entity updated successfully", profileDao.getById(id));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @Operation(summary = "Delete Profile by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful deleted Profile Entity by ID"),
            @ApiResponse(responseCode = "400", description = "Invalid Profile ID")
    })
    public Response deleteProfileById(@PathVariable("id") Integer id) throws BadRequestException {
        if (!profileDao.isPresent(id)) {
            throw new BadRequestException("Invalid Id");
        }
        profileDao.deleteEntity(id);
        return new OkResponse("Profile deleted successfully");
    }
}
