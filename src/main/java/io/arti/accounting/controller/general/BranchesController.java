package io.arti.accounting.controller.general;

import io.arti.accounting.dao.general.BranchesDao;
import io.arti.accounting.model.general.Branches;
import io.arti.accounting.service.response.Response;
import io.arti.accounting.service.response.error.exception.client.BadRequestException;
import io.arti.accounting.service.response.success.CreatedResponse;
import io.arti.accounting.service.response.success.OkResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping(value = "/v1/branches")

public class BranchesController {

    @Autowired
    BranchesDao branchesDao;

    @RequestMapping(method = RequestMethod.GET)
    @Operation(summary = "Get Branches")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful return list of Branches")})
    public Response getBranches(
            @RequestParam(value = "size", required = false, defaultValue = "5") @Min(value=-1) @Max(value = 25) int size,
            @RequestParam(value = "offset", required = false, defaultValue = "0") @Min(value=0) int offset) {

        if (size < 0) {
            size = Integer.MAX_VALUE;
            offset = 0;
        }
        List<Branches> BranchesList = branchesDao.getEntities(size, offset);
        return new OkResponse("Successful", BranchesList, BranchesList.size(), size, offset);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @Operation(summary = "Get Branch by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful return Branch Entity by ID"),
            @ApiResponse(responseCode = "400", description = "Invalid Branch ID")
    })
    public Response getBranchById(@PathVariable("id") Integer id) throws BadRequestException {
        if (!branchesDao.isPresent(id)) {
            throw new BadRequestException("Invalid Id");
        }
        return new OkResponse("Success response", branchesDao.getById(id));
    }

    @RequestMapping(method = RequestMethod.POST)
    @Operation(summary = "Add Branch Entity")
    @ApiResponses(value = {@ApiResponse(responseCode = "201", description = "Successful adding Branch entity")})
    public Response addBranch(@NotNull(message = "Request body cannot be null") @Valid @RequestBody Branches branches,
                                  HttpServletRequest request) throws BadRequestException {

        return new CreatedResponse("Branch created successfully", branchesDao.insertEntity(branches));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @Operation(summary = "Update Branch Entity")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful updating Branch entity")})
    public Response updateBranch(@PathVariable("id") Integer id, @NotNull(message = "Request body cannot be null") @Valid @RequestBody Branches branches,
                                     HttpServletRequest request) throws BadRequestException {

        if (!branchesDao.isPresent(id)) {
            throw new BadRequestException("Invalid Id");
        }

        branches.setId(id);
        branchesDao.updateEntity(branches);
        return new OkResponse("Branch entity updated successfully", branchesDao.getById(id));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @Operation(summary = "Delete Branch by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful deleted Branch Entity by ID"),
            @ApiResponse(responseCode = "400", description = "Invalid Branch ID")
    })
    public Response deleteBranchById(@PathVariable("id") Integer id) throws BadRequestException {
        if (!branchesDao.isPresent(id)) {
            throw new BadRequestException("Invalid Id");
        }
        branchesDao.deleteEntity(id);
        return new OkResponse("Branch deleted successfully");
    }
}
