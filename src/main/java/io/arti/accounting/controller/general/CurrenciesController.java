package io.arti.accounting.controller.general;

import io.arti.accounting.dao.accountstree.AccountBalanceDao;
import io.arti.accounting.dao.accountstree.FourthLevelDao;
import io.arti.accounting.dao.general.CurrenciesDao;
import io.arti.accounting.model.general.Currencies;
import io.arti.accounting.service.response.Response;
import io.arti.accounting.service.response.error.exception.client.BadRequestException;
import io.arti.accounting.service.response.success.CreatedResponse;
import io.arti.accounting.service.response.success.OkResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping(value = "/v1/currencies")
public class CurrenciesController {

    @Autowired
    CurrenciesDao currenciesDao;
    @Autowired
    FourthLevelDao fourthLevelDao;

    @RequestMapping(method = RequestMethod.GET)
    @Operation(summary = "Get Currencies")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful return list of Currencies")})
    public Response getCurrencies(
            @RequestParam(value = "size", required = false, defaultValue = "5") @Min(value=-1) @Max(value = 25) int size,
            @RequestParam(value = "offset", required = false, defaultValue = "0") @Min(value=0) int offset) {

        if (size < 0) {
            size = Integer.MAX_VALUE;
            offset = 0;
        }
        List<Currencies> CurrenciesList = currenciesDao.getEntities(size, offset);
        return new OkResponse("Successful", CurrenciesList, CurrenciesList.size(), size, offset);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @Operation(summary = "Get Currency by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful return Currency Entity by ID"),
            @ApiResponse(responseCode = "400", description = "Invalid Currency ID")
    })
    public Response getCurrencyById(@PathVariable("id") Integer id) throws BadRequestException {
        if (!currenciesDao.isPresent(id)) {
            throw new BadRequestException("Invalid Id");
        }
        return new OkResponse("Success response", currenciesDao.getById(id));
    }

    @RequestMapping(method = RequestMethod.POST)
    @Operation(summary = "Add Currency Entity")
    @ApiResponses(value = {@ApiResponse(responseCode = "201", description = "Successful adding Currency entity")})
    public Response addCurrency(@NotNull(message = "Request body cannot be null") @Valid @RequestBody Currencies currencies,
                              HttpServletRequest request) throws BadRequestException {

        return new CreatedResponse("Currency created successfully", currenciesDao.insertEntity(currencies));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @Operation(summary = "Update Currency Entity")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful updating Currency entity")})
    public Response updateCurrency(@PathVariable("id") Integer id, @NotNull(message = "Request body cannot be null") @Valid @RequestBody Currencies currencies,
                                 HttpServletRequest request) throws BadRequestException {

        if (!currenciesDao.isPresent(id)) {
            throw new BadRequestException("Invalid Id");
        }

        currencies.setId(id);
        currenciesDao.updateEntity(currencies);
        return new OkResponse("Currency entity updated successfully", currenciesDao.getById(id));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @Operation(summary = "Delete Currency by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful deleted Currency Entity by ID"),
            @ApiResponse(responseCode = "400", description = "Invalid Currency ID")
    })
    public Response deleteCurrencyById(@PathVariable("id") Integer id) throws BadRequestException {
        if (!currenciesDao.isPresent(id)) {
            throw new BadRequestException("Invalid Id");
        }
        if (fourthLevelDao.isPresentByCurrency(id)) {
            throw new BadRequestException("this currency in use!");
        }
        currenciesDao.deleteEntity(id);
        return new OkResponse("Currency deleted successfully");
    }
}
