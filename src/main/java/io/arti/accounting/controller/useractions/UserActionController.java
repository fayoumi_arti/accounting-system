package io.arti.accounting.controller.useractions;

import io.arti.accounting.dao.vouchers.VoucherBaseDao;
import io.arti.accounting.model.general.Branches;
import io.arti.accounting.model.useractions.ApprovedVoucher;
import io.arti.accounting.model.useractions.SubmissionRequest;
import io.arti.accounting.model.useractions.VoucherApprove;
import io.arti.accounting.model.useractions.VoucherDeny;
import io.arti.accounting.model.vouchers.VoucherBase;
import io.arti.accounting.model.vouchers.enums.VoucherStatus;
import io.arti.accounting.service.response.Response;
import io.arti.accounting.service.response.error.exception.client.BadRequestException;
import io.arti.accounting.service.response.success.CreatedResponse;
import io.arti.accounting.service.useractions.UserActionsService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping(value = "/v1/user-action")
public class UserActionController {

    @Autowired
    VoucherBaseDao voucherBaseDao;

    @Autowired
    UserActionsService userActionsService;


    @RequestMapping(value = "/submit", method = RequestMethod.POST)
    @Operation(summary = "Voucher submi Entity")
    @ApiResponses(value = {@ApiResponse(responseCode = "201", description = "Successful submitted voucher")})
    public Response addBranch(@NotNull(message = "Request body cannot be null") @Valid @RequestBody SubmissionRequest submissionRequest,
                              HttpServletRequest request) throws BadRequestException {
        userActionsService.voucherSubmit(submissionRequest);
        return new CreatedResponse("Voucher submitted successfully");
    }


    @RequestMapping(value = "/approve", method = RequestMethod.POST)
    @Operation(summary = "Voucher approve request")
    @ApiResponses(value = {@ApiResponse(responseCode = "201", description = "Successful approved voucher")})
    public Response approveVoucher(@NotNull(message = "Request body cannot be null") @Valid @RequestBody VoucherApprove voucherApprove,
                                   HttpServletRequest request) throws BadRequestException {
        userActionsService.checkVoucherStatus(voucherApprove.getBaseId());
        userActionsService.approve(voucherApprove, request.getHeader(HttpHeaders.AUTHORIZATION));
        return new CreatedResponse("Journal approved Successfully");
    }

    @RequestMapping(value = "/deny", method = RequestMethod.POST)
    @Operation(summary = "Voucher approve request")
    @ApiResponses(value = {@ApiResponse(responseCode = "201", description = "Successful denied voucher")})
    public Response denyVoucher(@NotNull(message = "Request body cannot be null") @Valid @RequestBody VoucherDeny voucherDeny,
                                   HttpServletRequest request) throws BadRequestException {
        userActionsService.checkVoucherStatus(voucherDeny.getBaseId());
        userActionsService.deny(voucherDeny);
        return new CreatedResponse("Journal denied Successfully");
    }

}
