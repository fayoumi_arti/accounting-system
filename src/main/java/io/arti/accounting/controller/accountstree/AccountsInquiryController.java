package io.arti.accounting.controller.accountstree;

import io.arti.accounting.service.account.AccountsInquiryService;
import io.arti.accounting.service.response.Response;
import io.arti.accounting.service.response.success.OkResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/v1/accounts-inquiry")
public class AccountsInquiryController {

    @Autowired
    AccountsInquiryService accountsInquiryService;

    @RequestMapping(method = RequestMethod.GET)
    @Operation(summary = "Search accounts by name and level")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful return list of accounts by name and level.")})
    public Response getBalanceSheet(@RequestParam(value = "accountLevel") Integer level,
                                    @RequestParam(value = "name") String name) {

        return new OkResponse("success", accountsInquiryService.getAccountsList(level, name));
    }

}
