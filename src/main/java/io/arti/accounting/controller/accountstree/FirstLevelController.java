package io.arti.accounting.controller.accountstree;

import io.arti.accounting.dao.accountstree.FirstLevelDao;
import io.arti.accounting.service.response.Response;
import io.arti.accounting.service.response.error.exception.client.BadRequestException;
import io.arti.accounting.service.response.success.OkResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/v1/first-level")
public class FirstLevelController {

    @Autowired
    FirstLevelDao firstLevelDao;

    @RequestMapping(method = RequestMethod.GET)
    @Operation(summary = "Get Accounts Tree First Level")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful return list of Accounts tree first level")})
    public Response getFirstLevel() {

        return new OkResponse("Successful", firstLevelDao.getEntities());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @Operation(summary = "Get Accounts Tree First Level by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful return Accounts tree first level Entity by ID"),
            @ApiResponse(responseCode = "400", description = "Invalid Accounts tree first level ID")
    })
    public Response getFirstById(@PathVariable("id") Integer id) throws BadRequestException {
        if (!firstLevelDao.isPresent(id)) {
            throw new BadRequestException("Invalid Id");
        }

        return new OkResponse("Success response", firstLevelDao.getById(id));
    }
}
