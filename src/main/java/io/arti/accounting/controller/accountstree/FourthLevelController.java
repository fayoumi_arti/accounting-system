package io.arti.accounting.controller.accountstree;

import io.arti.accounting.dao.accountstree.AccountBalanceDao;
import io.arti.accounting.dao.accountstree.FourthLevelDao;
import io.arti.accounting.dao.accountstree.ThirdLevelDao;
import io.arti.accounting.model.accountstree.FourthLevel;
import io.arti.accounting.model.general.enums.CRDR;
import io.arti.accounting.service.response.Response;
import io.arti.accounting.service.response.error.exception.client.BadRequestException;
import io.arti.accounting.service.response.success.CreatedResponse;
import io.arti.accounting.service.response.success.OkResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping(value = "/v1/fourth-level")
public class FourthLevelController {

    @Autowired
    ThirdLevelDao thirdLevelDao;

    @Autowired
    FourthLevelDao fourthLevelDao;

    @Autowired
    AccountBalanceDao accountBalanceDao;

    @RequestMapping(method = RequestMethod.GET)
    @Operation(summary = "Get Accounts Tree Fourth Level")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful return list of Accounts tree fourth level")})
    public Response getFourthLevel(
            @RequestParam(value = "size", required = false, defaultValue = "5") @Min(value=-1) @Max(value = 25) int size,
            @RequestParam(value = "offset", required = false, defaultValue = "0") @Min(value=0) int offset) {

        if (size < 0) {
            size = Integer.MAX_VALUE;
            offset = 0;
        }
        List<FourthLevel> fourthLevelList = fourthLevelDao.getEntities(size, offset);
        return new OkResponse("Successful", fourthLevelList, fourthLevelList.size(), size, offset);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @Operation(summary = "Get Accounts Tree Fourth Level by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful return Accounts fourth level Entity by ID"),
            @ApiResponse(responseCode = "400", description = "Invalid Accounts tree fourth level ID")
    })
    public Response getFourthLevelById(@PathVariable("id") Integer id) throws BadRequestException {
        if (!fourthLevelDao.isPresent(id)) {
            throw new BadRequestException("Invalid Id");
        }
        return new OkResponse("Success response", fourthLevelDao.getById(id));
    }

    @RequestMapping(value = "/parent/{parentId}", method = RequestMethod.GET)
    @Operation(summary = "Get Accounts Tree Fourth Level by parent ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful return Accounts fourth level Entity by parent ID"),
            @ApiResponse(responseCode = "400", description = "Invalid Accounts tree fourth level ID")
    })
    public Response getFourthLevelByParentId(@PathVariable("parentId") Integer parentId) throws BadRequestException {
        if (!thirdLevelDao.isPresent(parentId)) {
            throw new BadRequestException("Invalid parent Id");
        }
        return new OkResponse("Success response", fourthLevelDao.getEntitiesByParent(parentId));
    }

    @RequestMapping(method = RequestMethod.POST)
    @Operation(summary = "Add Accounts Tree Fourth Level Entity")
    @ApiResponses(value = {@ApiResponse(responseCode = "201", description = "Successful adding Accounts tree fourth level entity")})
    public Response addFourthLevel(@NotNull(message = "Request body cannot be null") @Valid @RequestBody FourthLevel fourthLevel,
                                  HttpServletRequest request) throws BadRequestException {
        if (!thirdLevelDao.isPresent(fourthLevel.getThirdLevelId())) {
            throw new BadRequestException("third level ID not exists");
        }

        return new CreatedResponse("Fourth level entity created successfully", fourthLevelDao.insertEntity(fourthLevel));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @Operation(summary = "Update Accounts Tree Fourth Level Entity")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful updating Accounts tree fourth level entity")})
    public Response updateFourthLevel(@PathVariable("id") Integer id, @NotNull(message = "Request body cannot be null") @Valid @RequestBody FourthLevel fourthLevel,
                                     HttpServletRequest request) throws BadRequestException {

        if (!fourthLevelDao.isPresent(id)) {
            throw new BadRequestException("Invalid Id");
        }
        if (!thirdLevelDao.isPresent(fourthLevel.getThirdLevelId())) {
            throw new BadRequestException("third level ID not exists");
        }
        fourthLevelDao.updateEntity(fourthLevel);
        return new OkResponse("Fourth level entity updated successfully", fourthLevelDao.getById(id));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @Operation(summary = "Delete Accounts Tree Fourth Level by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful deleted Accounts tree fourth level Entity by ID"),
            @ApiResponse(responseCode = "400", description = "Invalid Accounts tree fourth level ID")
    })
    public Response deleteFourthLevelById(@PathVariable("id") Integer id) throws BadRequestException {
        if (!fourthLevelDao.isPresent(id)) {
            throw new BadRequestException("Invalid Id");
        }
        if(accountBalanceDao.isAccountUsed(id)){
            throw new BadRequestException("this account already used in vouchers");
        }
        fourthLevelDao.deleteEntity(id);
        return new OkResponse("Entity deleted successfully");
    }
    @GetMapping("/types")
    @Operation(summary = "Get fourth-level Account types")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful return list of fourth-level Account types")})
    public Response getFourthLevelTypes() {

        return new OkResponse("Successful", CRDR.values());
    }

    @GetMapping("/expenses-accounts")
    @Operation(summary = "Get fourth-level Accounts related to the second level expenses accounts")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful return list of fourth-level Accounts")})
    public Response getFourthLevelExpensesAccounts(
            @RequestParam(value = "size", required = false, defaultValue = "5") @Min(value=-1) @Max(value = 25) int size,
            @RequestParam(value = "offset", required = false, defaultValue = "0") @Min(value=0) int offset) {

        if (size < 0) {
            size = Integer.MAX_VALUE;
            offset = 0;
        }
        List<FourthLevel> fourthLevelList = fourthLevelDao.getExpensesEntities(size, offset);
        return new OkResponse("Successful", fourthLevelList, fourthLevelList.size(), size, offset);
    }

    @GetMapping("/{id}/balance")
    @Operation(summary = "Get fourth-level Account balance per currency")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful return fourth-level Account Balance")})
    public Response getFourthLevelBalance(
            @PathVariable(value = "id")  int accountId,
            @RequestParam(value = "currency")  int currencyId) {

        if (!fourthLevelDao.isPresent(accountId)) {
            throw new BadRequestException("Invalid Account Id");
        }
        return new OkResponse("Successful", accountBalanceDao.getLatestEntityByAccountAndCurrency(accountId, currencyId));
    }
}
