package io.arti.accounting.controller.accountstree;

import io.arti.accounting.dao.accountstree.SecondLevelDao;
import io.arti.accounting.dao.accountstree.ThirdLevelDao;
import io.arti.accounting.model.accountstree.ThirdLevel;
import io.arti.accounting.model.accountstree.enums.MainAccountType;
import io.arti.accounting.service.response.Response;
import io.arti.accounting.service.response.error.exception.client.BadRequestException;
import io.arti.accounting.service.response.success.CreatedResponse;
import io.arti.accounting.service.response.success.OkResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping(value = "/v1/third-level")
public class ThirdLevelController {

    @Autowired
    ThirdLevelDao thirdLevelDao;

    @Autowired
    SecondLevelDao secondLevelDao;

    @RequestMapping(method = RequestMethod.GET)
    @Operation(summary = "Get Accounts Tree Third Level")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful return list of Accounts tree third level")})
    public Response getThirdLevel(
            @RequestParam(value = "size", required = false, defaultValue = "5") @Min(value=-1) @Max(value = 25) int size,
            @RequestParam(value = "offset", required = false, defaultValue = "0") @Min(value=0) int offset) {

        if (size < 0) {
            size = Integer.MAX_VALUE;
            offset = 0;
        }
        List<ThirdLevel> thirdLevelList = thirdLevelDao.getEntities(size, offset);
        return new OkResponse("Successful", thirdLevelList, thirdLevelList.size(), size, offset);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @Operation(summary = "Get Accounts Tree Third Level by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful return Accounts tree third level Entity by ID"),
            @ApiResponse(responseCode = "400", description = "Invalid Accounts tree third level ID")
    })
    public Response getThirdLevelById(@PathVariable("id") Integer id) throws BadRequestException {
        if (!thirdLevelDao.isPresent(id)) {
            throw new BadRequestException("Invalid Id");
        }
        return new OkResponse("Success response", thirdLevelDao.getById(id));
    }

    @RequestMapping(value = "/parent/{parentId}", method = RequestMethod.GET)
    @Operation(summary = "Get Accounts Tree Third Level by parent ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful return Accounts tree third level Entity by parent ID"),
            @ApiResponse(responseCode = "400", description = "Invalid Accounts tree third level ID")
    })
    public Response getThirdLevelByParentId(@PathVariable("parentId") Integer parentId) throws BadRequestException {
        if (!secondLevelDao.isPresent(parentId)) {
            throw new BadRequestException("Invalid parent Id");
        }
        return new OkResponse("Success response", thirdLevelDao.getEntitiesByParent(parentId));
    }
    @RequestMapping(method = RequestMethod.POST)
    @Operation(summary = "Add Accounts Tree Third Level Entity")
    @ApiResponses(value = {@ApiResponse(responseCode = "201", description = "Successful adding Accounts tree third level entity")})
    public Response addThirdLevel(@NotNull(message = "Request body cannot be null") @Valid @RequestBody ThirdLevel thirdLevel,
                                  HttpServletRequest request) throws BadRequestException {
        if (!secondLevelDao.isPresent(thirdLevel.getSecondLevelId())) {
            throw new BadRequestException("Invalid second level Id");
        }
        return new CreatedResponse("Third level entity created successfully", thirdLevelDao.insertEntity(thirdLevel));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @Operation(summary = "Update Accounts Tree Third Level Entity")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful updating Accounts tree third level entity")})
    public Response updateThirdLevel(@PathVariable("id") Integer id, @NotNull(message = "Request body cannot be null") @Valid @RequestBody ThirdLevel thirdLevel,
                                  HttpServletRequest request) throws BadRequestException {

        if (!thirdLevelDao.isPresent(id)) {
            throw new BadRequestException("Invalid Id");
        }

        if (!secondLevelDao.isPresent(thirdLevel.getSecondLevelId())) {
            throw new BadRequestException("Invalid second level Id");
        }

        thirdLevelDao.updateEntity(thirdLevel);
        return new OkResponse("Third level entity updated successfully", thirdLevelDao.getById(id));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @Operation(summary = "Delete Accounts Tree Third Level by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful deleted Accounts tree third level Entity by ID"),
            @ApiResponse(responseCode = "400", description = "Invalid Accounts tree third level ID")
    })
    public Response deleteThirdLevelById(@PathVariable("id") Integer id) throws BadRequestException {
        if (!thirdLevelDao.isPresent(id)) {
            throw new BadRequestException("Invalid Id");
        }
        thirdLevelDao.deleteEntity(id);
        return new OkResponse("Entity deleted successfully");
    }

    @GetMapping("/types")
    @Operation(summary = "Get Main Account types")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful return list of main account types")})
    public Response getMainAccountTypes() {

        return new OkResponse("Successful", MainAccountType.values());
    }

}
