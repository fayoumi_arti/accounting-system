package io.arti.accounting.controller.accountstree;

import io.arti.accounting.dao.accountstree.VirtualAccountsDao;
import io.arti.accounting.model.accountstree.VirtualAccounts;
import io.arti.accounting.service.response.Response;
import io.arti.accounting.service.response.error.exception.client.BadRequestException;
import io.arti.accounting.service.response.success.CreatedResponse;
import io.arti.accounting.service.response.success.OkResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping(value = "/v1/virtual-accounts")
public class VirtualAccountsController {

    @Autowired
    VirtualAccountsDao virtualAccountsDao;

    @RequestMapping(method = RequestMethod.GET)
    @Operation(summary = "Get Virtual Accounts")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful return list of Virtual Accounts")})
    public Response getFirstLevel() {

        return new OkResponse("Successful", virtualAccountsDao.getEntities());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @Operation(summary = "Get Virtual Accounts by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful return Virtual Accounts by ID"),
            @ApiResponse(responseCode = "400", description = "Invalid virtual Account ID")
    })
    public Response getFourthLevelById(@PathVariable("id") Integer id) throws BadRequestException {
        if (!virtualAccountsDao.isPresent(id)) {
            throw new BadRequestException("Invalid Id");
        }
        return new OkResponse("Success response", virtualAccountsDao.getById(id));
    }

    @RequestMapping(method = RequestMethod.POST)
    @Operation(summary = "Add Virtual Accounts Entity")
    @ApiResponses(value = {@ApiResponse(responseCode = "201", description = "Successful adding Virtual Accounts entity")})
    public Response addFourthLevel(@NotNull(message = "Request body cannot be null") @Valid @RequestBody List<VirtualAccounts> virtualAccounts,
                                   HttpServletRequest request) throws BadRequestException {
        virtualAccountsDao.insertBulk(virtualAccounts);
        return new CreatedResponse("Virtual Accounts created successfully");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @Operation(summary = "Update Virtual Accounts Entity")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful updating Virtual Accounts entity")})
    public Response updateFourthLevel(@PathVariable("id") Integer id, @NotNull(message = "Request body cannot be null") @Valid @RequestBody VirtualAccounts virtualAccounts,
                                      HttpServletRequest request) throws BadRequestException {

        if (!virtualAccountsDao.isPresent(id)) {
            throw new BadRequestException("Invalid Id");
        }

        virtualAccountsDao.updateEntity(virtualAccounts);
        return new OkResponse("Virtual Accounts entity updated successfully", virtualAccountsDao.getById(id));
    }

    @RequestMapping(method = RequestMethod.PUT)
    @Operation(summary = "Update Virtual Accounts Entity")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful updating Virtual Accounts entity")})
    public Response updateFourthLevel(@Valid @RequestBody List<VirtualAccounts> virtualAccounts,
                                      HttpServletRequest request) throws BadRequestException {

        virtualAccountsDao.updateBulk(virtualAccounts);
        return new OkResponse("Virtual Accounts entity updated successfully");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @Operation(summary = "Delete Virtual Accounts by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful Virtual Accounts by ID"),
            @ApiResponse(responseCode = "400", description = "Invalid Virtual Accounts ID")
    })
    public Response deleteThirdLevelById(@PathVariable("id") Integer id) throws BadRequestException {
        if (!virtualAccountsDao.isPresent(id)) {
            throw new BadRequestException("Invalid Id");
        }
        virtualAccountsDao.deleteEntity(id);
        return new OkResponse("Virtual Accountsg deleted successfully");
    }
}
