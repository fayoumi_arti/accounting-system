package io.arti.accounting.controller.accountstree;

import io.arti.accounting.dao.accountstree.FirstLevelDao;
import io.arti.accounting.dao.accountstree.SecondLevelDao;
import io.arti.accounting.service.response.Response;
import io.arti.accounting.service.response.error.exception.client.BadRequestException;
import io.arti.accounting.service.response.success.OkResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/v1/second-level")
public class SecondLevelController {

    @Autowired
    SecondLevelDao secondLevelDao;

    @Autowired
    FirstLevelDao firstLevelDao;

    @RequestMapping(method = RequestMethod.GET)
    @Operation(summary = "Get Accounts Tree Second Level")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful return list of Accounts tree second level")})
    public Response getSecondLevel() {

        return new OkResponse("Successful", secondLevelDao.getEntities());
    }

    @RequestMapping(value = "/parent/{parentId}", method = RequestMethod.GET)
    @Operation(summary = "Get Accounts Tree Second Level by parent ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful return Accounts tree second level Entity by parent ID"),
            @ApiResponse(responseCode = "400", description = "Invalid Accounts tree second level ID")
    })
    public Response getSecondLevelByParentId(@PathVariable("parentId") Integer parentId) throws BadRequestException {
        if (!secondLevelDao.isPresent(parentId)) {
            throw new BadRequestException("Invalid Id");
        }
        return new OkResponse("Success response", secondLevelDao.getByParent(parentId));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @Operation(summary = "Get Accounts Tree Second Level by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful return Accounts tree second level Entity by ID"),
            @ApiResponse(responseCode = "400", description = "Invalid Accounts tree second level ID")
    })
    public Response getSecondLevelById(@PathVariable("id") Integer id) throws BadRequestException {
        if (!secondLevelDao.isPresent(id)) {
            throw new BadRequestException("Invalid parent Id");
        }
        return new OkResponse("Success response", secondLevelDao.getById(id));
    }
}
