package io.arti.accounting.service;

import io.arti.accounting.authentication.JwtTokenProvider;
import io.arti.accounting.dao.accountstree.FourthLevelDao;
import io.arti.accounting.model.accountstree.FourthLevel;
import io.arti.accounting.model.accountstree.enums.VirtualType;
import io.arti.accounting.model.general.enums.CRDR;
import io.arti.accounting.model.vouchers.*;
import io.arti.accounting.model.vouchers.enums.VoucherType;
import io.arti.accounting.service.response.error.exception.client.BadRequestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ValidationService {

    @Autowired
    FourthLevelDao fourthLevelDao;
    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    private static final Logger logger = LoggerFactory.getLogger(ValidationService.class);

    public boolean isAllowedJournalTransaction(JournalBaseVoucher journals, String authToken) {

        isValidVoucherBase(journals.getVoucherBase(), authToken);
        BigDecimal debitAmount = BigDecimal.ZERO;
        BigDecimal creditAmount = BigDecimal.ZERO;

        VoucherType voucherType = journals.getVoucherBase().getVoucherType();

        Map<Integer, DrCrBalance> currenciesBalance = new HashMap<>();
        for (JournalVoucher voucher : journals.getJournalVoucherList()) {

            if(voucher.getAmount().equals(BigDecimal.ZERO)){
                throw new BadRequestException("amount should be more than zero");
            }
            Integer currencyReferenceId = voucher.getCurrencyId();
            if (voucher.getCrdr() == CRDR.DEBIT) {
                debitAmount = debitAmount.add(voucher.getEquivalence());

                if (!currenciesBalance.containsKey(currencyReferenceId)) {
                    currenciesBalance.put(currencyReferenceId, new DrCrBalance());
                }
                currenciesBalance.get(currencyReferenceId).increaseDrBalance(voucher.getAmount());


            }
            if (voucher.getCrdr() == CRDR.CREDIT) {
                creditAmount = creditAmount.add(voucher.getEquivalence());
                FourthLevel fourthLevel = fourthLevelDao.getById(voucher.getAccountId());
                if((fourthLevel.getCrdr() == CRDR.DEBIT) && fourthLevel.getBalance() - debitAmount.floatValue() < 0){
                    throw new BadRequestException("Debit account has inefficient Balance: " + voucher.getAccountId());
                }
                if (!currenciesBalance.containsKey(currencyReferenceId)) {
                    currenciesBalance.put(currencyReferenceId, new DrCrBalance());
                }
                currenciesBalance.get(currencyReferenceId).increaseCrBalance(voucher.getAmount());
            }

        }
        if (!debitAmount.subtract(creditAmount).equals(BigDecimal.ZERO)) {
            throw new BadRequestException("Equivalence Difference not equal zero");
        }

        for (Map.Entry<Integer, DrCrBalance> entry: currenciesBalance.entrySet()) {
            if (!entry.getValue().isValid()){
                throw new BadRequestException("currency with RefId = " + entry.getKey() + " does not match");
            }
        }

        return true;

    }

    public boolean isAllowedExpensesTransaction(ExpensesVoucher expensesVoucher, String authToken) {

        isValidVoucherBase(expensesVoucher.getVoucherBase(), authToken);
        BigDecimal debitAmount = BigDecimal.ZERO;
        BigDecimal creditAmount = BigDecimal.ZERO;
        boolean creditExist = false;

        Integer currencyReferenceId = expensesVoucher.getAccounts().get(0).getCurrencyId();

        for (ExpensesAccount account : expensesVoucher.getAccounts()) {
            if(account.getAmount().equals(BigDecimal.ZERO)){
                throw new BadRequestException("amount should be more than zero");
            }
            if (!account.getCurrencyId().equals(currencyReferenceId)) {
                throw new BadRequestException("you can't use more than one currency");
            }
            if (account.getCrdr() == CRDR.DEBIT) {
                debitAmount = account.getEquivalence();
            }
            if (account.getCrdr() == CRDR.CREDIT) {
                if(creditExist){
                    throw new BadRequestException("Credit account already added to the voucher!");
                }
                creditAmount = creditAmount.add(account.getEquivalence());
                FourthLevel fourthLevel = fourthLevelDao.getById(account.getAccountId());
                creditExist = true;
                if (BigDecimal.valueOf(fourthLevel.getBalance()).subtract(debitAmount).compareTo(BigDecimal.ZERO) < 0) {

                    throw new BadRequestException("Credit account has inefficient Balance: " + account.getAccountId());
                }
            }

        }
        if (!debitAmount.subtract(creditAmount).equals(BigDecimal.ZERO)) {
            throw new BadRequestException("Equivalence Difference not equal zero");
        }

        return true;

    }

    public boolean isAllowedInternalVoucher(InternalVoucher internalVoucher,String authToken) {
        isValidVoucherBase(internalVoucher.getVoucherBase(), authToken);
        if (internalVoucher.getCreditAmount().equals(BigDecimal.ZERO) || internalVoucher.getDebitAmount().equals(BigDecimal.ZERO)) {
            throw new BadRequestException("Amount must be greater than zero");
        }
        if (internalVoucher.getDebitEquivalence() != internalVoucher.getCreditEquivalence()) {
            throw new BadRequestException("Voucher not Equivalent!");
        }
        return true;
    }
    private boolean isValidVoucherBase(VoucherBase voucherBase, String authToken){
        List<GrantedAuthority> grantedAuthorities = jwtTokenProvider.getAuthoritiesFromToken(authToken);
        Integer userBranchId = jwtTokenProvider.getBranchIdFromToken(authToken);

        if (!userBranchId.equals(voucherBase.getBranchId()) && !grantedAuthorities.contains(new SimpleGrantedAuthority("manageMultiBranches"))) {
            throw new BadRequestException("User not allowed to use this Branch");
        }
        LocalDate entryDate = voucherBase.getEntryDate().toLocalDate();

        if (entryDate.isAfter(LocalDate.now())) {
            logger.error("entry date couldn't be more than today");
            throw new BadRequestException("entry date couldn't be more than today");
        }
        if (entryDate.isBefore(LocalDate.now()) && !grantedAuthorities.contains(new SimpleGrantedAuthority("manageOlderDate"))) {
            logger.error("User not allowed to use day older than today");
            throw new BadRequestException("User not allowed to use day older than today");
        }
        return true;
    }

}
