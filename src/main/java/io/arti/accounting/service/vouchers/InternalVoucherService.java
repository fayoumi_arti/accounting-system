package io.arti.accounting.service.vouchers;

import io.arti.accounting.dao.vouchers.InternalVoucherDao;
import io.arti.accounting.dao.vouchers.VoucherBaseDao;
import io.arti.accounting.model.vouchers.InternalVoucher;
import io.arti.accounting.model.vouchers.VoucherBase;
import io.arti.accounting.model.vouchers.VoucherListing;
import io.arti.accounting.model.vouchers.enums.VoucherStatus;
import io.arti.accounting.model.vouchers.enums.VoucherType;
import io.arti.accounting.service.ModelService;
import io.arti.accounting.service.ValidationService;
import io.arti.accounting.service.response.error.exception.client.BadRequestException;
import io.arti.accounting.service.response.error.exception.client.ForbiddenException;
import io.arti.accounting.service.response.error.exception.client.NotFoundException;
import io.arti.accounting.service.useractions.UserActionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.List;
@Service
public class InternalVoucherService implements ModelService<InternalVoucher> {
    @Autowired
    InternalVoucherDao internalVoucherDao;

    @Autowired
    VoucherBaseDao voucherBaseDao;

    @Autowired
    UserActionsService userActionsService;
    @Autowired
    ValidationService validationService;

    @Override
    public List<InternalVoucher> getEntities(int size, int offset, String name) {
        return null;
    }

    @Override
    public InternalVoucher getEntityById(Integer id) throws NotFoundException, ForbiddenException {
        return null;
    }

    @Override
    public boolean isPresent(Integer id) throws NotFoundException {
        if(!internalVoucherDao.isPresentByVoucherId(id)){
            throw new BadRequestException("invalid voucher Id");
        }
        return true;
    }

    @Override
    public InternalVoucher insertEntity(InternalVoucher internalVoucher) {
        validationService.isAllowedInternalVoucher(internalVoucher, "");
        InternalVoucher insertedPaymentReceiptVoucher = internalVoucherDao.insertEntity(internalVoucher);

        if (insertedPaymentReceiptVoucher == null) {
            throw new BadRequestException("Invalid insertion");
        }
        return insertedPaymentReceiptVoucher;
    }

    @Override
    public void updateEntity(Integer id, InternalVoucher internalVoucher) throws BadRequestException {
        isPresent(id);
        validationService.isAllowedInternalVoucher(internalVoucher, "");
        internalVoucherDao.updateEntity(internalVoucher);
    }

    @Transactional
    public VoucherListing updateEntry(InternalVoucher internalVoucher, Integer baseId) {
        VoucherBase voucherBase = internalVoucher.getVoucherBase();
        voucherBase.setId(baseId);
        voucherBaseDao.updateEntity(voucherBase);
        internalVoucherDao.updateEntity(internalVoucher);
        return getByVoucherId(internalVoucher.getVoucherBase().getVoucherId());
    }

    @Override
    public void deleteEntity(Integer id) throws NotFoundException, ForbiddenException {
        isPresent(id);
        internalVoucherDao.deleteEntity(id);

    }

    public void deleteEntityByVoucherId(Integer voucherId) {
        internalVoucherDao.deleteByVoucherId(voucherId);
    }

    public VoucherListing getByVoucherId(Integer id) {
        isPresent(id);
        return internalVoucherDao.getByVoucherId(id);
    }

    @Transactional
    public InternalVoucher reverse(Integer voucherId, String token) {

        VoucherBase originalBase = getByVoucherId(voucherId).getVoucherBase();


        InternalVoucher originalVoucher = internalVoucherDao.getVoucherId(voucherId);
        if (originalBase.getVoucherType().equals(VoucherType.REVERSED_INTERNAL)) {
            throw new BadRequestException("Voucher cannot be Reversed");
        }

        if (originalBase.getReverseId() > 0) {
            throw new BadRequestException("Voucher Already Reversed");
        }

        Integer creditAccountId = originalVoucher.getCreditAccountId();
        Integer debitAccountId = originalVoucher.getDebitAccountId();

        originalVoucher.setCreditAccountId(debitAccountId);
        originalVoucher.setDebitAccountId(creditAccountId);


        originalBase.setVoucherStatus(VoucherStatus.APPROVED);
        originalBase.setReverseId(voucherId);
        originalBase.setVoucherType(VoucherType.REVERSED_INTERNAL);
        originalVoucher.setVoucherBase(originalBase);
        originalBase.setDateTime(OffsetDateTime.now());

        InternalVoucher insertedInternalVoucher = insertEntity(originalVoucher);

        Integer originalId = originalBase.getId();
        Integer newId = insertedInternalVoucher.getVoucherBase().getId();

        voucherBaseDao.setReverseId(originalId, newId);

        return insertedInternalVoucher;
    }
}
