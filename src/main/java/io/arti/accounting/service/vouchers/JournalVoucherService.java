package io.arti.accounting.service.vouchers;

import io.arti.accounting.dao.vouchers.JournalVoucherDao;
import io.arti.accounting.dao.vouchers.VoucherBaseDao;
import io.arti.accounting.model.vouchers.*;
import io.arti.accounting.model.vouchers.enums.VoucherStatus;
import io.arti.accounting.model.vouchers.enums.VoucherType;
import io.arti.accounting.service.ModelService;
import io.arti.accounting.service.ValidationService;
import io.arti.accounting.service.response.error.exception.client.BadRequestException;
import io.arti.accounting.service.response.error.exception.client.ForbiddenException;
import io.arti.accounting.service.response.error.exception.client.NotFoundException;
import io.arti.accounting.service.useractions.UserActionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class JournalVoucherService implements ModelService<JournalBaseVoucher> {

    @Autowired
    JournalVoucherDao journalVoucherDao;

    @Autowired
    ValidationService validationService;

    @Autowired
    CommonVouchersServices commonVouchersServices;

    @Autowired
    VoucherBaseDao voucherBaseDao;

    @Autowired
    UserActionsService userActionsService;

    @Override
    public List<JournalBaseVoucher> getEntities(int size, int offset, String name) {
        return null;
    }

    @Override
    public JournalBaseVoucher getEntityById(Integer id) throws NotFoundException, ForbiddenException {
        return null;
    }

    @Override
    public boolean isPresent(Integer id) throws NotFoundException {
        if(!journalVoucherDao.isVoucherPresent(id)){
            throw new BadRequestException("invalid voucher Id");
        }
        return true;
    }

    @Override
    public JournalBaseVoucher insertEntity(JournalBaseVoucher journalBaseVoucher){
        return null;
    }
    public JournalBaseVoucher insertEntity(JournalBaseVoucher journalBaseVoucher, String token) {
        token = token.substring(7);
        validationService.isAllowedJournalTransaction(journalBaseVoucher, token);
        JournalBaseVoucher insertedJournalBaseVoucher = journalVoucherDao.insertBulk(journalBaseVoucher);

        if (insertedJournalBaseVoucher == null) {
            throw new BadRequestException("Invalid insertion");
        }
        return insertedJournalBaseVoucher;
    }

    @Override
    public void updateEntity(Integer id, JournalBaseVoucher journalBaseVoucher) throws BadRequestException {

    }

    @Transactional
    public VoucherListing updateEntry(JournalBaseVoucher journalBaseVoucher, Integer baseId) {

        VoucherBase voucherBase = journalBaseVoucher.getVoucherBase();
        voucherBase.setId(baseId);
        voucherBaseDao.updateEntity(voucherBase);
        for (JournalVoucher jv: journalBaseVoucher.getJournalVoucherList()) {
            if(jv.getAmount().equals(BigDecimal.ZERO)){
                throw new BadRequestException("amount should be more than zero");
            }
            journalVoucherDao.updateEntity(jv);
        }
        return getByVoucherId(journalBaseVoucher.getVoucherBase().getVoucherId());
    }
    @Override
    public void deleteEntity(Integer id) throws NotFoundException, ForbiddenException {

    }

    public void deleteEntityByVoucherId(Integer voucherId) {
        journalVoucherDao.deleteByVoucherId(voucherId);
    }

    public VoucherListing getByVoucherId(Integer id) {
        isPresent(id);
        return journalVoucherDao.getByVoucherId(id);
    }

    @Transactional
    public JournalBaseVoucher reverse(Integer voucherId, String token, VoucherType voucherType) {

        List<JournalVoucher> reversedVoucher = new ArrayList<>();
        VoucherBase originalBase = getByVoucherId(voucherId).getVoucherBase();
        List<JournalVoucher> originalVoucher = journalVoucherDao.getListById(voucherId);



        if (originalBase.getVoucherType().toString().toUpperCase().startsWith("REVERSED_")) {
            throw new BadRequestException("Voucher cannot be Reversed");
        }

        if (originalBase.getReverseId() > 0) {
            throw new BadRequestException("Voucher Already Reversed");
        }

        for (JournalVoucher voucher: originalVoucher) {
            voucher.setCrdr(commonVouchersServices.reverseCRDR(voucher.getCrdr()));
            reversedVoucher.add(voucher);
        }


        VoucherType reversedVoucherType = VoucherType.valueOf("REVERSED_"+voucherType.toString());
        originalBase.setVoucherStatus(VoucherStatus.APPROVED);
        originalBase.setReverseId(voucherId);
        originalBase.setVoucherType(reversedVoucherType);
        originalBase.setDateTime(OffsetDateTime.now());

        JournalBaseVoucher journalBaseVoucher = insertEntity(new JournalBaseVoucher(reversedVoucher, originalBase, voucherType), token);

        Integer originalId = originalBase.getId();
        Integer newId = journalBaseVoucher.getVoucherBase().getId();

        voucherBaseDao.setReverseId(originalId, newId);

        return journalBaseVoucher;
    }
}
