package io.arti.accounting.service.vouchers;

import io.arti.accounting.dao.accountstree.*;
import io.arti.accounting.dao.general.CurrenciesDao;
import io.arti.accounting.model.accountstree.*;
import io.arti.accounting.model.general.Currencies;
import io.arti.accounting.model.general.enums.CRDR;
import io.arti.accounting.model.vouchers.AccountStatement;
import io.arti.accounting.model.vouchers.TrialBalance;
import io.arti.accounting.service.response.error.exception.client.BadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.DateTimeException;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
public class TrialBalanceService {

    @Autowired
    FourthLevelDao fourthLevelDao;
    @Autowired
    CurrenciesDao currenciesDao;
    @Autowired
    AccountBalanceDao accountBalanceDao;
    @Autowired
    ThirdLevelDao thirdLevelDao;
    @Autowired
    SecondLevelDao secondLevelDao;
    @Autowired
    FirstLevelDao firstLevelDao;
    private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public Map<String,Map<Integer, List<TrialBalance>>> getTrialBalanceToDate(String to, Integer branchId) {
        try {
            DATE_FORMAT.parse(to);
            to = to + " 23:59:59";
        } catch (DateTimeException e) {
            throw new BadRequestException(e.getMessage());
        }

        Map<String,Map<Integer, List<TrialBalance>>>  result = new HashMap<>();
        Map<Integer,List<TrialBalance>> fourthTrialBalances = new HashMap<>();
        Map<Integer,List<TrialBalance>> thirdTrialBalances = new HashMap<>();
        Map<Integer,List<TrialBalance>>  secondTrialBalances = new HashMap<>();
        Map<Integer,List<TrialBalance>>  firstTrialBalances = new HashMap<>();

        List<FourthLevel> fourthLevels = fourthLevelDao.getEntities();

        for (FourthLevel fourthLevel : fourthLevels) {
            fourthTrialBalances.put(fourthLevel.getId(), new ArrayList<>());
            for (Integer currency : fourthLevel.getAllowedCurrencies()) {
                TrialBalance trialBalance = new TrialBalance();
                trialBalance.setAccountId(fourthLevel.getId());
                trialBalance.setNameAr(fourthLevel.getArName());
                trialBalance.setNameEn(fourthLevel.getEnName());
                trialBalance.setCurrencyId(currency);
                Currencies currencies = currenciesDao.getById(currency);
                trialBalance.setEvaluationPrice(currencies.getEvaluationPrice());
                trialBalance.setOpeningBalance(BigDecimal.ZERO);
                List<AccountBalance> accountBalances = accountBalanceDao.getEntitiesByAccountAndCurrencyAndBranch(fourthLevel.getId(), currency, to, branchId);
                if(accountBalances != null && !accountBalances.isEmpty()){
                    calculateTrialBalance(accountBalances, trialBalance, thirdTrialBalances, secondTrialBalances, firstTrialBalances);
                }
                fourthTrialBalances.get(trialBalance.getAccountId()).add(trialBalance);
            }
        }
        result.put("fourthLevel",fourthTrialBalances);
        result.put("thirdLevel",thirdTrialBalances);
        result.put("secondLevel",secondTrialBalances);
        result.put("firstLevel",firstTrialBalances);
        return result;

    }

    public Map<String,Map<Integer, List<TrialBalance>>> getTrialBalanceFromToDate(String from, String to, Integer branchId) {

        try {
            DATE_FORMAT.parse(from);
            DATE_FORMAT.parse(to);
            from = from + " 00:00:00";
            to = to + " 23:59:59";
        } catch (DateTimeException e) {
            throw new BadRequestException(e.getMessage());
        }
        Map<String,Map<Integer, List<TrialBalance>>>  result = new HashMap<>();
        Map<Integer,List<TrialBalance>> fourthTrialBalances = new HashMap<>();
        Map<Integer,List<TrialBalance>> thirdTrialBalances = new HashMap<>();
        Map<Integer,List<TrialBalance>>  secondTrialBalances = new HashMap<>();
        Map<Integer,List<TrialBalance>>  firstTrialBalances = new HashMap<>();
        List<FourthLevel> fourthLevels = fourthLevelDao.getEntities();


        for (FourthLevel fourthLevel : fourthLevels) {
            fourthTrialBalances.put(fourthLevel.getId(), new ArrayList<>());
            for (Integer currency : fourthLevel.getAllowedCurrencies()) {
                TrialBalance trialBalance = new TrialBalance();
                trialBalance.setAccountId(fourthLevel.getId());
                trialBalance.setNameAr(fourthLevel.getArName());
                trialBalance.setNameEn(fourthLevel.getEnName());
                trialBalance.setCurrencyId(currency);
                Currencies currencies = currenciesDao.getById(currency);
                trialBalance.setEvaluationPrice(currencies.getEvaluationPrice());
                AccountBalance latestAccountBalance = accountBalanceDao.getLatestEntityByAccountAndCurrencyAndBranch(fourthLevel.getId(), currency, from, branchId);
                if (latestAccountBalance != null) {
                    trialBalance.setOpeningBalance(latestAccountBalance.getCurrentBalance());
                }else{
                    trialBalance.setOpeningBalance(BigDecimal.ZERO);
                }

                List<AccountBalance> accountBalances = accountBalanceDao.getEntitiesByAccountAndCurrency(fourthLevel.getId(), currency, from, to, branchId);
                if(accountBalances != null && !accountBalances.isEmpty()){
                    calculateTrialBalance(accountBalances, trialBalance, thirdTrialBalances, secondTrialBalances, firstTrialBalances);
                }
                fourthTrialBalances.get(trialBalance.getAccountId()).add(trialBalance);
            }
        }
        result.put("fourthLevel",fourthTrialBalances);
        result.put("thirdLevel",thirdTrialBalances);
        result.put("secondLevel",secondTrialBalances);
        result.put("firstLevel",firstTrialBalances);
        return result;

    }

    private void calculateTrialBalance(List<AccountBalance> accountBalances, TrialBalance trialBalance, Map<Integer,List<TrialBalance>> thirdTrialBalances, Map<Integer, List<TrialBalance>> secondTrialBalances, Map<Integer, List<TrialBalance>> firstTrialBalances) {
        BigDecimal totalDebit = BigDecimal.ZERO;
        BigDecimal totalCredit = BigDecimal.ZERO;
        int multiplyFactor = 1;
        for (AccountBalance accountBalance: accountBalances) {
            if (accountBalance.getCrdr() == CRDR.DEBIT) {
                totalDebit = totalDebit.add(accountBalance.getAmount());
            }
            if (accountBalance.getCrdr() == CRDR.CREDIT) {
                totalCredit = totalCredit.add(accountBalance.getAmount());
            }
        }
        if (totalCredit.compareTo(totalDebit) > 0) {
            trialBalance.setCrdr(CRDR.CREDIT);
            multiplyFactor = -1;
        } else {
            trialBalance.setCrdr(CRDR.DEBIT);
        }

        BigDecimal finalBalance = trialBalance.getOpeningBalance().add(totalDebit).subtract(totalCredit);
        trialBalance.setCreditBalance(totalCredit);
        trialBalance.setDebitBalance(totalDebit);
        trialBalance.setBalance(finalBalance.multiply(BigDecimal.valueOf(multiplyFactor)));
        trialBalance.setLocalBalance(trialBalance.getBalance().multiply(BigDecimal.valueOf(trialBalance.getEvaluationPrice())));

        trialBalance.setThirdLevelId(accountBalances.get(0).getThirdLevelId());
        trialBalance.setSecondLevelId(accountBalances.get(0).getSecondLevelId());
        trialBalance.setFirstLevelId(accountBalances.get(0).getFirstLevelId());

        List<TrialBalance> thirdBalances = thirdTrialBalances.get(accountBalances.get(0).getThirdLevelId());
        ThirdLevel thirdLevel = thirdLevelDao.getById(accountBalances.get(0).getThirdLevelId());
        if(thirdBalances !=null){
            if(thirdBalances.contains(new TrialBalance(trialBalance.getCurrencyId()))){
                TrialBalance third = thirdBalances.get(thirdBalances.indexOf(new TrialBalance(trialBalance.getCurrencyId())));
                third.setDebitBalance(third.getDebitBalance().add(trialBalance.getDebitBalance()));
                third.setCreditBalance(third.getCreditBalance().add(trialBalance.getCreditBalance()));
                third.setOpeningBalance(third.getOpeningBalance().add(trialBalance.getOpeningBalance()));
                third.setBalance(third.getOpeningBalance().add(third.getDebitBalance()).subtract(third.getCreditBalance()).multiply((third.getCreditBalance().compareTo(third.getDebitBalance()) > 0 ? BigDecimal.valueOf(-1) : BigDecimal.ONE)));
                third.setLocalBalance(third.getBalance().multiply(BigDecimal.valueOf(third.getEvaluationPrice())));
            }else{
                thirdBalances.add(new TrialBalance(trialBalance,thirdLevel.getEnName(),thirdLevel.getArName()));
            }
        }else {
            List<TrialBalance> ttb = new ArrayList<>();
            ttb.add(new TrialBalance(trialBalance, thirdLevel.getEnName(), thirdLevel.getArName()));
            thirdTrialBalances.put(trialBalance.getThirdLevelId(), ttb);
        }

        List<TrialBalance> secondBalances = secondTrialBalances.get(accountBalances.get(0).getSecondLevelId());
        SecondLevel secondLevel = secondLevelDao.getById(accountBalances.get(0).getSecondLevelId());
        if(secondBalances !=null){
            if(secondBalances.contains(new TrialBalance(trialBalance.getCurrencyId()))){
                TrialBalance second = secondBalances.get(secondBalances.indexOf(new TrialBalance(trialBalance.getCurrencyId())));
                second.setDebitBalance(second.getDebitBalance().add(trialBalance.getDebitBalance()));
                second.setCreditBalance(second.getCreditBalance().add(trialBalance.getCreditBalance()));
                second.setOpeningBalance(second.getOpeningBalance().add(trialBalance.getOpeningBalance()));
                second.setBalance(second.getOpeningBalance().add(second.getDebitBalance()).subtract(second.getCreditBalance()).multiply(second.getCreditBalance().compareTo(second.getDebitBalance()) > 0 ? BigDecimal.valueOf(-1) : BigDecimal.ONE));
                second.setLocalBalance(second.getBalance().multiply(BigDecimal.valueOf(second.getEvaluationPrice())));
            }else{
                secondBalances.add(new TrialBalance(trialBalance, secondLevel.getEnName(), secondLevel.getArName()));
            }
        }else {
            List<TrialBalance> stb = new ArrayList<>();
            stb.add(new TrialBalance(trialBalance, secondLevel.getEnName(), secondLevel.getArName()));
            secondTrialBalances.put(trialBalance.getSecondLevelId(), stb);
        }



        List<TrialBalance> firstBalances = firstTrialBalances.get(accountBalances.get(0).getFirstLevelId());
        FirstLevel firstLevel = firstLevelDao.getById(accountBalances.get(0).getFirstLevelId());
        if(firstBalances !=null){
            if(firstBalances.contains(new TrialBalance(trialBalance.getCurrencyId()))){
                TrialBalance first = firstBalances.get(firstBalances.indexOf(new TrialBalance(trialBalance.getCurrencyId())));
                first.setDebitBalance(first.getDebitBalance().add(trialBalance.getDebitBalance()));
                first.setCreditBalance(first.getCreditBalance().add(trialBalance.getCreditBalance()));
                first.setOpeningBalance(first.getOpeningBalance().add(trialBalance.getOpeningBalance()));
                first.setBalance(first.getOpeningBalance().add(first.getDebitBalance()).subtract(first.getCreditBalance()).multiply(first.getCreditBalance().compareTo(first.getDebitBalance()) > 0 ? BigDecimal.valueOf(-1) : BigDecimal.ONE));
                first.setLocalBalance(first.getBalance().multiply(BigDecimal.valueOf(first.getEvaluationPrice())));
            }else{
                firstBalances.add(new TrialBalance(trialBalance, firstLevel.getEnName(), firstLevel.getArName()));
            }
        }else {
            List<TrialBalance> ftb = new ArrayList<>();
            ftb.add(new TrialBalance(trialBalance, firstLevel.getEnName(), firstLevel.getArName()));
            firstTrialBalances.put(trialBalance.getFirstLevelId(), ftb);
        }
    }

    private void calculateAccountStatement(AccountStatement accountStatement) {
        BigDecimal totalDebit = BigDecimal.ZERO;
        BigDecimal totalCredit = BigDecimal.ZERO;
        BigDecimal totalBalances = BigDecimal.ZERO;
        int debitTransactions = 0;
        int creditTransactions = 0;
        int totalTransactions = 0;

        for (AccountBalance accountBalance: accountStatement.getAccountBalances()) {
            if (accountBalance.getCrdr() == CRDR.DEBIT) {
                debitTransactions += 1;
                totalDebit = totalDebit.add(accountBalance.getAmount());
            }
            if (accountBalance.getCrdr() == CRDR.CREDIT) {
                creditTransactions += 1;
                totalCredit = totalCredit.add(accountBalance.getAmount());
            }
        }
        totalTransactions = debitTransactions + creditTransactions;
        totalBalances = totalCredit.add(totalDebit);

        accountStatement.setCreditTransactions(creditTransactions);
        accountStatement.setDebitTransactions(debitTransactions);
        accountStatement.setTotalBalances(totalBalances);
        accountStatement.setTotalTransactions(totalTransactions);
        accountStatement.setTotalCredit(totalCredit);
        accountStatement.setTotalDebit(totalDebit);

        getStatementBalances(accountStatement);

    }

    private void getStatementBalances(AccountStatement accountStatement) {

        List<AccountBalance> balances = accountStatement.getAccountBalances();
        if(balances !=null && !balances.isEmpty()) {
            accountStatement.setStartingBalance(balances.get(0).getPreviousBalance());
            accountStatement.setCurrentBalance(balances.get(balances.size() - 1).getCurrentBalance());
        }
    }
}
