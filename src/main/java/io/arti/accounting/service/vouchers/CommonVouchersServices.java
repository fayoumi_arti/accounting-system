package io.arti.accounting.service.vouchers;

import io.arti.accounting.dao.vouchers.VoucherBaseDao;
import io.arti.accounting.model.general.enums.CRDR;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

@Service
public class CommonVouchersServices implements Serializable {

    @Autowired
    VoucherBaseDao voucherBaseDao;

    public CommonVouchersServices() {
    }

    public CRDR reverseCRDR(CRDR crdr) {
        if (crdr.equals(CRDR.CREDIT)) {
            return CRDR.DEBIT;
        } else if (crdr.equals(CRDR.DEBIT)) {
            return CRDR.CREDIT;
        }
        return null;
    }
}
