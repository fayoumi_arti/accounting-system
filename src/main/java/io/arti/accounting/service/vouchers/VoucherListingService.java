package io.arti.accounting.service.vouchers;

import io.arti.accounting.dao.vouchers.*;
import io.arti.accounting.model.vouchers.VoucherListing;
import io.arti.accounting.model.vouchers.enums.VoucherStatus;
import io.arti.accounting.model.vouchers.enums.VoucherType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;

@Service
public class VoucherListingService implements Serializable {

    public VoucherListingService() {}

    @Autowired
    JournalVoucherDao journalVoucherDao;

    @Autowired
    ExpensesVoucherDao expensesVoucherDao;

    @Autowired
    PaymentVoucherDao paymentVoucherDao;

    @Autowired
    ReceiptVoucherDao receiptVoucherDao;
    @Autowired
    InternalVoucherDao internalVoucherDao;

    public List<VoucherListing> getVouchers(int size, int offset, VoucherType type, VoucherStatus voucherStatus) {

        switch (type) {
            case JOURNAL:
                return journalVoucherDao.getWithBase(size, offset, VoucherType.JOURNAL, voucherStatus);
            case TRANSFER:
                return journalVoucherDao.getWithBase(size, offset, VoucherType.TRANSFER, voucherStatus);
            case REVERSED_TRANSFER:
                return journalVoucherDao.getWithBase(size, offset, VoucherType.REVERSED_TRANSFER, voucherStatus);
            case PAID_TRANSFER:
                return journalVoucherDao.getWithBase(size, offset, VoucherType.PAID_TRANSFER, voucherStatus);
            case REVERSED_PAID_TRANSFER:
                return journalVoucherDao.getWithBase(size, offset, VoucherType.REVERSED_PAID_TRANSFER, voucherStatus);
            case REVERSED_JOURNAL:
                return journalVoucherDao.getWithBase(size, offset, VoucherType.REVERSED_JOURNAL, voucherStatus);
            case EXPENSE:
                return expensesVoucherDao.getWithBase(size, offset, VoucherType.EXPENSE, voucherStatus);
            case REVERSED_EXPENSE:
                return expensesVoucherDao.getWithBase(size, offset, VoucherType.REVERSED_EXPENSE, voucherStatus);
            case PAYMENT:
                return paymentVoucherDao.getWithBase(size, offset, VoucherType.PAYMENT, voucherStatus);
            case REVERSED_PAYMENT:
                return paymentVoucherDao.getWithBase(size, offset, VoucherType.REVERSED_PAYMENT, voucherStatus);
            case RECEIPT:
                return receiptVoucherDao.getWithBase(size, offset, VoucherType.RECEIPT, voucherStatus);
            case REVERSED_RECEIPT:
                return receiptVoucherDao.getWithBase(size, offset, VoucherType.REVERSED_RECEIPT, voucherStatus);
            case INTERNAL:
                return internalVoucherDao.getWithBase(size, offset, VoucherType.INTERNAL, voucherStatus);
            case REVERSED_INTERNAL:
                return internalVoucherDao.getWithBase(size, offset, VoucherType.REVERSED_INTERNAL, voucherStatus);
        }
        return null;
    }


}
