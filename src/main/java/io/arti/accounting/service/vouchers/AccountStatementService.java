package io.arti.accounting.service.vouchers;


import io.arti.accounting.dao.accountstree.AccountBalanceDao;
import io.arti.accounting.dao.accountstree.FourthLevelDao;
import io.arti.accounting.dao.vouchers.*;
import io.arti.accounting.model.accountstree.AccountBalance;
import io.arti.accounting.model.general.enums.CRDR;
import io.arti.accounting.model.vouchers.AccountStatement;
import io.arti.accounting.service.response.error.exception.client.BadRequestException;
import io.arti.accounting.service.response.error.exception.client.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.DateTimeException;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class AccountStatementService {
    @Autowired
    FourthLevelDao fourthLevelDao;
    @Autowired
    JournalVoucherDao journalVoucherDao;
    @Autowired
    PaymentVoucherDao paymentVoucherDao;
    @Autowired
    ReceiptVoucherDao receiptVoucherDao;
    @Autowired
    ExpensesVoucherDao expensesVoucherDao;
    @Autowired
    InternalVoucherDao internalVoucherDao;
    @Autowired
    AccountBalanceDao accountBalanceDao;
    private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public AccountStatement getAccountStatement(Integer accountId, Integer currencyId, String from, String to, Integer branchId) {
        if (!fourthLevelDao.isPresent(accountId)) {
            throw new NotFoundException("Invalid account Id");
        }
        try {
            DATE_FORMAT.parse(from);
            DATE_FORMAT.parse(to);
            from = from + " 00:00:00";
            to = to + " 23:59:59";
        } catch (DateTimeException e) {
            throw new BadRequestException(e.getMessage());
        }
        AccountStatement accountStatement = new AccountStatement();
        accountStatement.setAccount(fourthLevelDao.getById(accountId));

        accountStatement.setJournalTransactions(journalVoucherDao.getListByAccountId(accountId, currencyId, from, to, branchId));
        accountStatement.setPaymentTransactions(paymentVoucherDao.getListByAccountId(accountId, currencyId, from, to, branchId));
        accountStatement.setExpensesTransactions(expensesVoucherDao.getListByAccountId(accountId, currencyId, from, to, branchId));
        accountStatement.setReceiptTransactions(receiptVoucherDao.getListByAccountId(accountId, currencyId, from, to, branchId));
        accountStatement.setInternalTransactions(internalVoucherDao.getListByAccountId(accountId, currencyId, from, to, branchId));
        accountStatement.setAccountBalances(accountBalanceDao.getEntitiesByAccountAndCurrency(accountId, currencyId, from, to, branchId));
        calculateAccountStatement(accountStatement, accountId, currencyId, from, branchId);

        return accountStatement;


    }

    private void calculateAccountStatement(AccountStatement accountStatement, int accountId, int currencyId, String from, Integer branchId) {
        BigDecimal totalDebit = BigDecimal.ZERO;
        BigDecimal totalCredit = BigDecimal.ZERO;
        BigDecimal totalBalances = BigDecimal.ZERO;
        int debitTransactions = 0;
        int creditTransactions = 0;
        int totalTransactions = 0;

        for (AccountBalance accountBalance : accountStatement.getAccountBalances()) {
            if (accountBalance.getCrdr() == CRDR.DEBIT) {
                debitTransactions += 1;
                totalDebit = totalDebit.add(accountBalance.getAmount());
            }
            if (accountBalance.getCrdr() == CRDR.CREDIT) {
                creditTransactions += 1;
                totalCredit = totalCredit.add(accountBalance.getAmount());
            }
        }
        totalTransactions = debitTransactions + creditTransactions;
        totalBalances = totalCredit.add(totalDebit);

        accountStatement.setCreditTransactions(creditTransactions);
        accountStatement.setDebitTransactions(debitTransactions);
        accountStatement.setTotalBalances(totalBalances);
        accountStatement.setTotalTransactions(totalTransactions);
        accountStatement.setTotalCredit(totalCredit);
        accountStatement.setTotalDebit(totalDebit);

        getStatementBalances(accountStatement, accountId, currencyId, from, branchId);

    }

    private void getStatementBalances(AccountStatement accountStatement, int accountId, int currencyId, String from, Integer branchId) {

        List<AccountBalance> balances = accountStatement.getAccountBalances();
        if (balances != null && !balances.isEmpty()) {
            accountStatement.setStartingBalance(balances.get(0).getPreviousBalance());
            accountStatement.setCurrentBalance(balances.get(balances.size() - 1).getCurrentBalance());
        } else {
            AccountBalance latestBalance = accountBalanceDao.getLatestEntityByAccountAndCurrencyAndBranch(accountId, currencyId, from, branchId);
            if (latestBalance != null) {
                accountStatement.setStartingBalance(latestBalance.getCurrentBalance());
                accountStatement.setCurrentBalance(latestBalance.getCurrentBalance());
            }
        }
    }
}
