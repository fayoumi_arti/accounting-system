package io.arti.accounting.service.vouchers;

import io.arti.accounting.dao.vouchers.ExpensesVoucherDao;
import io.arti.accounting.dao.vouchers.VoucherBaseDao;
import io.arti.accounting.model.vouchers.*;
import io.arti.accounting.model.vouchers.enums.VoucherStatus;
import io.arti.accounting.model.vouchers.enums.VoucherType;
import io.arti.accounting.service.ModelService;
import io.arti.accounting.service.ValidationService;
import io.arti.accounting.service.response.error.exception.client.BadRequestException;
import io.arti.accounting.service.response.error.exception.client.ForbiddenException;
import io.arti.accounting.service.response.error.exception.client.NotFoundException;
import io.arti.accounting.service.useractions.UserActionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class ExpensesVoucherService implements ModelService<ExpensesVoucher> {

    @Autowired
    ExpensesVoucherDao expensesVoucherDao;

    @Autowired
    ValidationService validationService;

    @Autowired
    CommonVouchersServices commonVouchersServices;

    @Autowired
    VoucherBaseDao voucherBaseDao;

    @Autowired
    UserActionsService userActionsService;

    @Override
    public List<ExpensesVoucher> getEntities(int size, int offset, String name) {
        return null;
    }

    @Override
    public ExpensesVoucher getEntityById(Integer id) throws NotFoundException, ForbiddenException {
        return null;
    }

    @Override
    public boolean isPresent(Integer id) throws NotFoundException {
        if(!expensesVoucherDao.isPresentByVoucherId(id)){
            throw new BadRequestException("invalid voucher Id");
        }
        return true;
    }

    @Override
    public ExpensesVoucher insertEntity(ExpensesVoucher expensesVoucher) {
        return null;
    }

    public ExpensesVoucher insertEntity(ExpensesVoucher expensesVoucher,String authToken) {
        authToken = authToken.substring(7);
        validationService.isAllowedExpensesTransaction(expensesVoucher, authToken);
        ExpensesVoucher insertedExpensesVoucher = expensesVoucherDao.insertBulk(expensesVoucher);

        if (insertedExpensesVoucher == null) {
            throw new BadRequestException("Invalid insertion");
        }
        return insertedExpensesVoucher;
    }

    @Override
    public void updateEntity(Integer id, ExpensesVoucher expensesVoucher) throws BadRequestException {

    }

    @Transactional
    public VoucherListing updateEntry(ExpensesVoucher expensesVoucher, Integer baseId) {
        VoucherBase voucherBase = expensesVoucher.getVoucherBase();
        voucherBase.setId(baseId);
        voucherBaseDao.updateEntity(voucherBase);
        for (ExpensesAccount ea: expensesVoucher.getAccounts()) {
            if(ea.getAmount().equals(BigDecimal.ZERO)){
                throw new BadRequestException("amount should be more than zero");
            }
            expensesVoucherDao.updateEntity(ea);
        }
        return getByVoucherId(expensesVoucher.getVoucherBase().getVoucherId());
    }

    @Override
    public void deleteEntity(Integer id) throws NotFoundException, ForbiddenException {

    }

    public void deleteEntityByVoucherId(Integer voucherId) {
        expensesVoucherDao.deleteByVoucherId(voucherId);
    }

    public VoucherListing getByVoucherId(Integer id) {
        isPresent(id);
        return expensesVoucherDao.getByVoucherId(id);
    }

    @Transactional
    public ExpensesVoucher reverse(Integer voucherId, String token) {

        List<ExpensesAccount> reversedVoucher = new ArrayList<>();
        VoucherBase originalBase = getByVoucherId(voucherId).getVoucherBase();
        List<ExpensesAccount> originalVoucher = expensesVoucherDao.getListById(voucherId);

        if (originalBase.getVoucherType().equals(VoucherType.REVERSED_EXPENSE)) {
            throw new BadRequestException("Voucher cannot be Reversed");
        }

        if (originalBase.getReverseId() > 0) {
            throw new BadRequestException("Voucher Already Reversed");
        }

        for (ExpensesAccount voucher: originalVoucher) {
            voucher.setCrdr(commonVouchersServices.reverseCRDR(voucher.getCrdr()));
            reversedVoucher.add(voucher);
        }

        originalBase.setVoucherStatus(VoucherStatus.APPROVED);
        originalBase.setReverseId(voucherId);
        originalBase.setVoucherType(VoucherType.REVERSED_EXPENSE);
        originalBase.setDateTime(OffsetDateTime.now());

        ExpensesVoucher expensesVoucher = insertEntity(new ExpensesVoucher(reversedVoucher, originalBase), token);

        Integer originalId = originalBase.getId();
        Integer newId = expensesVoucher.getVoucherBase().getId();

        voucherBaseDao.setReverseId(originalId, newId);

        return expensesVoucher;
    }
}
