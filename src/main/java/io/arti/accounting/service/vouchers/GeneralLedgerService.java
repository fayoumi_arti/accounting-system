package io.arti.accounting.service.vouchers;

import io.arti.accounting.dao.accountstree.FourthLevelDao;
import io.arti.accounting.model.accountstree.FourthLevel;
import io.arti.accounting.model.vouchers.AccountStatement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GeneralLedgerService {
    @Autowired
    AccountStatementService accountStatementService;
    @Autowired
    FourthLevelDao fourthLevelDao;

    public List<AccountStatement> getGeneralLedger(String from, String to, Integer branchId) {

        List<AccountStatement> accountStatements = new ArrayList<>();
        List<FourthLevel> fourthLevels = fourthLevelDao.getEntities();
        for (FourthLevel fourthLevel : fourthLevels) {
            for (Integer currency : fourthLevel.getAllowedCurrencies()) {
                accountStatements.add(accountStatementService.getAccountStatement(fourthLevel.getId(), currency, from, to, branchId));
            }
        }

        return accountStatements;
    }
}
