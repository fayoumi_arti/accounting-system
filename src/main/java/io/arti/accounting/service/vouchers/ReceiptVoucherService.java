package io.arti.accounting.service.vouchers;

import io.arti.accounting.dao.vouchers.ReceiptVoucherDao;
import io.arti.accounting.dao.vouchers.VoucherBaseDao;
import io.arti.accounting.model.vouchers.PaymentReceiptVoucher;
import io.arti.accounting.model.vouchers.VoucherBase;
import io.arti.accounting.model.vouchers.VoucherListing;
import io.arti.accounting.model.vouchers.enums.VoucherStatus;
import io.arti.accounting.model.vouchers.enums.VoucherType;
import io.arti.accounting.service.ModelService;
import io.arti.accounting.service.response.error.exception.client.BadRequestException;
import io.arti.accounting.service.response.error.exception.client.ForbiddenException;
import io.arti.accounting.service.response.error.exception.client.NotFoundException;
import io.arti.accounting.service.useractions.UserActionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.List;

@Service
public class ReceiptVoucherService implements ModelService<PaymentReceiptVoucher> {

    @Autowired
    ReceiptVoucherDao receiptVoucherDao;

    @Autowired
    VoucherBaseDao voucherBaseDao;

    @Autowired
    UserActionsService userActionsService;

    @Override
    public List<PaymentReceiptVoucher> getEntities(int size, int offset, String name) {
        return null;
    }

    @Override
    public PaymentReceiptVoucher getEntityById(Integer id) throws NotFoundException, ForbiddenException {
        return null;
    }

    @Override
    public boolean isPresent(Integer id) throws NotFoundException {
        if(!receiptVoucherDao.isPresentByVoucherId(id)){
            throw new BadRequestException("invalid voucher Id");
        }
        return true;
    }

    @Override
    public PaymentReceiptVoucher insertEntity(PaymentReceiptVoucher paymentVoucher) {
        if(paymentVoucher.getAmount().equals(BigDecimal.ZERO)){
            throw new BadRequestException("amount should be more than zero");
        }
        PaymentReceiptVoucher insertedPaymentReceiptVoucher = receiptVoucherDao.insertEntity(paymentVoucher);

        if (insertedPaymentReceiptVoucher == null) {
            throw new BadRequestException("Invalid insertion");
        }
        return insertedPaymentReceiptVoucher;
    }

    @Override
    public void updateEntity(Integer id, PaymentReceiptVoucher paymentVoucher) throws BadRequestException {
        isPresent(id);
        if(paymentVoucher.getAmount().equals(BigDecimal.ZERO)){
            throw new BadRequestException("amount should be more than zero");
        }
        receiptVoucherDao.updateEntity(paymentVoucher);
    }

    @Override
    public void deleteEntity(Integer id) throws NotFoundException, ForbiddenException {
        isPresent(id);
        receiptVoucherDao.deleteEntity(id);
    }

    public void deleteEntityByVoucherId(Integer voucherId) {
        receiptVoucherDao.deleteByVoucherId(voucherId);
    }

    public VoucherListing updateEntry(PaymentReceiptVoucher paymentVoucher, Integer baseId) {
        VoucherBase voucherBase = paymentVoucher.getVoucherBase();
        voucherBase.setId(baseId);
        voucherBaseDao.updateEntity(voucherBase);
        receiptVoucherDao.updateEntity(paymentVoucher);
        return getByVoucherId(paymentVoucher.getVoucherBase().getVoucherId());
    }
    public VoucherListing getByVoucherId(Integer id) {
        isPresent(id);
        return receiptVoucherDao.getByVoucherId(id);
    }

    @Transactional
    public PaymentReceiptVoucher reverse(Integer voucherId, String token) {

        VoucherBase originalBase = getByVoucherId(voucherId).getVoucherBase();


        PaymentReceiptVoucher originalVoucher = receiptVoucherDao.getVoucherId(voucherId);
        if (originalBase.getVoucherType().equals(VoucherType.REVERSED_RECEIPT)) {
            throw new BadRequestException("Voucher cannot be Reversed");
        }

        if (originalBase.getReverseId() > 0) {
            throw new BadRequestException("Voucher Already Reversed");
        }

        Integer creditAccountId = originalVoucher.getCreditAccountId();
        Integer debitAccountId = originalVoucher.getDebitAccountId();

        originalVoucher.setCreditAccountId(debitAccountId);
        originalVoucher.setDebitAccountId(creditAccountId);


        originalBase.setVoucherStatus(VoucherStatus.APPROVED);
        originalBase.setReverseId(voucherId);
        originalBase.setVoucherType(VoucherType.REVERSED_RECEIPT);
        originalVoucher.setVoucherBase(originalBase);
        originalBase.setDateTime(OffsetDateTime.now());

        PaymentReceiptVoucher insertedPaymentReceiptVoucher = insertEntity(originalVoucher);

        Integer originalId = originalBase.getId();
        Integer newId = insertedPaymentReceiptVoucher.getVoucherBase().getId();

        voucherBaseDao.setReverseId(originalId, newId);

        return insertedPaymentReceiptVoucher;
    }
}
