package io.arti.accounting.service.account;

import io.arti.accounting.dao.accountstree.AccountBalanceDao;
import io.arti.accounting.dao.accountstree.FourthLevelDao;
import io.arti.accounting.dao.accountstree.SecondLevelDao;
import io.arti.accounting.dao.accountstree.ThirdLevelDao;
import io.arti.accounting.model.accountstree.BalanceSheet;
import io.arti.accounting.model.accountstree.FirstLevel;
import io.arti.accounting.model.accountstree.SecondLevel;
import io.arti.accounting.model.accountstree.ThirdLevel;
import io.arti.accounting.service.response.error.exception.client.BadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.DateTimeException;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class AccountBalanceSheetService {
    @Autowired
    FourthLevelDao fourthLevelDao;
    @Autowired
    ThirdLevelDao thirdLevelDao;
    @Autowired
    SecondLevelDao secondLevelDao;
    @Autowired
    AccountBalanceDao accountBalanceDao;
    private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public BalanceSheet getAccountsLevelBalancesSheet(Integer branch, String from, String to, Boolean isIncomeStatement) {
        try {
            if (from != null) {
                DATE_FORMAT.parse(from);
                from = from + " 00:00:00";
            }

            DATE_FORMAT.parse(to);
            to = to + " 23:59:59";
        } catch (DateTimeException e) {
            throw new BadRequestException(e.getMessage());
        }

        List<Map<String, Object>> thirdLevelAccountBalances = accountBalanceDao.getEntitiesByThirdLevelAccountBalanceByBranchAndDate(branch, from, to, isIncomeStatement);
        List<Map<String, Object>> secondLevelAccountBalances = accountBalanceDao.getEntitiesBySecondLevelAccountBalanceByBranchAndDate(branch, from, to, isIncomeStatement);
        List<Map<String, Object>> firstLevelAccountBalances = accountBalanceDao.getEntitiesByFirstLevelAccountBalanceByBranchAndDate(branch, from, to, isIncomeStatement);

        List<ThirdLevel> thirdLevelList = new ArrayList<>();
        List<SecondLevel> secondLevelList = new ArrayList<>();
        List<FirstLevel> firstLevelList = new ArrayList<>();

        // Third level balance sheet.
        for (Map<String, Object> accountBalance: thirdLevelAccountBalances) {
            ThirdLevel thirdLevel = new ThirdLevel();
            thirdLevel.setId((Integer) accountBalance.get("account_id"));
            thirdLevel.setBalance(((Double) accountBalance.get("balance")).floatValue());
            thirdLevelList.add(thirdLevel);
        }

        // Second level balance sheet.
        for (Map<String, Object> accountBalance: secondLevelAccountBalances) {
            SecondLevel secondLevel = new SecondLevel();
            secondLevel.setId((Integer) accountBalance.get("account_id"));
            secondLevel.setBalance(((Double) accountBalance.get("balance")).floatValue());
            secondLevel.setThirdLevelList(thirdLevelList);
            secondLevelList.add(secondLevel);
        }

        // First level balance sheet.
        for (Map<String, Object> accountBalance: firstLevelAccountBalances) {
            FirstLevel firstLevel = new FirstLevel();
            firstLevel.setId((Integer) accountBalance.get("account_id"));
            firstLevel.setBalance(((Double) accountBalance.get("balance")).floatValue());
            firstLevel.setSecondLevelList(secondLevelList);
            firstLevelList.add(firstLevel);
        }

        BalanceSheet balanceSheet = new BalanceSheet();
        balanceSheet.setBranchId(branch);
        balanceSheet.setFirstLevelList(firstLevelList);
        balanceSheet.setTo(to);

        if (from != null) {
            balanceSheet.setFrom(from);
        }

        return balanceSheet;
    }
}
