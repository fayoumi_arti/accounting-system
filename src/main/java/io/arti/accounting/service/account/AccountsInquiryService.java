package io.arti.accounting.service.account;

import io.arti.accounting.dao.accountstree.FirstLevelDao;
import io.arti.accounting.dao.accountstree.FourthLevelDao;
import io.arti.accounting.dao.accountstree.SecondLevelDao;
import io.arti.accounting.dao.accountstree.ThirdLevelDao;
import io.arti.accounting.model.accountstree.FirstLevel;
import io.arti.accounting.model.general.enums.CRDR;
import io.arti.accounting.model.vouchers.enums.VoucherType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class AccountsInquiryService {
    @Autowired
    FirstLevelDao firstLevelDao;

    @Autowired
    SecondLevelDao secondLevelDao;

    @Autowired
    ThirdLevelDao thirdLevelDao;

    @Autowired
    FourthLevelDao fourthLevelDao;

    public List<Object> getAccountsList(Integer level, String name) {
        switch (level) {
            case 1:
                return Collections.singletonList(firstLevelDao.getByName(name));
            case 2:
                return Collections.singletonList(secondLevelDao.getByName(name));
            case 3:
                return Collections.singletonList(thirdLevelDao.getByName(name));
            case 4:
                return Collections.singletonList(fourthLevelDao.getByName(name));
        }

        return null;
    }
}
