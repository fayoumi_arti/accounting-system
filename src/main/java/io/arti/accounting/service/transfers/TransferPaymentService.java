package io.arti.accounting.service.transfers;

import io.arti.accounting.dao.accountstree.FourthLevelDao;
import io.arti.accounting.dao.general.CurrenciesDao;
import io.arti.accounting.dao.transfers.TransferPaymentDao;
import io.arti.accounting.model.transfers.TransferPayment;
import io.arti.accounting.model.transfers.enums.TransferDestination;
import io.arti.accounting.model.transfers.enums.TransferStatus;
import io.arti.accounting.service.response.error.exception.client.BadRequestException;
import io.arti.accounting.service.response.error.exception.client.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.DateTimeException;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
public class TransferPaymentService {

    @Autowired
    TransferPaymentDao transferPaymentDao;

    @Autowired
    FourthLevelDao fourthLevelDao;

    @Autowired
    CurrenciesDao currenciesDao;

    private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public List<TransferPayment> getUnpaidTransfersByAccountAndCurrency(Integer senderAccountId,
                                                                        Float amountMax,
                                                                        Float amountMin,
                                                                        Integer currencyId,
                                                                        String beneficiaryName,
                                                                        String senderName,
                                                                        String status,
                                                                        String from,
                                                                        String to,
                                                                        int size,
                                                                        int offset) {

        if (senderAccountId != null && !fourthLevelDao.isPresent(senderAccountId)) {
            throw new NotFoundException("Invalid account Id");
        }

        if (currencyId != null && !currenciesDao.isPresent(currencyId)) {
            throw new NotFoundException("Invalid currency Id");
        }

        if ((from != null && to == null) || (from == null && to != null)) {
            throw new BadRequestException("Invalid date range values.");
        }

        if ((amountMax != null && amountMax < 0) || (amountMin != null && amountMin < 0)) {
            throw new BadRequestException("Invalid values for amounts, they must exceed 0.");
        }

        try {
            if (from != null) {
                DATE_FORMAT.parse(from);
                from = from + " 00:00:00";
            }
            if (to != null) {
                DATE_FORMAT.parse(to);
                to = to + " 23:59:59";
            }
        } catch (DateTimeException e) {
            throw new BadRequestException(e.getMessage());
        }

        return transferPaymentDao.getUnpaidEntitiesByAccountAndCurrency(senderAccountId, amountMax, amountMin, currencyId, beneficiaryName, senderName, status, from, to, size, offset);
    }

    public TransferPayment insert(TransferPayment transferPayment, String token) throws BadRequestException {
        validateUserEntry(transferPayment);
        validateTransferStatus(transferPayment);

        if (transferPayment.getDestination().equals(TransferDestination.BOX)) {
            return transferPaymentDao.insertBoxEntity(transferPayment, token);
        }
        validateTransferPaymentInfo(transferPayment);
        return transferPaymentDao.insertEntity(transferPayment, token);
    }

    public void update(TransferPayment transferPayment, boolean payment) throws BadRequestException {
        if (payment) {
            validateTransferPaymentInfo(transferPayment);
        }
        validateUserEntry(transferPayment);
        transferPaymentDao.updateEntity(transferPayment);
    }

    public void validateDateTime(OffsetDateTime dateTime) throws BadRequestException {
        if (dateTime.isAfter(OffsetDateTime.now())) {
            throw new BadRequestException("Date and time values that are not before or equal to instant time are invalid");
        }
    }

    public void validateCommission(TransferPayment transferPayment) throws BadRequestException {
        float commissionRatio = transferPayment.getCommissionRatio();
        float receivedCommissionRatio = transferPayment.getReceivedCommission();
        float amount = transferPayment.getAmount();

        if (!(receivedCommissionRatio >= 0f)) {
            if (!(commissionRatio >= 0f && commissionRatio <= 1f)) {
                throw new BadRequestException("Invalid commission ratio value is required.");
            } else {
                receivedCommissionRatio = amount * commissionRatio;
                transferPayment.setReceivedCommission(receivedCommissionRatio);
            }
        }
    }

    public void validatePayerAccountId(TransferPayment transferPayment) throws BadRequestException {
        if (transferPayment.getDestination().equals(TransferDestination.TELLER) &&
                !fourthLevelDao.isPresent(transferPayment.getPayerAccountId()) &&
                !fourthLevelDao.isPresent(transferPayment.getSenderAccountId())) {
            throw new BadRequestException("Invalid Payer or Sender Account Id value.");
        }

        if (transferPayment.getDestination().equals(TransferDestination.BOX) &&
                !fourthLevelDao.isPresent(transferPayment.getSenderAccountId())) {
            throw new BadRequestException("Invalid Sender Account Id value.");
        }

        validateCommission(transferPayment);

        if (!(transferPayment.getPaidCommission() >= 0)) {
            transferPayment.setPaidCommission(transferPayment.getReceivedCommission());
        }
    }

    public void validateTransferPaymentInfo(TransferPayment transferPayment) throws BadRequestException {
        OffsetDateTime paymentDate = transferPayment.getPaymentDate();

        if (paymentDate == null || paymentDate.isAfter(OffsetDateTime.now())) {
            throw new BadRequestException("The payment date values that are not before or equal to instant time are invalid");
        }
    }

    public void validateTransferStatus(TransferPayment transferPayment) throws BadRequestException {
        TransferDestination destination = transferPayment.getDestination();
        TransferStatus status = transferPayment.getStatus();

        if (destination.equals(TransferDestination.BOX) && !status.equals(TransferStatus.UNPAID)) {
            throw new BadRequestException("BOX Transfer must be in the UNPAID status.");
        } else if (destination.equals(TransferDestination.TELLER) && !status.equals(TransferStatus.PAID)) {
            throw new BadRequestException("TELLER Transfer must be in the PAID status.");
        }
    }

    public String processSqlInClause(String text) {
        List<String> statusList = Arrays.asList(text.split(","));

        int i=0;
        String statusIn = "";
        for (String item: statusList){
            if (i < statusList.size()-1){
                statusIn += "'" + item + "',";
            }
            else {
                statusIn += "'" + item + "'";
            }
            i++;
        }

        return statusIn;
    }

    public void validateUserEntry(TransferPayment transferPayment) throws BadRequestException {
        validateDateTime(transferPayment.getDateTime());
        validateCommission(transferPayment);
        validatePayerAccountId(transferPayment);
    }
}
