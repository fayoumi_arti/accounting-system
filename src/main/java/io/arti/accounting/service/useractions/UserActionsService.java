package io.arti.accounting.service.useractions;

import io.arti.accounting.dao.accountstree.AccountBalanceDao;
import io.arti.accounting.dao.vouchers.VoucherBaseDao;
import io.arti.accounting.model.useractions.SubmissionRequest;
import io.arti.accounting.model.useractions.VoucherApprove;
import io.arti.accounting.model.useractions.VoucherDeny;
import io.arti.accounting.model.useractions.enums.SubmissionType;
import io.arti.accounting.model.vouchers.VoucherBase;
import io.arti.accounting.model.vouchers.VoucherListing;
import io.arti.accounting.model.vouchers.enums.VoucherStatus;
import io.arti.accounting.service.response.error.exception.client.BadRequestException;
import io.arti.accounting.service.vouchers.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserActionsService {

    @Autowired
    VoucherBaseDao voucherBaseDao;
    @Autowired
    AccountBalanceDao accountBalanceDao;

    @Autowired
    JournalVoucherService journalVoucherService;

    @Autowired
    PaymentVoucherService paymentVoucherService;

    @Autowired
    ExpensesVoucherService expensesVoucherService;

    @Autowired
    ReceiptVoucherService receiptVoucherService;
    @Autowired
    InternalVoucherService internalVoucherService;

    public void voucherSubmit(SubmissionRequest submissionRequest) {

        //TODO: validate entryID before submission
        if (!voucherBaseDao.isPresent(submissionRequest.getVoucherId(), submissionRequest.getVoucherType())) {
            throw new BadRequestException("invalid " + submissionRequest.getVoucherType().toString() + " voucher Id");
        }

        VoucherStatus voucherStatus = voucherBaseDao.getStatus(submissionRequest.getBaseId());

        if (!voucherStatus.equals(VoucherStatus.ENTRY) && submissionRequest.getSubmissionType() != SubmissionType.REVERSE) {
            throw new BadRequestException("The voucher already " + voucherStatus);
        }

        voucherBaseDao.voucherSubmission(submissionRequest);
    }

    public void checkVoucherStatus(Integer baseId) {
        VoucherStatus voucherStatus = voucherBaseDao.getStatus(baseId);

        switch (voucherStatus) {
            case DENIED:
                throw new BadRequestException("The voucher already denied");
            case APPROVED:
                throw new BadRequestException("The voucher already approved");
        }
    }

    @Transactional
    public void approve(VoucherApprove voucherApprove, String token) {
        //TODO: validate approverID before submission

        VoucherBase voucherBase = voucherBaseDao.getById(voucherApprove.getBaseId());
        switch (voucherBase.getSubmissionType()) {
            case REVERSE:
                switch (voucherBase.getVoucherType()) {
                    case JOURNAL:
                    case TRANSFER:
                    case PAID_TRANSFER:
                        journalVoucherService.reverse(voucherBase.getVoucherId(), token, voucherBase.getVoucherType());
                        break;
                    case RECEIPT:
                        receiptVoucherService.reverse(voucherBase.getVoucherId(), token);
                        break;
                    case PAYMENT:
                        paymentVoucherService.reverse(voucherBase.getVoucherId(), token);
                        break;
                    case EXPENSE:
                        expensesVoucherService.reverse(voucherBase.getVoucherId(), token);
                        break;
                    case INTERNAL:
                        internalVoucherService.reverse(voucherBase.getVoucherId(), token);
                        break;
                }
                voucherBaseDao.voucherApprove(voucherApprove.getBaseId(), voucherApprove.getApproverId());
                break;
            case ADD:
                voucherBaseDao.voucherApprove(voucherApprove.getBaseId(), voucherApprove.getApproverId());
                break;
//            case DELETE:

        }
//        voucherBaseDao.voucherApprove(voucherApprove.getBaseId(), voucherApprove.getApproverId());
    }

    public void deny(VoucherDeny voucherDeny) {
        voucherBaseDao.voucherDeny(voucherDeny.getBaseId(), voucherDeny.getApproverId(), voucherDeny.getDenyReason());
    }

    @Transactional
    public void entryVoucherDelete(Integer baseId) {
        VoucherBase voucherBase = voucherBaseDao.getById(baseId);
        VoucherListing voucherListing = null;
        switch (voucherBase.getVoucherType()) {
            case JOURNAL:
                voucherListing = journalVoucherService.getByVoucherId(voucherBase.getVoucherId());
                journalVoucherService.deleteEntityByVoucherId(voucherBase.getVoucherId());
                break;
            case RECEIPT:
                voucherListing = receiptVoucherService.getByVoucherId(voucherBase.getVoucherId());
                receiptVoucherService.deleteEntityByVoucherId(voucherBase.getVoucherId());
                break;
            case PAYMENT:
                voucherListing = paymentVoucherService.getByVoucherId(voucherBase.getVoucherId());
                paymentVoucherService.deleteEntityByVoucherId(voucherBase.getVoucherId());
                break;
            case EXPENSE:
                voucherListing = expensesVoucherService.getByVoucherId(voucherBase.getVoucherId());
                expensesVoucherService.deleteEntityByVoucherId(voucherBase.getVoucherId());
                break;
            case INTERNAL:
                voucherListing = internalVoucherService.getByVoucherId(voucherBase.getVoucherId());
                internalVoucherService.deleteEntityByVoucherId(voucherBase.getVoucherId());
                break;
        }
        voucherBaseDao.deleteEntity(baseId);
        accountBalanceDao.deleteVoucherListing(voucherListing);
    }

    public void entryActionValidate(Integer baseId) {
        VoucherStatus voucherStatus = voucherBaseDao.getStatus(baseId);
        if (!voucherStatus.equals(VoucherStatus.ENTRY) && !voucherStatus.equals(VoucherStatus.DENIED)) {
            throw new BadRequestException("voucher is " +voucherStatus+ " and cannot be deleted");
        }
    }
}
