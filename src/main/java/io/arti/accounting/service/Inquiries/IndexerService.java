package io.arti.accounting.service.Inquiries;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import io.arti.accounting.config.ElasticsearchConfig;
import io.arti.accounting.dao.accountstree.FourthLevelDao;
import io.arti.accounting.dao.vouchers.*;
import io.arti.accounting.model.general.enums.CRDR;
import io.arti.accounting.model.vouchers.*;
import io.arti.accounting.model.vouchers.enums.VoucherType;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;

//import org.json.JSONException;
//import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class IndexerService {

    @Autowired
    VoucherBaseDao voucherBaseDao;

    @Autowired
    JournalVoucherDao journalVoucherDao;

    @Autowired
    ExpensesVoucherDao expensesVoucherDao;

    @Autowired
    PaymentVoucherDao paymentVoucherDao;

    @Autowired
    ReceiptVoucherDao receiptVoucherDao;

    @Autowired
    FourthLevelDao fourthLevelDao;
    @Autowired
    ElasticsearchConfig elasticsearchConfig;

    public void voucherIndexer (Integer baseId) {

        VoucherBase voucherBase = voucherBaseDao.getById(baseId);
        VoucherType voucherType = voucherBase.getVoucherType();

        switch (voucherType) {
            case JOURNAL:
            case REVERSED_JOURNAL:
                journalVoucherIndexer(voucherBase);
                break;
            case PAYMENT:
            case REVERSED_PAYMENT:
                receiptPaymentVoucherIndexer(voucherBase, CRDR.CREDIT, VoucherType.PAYMENT);
                receiptPaymentVoucherIndexer(voucherBase, CRDR.DEBIT, VoucherType.PAYMENT);
            case RECEIPT:
            case REVERSED_RECEIPT:
                receiptPaymentVoucherIndexer(voucherBase, CRDR.CREDIT, VoucherType.RECEIPT);
                receiptPaymentVoucherIndexer(voucherBase, CRDR.DEBIT, VoucherType.RECEIPT);
                break;
            case EXPENSE:
            case REVERSED_EXPENSE:
                expensesVoucherIndexer(voucherBase);
                break;
        }
    }

    public void JSONIndexer(Map<String, Object> jsonMap, Integer id) {
        IndexRequest indexRequest = new IndexRequest("vouchers")
                .id(id.toString()).source(jsonMap);

        try {
            IndexResponse indexResponse = elasticsearchConfig.client().index(indexRequest, RequestOptions.DEFAULT);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public Map<String, Object> voucherBaseMap(VoucherBase voucherBase) {
        Map<String, Object> jsonMap = new HashMap<>();

        jsonMap.put("baseId", voucherBase.getId());
        jsonMap.put("voucherId", voucherBase.getVoucherId());
        jsonMap.put("branchId", voucherBase.getBranchId());
        jsonMap.put("entryId", voucherBase.getEntryId());
        jsonMap.put("approverId", voucherBase.getApproverId());
        jsonMap.put("voucherType", voucherBase.getVoucherType().toString());
        jsonMap.put("report", voucherBase.getReport());
        jsonMap.put("dateTime", voucherBase.getDateTime());
        jsonMap.put("createdAt", voucherBase.getCreatedAt());
        jsonMap.put("approvalDate", voucherBase.getApprovalDate());

        return jsonMap;
    }

    public void journalVoucherIndexer(VoucherBase voucherBase) {
        List<JournalVoucher> journalVoucherList = journalVoucherDao.getEntities(voucherBase.getVoucherId());

        for (JournalVoucher jv: journalVoucherList) {
            Map<String, Object> jvJsonMap = new HashMap<>();
            Map<String, Object> baseJsonMap = voucherBaseMap(voucherBase);
            Map<String, Object> indexerJsonMap = new HashMap<>();

            jvJsonMap.put("accountId", jv.getAccountId());
            jvJsonMap.put("currencyId", jv.getCurrencyId());
            jvJsonMap.put("amount", jv.getAmount());
            jvJsonMap.put("equivalence", jv.getEquivalence());
            jvJsonMap.put("thirdLevelId", fourthLevelDao.getParentId(jv.getAccountId()));
            jvJsonMap.put("voucher", getVoucherObject(journalVoucherDao.getByVoucherId(jv.getVoucherId())));

            indexerJsonMap.putAll(baseJsonMap);
            indexerJsonMap.putAll(jvJsonMap);

            JSONIndexer(indexerJsonMap, voucherBase.getId());
        }
    }
    public void expensesVoucherIndexer(VoucherBase voucherBase) {
        List<ExpensesAccount> expensesVoucherList = expensesVoucherDao.getEntities(voucherBase.getVoucherId());

        for (ExpensesAccount ev: expensesVoucherList) {
            Map<String, Object> evJsonMap = new HashMap<>();
            Map<String, Object> baseJsonMap = voucherBaseMap(voucherBase);
            Map<String, Object> indexerJsonMap = new HashMap<>();

            evJsonMap.put("accountId", ev.getAccountId());
            evJsonMap.put("currencyId", ev.getCurrencyId());
            evJsonMap.put("amount", ev.getAmount());
            evJsonMap.put("equivalence", ev.getEquivalence());
            evJsonMap.put("thirdLevelId", fourthLevelDao.getParentId(ev.getAccountId()));
            evJsonMap.put("voucher", getVoucherObject(expensesVoucherDao.getByVoucherId(ev.getVoucherId())));

            indexerJsonMap.putAll(baseJsonMap);
            indexerJsonMap.putAll(evJsonMap);

            JSONIndexer(indexerJsonMap, voucherBase.getId());
        }
    }

    public String getVoucherObject(VoucherListing voucher) {
        ObjectMapper mapper = new ObjectMapper()
                .registerModule(new ParameterNamesModule())
                .registerModule(new Jdk8Module())
                .registerModule(new JavaTimeModule());
        String bytes = null;
        try {
            bytes = mapper.writeValueAsString(voucher);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return bytes;
    }

    public void receiptPaymentVoucherIndexer(VoucherBase voucherBase, CRDR crdr, VoucherType voucherType) {
        List<PaymentReceiptVoucher> paymentReceiptVouchers = receiptVoucherDao.getEntities(voucherBase.getVoucherId());

        for (PaymentReceiptVoucher rv: paymentReceiptVouchers) {
            Map<String, Object> debitRvJsonMap = new HashMap<>();
            Map<String, Object> baseJsonMap = voucherBaseMap(voucherBase);
            Map<String, Object> indexerJsonMap = new HashMap<>();

            debitRvJsonMap.put("currencyId", rv.getCurrencyId());
            debitRvJsonMap.put("amount", rv.getAmount());
            debitRvJsonMap.put("equivalence", rv.getEquivalence());

            switch (crdr) {
                case DEBIT:
                    debitRvJsonMap.put("accountId", rv.getDebitAccountId());
                    debitRvJsonMap.put("thirdLevelId", fourthLevelDao.getParentId(rv.getDebitAccountId()));
                    break;
                case CREDIT:
                    debitRvJsonMap.put("accountId", rv.getCreditAccountId());
                    debitRvJsonMap.put("thirdLevelId", fourthLevelDao.getParentId(rv.getCreditAccountId()));
                    break;
            }

            switch (voucherType) {
                case PAYMENT:
                    debitRvJsonMap.put("voucher", getVoucherObject(paymentVoucherDao.getByVoucherId(rv.getVoucherId())));
                    break;
                case RECEIPT:
                    debitRvJsonMap.put("voucher", getVoucherObject(receiptVoucherDao.getByVoucherId(rv.getVoucherId())));
            }

            indexerJsonMap.putAll(baseJsonMap);
            indexerJsonMap.putAll(debitRvJsonMap);

            JSONIndexer(indexerJsonMap, voucherBase.getId());
        }
    }

    public void indexAllVouchers() {
        List<Integer> ids = voucherBaseDao.getIdsList();
        for (Integer id: ids) {
            voucherIndexer(id);
        }
    }
}
