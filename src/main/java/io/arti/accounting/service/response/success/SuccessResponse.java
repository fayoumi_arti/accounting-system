package io.arti.accounting.service.response.success;



import io.arti.accounting.service.response.Response;
import io.arti.accounting.service.response.ResponseAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;

public abstract class SuccessResponse<T> extends Response {

    public SuccessResponse(HttpStatus status, MultiValueMap headers, String message, T data) {
        super(headers, new ResponseAttributes(status, message, data));
    }

    public SuccessResponse(HttpStatus status, MultiValueMap headers, String message, T data, int total, int limit, int offset) {
        super(headers, new ResponseAttributes(status, message, data, total, limit, offset));
    }
}
