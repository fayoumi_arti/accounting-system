package io.arti.accounting.service.response.error.exception.client;



import io.arti.accounting.service.response.error.ErrorResponse;
import io.arti.accounting.service.response.error.exception.ResponseException;
import org.springframework.http.HttpStatus;

public class BadRequestException extends ResponseException {

    public BadRequestException(String message) {
        super(new ErrorResponse(HttpStatus.BAD_REQUEST, null, message));
    }
}
