package io.arti.accounting.service.response.error.exception.client;


import io.arti.accounting.service.response.error.ErrorResponse;
import io.arti.accounting.service.response.error.exception.ResponseException;
import org.springframework.http.HttpStatus;

public class ForbiddenException extends ResponseException {

    public ForbiddenException(String message) {
        super(new ErrorResponse(HttpStatus.FORBIDDEN, null, message));
    }

}
