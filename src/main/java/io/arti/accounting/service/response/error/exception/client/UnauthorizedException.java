package io.arti.accounting.service.response.error.exception.client;


import io.arti.accounting.service.response.error.ErrorResponse;
import io.arti.accounting.service.response.error.exception.ResponseException;
import org.springframework.http.HttpStatus;

public class UnauthorizedException extends ResponseException {

    public UnauthorizedException(String message) {
        super(new ErrorResponse(HttpStatus.UNAUTHORIZED, null, message));
    }
}
