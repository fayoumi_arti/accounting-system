package io.arti.accounting.service.response.error.exception.server;



import io.arti.accounting.service.response.error.ErrorResponse;
import io.arti.accounting.service.response.error.exception.ResponseException;
import org.springframework.http.HttpStatus;

public class ServerErrorException extends ResponseException {

    public ServerErrorException(String message) {
        super(new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, null, message));
    }

    public ServerErrorException(String message, Throwable e) {
        super(new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, null, message), e);
    }
}
