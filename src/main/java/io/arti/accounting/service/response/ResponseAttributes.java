package io.arti.accounting.service.response;

import org.springframework.http.HttpStatus;

public class ResponseAttributes<T> {

    private HttpStatus status;
    private String message;
    private T data;

    private int total;
    private int limit;
    private int offset;

    public ResponseAttributes() {
    }

    public ResponseAttributes(HttpStatus status, String message, T data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public ResponseAttributes(HttpStatus status, String message, T data, int total, int limit, int offset) {
        this.status = status;
        this.message = message;
        this.data = data;
        this.total = total;
        this.limit = limit;
        this.offset = offset;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public int getTotal() { return total; }

    public void setTotal(int total) { this.total = total; }

    public int getLimit() { return limit; }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }
}
