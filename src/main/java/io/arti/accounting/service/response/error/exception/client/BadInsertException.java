package io.arti.accounting.service.response.error.exception.client;

import io.arti.accounting.service.response.error.ErrorResponse;
import io.arti.accounting.service.response.error.exception.ResponseException;
import org.springframework.http.HttpStatus;

public class BadInsertException extends ResponseException {

    public BadInsertException(String message) {
        super(new ErrorResponse(HttpStatus.METHOD_NOT_ALLOWED, null, message));
    }
}
