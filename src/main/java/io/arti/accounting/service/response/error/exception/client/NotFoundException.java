package io.arti.accounting.service.response.error.exception.client;




import io.arti.accounting.service.response.error.ErrorResponse;
import io.arti.accounting.service.response.error.exception.ResponseException;
import org.springframework.http.HttpStatus;


public class NotFoundException extends ResponseException {

    public NotFoundException(String message) {
        super(new ErrorResponse(HttpStatus.NOT_FOUND, null, message));
    }
}
